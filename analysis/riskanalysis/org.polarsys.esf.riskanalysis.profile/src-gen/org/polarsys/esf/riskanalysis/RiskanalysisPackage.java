/*******************************************************************************
 * Copyright (c) 2019 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.riskanalysis;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.polarsys.esf.riskanalysis.RiskanalysisFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='TaskBasedRiskAnalysis'"
 * @generated
 */
public interface RiskanalysisPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "riskanalysis";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.polarsys.org/esf/0.7.0/RiskAnalysis";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "RiskAnalysis";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RiskanalysisPackage eINSTANCE = org.polarsys.esf.riskanalysis.impl.RiskanalysisPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.polarsys.esf.riskanalysis.impl.HazardAnalysisImpl <em>Hazard Analysis</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polarsys.esf.riskanalysis.impl.HazardAnalysisImpl
	 * @see org.polarsys.esf.riskanalysis.impl.RiskanalysisPackageImpl#getHazardAnalysis()
	 * @generated
	 */
	int HAZARD_ANALYSIS = 0;

	/**
	 * The feature id for the '<em><b>Task</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__TASK = 0;

	/**
	 * The feature id for the '<em><b>Origin</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__ORIGIN = 1;

	/**
	 * The feature id for the '<em><b>Hazardous Situation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__HAZARDOUS_SITUATION = 2;

	/**
	 * The feature id for the '<em><b>Hazardous Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__HAZARDOUS_EVENT = 3;

	/**
	 * The feature id for the '<em><b>Possible Harm</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__POSSIBLE_HARM = 4;

	/**
	 * The feature id for the '<em><b>Base Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__BASE_OPERATION = 5;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__COMMENT = 6;

	/**
	 * The feature id for the '<em><b>Initial Occurence</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__INITIAL_OCCURENCE = 7;

	/**
	 * The feature id for the '<em><b>Initial Avoidance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__INITIAL_AVOIDANCE = 8;

	/**
	 * The feature id for the '<em><b>Initial Frequency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__INITIAL_FREQUENCY = 9;

	/**
	 * The feature id for the '<em><b>Initial Severity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__INITIAL_SEVERITY = 10;

	/**
	 * The feature id for the '<em><b>Initial Criticality</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__INITIAL_CRITICALITY = 11;

	/**
	 * The feature id for the '<em><b>Initial Risk Reduction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__INITIAL_RISK_REDUCTION = 12;

	/**
	 * The feature id for the '<em><b>Final Occurence</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__FINAL_OCCURENCE = 13;

	/**
	 * The feature id for the '<em><b>Final Avoidance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__FINAL_AVOIDANCE = 14;

	/**
	 * The feature id for the '<em><b>Final Frequency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__FINAL_FREQUENCY = 15;

	/**
	 * The feature id for the '<em><b>Final Severity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__FINAL_SEVERITY = 16;

	/**
	 * The feature id for the '<em><b>Final Criticality</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__FINAL_CRITICALITY = 17;

	/**
	 * The feature id for the '<em><b>Final Risk Reduction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__FINAL_RISK_REDUCTION = 18;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__BASE_PROPERTY = 19;

	/**
	 * The number of structural features of the '<em>Hazard Analysis</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS_FEATURE_COUNT = 20;

	/**
	 * The number of operations of the '<em>Hazard Analysis</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.polarsys.esf.riskanalysis.OccurenceEstimation <em>Occurence Estimation</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polarsys.esf.riskanalysis.OccurenceEstimation
	 * @see org.polarsys.esf.riskanalysis.impl.RiskanalysisPackageImpl#getOccurenceEstimation()
	 * @generated
	 */
	int OCCURENCE_ESTIMATION = 1;

	/**
	 * The meta object id for the '{@link org.polarsys.esf.riskanalysis.RiskEstimation <em>Risk Estimation</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polarsys.esf.riskanalysis.RiskEstimation
	 * @see org.polarsys.esf.riskanalysis.impl.RiskanalysisPackageImpl#getRiskEstimation()
	 * @generated
	 */
	int RISK_ESTIMATION = 2;

	/**
	 * The meta object id for the '{@link org.polarsys.esf.riskanalysis.RiskIndex <em>Risk Index</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polarsys.esf.riskanalysis.RiskIndex
	 * @see org.polarsys.esf.riskanalysis.impl.RiskanalysisPackageImpl#getRiskIndex()
	 * @generated
	 */
	int RISK_INDEX = 3;


	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.riskanalysis.HazardAnalysis <em>Hazard Analysis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hazard Analysis</em>'.
	 * @see org.polarsys.esf.riskanalysis.HazardAnalysis
	 * @generated
	 */
	EClass getHazardAnalysis();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.riskanalysis.HazardAnalysis#getTask <em>Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Task</em>'.
	 * @see org.polarsys.esf.riskanalysis.HazardAnalysis#getTask()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EReference getHazardAnalysis_Task();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.riskanalysis.HazardAnalysis#getOrigin <em>Origin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Origin</em>'.
	 * @see org.polarsys.esf.riskanalysis.HazardAnalysis#getOrigin()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EAttribute getHazardAnalysis_Origin();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.riskanalysis.HazardAnalysis#getHazardousSituation <em>Hazardous Situation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hazardous Situation</em>'.
	 * @see org.polarsys.esf.riskanalysis.HazardAnalysis#getHazardousSituation()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EAttribute getHazardAnalysis_HazardousSituation();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.riskanalysis.HazardAnalysis#getHazardousEvent <em>Hazardous Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hazardous Event</em>'.
	 * @see org.polarsys.esf.riskanalysis.HazardAnalysis#getHazardousEvent()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EAttribute getHazardAnalysis_HazardousEvent();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.riskanalysis.HazardAnalysis#getPossibleHarm <em>Possible Harm</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Possible Harm</em>'.
	 * @see org.polarsys.esf.riskanalysis.HazardAnalysis#getPossibleHarm()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EAttribute getHazardAnalysis_PossibleHarm();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.riskanalysis.HazardAnalysis#getBase_Operation <em>Base Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Operation</em>'.
	 * @see org.polarsys.esf.riskanalysis.HazardAnalysis#getBase_Operation()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EReference getHazardAnalysis_Base_Operation();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.riskanalysis.HazardAnalysis#getComment <em>Comment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Comment</em>'.
	 * @see org.polarsys.esf.riskanalysis.HazardAnalysis#getComment()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EAttribute getHazardAnalysis_Comment();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.riskanalysis.HazardAnalysis#getInitial_Occurence <em>Initial Occurence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Initial Occurence</em>'.
	 * @see org.polarsys.esf.riskanalysis.HazardAnalysis#getInitial_Occurence()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EAttribute getHazardAnalysis_Initial_Occurence();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.riskanalysis.HazardAnalysis#getInitial_Avoidance <em>Initial Avoidance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Initial Avoidance</em>'.
	 * @see org.polarsys.esf.riskanalysis.HazardAnalysis#getInitial_Avoidance()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EAttribute getHazardAnalysis_Initial_Avoidance();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.riskanalysis.HazardAnalysis#getInitial_Frequency <em>Initial Frequency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Initial Frequency</em>'.
	 * @see org.polarsys.esf.riskanalysis.HazardAnalysis#getInitial_Frequency()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EAttribute getHazardAnalysis_Initial_Frequency();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.riskanalysis.HazardAnalysis#getInitial_Severity <em>Initial Severity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Initial Severity</em>'.
	 * @see org.polarsys.esf.riskanalysis.HazardAnalysis#getInitial_Severity()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EAttribute getHazardAnalysis_Initial_Severity();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.riskanalysis.HazardAnalysis#getInitial_Criticality <em>Initial Criticality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Initial Criticality</em>'.
	 * @see org.polarsys.esf.riskanalysis.HazardAnalysis#getInitial_Criticality()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EAttribute getHazardAnalysis_Initial_Criticality();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.riskanalysis.HazardAnalysis#getInitial_RiskReduction <em>Initial Risk Reduction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Initial Risk Reduction</em>'.
	 * @see org.polarsys.esf.riskanalysis.HazardAnalysis#getInitial_RiskReduction()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EAttribute getHazardAnalysis_Initial_RiskReduction();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.riskanalysis.HazardAnalysis#getFinal_Occurence <em>Final Occurence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Final Occurence</em>'.
	 * @see org.polarsys.esf.riskanalysis.HazardAnalysis#getFinal_Occurence()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EAttribute getHazardAnalysis_Final_Occurence();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.riskanalysis.HazardAnalysis#getFinal_Avoidance <em>Final Avoidance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Final Avoidance</em>'.
	 * @see org.polarsys.esf.riskanalysis.HazardAnalysis#getFinal_Avoidance()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EAttribute getHazardAnalysis_Final_Avoidance();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.riskanalysis.HazardAnalysis#getFinal_Frequency <em>Final Frequency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Final Frequency</em>'.
	 * @see org.polarsys.esf.riskanalysis.HazardAnalysis#getFinal_Frequency()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EAttribute getHazardAnalysis_Final_Frequency();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.riskanalysis.HazardAnalysis#getFinal_Severity <em>Final Severity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Final Severity</em>'.
	 * @see org.polarsys.esf.riskanalysis.HazardAnalysis#getFinal_Severity()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EAttribute getHazardAnalysis_Final_Severity();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.riskanalysis.HazardAnalysis#getFinal_Criticality <em>Final Criticality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Final Criticality</em>'.
	 * @see org.polarsys.esf.riskanalysis.HazardAnalysis#getFinal_Criticality()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EAttribute getHazardAnalysis_Final_Criticality();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.riskanalysis.HazardAnalysis#getFinal_RiskReduction <em>Final Risk Reduction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Final Risk Reduction</em>'.
	 * @see org.polarsys.esf.riskanalysis.HazardAnalysis#getFinal_RiskReduction()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EAttribute getHazardAnalysis_Final_RiskReduction();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.riskanalysis.HazardAnalysis#getBase_Property <em>Base Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Property</em>'.
	 * @see org.polarsys.esf.riskanalysis.HazardAnalysis#getBase_Property()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EReference getHazardAnalysis_Base_Property();

	/**
	 * Returns the meta object for enum '{@link org.polarsys.esf.riskanalysis.OccurenceEstimation <em>Occurence Estimation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Occurence Estimation</em>'.
	 * @see org.polarsys.esf.riskanalysis.OccurenceEstimation
	 * @generated
	 */
	EEnum getOccurenceEstimation();

	/**
	 * Returns the meta object for enum '{@link org.polarsys.esf.riskanalysis.RiskEstimation <em>Risk Estimation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Risk Estimation</em>'.
	 * @see org.polarsys.esf.riskanalysis.RiskEstimation
	 * @generated
	 */
	EEnum getRiskEstimation();

	/**
	 * Returns the meta object for enum '{@link org.polarsys.esf.riskanalysis.RiskIndex <em>Risk Index</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Risk Index</em>'.
	 * @see org.polarsys.esf.riskanalysis.RiskIndex
	 * @generated
	 */
	EEnum getRiskIndex();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	RiskanalysisFactory getRiskanalysisFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.polarsys.esf.riskanalysis.impl.HazardAnalysisImpl <em>Hazard Analysis</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polarsys.esf.riskanalysis.impl.HazardAnalysisImpl
		 * @see org.polarsys.esf.riskanalysis.impl.RiskanalysisPackageImpl#getHazardAnalysis()
		 * @generated
		 */
		EClass HAZARD_ANALYSIS = eINSTANCE.getHazardAnalysis();

		/**
		 * The meta object literal for the '<em><b>Task</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HAZARD_ANALYSIS__TASK = eINSTANCE.getHazardAnalysis_Task();

		/**
		 * The meta object literal for the '<em><b>Origin</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAZARD_ANALYSIS__ORIGIN = eINSTANCE.getHazardAnalysis_Origin();

		/**
		 * The meta object literal for the '<em><b>Hazardous Situation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAZARD_ANALYSIS__HAZARDOUS_SITUATION = eINSTANCE.getHazardAnalysis_HazardousSituation();

		/**
		 * The meta object literal for the '<em><b>Hazardous Event</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAZARD_ANALYSIS__HAZARDOUS_EVENT = eINSTANCE.getHazardAnalysis_HazardousEvent();

		/**
		 * The meta object literal for the '<em><b>Possible Harm</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAZARD_ANALYSIS__POSSIBLE_HARM = eINSTANCE.getHazardAnalysis_PossibleHarm();

		/**
		 * The meta object literal for the '<em><b>Base Operation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HAZARD_ANALYSIS__BASE_OPERATION = eINSTANCE.getHazardAnalysis_Base_Operation();

		/**
		 * The meta object literal for the '<em><b>Comment</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAZARD_ANALYSIS__COMMENT = eINSTANCE.getHazardAnalysis_Comment();

		/**
		 * The meta object literal for the '<em><b>Initial Occurence</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAZARD_ANALYSIS__INITIAL_OCCURENCE = eINSTANCE.getHazardAnalysis_Initial_Occurence();

		/**
		 * The meta object literal for the '<em><b>Initial Avoidance</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAZARD_ANALYSIS__INITIAL_AVOIDANCE = eINSTANCE.getHazardAnalysis_Initial_Avoidance();

		/**
		 * The meta object literal for the '<em><b>Initial Frequency</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAZARD_ANALYSIS__INITIAL_FREQUENCY = eINSTANCE.getHazardAnalysis_Initial_Frequency();

		/**
		 * The meta object literal for the '<em><b>Initial Severity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAZARD_ANALYSIS__INITIAL_SEVERITY = eINSTANCE.getHazardAnalysis_Initial_Severity();

		/**
		 * The meta object literal for the '<em><b>Initial Criticality</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAZARD_ANALYSIS__INITIAL_CRITICALITY = eINSTANCE.getHazardAnalysis_Initial_Criticality();

		/**
		 * The meta object literal for the '<em><b>Initial Risk Reduction</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAZARD_ANALYSIS__INITIAL_RISK_REDUCTION = eINSTANCE.getHazardAnalysis_Initial_RiskReduction();

		/**
		 * The meta object literal for the '<em><b>Final Occurence</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAZARD_ANALYSIS__FINAL_OCCURENCE = eINSTANCE.getHazardAnalysis_Final_Occurence();

		/**
		 * The meta object literal for the '<em><b>Final Avoidance</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAZARD_ANALYSIS__FINAL_AVOIDANCE = eINSTANCE.getHazardAnalysis_Final_Avoidance();

		/**
		 * The meta object literal for the '<em><b>Final Frequency</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAZARD_ANALYSIS__FINAL_FREQUENCY = eINSTANCE.getHazardAnalysis_Final_Frequency();

		/**
		 * The meta object literal for the '<em><b>Final Severity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAZARD_ANALYSIS__FINAL_SEVERITY = eINSTANCE.getHazardAnalysis_Final_Severity();

		/**
		 * The meta object literal for the '<em><b>Final Criticality</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAZARD_ANALYSIS__FINAL_CRITICALITY = eINSTANCE.getHazardAnalysis_Final_Criticality();

		/**
		 * The meta object literal for the '<em><b>Final Risk Reduction</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAZARD_ANALYSIS__FINAL_RISK_REDUCTION = eINSTANCE.getHazardAnalysis_Final_RiskReduction();

		/**
		 * The meta object literal for the '<em><b>Base Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HAZARD_ANALYSIS__BASE_PROPERTY = eINSTANCE.getHazardAnalysis_Base_Property();

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.riskanalysis.OccurenceEstimation <em>Occurence Estimation</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polarsys.esf.riskanalysis.OccurenceEstimation
		 * @see org.polarsys.esf.riskanalysis.impl.RiskanalysisPackageImpl#getOccurenceEstimation()
		 * @generated
		 */
		EEnum OCCURENCE_ESTIMATION = eINSTANCE.getOccurenceEstimation();

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.riskanalysis.RiskEstimation <em>Risk Estimation</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polarsys.esf.riskanalysis.RiskEstimation
		 * @see org.polarsys.esf.riskanalysis.impl.RiskanalysisPackageImpl#getRiskEstimation()
		 * @generated
		 */
		EEnum RISK_ESTIMATION = eINSTANCE.getRiskEstimation();

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.riskanalysis.RiskIndex <em>Risk Index</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polarsys.esf.riskanalysis.RiskIndex
		 * @see org.polarsys.esf.riskanalysis.impl.RiskanalysisPackageImpl#getRiskIndex()
		 * @generated
		 */
		EEnum RISK_INDEX = eINSTANCE.getRiskIndex();

	}

} //RiskanalysisPackage
