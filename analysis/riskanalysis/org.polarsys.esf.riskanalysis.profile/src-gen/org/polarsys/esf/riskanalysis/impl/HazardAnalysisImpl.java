/*******************************************************************************
 * Copyright (c) 2019 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.riskanalysis.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.uml2.uml.Namespace;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Property;
import org.polarsys.esf.riskanalysis.HazardAnalysis;
import org.polarsys.esf.riskanalysis.OccurenceEstimation;
import org.polarsys.esf.riskanalysis.RiskEstimation;
import org.polarsys.esf.riskanalysis.RiskIndex;
import org.polarsys.esf.riskanalysis.RiskanalysisPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hazard Analysis</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.polarsys.esf.riskanalysis.impl.HazardAnalysisImpl#getTask <em>Task</em>}</li>
 *   <li>{@link org.polarsys.esf.riskanalysis.impl.HazardAnalysisImpl#getOrigin <em>Origin</em>}</li>
 *   <li>{@link org.polarsys.esf.riskanalysis.impl.HazardAnalysisImpl#getHazardousSituation <em>Hazardous Situation</em>}</li>
 *   <li>{@link org.polarsys.esf.riskanalysis.impl.HazardAnalysisImpl#getHazardousEvent <em>Hazardous Event</em>}</li>
 *   <li>{@link org.polarsys.esf.riskanalysis.impl.HazardAnalysisImpl#getPossibleHarm <em>Possible Harm</em>}</li>
 *   <li>{@link org.polarsys.esf.riskanalysis.impl.HazardAnalysisImpl#getBase_Operation <em>Base Operation</em>}</li>
 *   <li>{@link org.polarsys.esf.riskanalysis.impl.HazardAnalysisImpl#getComment <em>Comment</em>}</li>
 *   <li>{@link org.polarsys.esf.riskanalysis.impl.HazardAnalysisImpl#getInitial_Occurence <em>Initial Occurence</em>}</li>
 *   <li>{@link org.polarsys.esf.riskanalysis.impl.HazardAnalysisImpl#getInitial_Avoidance <em>Initial Avoidance</em>}</li>
 *   <li>{@link org.polarsys.esf.riskanalysis.impl.HazardAnalysisImpl#getInitial_Frequency <em>Initial Frequency</em>}</li>
 *   <li>{@link org.polarsys.esf.riskanalysis.impl.HazardAnalysisImpl#getInitial_Severity <em>Initial Severity</em>}</li>
 *   <li>{@link org.polarsys.esf.riskanalysis.impl.HazardAnalysisImpl#getInitial_Criticality <em>Initial Criticality</em>}</li>
 *   <li>{@link org.polarsys.esf.riskanalysis.impl.HazardAnalysisImpl#getInitial_RiskReduction <em>Initial Risk Reduction</em>}</li>
 *   <li>{@link org.polarsys.esf.riskanalysis.impl.HazardAnalysisImpl#getFinal_Occurence <em>Final Occurence</em>}</li>
 *   <li>{@link org.polarsys.esf.riskanalysis.impl.HazardAnalysisImpl#getFinal_Avoidance <em>Final Avoidance</em>}</li>
 *   <li>{@link org.polarsys.esf.riskanalysis.impl.HazardAnalysisImpl#getFinal_Frequency <em>Final Frequency</em>}</li>
 *   <li>{@link org.polarsys.esf.riskanalysis.impl.HazardAnalysisImpl#getFinal_Severity <em>Final Severity</em>}</li>
 *   <li>{@link org.polarsys.esf.riskanalysis.impl.HazardAnalysisImpl#getFinal_Criticality <em>Final Criticality</em>}</li>
 *   <li>{@link org.polarsys.esf.riskanalysis.impl.HazardAnalysisImpl#getFinal_RiskReduction <em>Final Risk Reduction</em>}</li>
 *   <li>{@link org.polarsys.esf.riskanalysis.impl.HazardAnalysisImpl#getBase_Property <em>Base Property</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HazardAnalysisImpl extends MinimalEObjectImpl.Container implements HazardAnalysis {
	/**
	 * The cached value of the '{@link #getTask() <em>Task</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTask()
	 * @generated
	 * @ordered
	 */
	protected Namespace task;

	/**
	 * The default value of the '{@link #getOrigin() <em>Origin</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrigin()
	 * @generated
	 * @ordered
	 */
	protected static final String ORIGIN_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOrigin() <em>Origin</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrigin()
	 * @generated
	 * @ordered
	 */
	protected String origin = ORIGIN_EDEFAULT;

	/**
	 * The default value of the '{@link #getHazardousSituation() <em>Hazardous Situation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHazardousSituation()
	 * @generated
	 * @ordered
	 */
	protected static final String HAZARDOUS_SITUATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHazardousSituation() <em>Hazardous Situation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHazardousSituation()
	 * @generated
	 * @ordered
	 */
	protected String hazardousSituation = HAZARDOUS_SITUATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getHazardousEvent() <em>Hazardous Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHazardousEvent()
	 * @generated
	 * @ordered
	 */
	protected static final String HAZARDOUS_EVENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHazardousEvent() <em>Hazardous Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHazardousEvent()
	 * @generated
	 * @ordered
	 */
	protected String hazardousEvent = HAZARDOUS_EVENT_EDEFAULT;

	/**
	 * The default value of the '{@link #getPossibleHarm() <em>Possible Harm</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPossibleHarm()
	 * @generated
	 * @ordered
	 */
	protected static final String POSSIBLE_HARM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPossibleHarm() <em>Possible Harm</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPossibleHarm()
	 * @generated
	 * @ordered
	 */
	protected String possibleHarm = POSSIBLE_HARM_EDEFAULT;

	/**
	 * The cached value of the '{@link #getBase_Operation() <em>Base Operation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_Operation()
	 * @generated
	 * @ordered
	 */
	protected Operation base_Operation;

	/**
	 * The default value of the '{@link #getComment() <em>Comment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComment()
	 * @generated
	 * @ordered
	 */
	protected static final String COMMENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getComment() <em>Comment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComment()
	 * @generated
	 * @ordered
	 */
	protected String comment = COMMENT_EDEFAULT;

	/**
	 * The default value of the '{@link #getInitial_Occurence() <em>Initial Occurence</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitial_Occurence()
	 * @generated
	 * @ordered
	 */
	protected static final OccurenceEstimation INITIAL_OCCURENCE_EDEFAULT = OccurenceEstimation._1;

	/**
	 * The cached value of the '{@link #getInitial_Occurence() <em>Initial Occurence</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitial_Occurence()
	 * @generated
	 * @ordered
	 */
	protected OccurenceEstimation initial_Occurence = INITIAL_OCCURENCE_EDEFAULT;

	/**
	 * The default value of the '{@link #getInitial_Avoidance() <em>Initial Avoidance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitial_Avoidance()
	 * @generated
	 * @ordered
	 */
	protected static final RiskEstimation INITIAL_AVOIDANCE_EDEFAULT = RiskEstimation._1;

	/**
	 * The cached value of the '{@link #getInitial_Avoidance() <em>Initial Avoidance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitial_Avoidance()
	 * @generated
	 * @ordered
	 */
	protected RiskEstimation initial_Avoidance = INITIAL_AVOIDANCE_EDEFAULT;

	/**
	 * The default value of the '{@link #getInitial_Frequency() <em>Initial Frequency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitial_Frequency()
	 * @generated
	 * @ordered
	 */
	protected static final RiskEstimation INITIAL_FREQUENCY_EDEFAULT = RiskEstimation._1;

	/**
	 * The cached value of the '{@link #getInitial_Frequency() <em>Initial Frequency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitial_Frequency()
	 * @generated
	 * @ordered
	 */
	protected RiskEstimation initial_Frequency = INITIAL_FREQUENCY_EDEFAULT;

	/**
	 * The default value of the '{@link #getInitial_Severity() <em>Initial Severity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitial_Severity()
	 * @generated
	 * @ordered
	 */
	protected static final RiskEstimation INITIAL_SEVERITY_EDEFAULT = RiskEstimation._1;

	/**
	 * The cached value of the '{@link #getInitial_Severity() <em>Initial Severity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitial_Severity()
	 * @generated
	 * @ordered
	 */
	protected RiskEstimation initial_Severity = INITIAL_SEVERITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getInitial_Criticality() <em>Initial Criticality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitial_Criticality()
	 * @generated
	 * @ordered
	 */
	protected static final RiskIndex INITIAL_CRITICALITY_EDEFAULT = RiskIndex._1;

	/**
	 * The default value of the '{@link #getInitial_RiskReduction() <em>Initial Risk Reduction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitial_RiskReduction()
	 * @generated
	 * @ordered
	 */
	protected static final String INITIAL_RISK_REDUCTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInitial_RiskReduction() <em>Initial Risk Reduction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitial_RiskReduction()
	 * @generated
	 * @ordered
	 */
	protected String initial_RiskReduction = INITIAL_RISK_REDUCTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getFinal_Occurence() <em>Final Occurence</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFinal_Occurence()
	 * @generated
	 * @ordered
	 */
	protected static final OccurenceEstimation FINAL_OCCURENCE_EDEFAULT = OccurenceEstimation._1;

	/**
	 * The cached value of the '{@link #getFinal_Occurence() <em>Final Occurence</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFinal_Occurence()
	 * @generated
	 * @ordered
	 */
	protected OccurenceEstimation final_Occurence = FINAL_OCCURENCE_EDEFAULT;

	/**
	 * The default value of the '{@link #getFinal_Avoidance() <em>Final Avoidance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFinal_Avoidance()
	 * @generated
	 * @ordered
	 */
	protected static final RiskEstimation FINAL_AVOIDANCE_EDEFAULT = RiskEstimation._1;

	/**
	 * The cached value of the '{@link #getFinal_Avoidance() <em>Final Avoidance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFinal_Avoidance()
	 * @generated
	 * @ordered
	 */
	protected RiskEstimation final_Avoidance = FINAL_AVOIDANCE_EDEFAULT;

	/**
	 * The default value of the '{@link #getFinal_Frequency() <em>Final Frequency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFinal_Frequency()
	 * @generated
	 * @ordered
	 */
	protected static final RiskEstimation FINAL_FREQUENCY_EDEFAULT = RiskEstimation._1;

	/**
	 * The cached value of the '{@link #getFinal_Frequency() <em>Final Frequency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFinal_Frequency()
	 * @generated
	 * @ordered
	 */
	protected RiskEstimation final_Frequency = FINAL_FREQUENCY_EDEFAULT;

	/**
	 * The default value of the '{@link #getFinal_Severity() <em>Final Severity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFinal_Severity()
	 * @generated
	 * @ordered
	 */
	protected static final RiskEstimation FINAL_SEVERITY_EDEFAULT = RiskEstimation._1;

	/**
	 * The cached value of the '{@link #getFinal_Severity() <em>Final Severity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFinal_Severity()
	 * @generated
	 * @ordered
	 */
	protected RiskEstimation final_Severity = FINAL_SEVERITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getFinal_Criticality() <em>Final Criticality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFinal_Criticality()
	 * @generated
	 * @ordered
	 */
	protected static final RiskIndex FINAL_CRITICALITY_EDEFAULT = RiskIndex._1;

	/**
	 * The default value of the '{@link #getFinal_RiskReduction() <em>Final Risk Reduction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFinal_RiskReduction()
	 * @generated
	 * @ordered
	 */
	protected static final String FINAL_RISK_REDUCTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFinal_RiskReduction() <em>Final Risk Reduction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFinal_RiskReduction()
	 * @generated
	 * @ordered
	 */
	protected String final_RiskReduction = FINAL_RISK_REDUCTION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getBase_Property() <em>Base Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_Property()
	 * @generated
	 * @ordered
	 */
	protected Property base_Property;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HazardAnalysisImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RiskanalysisPackage.Literals.HAZARD_ANALYSIS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Namespace getTask() {
		if (task != null && task.eIsProxy()) {
			InternalEObject oldTask = (InternalEObject)task;
			task = (Namespace)eResolveProxy(oldTask);
			if (task != oldTask) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RiskanalysisPackage.HAZARD_ANALYSIS__TASK, oldTask, task));
			}
		}
		return task;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Namespace basicGetTask() {
		return task;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTask(Namespace newTask) {
		Namespace oldTask = task;
		task = newTask;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RiskanalysisPackage.HAZARD_ANALYSIS__TASK, oldTask, task));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOrigin() {
		return origin;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOrigin(String newOrigin) {
		String oldOrigin = origin;
		origin = newOrigin;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RiskanalysisPackage.HAZARD_ANALYSIS__ORIGIN, oldOrigin, origin));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getHazardousSituation() {
		return hazardousSituation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setHazardousSituation(String newHazardousSituation) {
		String oldHazardousSituation = hazardousSituation;
		hazardousSituation = newHazardousSituation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RiskanalysisPackage.HAZARD_ANALYSIS__HAZARDOUS_SITUATION, oldHazardousSituation, hazardousSituation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getHazardousEvent() {
		return hazardousEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setHazardousEvent(String newHazardousEvent) {
		String oldHazardousEvent = hazardousEvent;
		hazardousEvent = newHazardousEvent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RiskanalysisPackage.HAZARD_ANALYSIS__HAZARDOUS_EVENT, oldHazardousEvent, hazardousEvent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getPossibleHarm() {
		return possibleHarm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPossibleHarm(String newPossibleHarm) {
		String oldPossibleHarm = possibleHarm;
		possibleHarm = newPossibleHarm;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RiskanalysisPackage.HAZARD_ANALYSIS__POSSIBLE_HARM, oldPossibleHarm, possibleHarm));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Operation getBase_Operation() {
		if (base_Operation != null && base_Operation.eIsProxy()) {
			InternalEObject oldBase_Operation = (InternalEObject)base_Operation;
			base_Operation = (Operation)eResolveProxy(oldBase_Operation);
			if (base_Operation != oldBase_Operation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RiskanalysisPackage.HAZARD_ANALYSIS__BASE_OPERATION, oldBase_Operation, base_Operation));
			}
		}
		return base_Operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Operation basicGetBase_Operation() {
		return base_Operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBase_Operation(Operation newBase_Operation) {
		Operation oldBase_Operation = base_Operation;
		base_Operation = newBase_Operation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RiskanalysisPackage.HAZARD_ANALYSIS__BASE_OPERATION, oldBase_Operation, base_Operation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getComment() {
		return comment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setComment(String newComment) {
		String oldComment = comment;
		comment = newComment;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RiskanalysisPackage.HAZARD_ANALYSIS__COMMENT, oldComment, comment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public OccurenceEstimation getInitial_Occurence() {
		return initial_Occurence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setInitial_Occurence(OccurenceEstimation newInitial_Occurence) {
		OccurenceEstimation oldInitial_Occurence = initial_Occurence;
		initial_Occurence = newInitial_Occurence == null ? INITIAL_OCCURENCE_EDEFAULT : newInitial_Occurence;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RiskanalysisPackage.HAZARD_ANALYSIS__INITIAL_OCCURENCE, oldInitial_Occurence, initial_Occurence));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RiskEstimation getInitial_Avoidance() {
		return initial_Avoidance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setInitial_Avoidance(RiskEstimation newInitial_Avoidance) {
		RiskEstimation oldInitial_Avoidance = initial_Avoidance;
		initial_Avoidance = newInitial_Avoidance == null ? INITIAL_AVOIDANCE_EDEFAULT : newInitial_Avoidance;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RiskanalysisPackage.HAZARD_ANALYSIS__INITIAL_AVOIDANCE, oldInitial_Avoidance, initial_Avoidance));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RiskEstimation getInitial_Frequency() {
		return initial_Frequency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setInitial_Frequency(RiskEstimation newInitial_Frequency) {
		RiskEstimation oldInitial_Frequency = initial_Frequency;
		initial_Frequency = newInitial_Frequency == null ? INITIAL_FREQUENCY_EDEFAULT : newInitial_Frequency;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RiskanalysisPackage.HAZARD_ANALYSIS__INITIAL_FREQUENCY, oldInitial_Frequency, initial_Frequency));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RiskEstimation getInitial_Severity() {
		return initial_Severity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setInitial_Severity(RiskEstimation newInitial_Severity) {
		RiskEstimation oldInitial_Severity = initial_Severity;
		initial_Severity = newInitial_Severity == null ? INITIAL_SEVERITY_EDEFAULT : newInitial_Severity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RiskanalysisPackage.HAZARD_ANALYSIS__INITIAL_SEVERITY, oldInitial_Severity, initial_Severity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RiskIndex getInitial_Criticality() {
		// TODO: implement this method to return the 'Initial Criticality' attribute
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getInitial_RiskReduction() {
		return initial_RiskReduction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setInitial_RiskReduction(String newInitial_RiskReduction) {
		String oldInitial_RiskReduction = initial_RiskReduction;
		initial_RiskReduction = newInitial_RiskReduction;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RiskanalysisPackage.HAZARD_ANALYSIS__INITIAL_RISK_REDUCTION, oldInitial_RiskReduction, initial_RiskReduction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public OccurenceEstimation getFinal_Occurence() {
		return final_Occurence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFinal_Occurence(OccurenceEstimation newFinal_Occurence) {
		OccurenceEstimation oldFinal_Occurence = final_Occurence;
		final_Occurence = newFinal_Occurence == null ? FINAL_OCCURENCE_EDEFAULT : newFinal_Occurence;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RiskanalysisPackage.HAZARD_ANALYSIS__FINAL_OCCURENCE, oldFinal_Occurence, final_Occurence));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RiskEstimation getFinal_Avoidance() {
		return final_Avoidance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFinal_Avoidance(RiskEstimation newFinal_Avoidance) {
		RiskEstimation oldFinal_Avoidance = final_Avoidance;
		final_Avoidance = newFinal_Avoidance == null ? FINAL_AVOIDANCE_EDEFAULT : newFinal_Avoidance;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RiskanalysisPackage.HAZARD_ANALYSIS__FINAL_AVOIDANCE, oldFinal_Avoidance, final_Avoidance));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RiskEstimation getFinal_Frequency() {
		return final_Frequency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFinal_Frequency(RiskEstimation newFinal_Frequency) {
		RiskEstimation oldFinal_Frequency = final_Frequency;
		final_Frequency = newFinal_Frequency == null ? FINAL_FREQUENCY_EDEFAULT : newFinal_Frequency;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RiskanalysisPackage.HAZARD_ANALYSIS__FINAL_FREQUENCY, oldFinal_Frequency, final_Frequency));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RiskEstimation getFinal_Severity() {
		return final_Severity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFinal_Severity(RiskEstimation newFinal_Severity) {
		RiskEstimation oldFinal_Severity = final_Severity;
		final_Severity = newFinal_Severity == null ? FINAL_SEVERITY_EDEFAULT : newFinal_Severity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RiskanalysisPackage.HAZARD_ANALYSIS__FINAL_SEVERITY, oldFinal_Severity, final_Severity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RiskIndex getFinal_Criticality() {
		// TODO: implement this method to return the 'Final Criticality' attribute
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getFinal_RiskReduction() {
		return final_RiskReduction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFinal_RiskReduction(String newFinal_RiskReduction) {
		String oldFinal_RiskReduction = final_RiskReduction;
		final_RiskReduction = newFinal_RiskReduction;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RiskanalysisPackage.HAZARD_ANALYSIS__FINAL_RISK_REDUCTION, oldFinal_RiskReduction, final_RiskReduction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Property getBase_Property() {
		if (base_Property != null && base_Property.eIsProxy()) {
			InternalEObject oldBase_Property = (InternalEObject)base_Property;
			base_Property = (Property)eResolveProxy(oldBase_Property);
			if (base_Property != oldBase_Property) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RiskanalysisPackage.HAZARD_ANALYSIS__BASE_PROPERTY, oldBase_Property, base_Property));
			}
		}
		return base_Property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Property basicGetBase_Property() {
		return base_Property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBase_Property(Property newBase_Property) {
		Property oldBase_Property = base_Property;
		base_Property = newBase_Property;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RiskanalysisPackage.HAZARD_ANALYSIS__BASE_PROPERTY, oldBase_Property, base_Property));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RiskanalysisPackage.HAZARD_ANALYSIS__TASK:
				if (resolve) return getTask();
				return basicGetTask();
			case RiskanalysisPackage.HAZARD_ANALYSIS__ORIGIN:
				return getOrigin();
			case RiskanalysisPackage.HAZARD_ANALYSIS__HAZARDOUS_SITUATION:
				return getHazardousSituation();
			case RiskanalysisPackage.HAZARD_ANALYSIS__HAZARDOUS_EVENT:
				return getHazardousEvent();
			case RiskanalysisPackage.HAZARD_ANALYSIS__POSSIBLE_HARM:
				return getPossibleHarm();
			case RiskanalysisPackage.HAZARD_ANALYSIS__BASE_OPERATION:
				if (resolve) return getBase_Operation();
				return basicGetBase_Operation();
			case RiskanalysisPackage.HAZARD_ANALYSIS__COMMENT:
				return getComment();
			case RiskanalysisPackage.HAZARD_ANALYSIS__INITIAL_OCCURENCE:
				return getInitial_Occurence();
			case RiskanalysisPackage.HAZARD_ANALYSIS__INITIAL_AVOIDANCE:
				return getInitial_Avoidance();
			case RiskanalysisPackage.HAZARD_ANALYSIS__INITIAL_FREQUENCY:
				return getInitial_Frequency();
			case RiskanalysisPackage.HAZARD_ANALYSIS__INITIAL_SEVERITY:
				return getInitial_Severity();
			case RiskanalysisPackage.HAZARD_ANALYSIS__INITIAL_CRITICALITY:
				return getInitial_Criticality();
			case RiskanalysisPackage.HAZARD_ANALYSIS__INITIAL_RISK_REDUCTION:
				return getInitial_RiskReduction();
			case RiskanalysisPackage.HAZARD_ANALYSIS__FINAL_OCCURENCE:
				return getFinal_Occurence();
			case RiskanalysisPackage.HAZARD_ANALYSIS__FINAL_AVOIDANCE:
				return getFinal_Avoidance();
			case RiskanalysisPackage.HAZARD_ANALYSIS__FINAL_FREQUENCY:
				return getFinal_Frequency();
			case RiskanalysisPackage.HAZARD_ANALYSIS__FINAL_SEVERITY:
				return getFinal_Severity();
			case RiskanalysisPackage.HAZARD_ANALYSIS__FINAL_CRITICALITY:
				return getFinal_Criticality();
			case RiskanalysisPackage.HAZARD_ANALYSIS__FINAL_RISK_REDUCTION:
				return getFinal_RiskReduction();
			case RiskanalysisPackage.HAZARD_ANALYSIS__BASE_PROPERTY:
				if (resolve) return getBase_Property();
				return basicGetBase_Property();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RiskanalysisPackage.HAZARD_ANALYSIS__TASK:
				setTask((Namespace)newValue);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS__ORIGIN:
				setOrigin((String)newValue);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS__HAZARDOUS_SITUATION:
				setHazardousSituation((String)newValue);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS__HAZARDOUS_EVENT:
				setHazardousEvent((String)newValue);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS__POSSIBLE_HARM:
				setPossibleHarm((String)newValue);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS__BASE_OPERATION:
				setBase_Operation((Operation)newValue);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS__COMMENT:
				setComment((String)newValue);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS__INITIAL_OCCURENCE:
				setInitial_Occurence((OccurenceEstimation)newValue);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS__INITIAL_AVOIDANCE:
				setInitial_Avoidance((RiskEstimation)newValue);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS__INITIAL_FREQUENCY:
				setInitial_Frequency((RiskEstimation)newValue);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS__INITIAL_SEVERITY:
				setInitial_Severity((RiskEstimation)newValue);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS__INITIAL_RISK_REDUCTION:
				setInitial_RiskReduction((String)newValue);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS__FINAL_OCCURENCE:
				setFinal_Occurence((OccurenceEstimation)newValue);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS__FINAL_AVOIDANCE:
				setFinal_Avoidance((RiskEstimation)newValue);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS__FINAL_FREQUENCY:
				setFinal_Frequency((RiskEstimation)newValue);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS__FINAL_SEVERITY:
				setFinal_Severity((RiskEstimation)newValue);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS__FINAL_RISK_REDUCTION:
				setFinal_RiskReduction((String)newValue);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS__BASE_PROPERTY:
				setBase_Property((Property)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RiskanalysisPackage.HAZARD_ANALYSIS__TASK:
				setTask((Namespace)null);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS__ORIGIN:
				setOrigin(ORIGIN_EDEFAULT);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS__HAZARDOUS_SITUATION:
				setHazardousSituation(HAZARDOUS_SITUATION_EDEFAULT);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS__HAZARDOUS_EVENT:
				setHazardousEvent(HAZARDOUS_EVENT_EDEFAULT);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS__POSSIBLE_HARM:
				setPossibleHarm(POSSIBLE_HARM_EDEFAULT);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS__BASE_OPERATION:
				setBase_Operation((Operation)null);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS__COMMENT:
				setComment(COMMENT_EDEFAULT);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS__INITIAL_OCCURENCE:
				setInitial_Occurence(INITIAL_OCCURENCE_EDEFAULT);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS__INITIAL_AVOIDANCE:
				setInitial_Avoidance(INITIAL_AVOIDANCE_EDEFAULT);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS__INITIAL_FREQUENCY:
				setInitial_Frequency(INITIAL_FREQUENCY_EDEFAULT);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS__INITIAL_SEVERITY:
				setInitial_Severity(INITIAL_SEVERITY_EDEFAULT);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS__INITIAL_RISK_REDUCTION:
				setInitial_RiskReduction(INITIAL_RISK_REDUCTION_EDEFAULT);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS__FINAL_OCCURENCE:
				setFinal_Occurence(FINAL_OCCURENCE_EDEFAULT);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS__FINAL_AVOIDANCE:
				setFinal_Avoidance(FINAL_AVOIDANCE_EDEFAULT);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS__FINAL_FREQUENCY:
				setFinal_Frequency(FINAL_FREQUENCY_EDEFAULT);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS__FINAL_SEVERITY:
				setFinal_Severity(FINAL_SEVERITY_EDEFAULT);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS__FINAL_RISK_REDUCTION:
				setFinal_RiskReduction(FINAL_RISK_REDUCTION_EDEFAULT);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS__BASE_PROPERTY:
				setBase_Property((Property)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RiskanalysisPackage.HAZARD_ANALYSIS__TASK:
				return task != null;
			case RiskanalysisPackage.HAZARD_ANALYSIS__ORIGIN:
				return ORIGIN_EDEFAULT == null ? origin != null : !ORIGIN_EDEFAULT.equals(origin);
			case RiskanalysisPackage.HAZARD_ANALYSIS__HAZARDOUS_SITUATION:
				return HAZARDOUS_SITUATION_EDEFAULT == null ? hazardousSituation != null : !HAZARDOUS_SITUATION_EDEFAULT.equals(hazardousSituation);
			case RiskanalysisPackage.HAZARD_ANALYSIS__HAZARDOUS_EVENT:
				return HAZARDOUS_EVENT_EDEFAULT == null ? hazardousEvent != null : !HAZARDOUS_EVENT_EDEFAULT.equals(hazardousEvent);
			case RiskanalysisPackage.HAZARD_ANALYSIS__POSSIBLE_HARM:
				return POSSIBLE_HARM_EDEFAULT == null ? possibleHarm != null : !POSSIBLE_HARM_EDEFAULT.equals(possibleHarm);
			case RiskanalysisPackage.HAZARD_ANALYSIS__BASE_OPERATION:
				return base_Operation != null;
			case RiskanalysisPackage.HAZARD_ANALYSIS__COMMENT:
				return COMMENT_EDEFAULT == null ? comment != null : !COMMENT_EDEFAULT.equals(comment);
			case RiskanalysisPackage.HAZARD_ANALYSIS__INITIAL_OCCURENCE:
				return initial_Occurence != INITIAL_OCCURENCE_EDEFAULT;
			case RiskanalysisPackage.HAZARD_ANALYSIS__INITIAL_AVOIDANCE:
				return initial_Avoidance != INITIAL_AVOIDANCE_EDEFAULT;
			case RiskanalysisPackage.HAZARD_ANALYSIS__INITIAL_FREQUENCY:
				return initial_Frequency != INITIAL_FREQUENCY_EDEFAULT;
			case RiskanalysisPackage.HAZARD_ANALYSIS__INITIAL_SEVERITY:
				return initial_Severity != INITIAL_SEVERITY_EDEFAULT;
			case RiskanalysisPackage.HAZARD_ANALYSIS__INITIAL_CRITICALITY:
				return getInitial_Criticality() != INITIAL_CRITICALITY_EDEFAULT;
			case RiskanalysisPackage.HAZARD_ANALYSIS__INITIAL_RISK_REDUCTION:
				return INITIAL_RISK_REDUCTION_EDEFAULT == null ? initial_RiskReduction != null : !INITIAL_RISK_REDUCTION_EDEFAULT.equals(initial_RiskReduction);
			case RiskanalysisPackage.HAZARD_ANALYSIS__FINAL_OCCURENCE:
				return final_Occurence != FINAL_OCCURENCE_EDEFAULT;
			case RiskanalysisPackage.HAZARD_ANALYSIS__FINAL_AVOIDANCE:
				return final_Avoidance != FINAL_AVOIDANCE_EDEFAULT;
			case RiskanalysisPackage.HAZARD_ANALYSIS__FINAL_FREQUENCY:
				return final_Frequency != FINAL_FREQUENCY_EDEFAULT;
			case RiskanalysisPackage.HAZARD_ANALYSIS__FINAL_SEVERITY:
				return final_Severity != FINAL_SEVERITY_EDEFAULT;
			case RiskanalysisPackage.HAZARD_ANALYSIS__FINAL_CRITICALITY:
				return getFinal_Criticality() != FINAL_CRITICALITY_EDEFAULT;
			case RiskanalysisPackage.HAZARD_ANALYSIS__FINAL_RISK_REDUCTION:
				return FINAL_RISK_REDUCTION_EDEFAULT == null ? final_RiskReduction != null : !FINAL_RISK_REDUCTION_EDEFAULT.equals(final_RiskReduction);
			case RiskanalysisPackage.HAZARD_ANALYSIS__BASE_PROPERTY:
				return base_Property != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (Origin: ");
		result.append(origin);
		result.append(", HazardousSituation: ");
		result.append(hazardousSituation);
		result.append(", HazardousEvent: ");
		result.append(hazardousEvent);
		result.append(", PossibleHarm: ");
		result.append(possibleHarm);
		result.append(", Comment: ");
		result.append(comment);
		result.append(", Initial_Occurence: ");
		result.append(initial_Occurence);
		result.append(", Initial_Avoidance: ");
		result.append(initial_Avoidance);
		result.append(", Initial_Frequency: ");
		result.append(initial_Frequency);
		result.append(", Initial_Severity: ");
		result.append(initial_Severity);
		result.append(", Initial_RiskReduction: ");
		result.append(initial_RiskReduction);
		result.append(", Final_Occurence: ");
		result.append(final_Occurence);
		result.append(", Final_Avoidance: ");
		result.append(final_Avoidance);
		result.append(", Final_Frequency: ");
		result.append(final_Frequency);
		result.append(", Final_Severity: ");
		result.append(final_Severity);
		result.append(", Final_RiskReduction: ");
		result.append(final_RiskReduction);
		result.append(')');
		return result.toString();
	}

} //HazardAnalysisImpl
