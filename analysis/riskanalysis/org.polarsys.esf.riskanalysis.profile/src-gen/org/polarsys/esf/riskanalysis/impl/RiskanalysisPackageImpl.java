/*******************************************************************************
 * Copyright (c) 2019 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.riskanalysis.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.uml2.types.TypesPackage;

import org.eclipse.uml2.uml.UMLPackage;
import org.polarsys.esf.riskanalysis.HazardAnalysis;
import org.polarsys.esf.riskanalysis.OccurenceEstimation;
import org.polarsys.esf.riskanalysis.RiskEstimation;
import org.polarsys.esf.riskanalysis.RiskIndex;
import org.polarsys.esf.riskanalysis.RiskanalysisFactory;
import org.polarsys.esf.riskanalysis.RiskanalysisPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RiskanalysisPackageImpl extends EPackageImpl implements RiskanalysisPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hazardAnalysisEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum occurenceEstimationEEnum = null;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum riskEstimationEEnum = null;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum riskIndexEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.polarsys.esf.riskanalysis.RiskanalysisPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private RiskanalysisPackageImpl() {
		super(eNS_URI, RiskanalysisFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link RiskanalysisPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static RiskanalysisPackage init() {
		if (isInited) return (RiskanalysisPackage)EPackage.Registry.INSTANCE.getEPackage(RiskanalysisPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredRiskanalysisPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		RiskanalysisPackageImpl theRiskanalysisPackage = registeredRiskanalysisPackage instanceof RiskanalysisPackageImpl ? (RiskanalysisPackageImpl)registeredRiskanalysisPackage : new RiskanalysisPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		UMLPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theRiskanalysisPackage.createPackageContents();

		// Initialize created meta-data
		theRiskanalysisPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theRiskanalysisPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(RiskanalysisPackage.eNS_URI, theRiskanalysisPackage);
		return theRiskanalysisPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getHazardAnalysis() {
		return hazardAnalysisEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getHazardAnalysis_Task() {
		return (EReference)hazardAnalysisEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getHazardAnalysis_Origin() {
		return (EAttribute)hazardAnalysisEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getHazardAnalysis_HazardousSituation() {
		return (EAttribute)hazardAnalysisEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getHazardAnalysis_HazardousEvent() {
		return (EAttribute)hazardAnalysisEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getHazardAnalysis_PossibleHarm() {
		return (EAttribute)hazardAnalysisEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getHazardAnalysis_Base_Operation() {
		return (EReference)hazardAnalysisEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getHazardAnalysis_Comment() {
		return (EAttribute)hazardAnalysisEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getHazardAnalysis_Initial_Occurence() {
		return (EAttribute)hazardAnalysisEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getHazardAnalysis_Initial_Avoidance() {
		return (EAttribute)hazardAnalysisEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getHazardAnalysis_Initial_Frequency() {
		return (EAttribute)hazardAnalysisEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getHazardAnalysis_Initial_Severity() {
		return (EAttribute)hazardAnalysisEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getHazardAnalysis_Initial_Criticality() {
		return (EAttribute)hazardAnalysisEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getHazardAnalysis_Initial_RiskReduction() {
		return (EAttribute)hazardAnalysisEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getHazardAnalysis_Final_Occurence() {
		return (EAttribute)hazardAnalysisEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getHazardAnalysis_Final_Avoidance() {
		return (EAttribute)hazardAnalysisEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getHazardAnalysis_Final_Frequency() {
		return (EAttribute)hazardAnalysisEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getHazardAnalysis_Final_Severity() {
		return (EAttribute)hazardAnalysisEClass.getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getHazardAnalysis_Final_Criticality() {
		return (EAttribute)hazardAnalysisEClass.getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getHazardAnalysis_Final_RiskReduction() {
		return (EAttribute)hazardAnalysisEClass.getEStructuralFeatures().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getHazardAnalysis_Base_Property() {
		return (EReference)hazardAnalysisEClass.getEStructuralFeatures().get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getOccurenceEstimation() {
		return occurenceEstimationEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getRiskEstimation() {
		return riskEstimationEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getRiskIndex() {
		return riskIndexEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RiskanalysisFactory getRiskanalysisFactory() {
		return (RiskanalysisFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		hazardAnalysisEClass = createEClass(HAZARD_ANALYSIS);
		createEReference(hazardAnalysisEClass, HAZARD_ANALYSIS__TASK);
		createEAttribute(hazardAnalysisEClass, HAZARD_ANALYSIS__ORIGIN);
		createEAttribute(hazardAnalysisEClass, HAZARD_ANALYSIS__HAZARDOUS_SITUATION);
		createEAttribute(hazardAnalysisEClass, HAZARD_ANALYSIS__HAZARDOUS_EVENT);
		createEAttribute(hazardAnalysisEClass, HAZARD_ANALYSIS__POSSIBLE_HARM);
		createEReference(hazardAnalysisEClass, HAZARD_ANALYSIS__BASE_OPERATION);
		createEAttribute(hazardAnalysisEClass, HAZARD_ANALYSIS__COMMENT);
		createEAttribute(hazardAnalysisEClass, HAZARD_ANALYSIS__INITIAL_OCCURENCE);
		createEAttribute(hazardAnalysisEClass, HAZARD_ANALYSIS__INITIAL_AVOIDANCE);
		createEAttribute(hazardAnalysisEClass, HAZARD_ANALYSIS__INITIAL_FREQUENCY);
		createEAttribute(hazardAnalysisEClass, HAZARD_ANALYSIS__INITIAL_SEVERITY);
		createEAttribute(hazardAnalysisEClass, HAZARD_ANALYSIS__INITIAL_CRITICALITY);
		createEAttribute(hazardAnalysisEClass, HAZARD_ANALYSIS__INITIAL_RISK_REDUCTION);
		createEAttribute(hazardAnalysisEClass, HAZARD_ANALYSIS__FINAL_OCCURENCE);
		createEAttribute(hazardAnalysisEClass, HAZARD_ANALYSIS__FINAL_AVOIDANCE);
		createEAttribute(hazardAnalysisEClass, HAZARD_ANALYSIS__FINAL_FREQUENCY);
		createEAttribute(hazardAnalysisEClass, HAZARD_ANALYSIS__FINAL_SEVERITY);
		createEAttribute(hazardAnalysisEClass, HAZARD_ANALYSIS__FINAL_CRITICALITY);
		createEAttribute(hazardAnalysisEClass, HAZARD_ANALYSIS__FINAL_RISK_REDUCTION);
		createEReference(hazardAnalysisEClass, HAZARD_ANALYSIS__BASE_PROPERTY);

		// Create enums
		occurenceEstimationEEnum = createEEnum(OCCURENCE_ESTIMATION);
		riskEstimationEEnum = createEEnum(RISK_ESTIMATION);
		riskIndexEEnum = createEEnum(RISK_INDEX);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		UMLPackage theUMLPackage = (UMLPackage)EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);
		TypesPackage theTypesPackage = (TypesPackage)EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(hazardAnalysisEClass, HazardAnalysis.class, "HazardAnalysis", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHazardAnalysis_Task(), theUMLPackage.getNamespace(), null, "Task", null, 1, 1, HazardAnalysis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getHazardAnalysis_Origin(), theTypesPackage.getString(), "Origin", null, 1, 1, HazardAnalysis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getHazardAnalysis_HazardousSituation(), theTypesPackage.getString(), "HazardousSituation", null, 1, 1, HazardAnalysis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getHazardAnalysis_HazardousEvent(), theTypesPackage.getString(), "HazardousEvent", null, 1, 1, HazardAnalysis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getHazardAnalysis_PossibleHarm(), theTypesPackage.getString(), "PossibleHarm", null, 1, 1, HazardAnalysis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getHazardAnalysis_Base_Operation(), theUMLPackage.getOperation(), null, "base_Operation", null, 0, 1, HazardAnalysis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getHazardAnalysis_Comment(), theTypesPackage.getString(), "Comment", null, 1, 1, HazardAnalysis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getHazardAnalysis_Initial_Occurence(), this.getOccurenceEstimation(), "Initial_Occurence", null, 1, 1, HazardAnalysis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getHazardAnalysis_Initial_Avoidance(), this.getRiskEstimation(), "Initial_Avoidance", null, 1, 1, HazardAnalysis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getHazardAnalysis_Initial_Frequency(), this.getRiskEstimation(), "Initial_Frequency", null, 1, 1, HazardAnalysis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getHazardAnalysis_Initial_Severity(), this.getRiskEstimation(), "Initial_Severity", null, 1, 1, HazardAnalysis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getHazardAnalysis_Initial_Criticality(), this.getRiskIndex(), "Initial_Criticality", null, 1, 1, HazardAnalysis.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);
		initEAttribute(getHazardAnalysis_Initial_RiskReduction(), theTypesPackage.getString(), "Initial_RiskReduction", null, 1, 1, HazardAnalysis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getHazardAnalysis_Final_Occurence(), this.getOccurenceEstimation(), "Final_Occurence", null, 1, 1, HazardAnalysis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getHazardAnalysis_Final_Avoidance(), this.getRiskEstimation(), "Final_Avoidance", null, 1, 1, HazardAnalysis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getHazardAnalysis_Final_Frequency(), this.getRiskEstimation(), "Final_Frequency", null, 1, 1, HazardAnalysis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getHazardAnalysis_Final_Severity(), this.getRiskEstimation(), "Final_Severity", null, 1, 1, HazardAnalysis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getHazardAnalysis_Final_Criticality(), this.getRiskIndex(), "Final_Criticality", null, 1, 1, HazardAnalysis.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);
		initEAttribute(getHazardAnalysis_Final_RiskReduction(), theTypesPackage.getString(), "Final_RiskReduction", null, 1, 1, HazardAnalysis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getHazardAnalysis_Base_Property(), theUMLPackage.getProperty(), null, "base_Property", null, 0, 1, HazardAnalysis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(occurenceEstimationEEnum, OccurenceEstimation.class, "OccurenceEstimation");
		addEEnumLiteral(occurenceEstimationEEnum, OccurenceEstimation._1);
		addEEnumLiteral(occurenceEstimationEEnum, OccurenceEstimation._2);
		addEEnumLiteral(occurenceEstimationEEnum, OccurenceEstimation._3);

		initEEnum(riskEstimationEEnum, RiskEstimation.class, "RiskEstimation");
		addEEnumLiteral(riskEstimationEEnum, RiskEstimation._1);
		addEEnumLiteral(riskEstimationEEnum, RiskEstimation._2);

		initEEnum(riskIndexEEnum, RiskIndex.class, "RiskIndex");
		addEEnumLiteral(riskIndexEEnum, RiskIndex._1);
		addEEnumLiteral(riskIndexEEnum, RiskIndex._2);
		addEEnumLiteral(riskIndexEEnum, RiskIndex._3);
		addEEnumLiteral(riskIndexEEnum, RiskIndex._4);
		addEEnumLiteral(riskIndexEEnum, RiskIndex._5);
		addEEnumLiteral(riskIndexEEnum, RiskIndex._6);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/uml2/2.0.0/UML
		createUMLAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/uml2/2.0.0/UML</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createUMLAnnotations() {
		String source = "http://www.eclipse.org/uml2/2.0.0/UML";
		addAnnotation
		  (this,
		   source,
		   new String[] {
			   "originalName", "TaskBasedRiskAnalysis"
		   });
		addAnnotation
		  (occurenceEstimationEEnum.getELiterals().get(0),
		   source,
		   new String[] {
			   "originalName", "1"
		   });
		addAnnotation
		  (occurenceEstimationEEnum.getELiterals().get(1),
		   source,
		   new String[] {
			   "originalName", "2"
		   });
		addAnnotation
		  (occurenceEstimationEEnum.getELiterals().get(2),
		   source,
		   new String[] {
			   "originalName", "3"
		   });
		addAnnotation
		  (riskEstimationEEnum.getELiterals().get(0),
		   source,
		   new String[] {
			   "originalName", "1"
		   });
		addAnnotation
		  (riskEstimationEEnum.getELiterals().get(1),
		   source,
		   new String[] {
			   "originalName", "2"
		   });
		addAnnotation
		  (riskIndexEEnum.getELiterals().get(0),
		   source,
		   new String[] {
			   "originalName", "1"
		   });
		addAnnotation
		  (riskIndexEEnum.getELiterals().get(1),
		   source,
		   new String[] {
			   "originalName", "2"
		   });
		addAnnotation
		  (riskIndexEEnum.getELiterals().get(2),
		   source,
		   new String[] {
			   "originalName", "3"
		   });
		addAnnotation
		  (riskIndexEEnum.getELiterals().get(3),
		   source,
		   new String[] {
			   "originalName", "4"
		   });
		addAnnotation
		  (riskIndexEEnum.getELiterals().get(4),
		   source,
		   new String[] {
			   "originalName", "5"
		   });
		addAnnotation
		  (riskIndexEEnum.getELiterals().get(5),
		   source,
		   new String[] {
			   "originalName", "6"
		   });
	}

} //RiskanalysisPackageImpl
