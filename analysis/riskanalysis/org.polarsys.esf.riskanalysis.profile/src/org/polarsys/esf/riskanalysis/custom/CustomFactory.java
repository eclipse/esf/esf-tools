/*******************************************************************************
 * Copyright (c) 2019 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.riskanalysis.custom;

import org.polarsys.esf.riskanalysis.HazardAnalysis;
import org.polarsys.esf.riskanalysis.impl.RiskanalysisFactoryImpl;

public class CustomFactory extends RiskanalysisFactoryImpl {

	@Override
	public HazardAnalysis createHazardAnalysis() {
		return new CustomHazardAnalysis();
	}
}
