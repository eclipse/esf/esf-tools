/*******************************************************************************
 * Copyright (c) 2019 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.riskanalysis.custom;


import org.polarsys.esf.riskanalysis.RiskIndex;
import org.polarsys.esf.riskanalysis.impl.HazardAnalysisImpl;

public class CustomHazardAnalysis extends HazardAnalysisImpl {

	/*
	 
	 * 
	 * (non-Javadoc)
	 * @see org.polarsys.esf.riskanalysis.impl.HazardAnalysisImpl#getInitial_Criticality()
	 */
	@Override
	public RiskIndex getInitial_Criticality() {
		Integer occurence= getInitial_Occurence().getValue();
		Integer avoidance = getInitial_Avoidance().getValue();
		Integer frequency = getInitial_Frequency().getValue();
		Integer severity= getInitial_Severity().getValue();
		return riskIndexCalculation(occurence, avoidance, frequency, severity);
	}
	
	/*
	 
	 * 
	 * (non-Javadoc)
	 * @see org.polarsys.esf.riskanalysis.impl.HazardAnalysisImpl#getInitial_Criticality()
	 */	
	@Override
	public RiskIndex getFinal_Criticality() {
		Integer occurence= getFinal_Occurence().getValue();
		Integer avoidance = getFinal_Avoidance().getValue();
		Integer frequency = getFinal_Frequency().getValue();
		Integer severity= getFinal_Severity().getValue();
		return riskIndexCalculation(occurence, avoidance, frequency, severity);
	}
	
	/*
	 * * Risk Index Calculation is implemented following the ISO/TR 14121-2:2007 (page 33)
	 * 
	 *		|	O1|   O2|	O3|
	 *		|A1|A2|A1|A2|A1|A2|
	 *_________________________
	 *|S1|F1|           |
	 *|  |F2|  1        | 2
	 *|__|__|___________|______
	 *|S2|F1|  2     |3   |4
	 *|  |__|________|____|____
	 *|  |F2|3|  4   |5   |6
	 *|__|__|_|______|____|____
	 */
	private RiskIndex riskIndexCalculation(Integer occurence, Integer avoidance, Integer frequency , Integer severity) {
		if(occurence!=null && severity!=null && frequency!=null && avoidance!=null)
			if((occurence==0 ||occurence==1)&& severity == 0) {
				return RiskIndex._1;
			} else
					if(occurence==2 && severity == 0) {
						return RiskIndex._2;
					}else if(occurence==0 && severity == 1 && frequency==0) {
						return RiskIndex._2;
					}else if(occurence==1 && avoidance == 0 && severity == 1 && frequency==0) {
						return RiskIndex._2;
					}else if(occurence==1 && avoidance == 1 && severity == 1 && frequency==0) {
						return RiskIndex._3;
					}else if(occurence==2 && avoidance == 0 && severity == 1 && frequency==0) {
						return RiskIndex._3;
						
					}else if(occurence==2 && avoidance == 1 && severity == 1 && frequency==0) {
						return RiskIndex._4;
					}else if(occurence==0 && avoidance == 0 && severity == 1 && frequency==1) {
						return RiskIndex._3;
					}else if(occurence==0 && avoidance == 1 && severity == 1 && frequency==1) {
						return RiskIndex._4;
					}else if(occurence==1 && avoidance == 0 && severity == 1 && frequency==1) {
						return RiskIndex._4;
					}else if(occurence==1 && avoidance == 1 && severity == 1 && frequency==1) {
						return RiskIndex._5;
					}else if(occurence==2 && avoidance == 0 && severity == 1 && frequency==1) {
						return RiskIndex._5;
					}else if(occurence==2 && avoidance == 1 && severity == 1 && frequency==1) {
						return RiskIndex._6;
					}
						
		return null;
	}
	

}
