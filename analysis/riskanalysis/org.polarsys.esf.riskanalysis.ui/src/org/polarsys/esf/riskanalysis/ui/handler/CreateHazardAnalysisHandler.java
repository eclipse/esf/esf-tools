/*******************************************************************************
 * Copyright (c) 2019 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.riskanalysis.ui.handler;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.UMLPackage;
import org.polarsys.esf.core.utils.ModelUtil;
import org.polarsys.esf.riskanalysis.HazardAnalysis;


public class CreateHazardAnalysisHandler extends AbstractHandler  {
	/** Create HazardAnalysis label. */
	private static final String CREATE_HAZARDANALYSIS_LABEL = "Create Hazard Analysis"; //$NON-NLS-1$

	/**
	 * Default constructor.
	 */
	public CreateHazardAnalysisHandler() {
		
	}	
	
	/**
	 * Get the selected element (Interface) and call the command for creating a HazardAnalysis.
	 *
	 * {@inheritDoc}
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection vSelection = HandlerUtil.getCurrentSelection(event);
		final Interface vSelectedInterface =
				(Interface) ModelUtil.getSelectedEObjectOfType(vSelection, UMLPackage.eINSTANCE.getInterface());
		if (vSelectedInterface != null) {
		
				TransactionalEditingDomain vDomain = ModelUtil.getTransactionalEditingDomain(vSelectedInterface.getPackage());
				RecordingCommand vCreateHazardAnalysis = new RecordingCommand(vDomain, CREATE_HAZARDANALYSIS_LABEL) {

					@Override
					protected void doExecute() {
						// compute the name for the new property
						String vName = "Hazard";
						// Create the object 'Property' of the interface containing the skills to analyze
						Property hazardAnalysisProperty = vSelectedInterface.createOwnedAttribute(vName, null);

						// Apply 'HazardAnalysis' stereotype on 'hazardAnalysisProperty'
						StereotypeUtil.apply(hazardAnalysisProperty, HazardAnalysis.class);
						
					}
				};

				// Verify if command can be executed
				if (vCreateHazardAnalysis.canExecute()) {
					// Execute command
					vDomain.getCommandStack().execute(vCreateHazardAnalysis);
				}
			}
		
		return null;
	}

	

}
