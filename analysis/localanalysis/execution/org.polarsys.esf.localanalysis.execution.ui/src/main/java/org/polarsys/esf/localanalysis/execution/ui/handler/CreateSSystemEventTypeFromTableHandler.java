/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.localanalysis.execution.ui.handler;

import org.eclipse.core.commands.IHandler;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.type.core.IHintedType;
import org.eclipse.papyrus.infra.nattable.manager.table.INattableModelManager;
import org.eclipse.papyrus.infra.nattable.utils.INattableModelManagerUtils;
import org.eclipse.papyrus.uml.nattable.menu.handlers.AbstractUmlNattableCreateCommandHandler;
import org.polarsys.esf.localanalysis.profile.service.types.set.ESFLocalAnalysisTypesSet;

/**
 * Handler class for creating a SSytemEventType from the SSystemEventsLibrary Table.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class CreateSSystemEventTypeFromTableHandler
	extends AbstractUmlNattableCreateCommandHandler
	implements IHandler {

	/**
	 * Default constructor.
	 */
	public CreateSSystemEventTypeFromTableHandler() {
	}

	/**
	 * @see org.eclipse.papyrus.uml.service.creation.handler.CreateHandler#getElementTypeToCreate().
	 *
	 * @return The IElementType this handler is supposed to create
	 *
	 */
	@Override
	protected IElementType getElementTypeToCreate() {
		return (IHintedType) ElementTypeRegistry.getInstance()
			.getType(ESFLocalAnalysisTypesSet.SSYSTEMEVENTTYPE_TYPE_ID);
	}

	/**
	 * Verify if this handler is currently active and the command can execute. Additionally, verify if this table can
	 * add this type of element.
	 *
	 * @see org.eclipse.papyrus.uml.service.types.handlers.AbstractCreateCommandHandler#setEnabled(java.lang.Object)
	 *
	 * @param pEvaluationContext
	 */
	@Override
	public void setEnabled(final Object pEvaluationContext) {
		INattableModelManager vTableManager =
			INattableModelManagerUtils.getTableManagerFromWorkbenchPart(getActiveWorkbenchPart());
		boolean vIsEnabled = vTableManager.canCreateRowElement(getElementTypeToCreate().getId());
		setBaseEnabled(vIsEnabled);
	}
}