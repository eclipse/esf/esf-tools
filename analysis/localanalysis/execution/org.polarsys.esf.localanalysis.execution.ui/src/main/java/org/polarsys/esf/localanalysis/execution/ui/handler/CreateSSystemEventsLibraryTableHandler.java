/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.localanalysis.execution.ui.handler;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.core.utils.ESFTablesUtil;
import org.polarsys.esf.core.utils.ModelUtil;
import org.polarsys.esf.esflocalanalysis.ISSystemEventsLibrary;
import org.polarsys.esf.localanalysis.table.set.ESFLocalAnalysisTablesSet;

/**
 * Handler class for creating a SSytemEventsLibrary Table.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class CreateSSystemEventsLibraryTableHandler
	extends AbstractHandler {

	/**
	 * Default constructor.
	 */
	public CreateSSystemEventsLibraryTableHandler() {
	}

	/**
	 * Get the selected element (SSytemEventsLibrary package) and call the action for creating a SSytemEventsLibrary
	 * Table.
	 *
	 * {@inheritDoc}
	 */
	@Override
	public Object execute(final ExecutionEvent pEvent) throws ExecutionException {

		ISelection vSelection = HandlerUtil.getCurrentSelection(pEvent);

		final Package vSelectedPackage =
			(Package) ModelUtil.getSelectedEObjectOfType(vSelection, UMLPackage.eINSTANCE.getPackage());

		if ((vSelectedPackage != null)
			&& (UMLUtil.getStereotypeApplication(vSelectedPackage, ISSystemEventsLibrary.class) != null)) {
			ESFTablesUtil.createTable(
				ESFLocalAnalysisTablesSet.SSYSTEMEVENTSLIBRARY_TABLE_ID,
				ESFLocalAnalysisTablesSet.SSYSTEMEVENTSLIBRARY_TABLE_NAME,
				vSelectedPackage);
		}
		return null;
	}

}
