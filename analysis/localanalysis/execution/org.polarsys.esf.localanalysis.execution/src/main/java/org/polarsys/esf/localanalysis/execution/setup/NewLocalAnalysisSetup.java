/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.localanalysis.execution.setup;

import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.papyrus.infra.core.resource.ReadOnlyAxis;
import org.eclipse.papyrus.infra.emf.readonly.ReadOnlyManager;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.core.common.ui.diagram.ESFDiagramUtil;
import org.polarsys.esf.core.utils.ESFTablesUtil;
import org.polarsys.esf.core.utils.ModelUtil;
import org.polarsys.esf.esflocalanalysis.ISLocalAnalysis;
import org.polarsys.esf.esflocalanalysis.ISSystemEventsLibrary;
import org.polarsys.esf.esflocalanalysis.impl.SLocalAnalysis;
import org.polarsys.esf.localanalysis.diagram.set.ESFLocalAnalysisDiagramSet;
import org.polarsys.esf.localanalysis.profile.tools.util.ESFLocalAnalysisUtil;
import org.polarsys.esf.localanalysis.table.set.ESFLocalAnalysisTablesSet;

/**
 * Setup class for starting a new local analysis.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public final class NewLocalAnalysisSetup {

	/**
	 * Default constructorr, private as it's a utility class.
	 */
	private NewLocalAnalysisSetup() {
	}

	/**
	 * Setup of a new Local Analysis.
	 *
	 * @param block
	 *            The block to be analysed
	 */
	public static void setup(final Class block) {

		Model vESFModel = ModelUtil.getWorkingModel();
		if (vESFModel != null) {

			// assure that model is writable
			ReadOnlyManager.getReadOnlyHandler(TransactionUtil.getEditingDomain(vESFModel)).makeWritable(ReadOnlyAxis.anyAxis(), block);

			if (block != null) {
				ESFLocalAnalysisUtil.createSLocalAnalysis(block.getNearestPackage());
				
				ESFLocalAnalysisUtil.createSBlockLAnalysis(block);
				// Create ESFLocalAnalysis diagram
				ESFDiagramUtil.createDiagram(
						ESFLocalAnalysisDiagramSet.ESFLOCALANALYSIS_DIAGRAM_NAME,
						block);
			}

			Package pkg = block.getNearestPackage();
			ISLocalAnalysis vSLocalAnalysis = UMLUtil.getStereotypeApplication(pkg, SLocalAnalysis.class);
			if (vSLocalAnalysis != null && vSLocalAnalysis.getSSystemEventsLibrary() == null) {
				createSSystemEventsLibrary(vSLocalAnalysis);
			}

			if (vSLocalAnalysis != null && vSLocalAnalysis.getSFearedEventsLibrary() == null) {
				ESFLocalAnalysisUtil.createSFearedEventsLibrary(vSLocalAnalysis.getBase_Package());
			}
		}
	}

	/**
	 * Create the SSystemEventsLibrary.
	 *
	 * @param pSLocalAnalysis
	 *            The SLocalAnalysis
	 */
	private static void createSSystemEventsLibrary(final ISLocalAnalysis pSLocalAnalysis) {
		ESFLocalAnalysisUtil.createSSystemEventsLibrary(pSLocalAnalysis.getBase_Package());
		if (pSLocalAnalysis != null) {
			ISSystemEventsLibrary vSSystemEventsLibrary = pSLocalAnalysis.getSSystemEventsLibrary();
			if (vSSystemEventsLibrary != null) {
				ESFTablesUtil.createTable(
						ESFLocalAnalysisTablesSet.SSYSTEMEVENTSLIBRARY_TABLE_ID,
						ESFLocalAnalysisTablesSet.SSYSTEMEVENTSLIBRARY_TABLE_NAME,
						vSSystemEventsLibrary.getBase_Package());
			}
		}
	}

}
