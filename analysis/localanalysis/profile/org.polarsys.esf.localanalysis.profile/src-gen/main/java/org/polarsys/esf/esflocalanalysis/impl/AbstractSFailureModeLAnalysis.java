/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esflocalanalysis.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis;
import org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeOwner;
import org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage;
import org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis;
import org.polarsys.esf.esflocalanalysis.ISFearedEvent;
import org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily;

import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureMode;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract SFailure Mode LAnalysis</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.AbstractSFailureModeLAnalysis#getSBlockLAnalysis <em>SBlock LAnalysis</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.AbstractSFailureModeLAnalysis#getSFailureMode <em>SFailure Mode</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.AbstractSFailureModeLAnalysis#getSFearedEventsFamiliesList <em>SFeared Events Families List</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.AbstractSFailureModeLAnalysis#getSFearedEventsList <em>SFeared Events List</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.AbstractSFailureModeLAnalysis#getOwner <em>Owner</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AbstractSFailureModeLAnalysis
		extends AbstractSPropagationElement
		implements IAbstractSFailureModeLAnalysis {

	/**
	 * The cached value of the '{@link #getSFailureMode() <em>SFailure Mode</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getSFailureMode()
	 * @generated
	 * @ordered
	 */
	protected ISFailureMode sFailureMode;

	/**
	 * The cached value of the '{@link #getSFearedEventsFamiliesList() <em>SFeared Events Families List</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getSFearedEventsFamiliesList()
	 * @generated
	 * @ordered
	 */
	protected EList<ISFearedEventsFamily> sFearedEventsFamiliesList;

	/**
	 * The cached value of the '{@link #getSFearedEventsList() <em>SFeared Events List</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getSFearedEventsList()
	 * @generated
	 * @ordered
	 */
	protected EList<ISFearedEvent> sFearedEventsList;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected AbstractSFailureModeLAnalysis() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IESFLocalAnalysisPackage.Literals.ABSTRACT_SFAILURE_MODE_LANALYSIS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISBlockLAnalysis getSBlockLAnalysis() {
		ISBlockLAnalysis sBlockLAnalysis = basicGetSBlockLAnalysis();
		return sBlockLAnalysis != null && sBlockLAnalysis.eIsProxy() ? (ISBlockLAnalysis) eResolveProxy((InternalEObject) sBlockLAnalysis) : sBlockLAnalysis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ISBlockLAnalysis basicGetSBlockLAnalysis() {
		// TODO: implement this method to return the 'SBlock LAnalysis' reference
		// -> do not perform proxy resolution
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setSBlockLAnalysis(ISBlockLAnalysis newSBlockLAnalysis) {
		// TODO: implement this method to set the 'SBlock LAnalysis' reference
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISFailureMode getSFailureMode() {
		if (sFailureMode != null && sFailureMode.eIsProxy()) {
			InternalEObject oldSFailureMode = (InternalEObject) sFailureMode;
			sFailureMode = (ISFailureMode) eResolveProxy(oldSFailureMode);
			if (sFailureMode != oldSFailureMode) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_MODE_LANALYSIS__SFAILURE_MODE, oldSFailureMode, sFailureMode));
			}
		}
		return sFailureMode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ISFailureMode basicGetSFailureMode() {
		return sFailureMode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setSFailureMode(ISFailureMode newSFailureMode) {
		ISFailureMode oldSFailureMode = sFailureMode;
		sFailureMode = newSFailureMode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_MODE_LANALYSIS__SFAILURE_MODE, oldSFailureMode, sFailureMode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EList<ISFearedEventsFamily> getSFearedEventsFamiliesList() {
		if (sFearedEventsFamiliesList == null) {
			sFearedEventsFamiliesList = new EObjectResolvingEList<ISFearedEventsFamily>(ISFearedEventsFamily.class, this, IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_MODE_LANALYSIS__SFEARED_EVENTS_FAMILIES_LIST);
		}
		return sFearedEventsFamiliesList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EList<ISFearedEvent> getSFearedEventsList() {
		if (sFearedEventsList == null) {
			sFearedEventsList = new EObjectResolvingEList<ISFearedEvent>(ISFearedEvent.class, this, IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_MODE_LANALYSIS__SFEARED_EVENTS_LIST);
		}
		return sFearedEventsList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public IAbstractSFailureModeOwner getOwner() {
		IAbstractSFailureModeOwner owner = basicGetOwner();
		return owner != null && owner.eIsProxy() ? (IAbstractSFailureModeOwner) eResolveProxy((InternalEObject) owner) : owner;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public IAbstractSFailureModeOwner basicGetOwner() {
		// TODO: implement this method to return the 'Owner' reference
		// -> do not perform proxy resolution
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setOwner(IAbstractSFailureModeOwner newOwner) {
		// TODO: implement this method to set the 'Owner' reference
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_MODE_LANALYSIS__SBLOCK_LANALYSIS:
			if (resolve)
				return getSBlockLAnalysis();
			return basicGetSBlockLAnalysis();
		case IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_MODE_LANALYSIS__SFAILURE_MODE:
			if (resolve)
				return getSFailureMode();
			return basicGetSFailureMode();
		case IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_MODE_LANALYSIS__SFEARED_EVENTS_FAMILIES_LIST:
			return getSFearedEventsFamiliesList();
		case IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_MODE_LANALYSIS__SFEARED_EVENTS_LIST:
			return getSFearedEventsList();
		case IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_MODE_LANALYSIS__OWNER:
			if (resolve)
				return getOwner();
			return basicGetOwner();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_MODE_LANALYSIS__SBLOCK_LANALYSIS:
			setSBlockLAnalysis((ISBlockLAnalysis) newValue);
			return;
		case IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_MODE_LANALYSIS__SFAILURE_MODE:
			setSFailureMode((ISFailureMode) newValue);
			return;
		case IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_MODE_LANALYSIS__SFEARED_EVENTS_FAMILIES_LIST:
			getSFearedEventsFamiliesList().clear();
			getSFearedEventsFamiliesList().addAll((Collection<? extends ISFearedEventsFamily>) newValue);
			return;
		case IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_MODE_LANALYSIS__SFEARED_EVENTS_LIST:
			getSFearedEventsList().clear();
			getSFearedEventsList().addAll((Collection<? extends ISFearedEvent>) newValue);
			return;
		case IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_MODE_LANALYSIS__OWNER:
			setOwner((IAbstractSFailureModeOwner) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_MODE_LANALYSIS__SBLOCK_LANALYSIS:
			setSBlockLAnalysis((ISBlockLAnalysis) null);
			return;
		case IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_MODE_LANALYSIS__SFAILURE_MODE:
			setSFailureMode((ISFailureMode) null);
			return;
		case IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_MODE_LANALYSIS__SFEARED_EVENTS_FAMILIES_LIST:
			getSFearedEventsFamiliesList().clear();
			return;
		case IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_MODE_LANALYSIS__SFEARED_EVENTS_LIST:
			getSFearedEventsList().clear();
			return;
		case IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_MODE_LANALYSIS__OWNER:
			setOwner((IAbstractSFailureModeOwner) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_MODE_LANALYSIS__SBLOCK_LANALYSIS:
			return basicGetSBlockLAnalysis() != null;
		case IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_MODE_LANALYSIS__SFAILURE_MODE:
			return sFailureMode != null;
		case IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_MODE_LANALYSIS__SFEARED_EVENTS_FAMILIES_LIST:
			return sFearedEventsFamiliesList != null && !sFearedEventsFamiliesList.isEmpty();
		case IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_MODE_LANALYSIS__SFEARED_EVENTS_LIST:
			return sFearedEventsList != null && !sFearedEventsList.isEmpty();
		case IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_MODE_LANALYSIS__OWNER:
			return basicGetOwner() != null;
		}
		return super.eIsSet(featureID);
	}

} // AbstractSFailureModeLAnalysis
