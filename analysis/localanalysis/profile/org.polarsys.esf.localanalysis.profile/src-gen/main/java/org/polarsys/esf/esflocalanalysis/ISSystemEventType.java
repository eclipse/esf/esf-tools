/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esflocalanalysis;

import org.eclipse.emf.common.util.EList;

import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SSystem Event Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISSystemEventType#getBase_Class <em>Base Class</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISSystemEventType#getSFailureEvent <em>SFailure Event</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISSystemEventType#getInstancesList <em>Instances List</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISSystemEventType#getSSystemEventsLibrary <em>SSystem Events Library</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSSystemEventType()
 * @model
 * @generated
 */
public interface ISSystemEventType
		extends IAbstractSLocalAnalysisElement {

	/**
	 * Returns the value of the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Base Class</em>' reference.
	 * @see #setBase_Class(org.eclipse.uml2.uml.Class)
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSSystemEventType_Base_Class()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	org.eclipse.uml2.uml.Class getBase_Class();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esflocalanalysis.ISSystemEventType#getBase_Class <em>Base Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Base Class</em>' reference.
	 * @see #getBase_Class()
	 * @generated
	 */
	void setBase_Class(org.eclipse.uml2.uml.Class value);

	/**
	 * Returns the value of the '<em><b>SFailure Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SFailure Event</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SFailure Event</em>' reference.
	 * @see #setSFailureEvent(ISFailureEvent)
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSSystemEventType_SFailureEvent()
	 * @model ordered="false"
	 * @generated
	 */
	ISFailureEvent getSFailureEvent();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esflocalanalysis.ISSystemEventType#getSFailureEvent <em>SFailure Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>SFailure Event</em>' reference.
	 * @see #getSFailureEvent()
	 * @generated
	 */
	void setSFailureEvent(ISFailureEvent value);

	/**
	 * Returns the value of the '<em><b>Instances List</b></em>' reference list.
	 * The list contents are of type {@link org.polarsys.esf.esflocalanalysis.ISSystemEvent}.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esflocalanalysis.ISSystemEvent#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Instances List</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Instances List</em>' reference list.
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSSystemEventType_InstancesList()
	 * @see org.polarsys.esf.esflocalanalysis.ISSystemEvent#getType
	 * @model opposite="type" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<ISSystemEvent> getInstancesList();

	/**
	 * Returns the value of the '<em><b>SSystem Events Library</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esflocalanalysis.ISSystemEventsLibrary#getSSystemEventTypesList <em>SSystem Event Types List</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SSystem Events Library</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SSystem Events Library</em>' reference.
	 * @see #setSSystemEventsLibrary(ISSystemEventsLibrary)
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSSystemEventType_SSystemEventsLibrary()
	 * @see org.polarsys.esf.esflocalanalysis.ISSystemEventsLibrary#getSSystemEventTypesList
	 * @model opposite="sSystemEventTypesList" required="true" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	ISSystemEventsLibrary getSSystemEventsLibrary();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esflocalanalysis.ISSystemEventType#getSSystemEventsLibrary <em>SSystem Events Library</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>SSystem Events Library</em>' reference.
	 * @see #getSSystemEventsLibrary()
	 * @generated
	 */
	void setSSystemEventsLibrary(ISSystemEventsLibrary value);

} // ISSystemEventType
