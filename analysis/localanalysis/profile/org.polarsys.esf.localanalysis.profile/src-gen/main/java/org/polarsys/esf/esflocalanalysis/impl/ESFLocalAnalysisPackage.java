/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esflocalanalysis.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.uml2.types.TypesPackage;

import org.eclipse.uml2.uml.UMLPackage;

import org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage;

import org.polarsys.esf.esfcore.IESFCorePackage;

import org.polarsys.esf.esflocalanalysis.IAbstractSFailureEventLAnalysis;
import org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis;
import org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeOwner;
import org.polarsys.esf.esflocalanalysis.IAbstractSLocalAnalysisElement;
import org.polarsys.esf.esflocalanalysis.IAbstractSLogicalGateLAnalysis;
import org.polarsys.esf.esflocalanalysis.IAbstractSPropagationElement;
import org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisFactory;
import org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage;
import org.polarsys.esf.esflocalanalysis.ISAbsentFailureMode;
import org.polarsys.esf.esflocalanalysis.ISAndGateLAnalysis;
import org.polarsys.esf.esflocalanalysis.ISBarrierLAnalysis;
import org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis;
import org.polarsys.esf.esflocalanalysis.ISDysfunctionalAssociation;
import org.polarsys.esf.esflocalanalysis.ISErroneousFailureMode;
import org.polarsys.esf.esflocalanalysis.ISFearedEvent;
import org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily;
import org.polarsys.esf.esflocalanalysis.ISFearedEventsLibrary;
import org.polarsys.esf.esflocalanalysis.ISLocalAnalysis;
import org.polarsys.esf.esflocalanalysis.ISLocalEvent;
import org.polarsys.esf.esflocalanalysis.ISOrGateLAnalysis;
import org.polarsys.esf.esflocalanalysis.ISPortLAnalysis;
import org.polarsys.esf.esflocalanalysis.ISPropagationLink;
import org.polarsys.esf.esflocalanalysis.ISSystemEvent;
import org.polarsys.esf.esflocalanalysis.ISSystemEventType;
import org.polarsys.esf.esflocalanalysis.ISSystemEventsLibrary;
import org.polarsys.esf.esflocalanalysis.ISUntimelyFailureMode;
import org.polarsys.esf.esfproperties.IESFPropertiesPackage;
import org.polarsys.esf.esfsafetyconcepts.IESFSafetyConceptsPackage;

import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage;

import org.polarsys.esf.esfsafetyconcepts.srecommendations.ISRecommendationsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class ESFLocalAnalysisPackage
		extends EPackageImpl
		implements IESFLocalAnalysisPackage {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass abstractSLocalAnalysisElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass sPortLAnalysisEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass abstractSFailureModeOwnerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass abstractSFailureModeLAnalysisEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass abstractSPropagationElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass sPropagationLinkEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass sBlockLAnalysisEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass abstractSFailureEventLAnalysisEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass abstractSLogicalGateLAnalysisEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass sBarrierLAnalysisEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass sLocalAnalysisEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass sSystemEventsLibraryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass sSystemEventTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass sSystemEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass sFearedEventsLibraryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass sFearedEventsFamilyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass sFearedEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass sDysfunctionalAssociationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass sAndGateLAnalysisEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass sOrGateLAnalysisEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass sUntimelyFailureModeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass sAbsentFailureModeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass sErroneousFailureModeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass sLocalEventEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>
	 * Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ESFLocalAnalysisPackage() {
		super(eNS_URI, IESFLocalAnalysisFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>
	 * This method is used to initialize {@link IESFLocalAnalysisPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static IESFLocalAnalysisPackage init() {
		if (isInited)
			return (IESFLocalAnalysisPackage) EPackage.Registry.INSTANCE.getEPackage(IESFLocalAnalysisPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredESFLocalAnalysisPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		ESFLocalAnalysisPackage theESFLocalAnalysisPackage = registeredESFLocalAnalysisPackage instanceof ESFLocalAnalysisPackage ? (ESFLocalAnalysisPackage) registeredESFLocalAnalysisPackage : new ESFLocalAnalysisPackage();

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();
		IESFArchitectureConceptsPackage.eINSTANCE.eClass();
		IESFCorePackage.eINSTANCE.eClass();
		IESFPropertiesPackage.eINSTANCE.eClass();
		IESFSafetyConceptsPackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		UMLPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theESFLocalAnalysisPackage.createPackageContents();

		// Initialize created meta-data
		theESFLocalAnalysisPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theESFLocalAnalysisPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(IESFLocalAnalysisPackage.eNS_URI, theESFLocalAnalysisPackage);
		return theESFLocalAnalysisPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EClass getAbstractSLocalAnalysisElement() {
		return abstractSLocalAnalysisElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EClass getSPortLAnalysis() {
		return sPortLAnalysisEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSPortLAnalysis_SBlockLAnalysis() {
		return (EReference) sPortLAnalysisEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSPortLAnalysis_SPort() {
		return (EReference) sPortLAnalysisEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSPortLAnalysis_Base_Port() {
		return (EReference) sPortLAnalysisEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EAttribute getSPortLAnalysis_SDirectionManual() {
		return (EAttribute) sPortLAnalysisEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EAttribute getSPortLAnalysis_SDirectionLAnalysis() {
		return (EAttribute) sPortLAnalysisEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EClass getAbstractSFailureModeOwner() {
		return abstractSFailureModeOwnerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getAbstractSFailureModeOwner_SFailureModesLAnalysisList() {
		return (EReference) abstractSFailureModeOwnerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EClass getAbstractSFailureModeLAnalysis() {
		return abstractSFailureModeLAnalysisEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getAbstractSFailureModeLAnalysis_SBlockLAnalysis() {
		return (EReference) abstractSFailureModeLAnalysisEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getAbstractSFailureModeLAnalysis_SFailureMode() {
		return (EReference) abstractSFailureModeLAnalysisEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getAbstractSFailureModeLAnalysis_SFearedEventsFamiliesList() {
		return (EReference) abstractSFailureModeLAnalysisEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getAbstractSFailureModeLAnalysis_SFearedEventsList() {
		return (EReference) abstractSFailureModeLAnalysisEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getAbstractSFailureModeLAnalysis_Owner() {
		return (EReference) abstractSFailureModeLAnalysisEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EClass getAbstractSPropagationElement() {
		return abstractSPropagationElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getAbstractSPropagationElement_OutSPropagationLinksList() {
		return (EReference) abstractSPropagationElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getAbstractSPropagationElement_InSPropagationLinksList() {
		return (EReference) abstractSPropagationElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EClass getSPropagationLink() {
		return sPropagationLinkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSPropagationLink_ElementTarget() {
		return (EReference) sPropagationLinkEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSPropagationLink_SBlockLAnalysis() {
		return (EReference) sPropagationLinkEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSPropagationLink_Base_Dependency() {
		return (EReference) sPropagationLinkEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSPropagationLink_ElementSource() {
		return (EReference) sPropagationLinkEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EClass getSBlockLAnalysis() {
		return sBlockLAnalysisEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSBlockLAnalysis_SBlock() {
		return (EReference) sBlockLAnalysisEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSBlockLAnalysis_SFailureEventsLAnalysisList() {
		return (EReference) sBlockLAnalysisEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSBlockLAnalysis_SPortsLAnalysisList() {
		return (EReference) sBlockLAnalysisEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSBlockLAnalysis_Base_Class() {
		return (EReference) sBlockLAnalysisEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSBlockLAnalysis_SLogicalGatesLAnalysisList() {
		return (EReference) sBlockLAnalysisEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSBlockLAnalysis_SFailureModesLAnalysisList() {
		return (EReference) sBlockLAnalysisEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSBlockLAnalysis_SBarriersLAnalysisList() {
		return (EReference) sBlockLAnalysisEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSBlockLAnalysis_SLocalAnalysis() {
		return (EReference) sBlockLAnalysisEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSBlockLAnalysis_SPropagationLinksList() {
		return (EReference) sBlockLAnalysisEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EClass getAbstractSFailureEventLAnalysis() {
		return abstractSFailureEventLAnalysisEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getAbstractSFailureEventLAnalysis_SFailureEvent() {
		return (EReference) abstractSFailureEventLAnalysisEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getAbstractSFailureEventLAnalysis_SBlockLAnalysis() {
		return (EReference) abstractSFailureEventLAnalysisEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EClass getAbstractSLogicalGateLAnalysis() {
		return abstractSLogicalGateLAnalysisEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getAbstractSLogicalGateLAnalysis_SBlockLAnalysis() {
		return (EReference) abstractSLogicalGateLAnalysisEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EClass getSBarrierLAnalysis() {
		return sBarrierLAnalysisEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSBarrierLAnalysis_SBarrier() {
		return (EReference) sBarrierLAnalysisEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSBarrierLAnalysis_Base_Property() {
		return (EReference) sBarrierLAnalysisEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSBarrierLAnalysis_SBlockLAnalysis() {
		return (EReference) sBarrierLAnalysisEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EClass getSLocalAnalysis() {
		return sLocalAnalysisEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSLocalAnalysis_Base_Package() {
		return (EReference) sLocalAnalysisEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSLocalAnalysis_SSystemEventsLibrary() {
		return (EReference) sLocalAnalysisEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSLocalAnalysis_SFearedEventsLibrary() {
		return (EReference) sLocalAnalysisEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSLocalAnalysis_SBlocksLAnalysisList() {
		return (EReference) sLocalAnalysisEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EClass getSSystemEventsLibrary() {
		return sSystemEventsLibraryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSSystemEventsLibrary_Base_Package() {
		return (EReference) sSystemEventsLibraryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSSystemEventsLibrary_SSystemEventTypesList() {
		return (EReference) sSystemEventsLibraryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSSystemEventsLibrary_SLocalAnalysis() {
		return (EReference) sSystemEventsLibraryEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EClass getSSystemEventType() {
		return sSystemEventTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSSystemEventType_Base_Class() {
		return (EReference) sSystemEventTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSSystemEventType_SFailureEvent() {
		return (EReference) sSystemEventTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSSystemEventType_InstancesList() {
		return (EReference) sSystemEventTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSSystemEventType_SSystemEventsLibrary() {
		return (EReference) sSystemEventTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EClass getSSystemEvent() {
		return sSystemEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSSystemEvent_Base_Property() {
		return (EReference) sSystemEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSSystemEvent_Type() {
		return (EReference) sSystemEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EClass getSFearedEventsLibrary() {
		return sFearedEventsLibraryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSFearedEventsLibrary_Base_Package() {
		return (EReference) sFearedEventsLibraryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSFearedEventsLibrary_SFearedEventsFamiliesList() {
		return (EReference) sFearedEventsLibraryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSFearedEventsLibrary_SFearedEventsList() {
		return (EReference) sFearedEventsLibraryEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSFearedEventsLibrary_SLocalAnalysis() {
		return (EReference) sFearedEventsLibraryEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EClass getSFearedEventsFamily() {
		return sFearedEventsFamilyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSFearedEventsFamily_Base_Class() {
		return (EReference) sFearedEventsFamilyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSFearedEventsFamily_SFearedEventsList() {
		return (EReference) sFearedEventsFamilyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EAttribute getSFearedEventsFamily_IsSelected() {
		return (EAttribute) sFearedEventsFamilyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSFearedEventsFamily_SubFamiliesList() {
		return (EReference) sFearedEventsFamilyEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSFearedEventsFamily_AllSFearedEventsList() {
		return (EReference) sFearedEventsFamilyEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSFearedEventsFamily_SFailureModesLAnalysisList() {
		return (EReference) sFearedEventsFamilyEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSFearedEventsFamily_SFearedEventsLibrary() {
		return (EReference) sFearedEventsFamilyEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EClass getSFearedEvent() {
		return sFearedEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EAttribute getSFearedEvent_IsSelected() {
		return (EAttribute) sFearedEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSFearedEvent_Base_Class() {
		return (EReference) sFearedEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSFearedEvent_SFailureModesLAnalysisList() {
		return (EReference) sFearedEventEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSFearedEvent_SFearedEventsLibrary() {
		return (EReference) sFearedEventEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EClass getSDysfunctionalAssociation() {
		return sDysfunctionalAssociationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSDysfunctionalAssociation_SFailureModeLAnalysis() {
		return (EReference) sDysfunctionalAssociationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSDysfunctionalAssociation_Base_Connector() {
		return (EReference) sDysfunctionalAssociationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSDysfunctionalAssociation_SFailureModeOwner() {
		return (EReference) sDysfunctionalAssociationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EClass getSAndGateLAnalysis() {
		return sAndGateLAnalysisEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSAndGateLAnalysis_Base_Property() {
		return (EReference) sAndGateLAnalysisEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EClass getSOrGateLAnalysis() {
		return sOrGateLAnalysisEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSOrGateLAnalysis_Base_Property() {
		return (EReference) sOrGateLAnalysisEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EClass getSUntimelyFailureMode() {
		return sUntimelyFailureModeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSUntimelyFailureMode_Base_Property() {
		return (EReference) sUntimelyFailureModeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EClass getSAbsentFailureMode() {
		return sAbsentFailureModeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSAbsentFailureMode_Base_Property() {
		return (EReference) sAbsentFailureModeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EClass getSErroneousFailureMode() {
		return sErroneousFailureModeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSErroneousFailureMode_Base_Property() {
		return (EReference) sErroneousFailureModeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EClass getSLocalEvent() {
		return sLocalEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSLocalEvent_Base_Property() {
		return (EReference) sLocalEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public IESFLocalAnalysisFactory getESFLocalAnalysisFactory() {
		return (IESFLocalAnalysisFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package. This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		abstractSLocalAnalysisElementEClass = createEClass(ABSTRACT_SLOCAL_ANALYSIS_ELEMENT);

		sPortLAnalysisEClass = createEClass(SPORT_LANALYSIS);
		createEReference(sPortLAnalysisEClass, SPORT_LANALYSIS__SBLOCK_LANALYSIS);
		createEReference(sPortLAnalysisEClass, SPORT_LANALYSIS__SPORT);
		createEReference(sPortLAnalysisEClass, SPORT_LANALYSIS__BASE_PORT);
		createEAttribute(sPortLAnalysisEClass, SPORT_LANALYSIS__SDIRECTION_MANUAL);
		createEAttribute(sPortLAnalysisEClass, SPORT_LANALYSIS__SDIRECTION_LANALYSIS);

		abstractSFailureModeOwnerEClass = createEClass(ABSTRACT_SFAILURE_MODE_OWNER);
		createEReference(abstractSFailureModeOwnerEClass, ABSTRACT_SFAILURE_MODE_OWNER__SFAILURE_MODES_LANALYSIS_LIST);

		abstractSFailureModeLAnalysisEClass = createEClass(ABSTRACT_SFAILURE_MODE_LANALYSIS);
		createEReference(abstractSFailureModeLAnalysisEClass, ABSTRACT_SFAILURE_MODE_LANALYSIS__SBLOCK_LANALYSIS);
		createEReference(abstractSFailureModeLAnalysisEClass, ABSTRACT_SFAILURE_MODE_LANALYSIS__SFAILURE_MODE);
		createEReference(abstractSFailureModeLAnalysisEClass, ABSTRACT_SFAILURE_MODE_LANALYSIS__SFEARED_EVENTS_FAMILIES_LIST);
		createEReference(abstractSFailureModeLAnalysisEClass, ABSTRACT_SFAILURE_MODE_LANALYSIS__SFEARED_EVENTS_LIST);
		createEReference(abstractSFailureModeLAnalysisEClass, ABSTRACT_SFAILURE_MODE_LANALYSIS__OWNER);

		abstractSPropagationElementEClass = createEClass(ABSTRACT_SPROPAGATION_ELEMENT);
		createEReference(abstractSPropagationElementEClass, ABSTRACT_SPROPAGATION_ELEMENT__OUT_SPROPAGATION_LINKS_LIST);
		createEReference(abstractSPropagationElementEClass, ABSTRACT_SPROPAGATION_ELEMENT__IN_SPROPAGATION_LINKS_LIST);

		sPropagationLinkEClass = createEClass(SPROPAGATION_LINK);
		createEReference(sPropagationLinkEClass, SPROPAGATION_LINK__ELEMENT_TARGET);
		createEReference(sPropagationLinkEClass, SPROPAGATION_LINK__SBLOCK_LANALYSIS);
		createEReference(sPropagationLinkEClass, SPROPAGATION_LINK__BASE_DEPENDENCY);
		createEReference(sPropagationLinkEClass, SPROPAGATION_LINK__ELEMENT_SOURCE);

		sBlockLAnalysisEClass = createEClass(SBLOCK_LANALYSIS);
		createEReference(sBlockLAnalysisEClass, SBLOCK_LANALYSIS__SBLOCK);
		createEReference(sBlockLAnalysisEClass, SBLOCK_LANALYSIS__SFAILURE_EVENTS_LANALYSIS_LIST);
		createEReference(sBlockLAnalysisEClass, SBLOCK_LANALYSIS__SPORTS_LANALYSIS_LIST);
		createEReference(sBlockLAnalysisEClass, SBLOCK_LANALYSIS__BASE_CLASS);
		createEReference(sBlockLAnalysisEClass, SBLOCK_LANALYSIS__SLOGICAL_GATES_LANALYSIS_LIST);
		createEReference(sBlockLAnalysisEClass, SBLOCK_LANALYSIS__SFAILURE_MODES_LANALYSIS_LIST);
		createEReference(sBlockLAnalysisEClass, SBLOCK_LANALYSIS__SBARRIERS_LANALYSIS_LIST);
		createEReference(sBlockLAnalysisEClass, SBLOCK_LANALYSIS__SLOCAL_ANALYSIS);
		createEReference(sBlockLAnalysisEClass, SBLOCK_LANALYSIS__SPROPAGATION_LINKS_LIST);

		abstractSFailureEventLAnalysisEClass = createEClass(ABSTRACT_SFAILURE_EVENT_LANALYSIS);
		createEReference(abstractSFailureEventLAnalysisEClass, ABSTRACT_SFAILURE_EVENT_LANALYSIS__SFAILURE_EVENT);
		createEReference(abstractSFailureEventLAnalysisEClass, ABSTRACT_SFAILURE_EVENT_LANALYSIS__SBLOCK_LANALYSIS);

		abstractSLogicalGateLAnalysisEClass = createEClass(ABSTRACT_SLOGICAL_GATE_LANALYSIS);
		createEReference(abstractSLogicalGateLAnalysisEClass, ABSTRACT_SLOGICAL_GATE_LANALYSIS__SBLOCK_LANALYSIS);

		sBarrierLAnalysisEClass = createEClass(SBARRIER_LANALYSIS);
		createEReference(sBarrierLAnalysisEClass, SBARRIER_LANALYSIS__SBARRIER);
		createEReference(sBarrierLAnalysisEClass, SBARRIER_LANALYSIS__BASE_PROPERTY);
		createEReference(sBarrierLAnalysisEClass, SBARRIER_LANALYSIS__SBLOCK_LANALYSIS);

		sLocalAnalysisEClass = createEClass(SLOCAL_ANALYSIS);
		createEReference(sLocalAnalysisEClass, SLOCAL_ANALYSIS__BASE_PACKAGE);
		createEReference(sLocalAnalysisEClass, SLOCAL_ANALYSIS__SSYSTEM_EVENTS_LIBRARY);
		createEReference(sLocalAnalysisEClass, SLOCAL_ANALYSIS__SFEARED_EVENTS_LIBRARY);
		createEReference(sLocalAnalysisEClass, SLOCAL_ANALYSIS__SBLOCKS_LANALYSIS_LIST);

		sSystemEventsLibraryEClass = createEClass(SSYSTEM_EVENTS_LIBRARY);
		createEReference(sSystemEventsLibraryEClass, SSYSTEM_EVENTS_LIBRARY__BASE_PACKAGE);
		createEReference(sSystemEventsLibraryEClass, SSYSTEM_EVENTS_LIBRARY__SSYSTEM_EVENT_TYPES_LIST);
		createEReference(sSystemEventsLibraryEClass, SSYSTEM_EVENTS_LIBRARY__SLOCAL_ANALYSIS);

		sSystemEventTypeEClass = createEClass(SSYSTEM_EVENT_TYPE);
		createEReference(sSystemEventTypeEClass, SSYSTEM_EVENT_TYPE__BASE_CLASS);
		createEReference(sSystemEventTypeEClass, SSYSTEM_EVENT_TYPE__SFAILURE_EVENT);
		createEReference(sSystemEventTypeEClass, SSYSTEM_EVENT_TYPE__INSTANCES_LIST);
		createEReference(sSystemEventTypeEClass, SSYSTEM_EVENT_TYPE__SSYSTEM_EVENTS_LIBRARY);

		sSystemEventEClass = createEClass(SSYSTEM_EVENT);
		createEReference(sSystemEventEClass, SSYSTEM_EVENT__BASE_PROPERTY);
		createEReference(sSystemEventEClass, SSYSTEM_EVENT__TYPE);

		sFearedEventsLibraryEClass = createEClass(SFEARED_EVENTS_LIBRARY);
		createEReference(sFearedEventsLibraryEClass, SFEARED_EVENTS_LIBRARY__BASE_PACKAGE);
		createEReference(sFearedEventsLibraryEClass, SFEARED_EVENTS_LIBRARY__SFEARED_EVENTS_FAMILIES_LIST);
		createEReference(sFearedEventsLibraryEClass, SFEARED_EVENTS_LIBRARY__SFEARED_EVENTS_LIST);
		createEReference(sFearedEventsLibraryEClass, SFEARED_EVENTS_LIBRARY__SLOCAL_ANALYSIS);

		sFearedEventsFamilyEClass = createEClass(SFEARED_EVENTS_FAMILY);
		createEReference(sFearedEventsFamilyEClass, SFEARED_EVENTS_FAMILY__BASE_CLASS);
		createEReference(sFearedEventsFamilyEClass, SFEARED_EVENTS_FAMILY__SFEARED_EVENTS_LIST);
		createEAttribute(sFearedEventsFamilyEClass, SFEARED_EVENTS_FAMILY__IS_SELECTED);
		createEReference(sFearedEventsFamilyEClass, SFEARED_EVENTS_FAMILY__SUB_FAMILIES_LIST);
		createEReference(sFearedEventsFamilyEClass, SFEARED_EVENTS_FAMILY__ALL_SFEARED_EVENTS_LIST);
		createEReference(sFearedEventsFamilyEClass, SFEARED_EVENTS_FAMILY__SFAILURE_MODES_LANALYSIS_LIST);
		createEReference(sFearedEventsFamilyEClass, SFEARED_EVENTS_FAMILY__SFEARED_EVENTS_LIBRARY);

		sFearedEventEClass = createEClass(SFEARED_EVENT);
		createEAttribute(sFearedEventEClass, SFEARED_EVENT__IS_SELECTED);
		createEReference(sFearedEventEClass, SFEARED_EVENT__BASE_CLASS);
		createEReference(sFearedEventEClass, SFEARED_EVENT__SFAILURE_MODES_LANALYSIS_LIST);
		createEReference(sFearedEventEClass, SFEARED_EVENT__SFEARED_EVENTS_LIBRARY);

		sDysfunctionalAssociationEClass = createEClass(SDYSFUNCTIONAL_ASSOCIATION);
		createEReference(sDysfunctionalAssociationEClass, SDYSFUNCTIONAL_ASSOCIATION__SFAILURE_MODE_LANALYSIS);
		createEReference(sDysfunctionalAssociationEClass, SDYSFUNCTIONAL_ASSOCIATION__BASE_CONNECTOR);
		createEReference(sDysfunctionalAssociationEClass, SDYSFUNCTIONAL_ASSOCIATION__SFAILURE_MODE_OWNER);

		sAndGateLAnalysisEClass = createEClass(SAND_GATE_LANALYSIS);
		createEReference(sAndGateLAnalysisEClass, SAND_GATE_LANALYSIS__BASE_PROPERTY);

		sOrGateLAnalysisEClass = createEClass(SOR_GATE_LANALYSIS);
		createEReference(sOrGateLAnalysisEClass, SOR_GATE_LANALYSIS__BASE_PROPERTY);

		sUntimelyFailureModeEClass = createEClass(SUNTIMELY_FAILURE_MODE);
		createEReference(sUntimelyFailureModeEClass, SUNTIMELY_FAILURE_MODE__BASE_PROPERTY);

		sAbsentFailureModeEClass = createEClass(SABSENT_FAILURE_MODE);
		createEReference(sAbsentFailureModeEClass, SABSENT_FAILURE_MODE__BASE_PROPERTY);

		sErroneousFailureModeEClass = createEClass(SERRONEOUS_FAILURE_MODE);
		createEReference(sErroneousFailureModeEClass, SERRONEOUS_FAILURE_MODE__BASE_PROPERTY);

		sLocalEventEClass = createEClass(SLOCAL_EVENT);
		createEReference(sLocalEventEClass, SLOCAL_EVENT__BASE_PROPERTY);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model. This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		IESFCorePackage theESFCorePackage = (IESFCorePackage) EPackage.Registry.INSTANCE.getEPackage(IESFCorePackage.eNS_URI);
		IESFArchitectureConceptsPackage theESFArchitectureConceptsPackage = (IESFArchitectureConceptsPackage) EPackage.Registry.INSTANCE.getEPackage(IESFArchitectureConceptsPackage.eNS_URI);
		UMLPackage theUMLPackage = (UMLPackage) EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);
		ISDysfunctionsPackage theSDysfunctionsPackage = (ISDysfunctionsPackage) EPackage.Registry.INSTANCE.getEPackage(ISDysfunctionsPackage.eNS_URI);
		ISRecommendationsPackage theSRecommendationsPackage = (ISRecommendationsPackage) EPackage.Registry.INSTANCE.getEPackage(ISRecommendationsPackage.eNS_URI);
		TypesPackage theTypesPackage = (TypesPackage) EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		abstractSLocalAnalysisElementEClass.getESuperTypes().add(theESFCorePackage.getAbstractSSafetyConcept());
		sPortLAnalysisEClass.getESuperTypes().add(this.getAbstractSFailureModeOwner());
		abstractSFailureModeOwnerEClass.getESuperTypes().add(this.getAbstractSLocalAnalysisElement());
		abstractSFailureModeLAnalysisEClass.getESuperTypes().add(this.getAbstractSPropagationElement());
		abstractSPropagationElementEClass.getESuperTypes().add(this.getAbstractSLocalAnalysisElement());
		sPropagationLinkEClass.getESuperTypes().add(this.getAbstractSLocalAnalysisElement());
		sBlockLAnalysisEClass.getESuperTypes().add(this.getAbstractSLocalAnalysisElement());
		abstractSFailureEventLAnalysisEClass.getESuperTypes().add(this.getAbstractSPropagationElement());
		abstractSLogicalGateLAnalysisEClass.getESuperTypes().add(this.getAbstractSPropagationElement());
		sBarrierLAnalysisEClass.getESuperTypes().add(this.getAbstractSFailureModeOwner());
		sLocalAnalysisEClass.getESuperTypes().add(theESFCorePackage.getAbstractSSafetyAnalysis());
		sSystemEventsLibraryEClass.getESuperTypes().add(this.getAbstractSLocalAnalysisElement());
		sSystemEventTypeEClass.getESuperTypes().add(this.getAbstractSLocalAnalysisElement());
		sSystemEventEClass.getESuperTypes().add(this.getAbstractSFailureEventLAnalysis());
		sFearedEventsLibraryEClass.getESuperTypes().add(this.getAbstractSLocalAnalysisElement());
		sFearedEventsFamilyEClass.getESuperTypes().add(this.getAbstractSLocalAnalysisElement());
		sFearedEventEClass.getESuperTypes().add(this.getAbstractSLocalAnalysisElement());
		sDysfunctionalAssociationEClass.getESuperTypes().add(this.getAbstractSLocalAnalysisElement());
		sAndGateLAnalysisEClass.getESuperTypes().add(this.getAbstractSLogicalGateLAnalysis());
		sOrGateLAnalysisEClass.getESuperTypes().add(this.getAbstractSLogicalGateLAnalysis());
		sUntimelyFailureModeEClass.getESuperTypes().add(this.getAbstractSFailureModeLAnalysis());
		sAbsentFailureModeEClass.getESuperTypes().add(this.getAbstractSFailureModeLAnalysis());
		sErroneousFailureModeEClass.getESuperTypes().add(this.getAbstractSFailureModeLAnalysis());
		sLocalEventEClass.getESuperTypes().add(this.getAbstractSFailureEventLAnalysis());

		// Initialize classes, features, and operations; add parameters
		initEClass(abstractSLocalAnalysisElementEClass, IAbstractSLocalAnalysisElement.class, "AbstractSLocalAnalysisElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(sPortLAnalysisEClass, ISPortLAnalysis.class, "SPortLAnalysis", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSPortLAnalysis_SBlockLAnalysis(), this.getSBlockLAnalysis(), this.getSBlockLAnalysis_SPortsLAnalysisList(), "sBlockLAnalysis", null, 1, 1, ISPortLAnalysis.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, //$NON-NLS-1$
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);
		initEReference(getSPortLAnalysis_SPort(), theESFArchitectureConceptsPackage.getSPort(), null, "sPort", null, 0, 1, ISPortLAnalysis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, //$NON-NLS-1$
				!IS_DERIVED, !IS_ORDERED);
		initEReference(getSPortLAnalysis_Base_Port(), theUMLPackage.getPort(), null, "base_Port", null, 1, 1, ISPortLAnalysis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEAttribute(getSPortLAnalysis_SDirectionManual(), theESFArchitectureConceptsPackage.getSDirection(), "sDirectionManual", "UNDEFINED", 0, 1, ISPortLAnalysis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, //$NON-NLS-1$ //$NON-NLS-2$
				!IS_DERIVED, !IS_ORDERED);
		initEAttribute(getSPortLAnalysis_SDirectionLAnalysis(), theESFArchitectureConceptsPackage.getSDirection(), "sDirectionLAnalysis", null, 1, 1, ISPortLAnalysis.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, //$NON-NLS-1$
				IS_DERIVED, !IS_ORDERED);

		initEClass(abstractSFailureModeOwnerEClass, IAbstractSFailureModeOwner.class, "AbstractSFailureModeOwner", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getAbstractSFailureModeOwner_SFailureModesLAnalysisList(), this.getAbstractSFailureModeLAnalysis(), this.getAbstractSFailureModeLAnalysis_Owner(), "sFailureModesLAnalysisList", null, 0, -1, IAbstractSFailureModeOwner.class, IS_TRANSIENT, //$NON-NLS-1$
				IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);

		initEClass(abstractSFailureModeLAnalysisEClass, IAbstractSFailureModeLAnalysis.class, "AbstractSFailureModeLAnalysis", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getAbstractSFailureModeLAnalysis_SBlockLAnalysis(), this.getSBlockLAnalysis(), this.getSBlockLAnalysis_SFailureModesLAnalysisList(), "sBlockLAnalysis", null, 1, 1, IAbstractSFailureModeLAnalysis.class, IS_TRANSIENT, IS_VOLATILE, //$NON-NLS-1$
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);
		initEReference(getAbstractSFailureModeLAnalysis_SFailureMode(), theSDysfunctionsPackage.getSFailureMode(), null, "sFailureMode", null, 0, 1, IAbstractSFailureModeLAnalysis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, //$NON-NLS-1$
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getAbstractSFailureModeLAnalysis_SFearedEventsFamiliesList(), this.getSFearedEventsFamily(), null, "sFearedEventsFamiliesList", null, 0, -1, IAbstractSFailureModeLAnalysis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, //$NON-NLS-1$
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getAbstractSFailureModeLAnalysis_SFearedEventsList(), this.getSFearedEvent(), null, "sFearedEventsList", null, 0, -1, IAbstractSFailureModeLAnalysis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, //$NON-NLS-1$
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getAbstractSFailureModeLAnalysis_Owner(), this.getAbstractSFailureModeOwner(), this.getAbstractSFailureModeOwner_SFailureModesLAnalysisList(), "owner", null, 1, 1, IAbstractSFailureModeLAnalysis.class, IS_TRANSIENT, IS_VOLATILE, //$NON-NLS-1$
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);

		initEClass(abstractSPropagationElementEClass, IAbstractSPropagationElement.class, "AbstractSPropagationElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getAbstractSPropagationElement_OutSPropagationLinksList(), this.getSPropagationLink(), this.getSPropagationLink_ElementSource(), "outSPropagationLinksList", null, 0, -1, IAbstractSPropagationElement.class, IS_TRANSIENT, IS_VOLATILE, //$NON-NLS-1$
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);
		initEReference(getAbstractSPropagationElement_InSPropagationLinksList(), this.getSPropagationLink(), this.getSPropagationLink_ElementTarget(), "inSPropagationLinksList", null, 0, -1, IAbstractSPropagationElement.class, IS_TRANSIENT, IS_VOLATILE, //$NON-NLS-1$
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);

		initEClass(sPropagationLinkEClass, ISPropagationLink.class, "SPropagationLink", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSPropagationLink_ElementTarget(), this.getAbstractSPropagationElement(), this.getAbstractSPropagationElement_InSPropagationLinksList(), "elementTarget", null, 1, 1, ISPropagationLink.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, //$NON-NLS-1$
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);
		initEReference(getSPropagationLink_SBlockLAnalysis(), this.getSBlockLAnalysis(), this.getSBlockLAnalysis_SPropagationLinksList(), "sBlockLAnalysis", null, 1, 1, ISPropagationLink.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, //$NON-NLS-1$
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);
		initEReference(getSPropagationLink_Base_Dependency(), theUMLPackage.getDependency(), null, "base_Dependency", null, 1, 1, ISPropagationLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, //$NON-NLS-1$
				!IS_DERIVED, !IS_ORDERED);
		initEReference(getSPropagationLink_ElementSource(), this.getAbstractSPropagationElement(), this.getAbstractSPropagationElement_OutSPropagationLinksList(), "elementSource", null, 1, 1, ISPropagationLink.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, //$NON-NLS-1$
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);

		initEClass(sBlockLAnalysisEClass, ISBlockLAnalysis.class, "SBlockLAnalysis", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSBlockLAnalysis_SBlock(), theESFArchitectureConceptsPackage.getSBlock(), null, "sBlock", null, 1, 1, ISBlockLAnalysis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, //$NON-NLS-1$
				!IS_DERIVED, !IS_ORDERED);
		initEReference(getSBlockLAnalysis_SFailureEventsLAnalysisList(), this.getAbstractSFailureEventLAnalysis(), this.getAbstractSFailureEventLAnalysis_SBlockLAnalysis(), "sFailureEventsLAnalysisList", null, 0, -1, ISBlockLAnalysis.class, IS_TRANSIENT, //$NON-NLS-1$
				IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);
		initEReference(getSBlockLAnalysis_SPortsLAnalysisList(), this.getSPortLAnalysis(), this.getSPortLAnalysis_SBlockLAnalysis(), "sPortsLAnalysisList", null, 0, -1, ISBlockLAnalysis.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, //$NON-NLS-1$
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);
		initEReference(getSBlockLAnalysis_Base_Class(), theUMLPackage.getClass_(), null, "base_Class", null, 1, 1, ISBlockLAnalysis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEReference(getSBlockLAnalysis_SLogicalGatesLAnalysisList(), this.getAbstractSLogicalGateLAnalysis(), this.getAbstractSLogicalGateLAnalysis_SBlockLAnalysis(), "sLogicalGatesLAnalysisList", null, 0, -1, ISBlockLAnalysis.class, IS_TRANSIENT, //$NON-NLS-1$
				IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);
		initEReference(getSBlockLAnalysis_SFailureModesLAnalysisList(), this.getAbstractSFailureModeLAnalysis(), this.getAbstractSFailureModeLAnalysis_SBlockLAnalysis(), "sFailureModesLAnalysisList", null, 0, -1, ISBlockLAnalysis.class, IS_TRANSIENT, //$NON-NLS-1$
				IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);
		initEReference(getSBlockLAnalysis_SBarriersLAnalysisList(), this.getSBarrierLAnalysis(), this.getSBarrierLAnalysis_SBlockLAnalysis(), "sBarriersLAnalysisList", null, 0, -1, ISBlockLAnalysis.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, //$NON-NLS-1$
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);
		initEReference(getSBlockLAnalysis_SLocalAnalysis(), this.getSLocalAnalysis(), this.getSLocalAnalysis_SBlocksLAnalysisList(), "sLocalAnalysis", null, 1, 1, ISBlockLAnalysis.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, //$NON-NLS-1$
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);
		initEReference(getSBlockLAnalysis_SPropagationLinksList(), this.getSPropagationLink(), this.getSPropagationLink_SBlockLAnalysis(), "sPropagationLinksList", null, 0, -1, ISBlockLAnalysis.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, //$NON-NLS-1$
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);

		initEClass(abstractSFailureEventLAnalysisEClass, IAbstractSFailureEventLAnalysis.class, "AbstractSFailureEventLAnalysis", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getAbstractSFailureEventLAnalysis_SFailureEvent(), theSDysfunctionsPackage.getSFailureEvent(), null, "sFailureEvent", null, 0, 1, IAbstractSFailureEventLAnalysis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, //$NON-NLS-1$
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getAbstractSFailureEventLAnalysis_SBlockLAnalysis(), this.getSBlockLAnalysis(), this.getSBlockLAnalysis_SFailureEventsLAnalysisList(), "sBlockLAnalysis", null, 1, 1, IAbstractSFailureEventLAnalysis.class, IS_TRANSIENT, IS_VOLATILE, //$NON-NLS-1$
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);

		initEClass(abstractSLogicalGateLAnalysisEClass, IAbstractSLogicalGateLAnalysis.class, "AbstractSLogicalGateLAnalysis", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getAbstractSLogicalGateLAnalysis_SBlockLAnalysis(), this.getSBlockLAnalysis(), this.getSBlockLAnalysis_SLogicalGatesLAnalysisList(), "sBlockLAnalysis", null, 1, 1, IAbstractSLogicalGateLAnalysis.class, IS_TRANSIENT, IS_VOLATILE, //$NON-NLS-1$
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);

		initEClass(sBarrierLAnalysisEClass, ISBarrierLAnalysis.class, "SBarrierLAnalysis", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSBarrierLAnalysis_SBarrier(), theSRecommendationsPackage.getSBarrier(), null, "sBarrier", null, 0, 1, ISBarrierLAnalysis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, //$NON-NLS-1$
				!IS_DERIVED, !IS_ORDERED);
		initEReference(getSBarrierLAnalysis_Base_Property(), theUMLPackage.getProperty(), null, "base_Property", null, 1, 1, ISBarrierLAnalysis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, //$NON-NLS-1$
				!IS_DERIVED, !IS_ORDERED);
		initEReference(getSBarrierLAnalysis_SBlockLAnalysis(), this.getSBlockLAnalysis(), this.getSBlockLAnalysis_SBarriersLAnalysisList(), "sBlockLAnalysis", null, 1, 1, ISBarrierLAnalysis.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, //$NON-NLS-1$
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);

		initEClass(sLocalAnalysisEClass, ISLocalAnalysis.class, "SLocalAnalysis", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSLocalAnalysis_Base_Package(), theUMLPackage.getPackage(), null, "base_Package", null, 1, 1, ISLocalAnalysis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEReference(getSLocalAnalysis_SSystemEventsLibrary(), this.getSSystemEventsLibrary(), this.getSSystemEventsLibrary_SLocalAnalysis(), "sSystemEventsLibrary", null, 0, 1, ISLocalAnalysis.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, //$NON-NLS-1$
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);
		initEReference(getSLocalAnalysis_SFearedEventsLibrary(), this.getSFearedEventsLibrary(), this.getSFearedEventsLibrary_SLocalAnalysis(), "sFearedEventsLibrary", null, 0, 1, ISLocalAnalysis.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, //$NON-NLS-1$
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);
		initEReference(getSLocalAnalysis_SBlocksLAnalysisList(), this.getSBlockLAnalysis(), this.getSBlockLAnalysis_SLocalAnalysis(), "sBlocksLAnalysisList", null, 0, -1, ISLocalAnalysis.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, //$NON-NLS-1$
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);

		initEClass(sSystemEventsLibraryEClass, ISSystemEventsLibrary.class, "SSystemEventsLibrary", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSSystemEventsLibrary_Base_Package(), theUMLPackage.getPackage(), null, "base_Package", null, 1, 1, ISSystemEventsLibrary.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, //$NON-NLS-1$
				!IS_DERIVED, !IS_ORDERED);
		initEReference(getSSystemEventsLibrary_SSystemEventTypesList(), this.getSSystemEventType(), this.getSSystemEventType_SSystemEventsLibrary(), "sSystemEventTypesList", null, 0, -1, ISSystemEventsLibrary.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, //$NON-NLS-1$
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);
		initEReference(getSSystemEventsLibrary_SLocalAnalysis(), this.getSLocalAnalysis(), this.getSLocalAnalysis_SSystemEventsLibrary(), "sLocalAnalysis", null, 1, 1, ISSystemEventsLibrary.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, //$NON-NLS-1$
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);

		initEClass(sSystemEventTypeEClass, ISSystemEventType.class, "SSystemEventType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSSystemEventType_Base_Class(), theUMLPackage.getClass_(), null, "base_Class", null, 1, 1, ISSystemEventType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEReference(getSSystemEventType_SFailureEvent(), theSDysfunctionsPackage.getSFailureEvent(), null, "sFailureEvent", null, 0, 1, ISSystemEventType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, //$NON-NLS-1$
				IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getSSystemEventType_InstancesList(), this.getSSystemEvent(), this.getSSystemEvent_Type(), "instancesList", null, 0, -1, ISSystemEventType.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, //$NON-NLS-1$
				IS_UNIQUE, IS_DERIVED, !IS_ORDERED);
		initEReference(getSSystemEventType_SSystemEventsLibrary(), this.getSSystemEventsLibrary(), this.getSSystemEventsLibrary_SSystemEventTypesList(), "sSystemEventsLibrary", null, 1, 1, ISSystemEventType.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, //$NON-NLS-1$
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);

		initEClass(sSystemEventEClass, ISSystemEvent.class, "SSystemEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSSystemEvent_Base_Property(), theUMLPackage.getProperty(), null, "base_Property", null, 1, 1, ISSystemEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEReference(getSSystemEvent_Type(), this.getSSystemEventType(), this.getSSystemEventType_InstancesList(), "type", null, 1, 1, ISSystemEvent.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, //$NON-NLS-1$
				IS_UNIQUE, IS_DERIVED, !IS_ORDERED);

		initEClass(sFearedEventsLibraryEClass, ISFearedEventsLibrary.class, "SFearedEventsLibrary", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSFearedEventsLibrary_Base_Package(), theUMLPackage.getPackage(), null, "base_Package", null, 1, 1, ISFearedEventsLibrary.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, //$NON-NLS-1$
				!IS_DERIVED, !IS_ORDERED);
		initEReference(getSFearedEventsLibrary_SFearedEventsFamiliesList(), this.getSFearedEventsFamily(), this.getSFearedEventsFamily_SFearedEventsLibrary(), "sFearedEventsFamiliesList", null, 0, -1, ISFearedEventsLibrary.class, IS_TRANSIENT, IS_VOLATILE, //$NON-NLS-1$
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);
		initEReference(getSFearedEventsLibrary_SFearedEventsList(), this.getSFearedEvent(), this.getSFearedEvent_SFearedEventsLibrary(), "sFearedEventsList", null, 0, -1, ISFearedEventsLibrary.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, //$NON-NLS-1$
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);
		initEReference(getSFearedEventsLibrary_SLocalAnalysis(), this.getSLocalAnalysis(), this.getSLocalAnalysis_SFearedEventsLibrary(), "sLocalAnalysis", null, 1, 1, ISFearedEventsLibrary.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, //$NON-NLS-1$
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);

		initEClass(sFearedEventsFamilyEClass, ISFearedEventsFamily.class, "SFearedEventsFamily", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSFearedEventsFamily_Base_Class(), theUMLPackage.getClass_(), null, "base_Class", null, 1, 1, ISFearedEventsFamily.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, //$NON-NLS-1$
				!IS_DERIVED, !IS_ORDERED);
		initEReference(getSFearedEventsFamily_SFearedEventsList(), this.getSFearedEvent(), null, "sFearedEventsList", null, 0, -1, ISFearedEventsFamily.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, //$NON-NLS-1$
				IS_UNIQUE, IS_DERIVED, !IS_ORDERED);
		initEAttribute(getSFearedEventsFamily_IsSelected(), theTypesPackage.getBoolean(), "isSelected", "false", 1, 1, ISFearedEventsFamily.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$ //$NON-NLS-2$
		initEReference(getSFearedEventsFamily_SubFamiliesList(), this.getSFearedEventsFamily(), null, "subFamiliesList", null, 0, -1, ISFearedEventsFamily.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, //$NON-NLS-1$
				IS_UNIQUE, IS_DERIVED, !IS_ORDERED);
		initEReference(getSFearedEventsFamily_AllSFearedEventsList(), this.getSFearedEvent(), null, "allSFearedEventsList", null, 0, -1, ISFearedEventsFamily.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, //$NON-NLS-1$
				IS_UNIQUE, IS_DERIVED, !IS_ORDERED);
		initEReference(getSFearedEventsFamily_SFailureModesLAnalysisList(), this.getAbstractSFailureModeLAnalysis(), null, "sFailureModesLAnalysisList", null, 0, -1, ISFearedEventsFamily.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, //$NON-NLS-1$
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);
		initEReference(getSFearedEventsFamily_SFearedEventsLibrary(), this.getSFearedEventsLibrary(), this.getSFearedEventsLibrary_SFearedEventsFamiliesList(), "sFearedEventsLibrary", null, 1, 1, ISFearedEventsFamily.class, IS_TRANSIENT, IS_VOLATILE, //$NON-NLS-1$
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);

		initEClass(sFearedEventEClass, ISFearedEvent.class, "SFearedEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getSFearedEvent_IsSelected(), theTypesPackage.getBoolean(), "isSelected", "false", 1, 1, ISFearedEvent.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$ //$NON-NLS-2$
		initEReference(getSFearedEvent_Base_Class(), theUMLPackage.getClass_(), null, "base_Class", null, 1, 1, ISFearedEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEReference(getSFearedEvent_SFailureModesLAnalysisList(), this.getAbstractSFailureModeLAnalysis(), null, "sFailureModesLAnalysisList", null, 0, -1, ISFearedEvent.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, //$NON-NLS-1$
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);
		initEReference(getSFearedEvent_SFearedEventsLibrary(), this.getSFearedEventsLibrary(), this.getSFearedEventsLibrary_SFearedEventsList(), "sFearedEventsLibrary", null, 1, 1, ISFearedEvent.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, //$NON-NLS-1$
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);

		initEClass(sDysfunctionalAssociationEClass, ISDysfunctionalAssociation.class, "SDysfunctionalAssociation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSDysfunctionalAssociation_SFailureModeLAnalysis(), this.getAbstractSFailureModeLAnalysis(), null, "sFailureModeLAnalysis", null, 1, 1, ISDysfunctionalAssociation.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, //$NON-NLS-1$
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);
		initEReference(getSDysfunctionalAssociation_Base_Connector(), theUMLPackage.getConnector(), null, "base_Connector", null, 1, 1, ISDysfunctionalAssociation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, //$NON-NLS-1$
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getSDysfunctionalAssociation_SFailureModeOwner(), this.getAbstractSFailureModeOwner(), null, "sFailureModeOwner", null, 1, 1, ISDysfunctionalAssociation.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, //$NON-NLS-1$
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);

		initEClass(sAndGateLAnalysisEClass, ISAndGateLAnalysis.class, "SAndGateLAnalysis", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSAndGateLAnalysis_Base_Property(), theUMLPackage.getProperty(), null, "base_Property", null, 1, 1, ISAndGateLAnalysis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, //$NON-NLS-1$
				!IS_DERIVED, !IS_ORDERED);

		initEClass(sOrGateLAnalysisEClass, ISOrGateLAnalysis.class, "SOrGateLAnalysis", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSOrGateLAnalysis_Base_Property(), theUMLPackage.getProperty(), null, "base_Property", null, 1, 1, ISOrGateLAnalysis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, //$NON-NLS-1$
				!IS_DERIVED, !IS_ORDERED);

		initEClass(sUntimelyFailureModeEClass, ISUntimelyFailureMode.class, "SUntimelyFailureMode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSUntimelyFailureMode_Base_Property(), theUMLPackage.getProperty(), null, "base_Property", null, 1, 1, ISUntimelyFailureMode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, //$NON-NLS-1$
				IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(sAbsentFailureModeEClass, ISAbsentFailureMode.class, "SAbsentFailureMode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSAbsentFailureMode_Base_Property(), theUMLPackage.getProperty(), null, "base_Property", null, 1, 1, ISAbsentFailureMode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, //$NON-NLS-1$
				!IS_DERIVED, !IS_ORDERED);

		initEClass(sErroneousFailureModeEClass, ISErroneousFailureMode.class, "SErroneousFailureMode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSErroneousFailureMode_Base_Property(), theUMLPackage.getProperty(), null, "base_Property", null, 1, 1, ISErroneousFailureMode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, //$NON-NLS-1$
				IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(sLocalEventEClass, ISLocalEvent.class, "SLocalEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSLocalEvent_Base_Property(), theUMLPackage.getProperty(), null, "base_Property", null, 1, 1, ISLocalEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/uml2/2.0.0/UML
		createUMLAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/uml2/2.0.0/UML</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void createUMLAnnotations() {
		String source = "http://www.eclipse.org/uml2/2.0.0/UML"; //$NON-NLS-1$
		addAnnotation(this,
				source,
				new String[] {
						"originalName", "ESFLocalAnalysis" //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(sDysfunctionalAssociationEClass,
				source,
				new String[] {
						"originalName", "SDysfunctionalAssociation " //$NON-NLS-1$ //$NON-NLS-2$
				});
	}

} // ESFLocalAnalysisPackage
