/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esflocalanalysis;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract SLogical Gate LAnalysis</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esflocalanalysis.IAbstractSLogicalGateLAnalysis#getSBlockLAnalysis <em>SBlock LAnalysis</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getAbstractSLogicalGateLAnalysis()
 * @model abstract="true"
 * @generated
 */
public interface IAbstractSLogicalGateLAnalysis
		extends IAbstractSPropagationElement {

	/**
	 * Returns the value of the '<em><b>SBlock LAnalysis</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSLogicalGatesLAnalysisList <em>SLogical Gates LAnalysis List</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SBlock LAnalysis</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SBlock LAnalysis</em>' reference.
	 * @see #setSBlockLAnalysis(ISBlockLAnalysis)
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getAbstractSLogicalGateLAnalysis_SBlockLAnalysis()
	 * @see org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSLogicalGatesLAnalysisList
	 * @model opposite="sLogicalGatesLAnalysisList" required="true" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	ISBlockLAnalysis getSBlockLAnalysis();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esflocalanalysis.IAbstractSLogicalGateLAnalysis#getSBlockLAnalysis <em>SBlock LAnalysis</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>SBlock LAnalysis</em>' reference.
	 * @see #getSBlockLAnalysis()
	 * @generated
	 */
	void setSBlockLAnalysis(ISBlockLAnalysis value);

} // IAbstractSLogicalGateLAnalysis
