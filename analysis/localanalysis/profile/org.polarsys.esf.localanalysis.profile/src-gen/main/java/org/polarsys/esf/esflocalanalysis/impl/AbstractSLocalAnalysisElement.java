/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esflocalanalysis.impl;

import org.eclipse.emf.ecore.EClass;

import org.polarsys.esf.esfcore.impl.AbstractSSafetyConcept;

import org.polarsys.esf.esflocalanalysis.IAbstractSLocalAnalysisElement;
import org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract SLocal Analysis Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class AbstractSLocalAnalysisElement
		extends AbstractSSafetyConcept
		implements IAbstractSLocalAnalysisElement {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected AbstractSLocalAnalysisElement() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IESFLocalAnalysisPackage.Literals.ABSTRACT_SLOCAL_ANALYSIS_ELEMENT;
	}

} // AbstractSLocalAnalysisElement
