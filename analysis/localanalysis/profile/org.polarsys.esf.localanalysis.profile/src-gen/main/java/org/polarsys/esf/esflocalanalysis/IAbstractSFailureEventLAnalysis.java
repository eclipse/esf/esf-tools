/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esflocalanalysis;

import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract SFailure Event LAnalysis</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esflocalanalysis.IAbstractSFailureEventLAnalysis#getSFailureEvent <em>SFailure Event</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.IAbstractSFailureEventLAnalysis#getSBlockLAnalysis <em>SBlock LAnalysis</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getAbstractSFailureEventLAnalysis()
 * @model abstract="true"
 * @generated
 */
public interface IAbstractSFailureEventLAnalysis
		extends IAbstractSPropagationElement {

	/**
	 * Returns the value of the '<em><b>SFailure Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SFailure Event</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SFailure Event</em>' reference.
	 * @see #setSFailureEvent(ISFailureEvent)
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getAbstractSFailureEventLAnalysis_SFailureEvent()
	 * @model ordered="false"
	 * @generated
	 */
	ISFailureEvent getSFailureEvent();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esflocalanalysis.IAbstractSFailureEventLAnalysis#getSFailureEvent <em>SFailure Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>SFailure Event</em>' reference.
	 * @see #getSFailureEvent()
	 * @generated
	 */
	void setSFailureEvent(ISFailureEvent value);

	/**
	 * Returns the value of the '<em><b>SBlock LAnalysis</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSFailureEventsLAnalysisList <em>SFailure Events LAnalysis List</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SBlock LAnalysis</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SBlock LAnalysis</em>' reference.
	 * @see #setSBlockLAnalysis(ISBlockLAnalysis)
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getAbstractSFailureEventLAnalysis_SBlockLAnalysis()
	 * @see org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSFailureEventsLAnalysisList
	 * @model opposite="sFailureEventsLAnalysisList" required="true" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	ISBlockLAnalysis getSBlockLAnalysis();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esflocalanalysis.IAbstractSFailureEventLAnalysis#getSBlockLAnalysis <em>SBlock LAnalysis</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>SBlock LAnalysis</em>' reference.
	 * @see #getSBlockLAnalysis()
	 * @generated
	 */
	void setSBlockLAnalysis(ISBlockLAnalysis value);

} // IAbstractSFailureEventLAnalysis
