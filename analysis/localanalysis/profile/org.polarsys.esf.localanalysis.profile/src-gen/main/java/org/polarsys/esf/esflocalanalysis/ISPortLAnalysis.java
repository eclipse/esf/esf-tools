/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esflocalanalysis;

import org.eclipse.uml2.uml.Port;

import org.polarsys.esf.esfarchitectureconcepts.ISPort;
import org.polarsys.esf.esfarchitectureconcepts.SDirection;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SPort LAnalysis</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISPortLAnalysis#getSBlockLAnalysis <em>SBlock LAnalysis</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISPortLAnalysis#getSPort <em>SPort</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISPortLAnalysis#getBase_Port <em>Base Port</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISPortLAnalysis#getSDirectionManual <em>SDirection Manual</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISPortLAnalysis#getSDirectionLAnalysis <em>SDirection LAnalysis</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSPortLAnalysis()
 * @model
 * @generated
 */
public interface ISPortLAnalysis
		extends IAbstractSFailureModeOwner {

	/**
	 * Returns the value of the '<em><b>SBlock LAnalysis</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSPortsLAnalysisList <em>SPorts LAnalysis List</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SBlock LAnalysis</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SBlock LAnalysis</em>' reference.
	 * @see #setSBlockLAnalysis(ISBlockLAnalysis)
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSPortLAnalysis_SBlockLAnalysis()
	 * @see org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSPortsLAnalysisList
	 * @model opposite="sPortsLAnalysisList" required="true" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	ISBlockLAnalysis getSBlockLAnalysis();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esflocalanalysis.ISPortLAnalysis#getSBlockLAnalysis <em>SBlock LAnalysis</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>SBlock LAnalysis</em>' reference.
	 * @see #getSBlockLAnalysis()
	 * @generated
	 */
	void setSBlockLAnalysis(ISBlockLAnalysis value);

	/**
	 * Returns the value of the '<em><b>SPort</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SPort</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SPort</em>' reference.
	 * @see #setSPort(ISPort)
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSPortLAnalysis_SPort()
	 * @model ordered="false"
	 * @generated
	 */
	ISPort getSPort();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esflocalanalysis.ISPortLAnalysis#getSPort <em>SPort</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>SPort</em>' reference.
	 * @see #getSPort()
	 * @generated
	 */
	void setSPort(ISPort value);

	/**
	 * Returns the value of the '<em><b>Base Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Port</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Base Port</em>' reference.
	 * @see #setBase_Port(Port)
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSPortLAnalysis_Base_Port()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Port getBase_Port();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esflocalanalysis.ISPortLAnalysis#getBase_Port <em>Base Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Base Port</em>' reference.
	 * @see #getBase_Port()
	 * @generated
	 */
	void setBase_Port(Port value);

	/**
	 * Returns the value of the '<em><b>SDirection Manual</b></em>' attribute.
	 * The default value is <code>"UNDEFINED"</code>.
	 * The literals are from the enumeration {@link org.polarsys.esf.esfarchitectureconcepts.SDirection}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SDirection Manual</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SDirection Manual</em>' attribute.
	 * @see org.polarsys.esf.esfarchitectureconcepts.SDirection
	 * @see #setSDirectionManual(SDirection)
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSPortLAnalysis_SDirectionManual()
	 * @model default="UNDEFINED" ordered="false"
	 * @generated
	 */
	SDirection getSDirectionManual();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esflocalanalysis.ISPortLAnalysis#getSDirectionManual <em>SDirection Manual</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>SDirection Manual</em>' attribute.
	 * @see org.polarsys.esf.esfarchitectureconcepts.SDirection
	 * @see #getSDirectionManual()
	 * @generated
	 */
	void setSDirectionManual(SDirection value);

	/**
	 * Returns the value of the '<em><b>SDirection LAnalysis</b></em>' attribute.
	 * The literals are from the enumeration {@link org.polarsys.esf.esfarchitectureconcepts.SDirection}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SDirection LAnalysis</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SDirection LAnalysis</em>' attribute.
	 * @see org.polarsys.esf.esfarchitectureconcepts.SDirection
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSPortLAnalysis_SDirectionLAnalysis()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	SDirection getSDirectionLAnalysis();

} // ISPortLAnalysis
