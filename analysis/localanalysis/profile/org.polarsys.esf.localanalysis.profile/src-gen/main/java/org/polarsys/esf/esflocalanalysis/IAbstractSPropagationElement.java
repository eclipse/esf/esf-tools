/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esflocalanalysis;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract SPropagation Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esflocalanalysis.IAbstractSPropagationElement#getOutSPropagationLinksList <em>Out SPropagation Links List</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.IAbstractSPropagationElement#getInSPropagationLinksList <em>In SPropagation Links List</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getAbstractSPropagationElement()
 * @model abstract="true"
 * @generated
 */
public interface IAbstractSPropagationElement
		extends IAbstractSLocalAnalysisElement {

	/**
	 * Returns the value of the '<em><b>Out SPropagation Links List</b></em>' reference list.
	 * The list contents are of type {@link org.polarsys.esf.esflocalanalysis.ISPropagationLink}.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esflocalanalysis.ISPropagationLink#getElementSource <em>Element Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Out SPropagation Links List</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Out SPropagation Links List</em>' reference list.
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getAbstractSPropagationElement_OutSPropagationLinksList()
	 * @see org.polarsys.esf.esflocalanalysis.ISPropagationLink#getElementSource
	 * @model opposite="elementSource" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<ISPropagationLink> getOutSPropagationLinksList();

	/**
	 * Returns the value of the '<em><b>In SPropagation Links List</b></em>' reference list.
	 * The list contents are of type {@link org.polarsys.esf.esflocalanalysis.ISPropagationLink}.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esflocalanalysis.ISPropagationLink#getElementTarget <em>Element Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>In SPropagation Links List</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>In SPropagation Links List</em>' reference list.
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getAbstractSPropagationElement_InSPropagationLinksList()
	 * @see org.polarsys.esf.esflocalanalysis.ISPropagationLink#getElementTarget
	 * @model opposite="elementTarget" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<ISPropagationLink> getInSPropagationLinksList();

} // IAbstractSPropagationElement
