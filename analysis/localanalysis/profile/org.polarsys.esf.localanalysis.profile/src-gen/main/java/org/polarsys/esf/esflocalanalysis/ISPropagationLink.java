/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esflocalanalysis;

import org.eclipse.uml2.uml.Dependency;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SPropagation Link</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISPropagationLink#getElementTarget <em>Element Target</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISPropagationLink#getSBlockLAnalysis <em>SBlock LAnalysis</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISPropagationLink#getBase_Dependency <em>Base Dependency</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISPropagationLink#getElementSource <em>Element Source</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSPropagationLink()
 * @model
 * @generated
 */
public interface ISPropagationLink
		extends IAbstractSLocalAnalysisElement {

	/**
	 * Returns the value of the '<em><b>Element Target</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esflocalanalysis.IAbstractSPropagationElement#getInSPropagationLinksList <em>In SPropagation Links List</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Element Target</em>' reference.
	 * @see #setElementTarget(IAbstractSPropagationElement)
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSPropagationLink_ElementTarget()
	 * @see org.polarsys.esf.esflocalanalysis.IAbstractSPropagationElement#getInSPropagationLinksList
	 * @model opposite="inSPropagationLinksList" required="true" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	IAbstractSPropagationElement getElementTarget();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esflocalanalysis.ISPropagationLink#getElementTarget <em>Element Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Element Target</em>' reference.
	 * @see #getElementTarget()
	 * @generated
	 */
	void setElementTarget(IAbstractSPropagationElement value);

	/**
	 * Returns the value of the '<em><b>SBlock LAnalysis</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSPropagationLinksList <em>SPropagation Links List</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SBlock LAnalysis</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SBlock LAnalysis</em>' reference.
	 * @see #setSBlockLAnalysis(ISBlockLAnalysis)
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSPropagationLink_SBlockLAnalysis()
	 * @see org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSPropagationLinksList
	 * @model opposite="sPropagationLinksList" required="true" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	ISBlockLAnalysis getSBlockLAnalysis();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esflocalanalysis.ISPropagationLink#getSBlockLAnalysis <em>SBlock LAnalysis</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>SBlock LAnalysis</em>' reference.
	 * @see #getSBlockLAnalysis()
	 * @generated
	 */
	void setSBlockLAnalysis(ISBlockLAnalysis value);

	/**
	 * Returns the value of the '<em><b>Base Dependency</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Dependency</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Base Dependency</em>' reference.
	 * @see #setBase_Dependency(Dependency)
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSPropagationLink_Base_Dependency()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Dependency getBase_Dependency();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esflocalanalysis.ISPropagationLink#getBase_Dependency <em>Base Dependency</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Base Dependency</em>' reference.
	 * @see #getBase_Dependency()
	 * @generated
	 */
	void setBase_Dependency(Dependency value);

	/**
	 * Returns the value of the '<em><b>Element Source</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esflocalanalysis.IAbstractSPropagationElement#getOutSPropagationLinksList <em>Out SPropagation Links List</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Element Source</em>' reference.
	 * @see #setElementSource(IAbstractSPropagationElement)
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSPropagationLink_ElementSource()
	 * @see org.polarsys.esf.esflocalanalysis.IAbstractSPropagationElement#getOutSPropagationLinksList
	 * @model opposite="outSPropagationLinksList" required="true" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	IAbstractSPropagationElement getElementSource();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esflocalanalysis.ISPropagationLink#getElementSource <em>Element Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Element Source</em>' reference.
	 * @see #getElementSource()
	 * @generated
	 */
	void setElementSource(IAbstractSPropagationElement value);

} // ISPropagationLink
