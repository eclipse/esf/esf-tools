/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esflocalanalysis.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.polarsys.esf.esfcore.impl.AbstractSSafetyAnalysis;

import org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage;
import org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis;
import org.polarsys.esf.esflocalanalysis.ISFearedEventsLibrary;
import org.polarsys.esf.esflocalanalysis.ISLocalAnalysis;
import org.polarsys.esf.esflocalanalysis.ISSystemEventsLibrary;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SLocal Analysis</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SLocalAnalysis#getBase_Package <em>Base Package</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SLocalAnalysis#getSSystemEventsLibrary <em>SSystem Events Library</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SLocalAnalysis#getSFearedEventsLibrary <em>SFeared Events Library</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SLocalAnalysis#getSBlocksLAnalysisList <em>SBlocks LAnalysis List</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SLocalAnalysis
		extends AbstractSSafetyAnalysis
		implements ISLocalAnalysis {

	/**
	 * The cached value of the '{@link #getBase_Package() <em>Base Package</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getBase_Package()
	 * @generated
	 * @ordered
	 */
	protected org.eclipse.uml2.uml.Package base_Package;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected SLocalAnalysis() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IESFLocalAnalysisPackage.Literals.SLOCAL_ANALYSIS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public org.eclipse.uml2.uml.Package getBase_Package() {
		if (base_Package != null && base_Package.eIsProxy()) {
			InternalEObject oldBase_Package = (InternalEObject) base_Package;
			base_Package = (org.eclipse.uml2.uml.Package) eResolveProxy(oldBase_Package);
			if (base_Package != oldBase_Package) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IESFLocalAnalysisPackage.SLOCAL_ANALYSIS__BASE_PACKAGE, oldBase_Package, base_Package));
			}
		}
		return base_Package;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public org.eclipse.uml2.uml.Package basicGetBase_Package() {
		return base_Package;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setBase_Package(org.eclipse.uml2.uml.Package newBase_Package) {
		org.eclipse.uml2.uml.Package oldBase_Package = base_Package;
		base_Package = newBase_Package;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFLocalAnalysisPackage.SLOCAL_ANALYSIS__BASE_PACKAGE, oldBase_Package, base_Package));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISSystemEventsLibrary getSSystemEventsLibrary() {
		ISSystemEventsLibrary sSystemEventsLibrary = basicGetSSystemEventsLibrary();
		return sSystemEventsLibrary != null && sSystemEventsLibrary.eIsProxy() ? (ISSystemEventsLibrary) eResolveProxy((InternalEObject) sSystemEventsLibrary) : sSystemEventsLibrary;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ISSystemEventsLibrary basicGetSSystemEventsLibrary() {
		// TODO: implement this method to return the 'SSystem Events Library' reference
		// -> do not perform proxy resolution
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setSSystemEventsLibrary(ISSystemEventsLibrary newSSystemEventsLibrary) {
		// TODO: implement this method to set the 'SSystem Events Library' reference
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISFearedEventsLibrary getSFearedEventsLibrary() {
		ISFearedEventsLibrary sFearedEventsLibrary = basicGetSFearedEventsLibrary();
		return sFearedEventsLibrary != null && sFearedEventsLibrary.eIsProxy() ? (ISFearedEventsLibrary) eResolveProxy((InternalEObject) sFearedEventsLibrary) : sFearedEventsLibrary;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ISFearedEventsLibrary basicGetSFearedEventsLibrary() {
		// TODO: implement this method to return the 'SFeared Events Library' reference
		// -> do not perform proxy resolution
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setSFearedEventsLibrary(ISFearedEventsLibrary newSFearedEventsLibrary) {
		// TODO: implement this method to set the 'SFeared Events Library' reference
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EList<ISBlockLAnalysis> getSBlocksLAnalysisList() {
		// TODO: implement this method to return the 'SBlocks LAnalysis List' reference list
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.SLOCAL_ANALYSIS__BASE_PACKAGE:
			if (resolve)
				return getBase_Package();
			return basicGetBase_Package();
		case IESFLocalAnalysisPackage.SLOCAL_ANALYSIS__SSYSTEM_EVENTS_LIBRARY:
			if (resolve)
				return getSSystemEventsLibrary();
			return basicGetSSystemEventsLibrary();
		case IESFLocalAnalysisPackage.SLOCAL_ANALYSIS__SFEARED_EVENTS_LIBRARY:
			if (resolve)
				return getSFearedEventsLibrary();
			return basicGetSFearedEventsLibrary();
		case IESFLocalAnalysisPackage.SLOCAL_ANALYSIS__SBLOCKS_LANALYSIS_LIST:
			return getSBlocksLAnalysisList();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.SLOCAL_ANALYSIS__BASE_PACKAGE:
			setBase_Package((org.eclipse.uml2.uml.Package) newValue);
			return;
		case IESFLocalAnalysisPackage.SLOCAL_ANALYSIS__SSYSTEM_EVENTS_LIBRARY:
			setSSystemEventsLibrary((ISSystemEventsLibrary) newValue);
			return;
		case IESFLocalAnalysisPackage.SLOCAL_ANALYSIS__SFEARED_EVENTS_LIBRARY:
			setSFearedEventsLibrary((ISFearedEventsLibrary) newValue);
			return;
		case IESFLocalAnalysisPackage.SLOCAL_ANALYSIS__SBLOCKS_LANALYSIS_LIST:
			getSBlocksLAnalysisList().clear();
			getSBlocksLAnalysisList().addAll((Collection<? extends ISBlockLAnalysis>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.SLOCAL_ANALYSIS__BASE_PACKAGE:
			setBase_Package((org.eclipse.uml2.uml.Package) null);
			return;
		case IESFLocalAnalysisPackage.SLOCAL_ANALYSIS__SSYSTEM_EVENTS_LIBRARY:
			setSSystemEventsLibrary((ISSystemEventsLibrary) null);
			return;
		case IESFLocalAnalysisPackage.SLOCAL_ANALYSIS__SFEARED_EVENTS_LIBRARY:
			setSFearedEventsLibrary((ISFearedEventsLibrary) null);
			return;
		case IESFLocalAnalysisPackage.SLOCAL_ANALYSIS__SBLOCKS_LANALYSIS_LIST:
			getSBlocksLAnalysisList().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.SLOCAL_ANALYSIS__BASE_PACKAGE:
			return base_Package != null;
		case IESFLocalAnalysisPackage.SLOCAL_ANALYSIS__SSYSTEM_EVENTS_LIBRARY:
			return basicGetSSystemEventsLibrary() != null;
		case IESFLocalAnalysisPackage.SLOCAL_ANALYSIS__SFEARED_EVENTS_LIBRARY:
			return basicGetSFearedEventsLibrary() != null;
		case IESFLocalAnalysisPackage.SLOCAL_ANALYSIS__SBLOCKS_LANALYSIS_LIST:
			return !getSBlocksLAnalysisList().isEmpty();
		}
		return super.eIsSet(featureID);
	}

} // SLocalAnalysis
