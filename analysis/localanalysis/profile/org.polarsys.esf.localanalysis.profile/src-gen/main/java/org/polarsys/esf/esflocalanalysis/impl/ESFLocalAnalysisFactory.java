/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esflocalanalysis.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.polarsys.esf.esflocalanalysis.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class ESFLocalAnalysisFactory
		extends EFactoryImpl
		implements IESFLocalAnalysisFactory {

	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static IESFLocalAnalysisFactory init() {
		try {
			IESFLocalAnalysisFactory theESFLocalAnalysisFactory = (IESFLocalAnalysisFactory) EPackage.Registry.INSTANCE.getEFactory(IESFLocalAnalysisPackage.eNS_URI);
			if (theESFLocalAnalysisFactory != null) {
				return theESFLocalAnalysisFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ESFLocalAnalysisFactory();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ESFLocalAnalysisFactory() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case IESFLocalAnalysisPackage.SPORT_LANALYSIS:
			return createSPortLAnalysis();
		case IESFLocalAnalysisPackage.SPROPAGATION_LINK:
			return createSPropagationLink();
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS:
			return createSBlockLAnalysis();
		case IESFLocalAnalysisPackage.SBARRIER_LANALYSIS:
			return createSBarrierLAnalysis();
		case IESFLocalAnalysisPackage.SLOCAL_ANALYSIS:
			return createSLocalAnalysis();
		case IESFLocalAnalysisPackage.SSYSTEM_EVENTS_LIBRARY:
			return createSSystemEventsLibrary();
		case IESFLocalAnalysisPackage.SSYSTEM_EVENT_TYPE:
			return createSSystemEventType();
		case IESFLocalAnalysisPackage.SSYSTEM_EVENT:
			return createSSystemEvent();
		case IESFLocalAnalysisPackage.SFEARED_EVENTS_LIBRARY:
			return createSFearedEventsLibrary();
		case IESFLocalAnalysisPackage.SFEARED_EVENTS_FAMILY:
			return createSFearedEventsFamily();
		case IESFLocalAnalysisPackage.SFEARED_EVENT:
			return createSFearedEvent();
		case IESFLocalAnalysisPackage.SDYSFUNCTIONAL_ASSOCIATION:
			return createSDysfunctionalAssociation();
		case IESFLocalAnalysisPackage.SAND_GATE_LANALYSIS:
			return createSAndGateLAnalysis();
		case IESFLocalAnalysisPackage.SOR_GATE_LANALYSIS:
			return createSOrGateLAnalysis();
		case IESFLocalAnalysisPackage.SUNTIMELY_FAILURE_MODE:
			return createSUntimelyFailureMode();
		case IESFLocalAnalysisPackage.SABSENT_FAILURE_MODE:
			return createSAbsentFailureMode();
		case IESFLocalAnalysisPackage.SERRONEOUS_FAILURE_MODE:
			return createSErroneousFailureMode();
		case IESFLocalAnalysisPackage.SLOCAL_EVENT:
			return createSLocalEvent();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier"); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISPortLAnalysis createSPortLAnalysis() {
		SPortLAnalysis sPortLAnalysis = new SPortLAnalysis();
		return sPortLAnalysis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISPropagationLink createSPropagationLink() {
		SPropagationLink sPropagationLink = new SPropagationLink();
		return sPropagationLink;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISBlockLAnalysis createSBlockLAnalysis() {
		SBlockLAnalysis sBlockLAnalysis = new SBlockLAnalysis();
		return sBlockLAnalysis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISBarrierLAnalysis createSBarrierLAnalysis() {
		SBarrierLAnalysis sBarrierLAnalysis = new SBarrierLAnalysis();
		return sBarrierLAnalysis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISLocalAnalysis createSLocalAnalysis() {
		SLocalAnalysis sLocalAnalysis = new SLocalAnalysis();
		return sLocalAnalysis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISSystemEventsLibrary createSSystemEventsLibrary() {
		SSystemEventsLibrary sSystemEventsLibrary = new SSystemEventsLibrary();
		return sSystemEventsLibrary;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISSystemEventType createSSystemEventType() {
		SSystemEventType sSystemEventType = new SSystemEventType();
		return sSystemEventType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISSystemEvent createSSystemEvent() {
		SSystemEvent sSystemEvent = new SSystemEvent();
		return sSystemEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISFearedEventsLibrary createSFearedEventsLibrary() {
		SFearedEventsLibrary sFearedEventsLibrary = new SFearedEventsLibrary();
		return sFearedEventsLibrary;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISFearedEventsFamily createSFearedEventsFamily() {
		SFearedEventsFamily sFearedEventsFamily = new SFearedEventsFamily();
		return sFearedEventsFamily;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISFearedEvent createSFearedEvent() {
		SFearedEvent sFearedEvent = new SFearedEvent();
		return sFearedEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISDysfunctionalAssociation createSDysfunctionalAssociation() {
		SDysfunctionalAssociation sDysfunctionalAssociation = new SDysfunctionalAssociation();
		return sDysfunctionalAssociation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISAndGateLAnalysis createSAndGateLAnalysis() {
		SAndGateLAnalysis sAndGateLAnalysis = new SAndGateLAnalysis();
		return sAndGateLAnalysis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISOrGateLAnalysis createSOrGateLAnalysis() {
		SOrGateLAnalysis sOrGateLAnalysis = new SOrGateLAnalysis();
		return sOrGateLAnalysis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISUntimelyFailureMode createSUntimelyFailureMode() {
		SUntimelyFailureMode sUntimelyFailureMode = new SUntimelyFailureMode();
		return sUntimelyFailureMode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISAbsentFailureMode createSAbsentFailureMode() {
		SAbsentFailureMode sAbsentFailureMode = new SAbsentFailureMode();
		return sAbsentFailureMode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISErroneousFailureMode createSErroneousFailureMode() {
		SErroneousFailureMode sErroneousFailureMode = new SErroneousFailureMode();
		return sErroneousFailureMode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISLocalEvent createSLocalEvent() {
		SLocalEvent sLocalEvent = new SLocalEvent();
		return sLocalEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public IESFLocalAnalysisPackage getESFLocalAnalysisPackage() {
		return (IESFLocalAnalysisPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static IESFLocalAnalysisPackage getPackage() {
		return IESFLocalAnalysisPackage.eINSTANCE;
	}

} // ESFLocalAnalysisFactory
