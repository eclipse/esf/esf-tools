/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esflocalanalysis.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import org.polarsys.esf.esfcore.IAbstractSElement;
import org.polarsys.esf.esfcore.IAbstractSSafetyAnalysis;
import org.polarsys.esf.esfcore.IAbstractSSafetyConcept;

import org.polarsys.esf.esflocalanalysis.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * 
 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage
 * @generated
 */
public class ESFLocalAnalysisAdapterFactory
		extends AdapterFactoryImpl {

	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected static IESFLocalAnalysisPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ESFLocalAnalysisAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = IESFLocalAnalysisPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance
	 * object of the model.
	 * <!-- end-user-doc -->
	 * 
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ESFLocalAnalysisSwitch<Adapter> modelSwitch = new ESFLocalAnalysisSwitch<Adapter>() {
		@Override
		public Adapter caseAbstractSLocalAnalysisElement(IAbstractSLocalAnalysisElement object) {
			return createAbstractSLocalAnalysisElementAdapter();
		}

		@Override
		public Adapter caseSPortLAnalysis(ISPortLAnalysis object) {
			return createSPortLAnalysisAdapter();
		}

		@Override
		public Adapter caseAbstractSFailureModeOwner(IAbstractSFailureModeOwner object) {
			return createAbstractSFailureModeOwnerAdapter();
		}

		@Override
		public Adapter caseAbstractSFailureModeLAnalysis(IAbstractSFailureModeLAnalysis object) {
			return createAbstractSFailureModeLAnalysisAdapter();
		}

		@Override
		public Adapter caseAbstractSPropagationElement(IAbstractSPropagationElement object) {
			return createAbstractSPropagationElementAdapter();
		}

		@Override
		public Adapter caseSPropagationLink(ISPropagationLink object) {
			return createSPropagationLinkAdapter();
		}

		@Override
		public Adapter caseSBlockLAnalysis(ISBlockLAnalysis object) {
			return createSBlockLAnalysisAdapter();
		}

		@Override
		public Adapter caseAbstractSFailureEventLAnalysis(IAbstractSFailureEventLAnalysis object) {
			return createAbstractSFailureEventLAnalysisAdapter();
		}

		@Override
		public Adapter caseAbstractSLogicalGateLAnalysis(IAbstractSLogicalGateLAnalysis object) {
			return createAbstractSLogicalGateLAnalysisAdapter();
		}

		@Override
		public Adapter caseSBarrierLAnalysis(ISBarrierLAnalysis object) {
			return createSBarrierLAnalysisAdapter();
		}

		@Override
		public Adapter caseSLocalAnalysis(ISLocalAnalysis object) {
			return createSLocalAnalysisAdapter();
		}

		@Override
		public Adapter caseSSystemEventsLibrary(ISSystemEventsLibrary object) {
			return createSSystemEventsLibraryAdapter();
		}

		@Override
		public Adapter caseSSystemEventType(ISSystemEventType object) {
			return createSSystemEventTypeAdapter();
		}

		@Override
		public Adapter caseSSystemEvent(ISSystemEvent object) {
			return createSSystemEventAdapter();
		}

		@Override
		public Adapter caseSFearedEventsLibrary(ISFearedEventsLibrary object) {
			return createSFearedEventsLibraryAdapter();
		}

		@Override
		public Adapter caseSFearedEventsFamily(ISFearedEventsFamily object) {
			return createSFearedEventsFamilyAdapter();
		}

		@Override
		public Adapter caseSFearedEvent(ISFearedEvent object) {
			return createSFearedEventAdapter();
		}

		@Override
		public Adapter caseSDysfunctionalAssociation(ISDysfunctionalAssociation object) {
			return createSDysfunctionalAssociationAdapter();
		}

		@Override
		public Adapter caseSAndGateLAnalysis(ISAndGateLAnalysis object) {
			return createSAndGateLAnalysisAdapter();
		}

		@Override
		public Adapter caseSOrGateLAnalysis(ISOrGateLAnalysis object) {
			return createSOrGateLAnalysisAdapter();
		}

		@Override
		public Adapter caseSUntimelyFailureMode(ISUntimelyFailureMode object) {
			return createSUntimelyFailureModeAdapter();
		}

		@Override
		public Adapter caseSAbsentFailureMode(ISAbsentFailureMode object) {
			return createSAbsentFailureModeAdapter();
		}

		@Override
		public Adapter caseSErroneousFailureMode(ISErroneousFailureMode object) {
			return createSErroneousFailureModeAdapter();
		}

		@Override
		public Adapter caseSLocalEvent(ISLocalEvent object) {
			return createSLocalEventAdapter();
		}

		@Override
		public Adapter caseAbstractSElement(IAbstractSElement object) {
			return createAbstractSElementAdapter();
		}

		@Override
		public Adapter caseAbstractSSafetyConcept(IAbstractSSafetyConcept object) {
			return createAbstractSSafetyConceptAdapter();
		}

		@Override
		public Adapter caseAbstractSSafetyAnalysis(IAbstractSSafetyAnalysis object) {
			return createAbstractSSafetyAnalysisAdapter();
		}

		@Override
		public Adapter defaultCase(EObject object) {
			return createEObjectAdapter();
		}
	};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param target
	 *            the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polarsys.esf.esflocalanalysis.IAbstractSLocalAnalysisElement <em>Abstract SLocal Analysis Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.polarsys.esf.esflocalanalysis.IAbstractSLocalAnalysisElement
	 * @generated
	 */
	public Adapter createAbstractSLocalAnalysisElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polarsys.esf.esflocalanalysis.ISPortLAnalysis <em>SPort LAnalysis</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.polarsys.esf.esflocalanalysis.ISPortLAnalysis
	 * @generated
	 */
	public Adapter createSPortLAnalysisAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeOwner <em>Abstract SFailure Mode Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeOwner
	 * @generated
	 */
	public Adapter createAbstractSFailureModeOwnerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis <em>Abstract SFailure Mode LAnalysis</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis
	 * @generated
	 */
	public Adapter createAbstractSFailureModeLAnalysisAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polarsys.esf.esflocalanalysis.IAbstractSPropagationElement <em>Abstract SPropagation Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.polarsys.esf.esflocalanalysis.IAbstractSPropagationElement
	 * @generated
	 */
	public Adapter createAbstractSPropagationElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polarsys.esf.esflocalanalysis.ISPropagationLink <em>SPropagation Link</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.polarsys.esf.esflocalanalysis.ISPropagationLink
	 * @generated
	 */
	public Adapter createSPropagationLinkAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis <em>SBlock LAnalysis</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis
	 * @generated
	 */
	public Adapter createSBlockLAnalysisAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polarsys.esf.esflocalanalysis.IAbstractSFailureEventLAnalysis <em>Abstract SFailure Event LAnalysis</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.polarsys.esf.esflocalanalysis.IAbstractSFailureEventLAnalysis
	 * @generated
	 */
	public Adapter createAbstractSFailureEventLAnalysisAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polarsys.esf.esflocalanalysis.IAbstractSLogicalGateLAnalysis <em>Abstract SLogical Gate LAnalysis</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.polarsys.esf.esflocalanalysis.IAbstractSLogicalGateLAnalysis
	 * @generated
	 */
	public Adapter createAbstractSLogicalGateLAnalysisAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polarsys.esf.esflocalanalysis.ISBarrierLAnalysis <em>SBarrier LAnalysis</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.polarsys.esf.esflocalanalysis.ISBarrierLAnalysis
	 * @generated
	 */
	public Adapter createSBarrierLAnalysisAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polarsys.esf.esflocalanalysis.ISLocalAnalysis <em>SLocal Analysis</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.polarsys.esf.esflocalanalysis.ISLocalAnalysis
	 * @generated
	 */
	public Adapter createSLocalAnalysisAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polarsys.esf.esflocalanalysis.ISSystemEventsLibrary <em>SSystem Events Library</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.polarsys.esf.esflocalanalysis.ISSystemEventsLibrary
	 * @generated
	 */
	public Adapter createSSystemEventsLibraryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polarsys.esf.esflocalanalysis.ISSystemEventType <em>SSystem Event Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.polarsys.esf.esflocalanalysis.ISSystemEventType
	 * @generated
	 */
	public Adapter createSSystemEventTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polarsys.esf.esflocalanalysis.ISSystemEvent <em>SSystem Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.polarsys.esf.esflocalanalysis.ISSystemEvent
	 * @generated
	 */
	public Adapter createSSystemEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polarsys.esf.esflocalanalysis.ISFearedEventsLibrary <em>SFeared Events Library</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.polarsys.esf.esflocalanalysis.ISFearedEventsLibrary
	 * @generated
	 */
	public Adapter createSFearedEventsLibraryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily <em>SFeared Events Family</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily
	 * @generated
	 */
	public Adapter createSFearedEventsFamilyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polarsys.esf.esflocalanalysis.ISFearedEvent <em>SFeared Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.polarsys.esf.esflocalanalysis.ISFearedEvent
	 * @generated
	 */
	public Adapter createSFearedEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polarsys.esf.esflocalanalysis.ISDysfunctionalAssociation <em>SDysfunctional Association</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.polarsys.esf.esflocalanalysis.ISDysfunctionalAssociation
	 * @generated
	 */
	public Adapter createSDysfunctionalAssociationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polarsys.esf.esflocalanalysis.ISAndGateLAnalysis <em>SAnd Gate LAnalysis</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.polarsys.esf.esflocalanalysis.ISAndGateLAnalysis
	 * @generated
	 */
	public Adapter createSAndGateLAnalysisAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polarsys.esf.esflocalanalysis.ISOrGateLAnalysis <em>SOr Gate LAnalysis</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.polarsys.esf.esflocalanalysis.ISOrGateLAnalysis
	 * @generated
	 */
	public Adapter createSOrGateLAnalysisAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polarsys.esf.esflocalanalysis.ISUntimelyFailureMode <em>SUntimely Failure Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.polarsys.esf.esflocalanalysis.ISUntimelyFailureMode
	 * @generated
	 */
	public Adapter createSUntimelyFailureModeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polarsys.esf.esflocalanalysis.ISAbsentFailureMode <em>SAbsent Failure Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.polarsys.esf.esflocalanalysis.ISAbsentFailureMode
	 * @generated
	 */
	public Adapter createSAbsentFailureModeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polarsys.esf.esflocalanalysis.ISErroneousFailureMode <em>SErroneous Failure Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.polarsys.esf.esflocalanalysis.ISErroneousFailureMode
	 * @generated
	 */
	public Adapter createSErroneousFailureModeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polarsys.esf.esflocalanalysis.ISLocalEvent <em>SLocal Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.polarsys.esf.esflocalanalysis.ISLocalEvent
	 * @generated
	 */
	public Adapter createSLocalEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polarsys.esf.esfcore.IAbstractSElement <em>Abstract SElement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.polarsys.esf.esfcore.IAbstractSElement
	 * @generated
	 */
	public Adapter createAbstractSElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polarsys.esf.esfcore.IAbstractSSafetyConcept <em>Abstract SSafety Concept</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.polarsys.esf.esfcore.IAbstractSSafetyConcept
	 * @generated
	 */
	public Adapter createAbstractSSafetyConceptAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polarsys.esf.esfcore.IAbstractSSafetyAnalysis <em>Abstract SSafety Analysis</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.polarsys.esf.esfcore.IAbstractSSafetyAnalysis
	 * @generated
	 */
	public Adapter createAbstractSSafetyAnalysisAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} // ESFLocalAnalysisAdapterFactory
