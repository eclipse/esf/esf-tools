/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esflocalanalysis;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract SFailure Mode Owner</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeOwner#getSFailureModesLAnalysisList <em>SFailure Modes LAnalysis List</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getAbstractSFailureModeOwner()
 * @model abstract="true"
 * @generated
 */
public interface IAbstractSFailureModeOwner
		extends IAbstractSLocalAnalysisElement {

	/**
	 * Returns the value of the '<em><b>SFailure Modes LAnalysis List</b></em>' reference list.
	 * The list contents are of type {@link org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis}.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SFailure Modes LAnalysis List</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SFailure Modes LAnalysis List</em>' reference list.
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getAbstractSFailureModeOwner_SFailureModesLAnalysisList()
	 * @see org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis#getOwner
	 * @model opposite="owner" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<IAbstractSFailureModeLAnalysis> getSFailureModesLAnalysisList();

} // IAbstractSFailureModeOwner
