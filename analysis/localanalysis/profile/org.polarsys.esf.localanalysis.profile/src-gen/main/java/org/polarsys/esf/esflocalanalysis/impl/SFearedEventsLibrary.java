/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esflocalanalysis.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage;
import org.polarsys.esf.esflocalanalysis.ISFearedEvent;
import org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily;
import org.polarsys.esf.esflocalanalysis.ISFearedEventsLibrary;
import org.polarsys.esf.esflocalanalysis.ISLocalAnalysis;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SFeared Events Library</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SFearedEventsLibrary#getBase_Package <em>Base Package</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SFearedEventsLibrary#getSFearedEventsFamiliesList <em>SFeared Events Families List</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SFearedEventsLibrary#getSFearedEventsList <em>SFeared Events List</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SFearedEventsLibrary#getSLocalAnalysis <em>SLocal Analysis</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SFearedEventsLibrary
		extends AbstractSLocalAnalysisElement
		implements ISFearedEventsLibrary {

	/**
	 * The cached value of the '{@link #getBase_Package() <em>Base Package</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getBase_Package()
	 * @generated
	 * @ordered
	 */
	protected org.eclipse.uml2.uml.Package base_Package;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected SFearedEventsLibrary() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IESFLocalAnalysisPackage.Literals.SFEARED_EVENTS_LIBRARY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public org.eclipse.uml2.uml.Package getBase_Package() {
		if (base_Package != null && base_Package.eIsProxy()) {
			InternalEObject oldBase_Package = (InternalEObject) base_Package;
			base_Package = (org.eclipse.uml2.uml.Package) eResolveProxy(oldBase_Package);
			if (base_Package != oldBase_Package) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IESFLocalAnalysisPackage.SFEARED_EVENTS_LIBRARY__BASE_PACKAGE, oldBase_Package, base_Package));
			}
		}
		return base_Package;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public org.eclipse.uml2.uml.Package basicGetBase_Package() {
		return base_Package;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setBase_Package(org.eclipse.uml2.uml.Package newBase_Package) {
		org.eclipse.uml2.uml.Package oldBase_Package = base_Package;
		base_Package = newBase_Package;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFLocalAnalysisPackage.SFEARED_EVENTS_LIBRARY__BASE_PACKAGE, oldBase_Package, base_Package));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EList<ISFearedEventsFamily> getSFearedEventsFamiliesList() {
		// TODO: implement this method to return the 'SFeared Events Families List' reference list
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EList<ISFearedEvent> getSFearedEventsList() {
		// TODO: implement this method to return the 'SFeared Events List' reference list
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISLocalAnalysis getSLocalAnalysis() {
		ISLocalAnalysis sLocalAnalysis = basicGetSLocalAnalysis();
		return sLocalAnalysis != null && sLocalAnalysis.eIsProxy() ? (ISLocalAnalysis) eResolveProxy((InternalEObject) sLocalAnalysis) : sLocalAnalysis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ISLocalAnalysis basicGetSLocalAnalysis() {
		// TODO: implement this method to return the 'SLocal Analysis' reference
		// -> do not perform proxy resolution
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setSLocalAnalysis(ISLocalAnalysis newSLocalAnalysis) {
		// TODO: implement this method to set the 'SLocal Analysis' reference
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.SFEARED_EVENTS_LIBRARY__BASE_PACKAGE:
			if (resolve)
				return getBase_Package();
			return basicGetBase_Package();
		case IESFLocalAnalysisPackage.SFEARED_EVENTS_LIBRARY__SFEARED_EVENTS_FAMILIES_LIST:
			return getSFearedEventsFamiliesList();
		case IESFLocalAnalysisPackage.SFEARED_EVENTS_LIBRARY__SFEARED_EVENTS_LIST:
			return getSFearedEventsList();
		case IESFLocalAnalysisPackage.SFEARED_EVENTS_LIBRARY__SLOCAL_ANALYSIS:
			if (resolve)
				return getSLocalAnalysis();
			return basicGetSLocalAnalysis();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.SFEARED_EVENTS_LIBRARY__BASE_PACKAGE:
			setBase_Package((org.eclipse.uml2.uml.Package) newValue);
			return;
		case IESFLocalAnalysisPackage.SFEARED_EVENTS_LIBRARY__SFEARED_EVENTS_FAMILIES_LIST:
			getSFearedEventsFamiliesList().clear();
			getSFearedEventsFamiliesList().addAll((Collection<? extends ISFearedEventsFamily>) newValue);
			return;
		case IESFLocalAnalysisPackage.SFEARED_EVENTS_LIBRARY__SFEARED_EVENTS_LIST:
			getSFearedEventsList().clear();
			getSFearedEventsList().addAll((Collection<? extends ISFearedEvent>) newValue);
			return;
		case IESFLocalAnalysisPackage.SFEARED_EVENTS_LIBRARY__SLOCAL_ANALYSIS:
			setSLocalAnalysis((ISLocalAnalysis) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.SFEARED_EVENTS_LIBRARY__BASE_PACKAGE:
			setBase_Package((org.eclipse.uml2.uml.Package) null);
			return;
		case IESFLocalAnalysisPackage.SFEARED_EVENTS_LIBRARY__SFEARED_EVENTS_FAMILIES_LIST:
			getSFearedEventsFamiliesList().clear();
			return;
		case IESFLocalAnalysisPackage.SFEARED_EVENTS_LIBRARY__SFEARED_EVENTS_LIST:
			getSFearedEventsList().clear();
			return;
		case IESFLocalAnalysisPackage.SFEARED_EVENTS_LIBRARY__SLOCAL_ANALYSIS:
			setSLocalAnalysis((ISLocalAnalysis) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.SFEARED_EVENTS_LIBRARY__BASE_PACKAGE:
			return base_Package != null;
		case IESFLocalAnalysisPackage.SFEARED_EVENTS_LIBRARY__SFEARED_EVENTS_FAMILIES_LIST:
			return !getSFearedEventsFamiliesList().isEmpty();
		case IESFLocalAnalysisPackage.SFEARED_EVENTS_LIBRARY__SFEARED_EVENTS_LIST:
			return !getSFearedEventsList().isEmpty();
		case IESFLocalAnalysisPackage.SFEARED_EVENTS_LIBRARY__SLOCAL_ANALYSIS:
			return basicGetSLocalAnalysis() != null;
		}
		return super.eIsSet(featureID);
	}

} // SFearedEventsLibrary
