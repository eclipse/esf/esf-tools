/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esflocalanalysis;

import org.eclipse.emf.common.util.EList;

import org.polarsys.esf.esfarchitectureconcepts.ISBlock;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SBlock LAnalysis</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSBlock <em>SBlock</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSFailureEventsLAnalysisList <em>SFailure Events LAnalysis List</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSPortsLAnalysisList <em>SPorts LAnalysis List</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getBase_Class <em>Base Class</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSLogicalGatesLAnalysisList <em>SLogical Gates LAnalysis List</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSFailureModesLAnalysisList <em>SFailure Modes LAnalysis List</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSBarriersLAnalysisList <em>SBarriers LAnalysis List</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSLocalAnalysis <em>SLocal Analysis</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSPropagationLinksList <em>SPropagation Links List</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSBlockLAnalysis()
 * @model
 * @generated
 */
public interface ISBlockLAnalysis
		extends IAbstractSLocalAnalysisElement {

	/**
	 * Returns the value of the '<em><b>SBlock</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SBlock</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SBlock</em>' reference.
	 * @see #setSBlock(ISBlock)
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSBlockLAnalysis_SBlock()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	ISBlock getSBlock();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSBlock <em>SBlock</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>SBlock</em>' reference.
	 * @see #getSBlock()
	 * @generated
	 */
	void setSBlock(ISBlock value);

	/**
	 * Returns the value of the '<em><b>SFailure Events LAnalysis List</b></em>' reference list.
	 * The list contents are of type {@link org.polarsys.esf.esflocalanalysis.IAbstractSFailureEventLAnalysis}.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esflocalanalysis.IAbstractSFailureEventLAnalysis#getSBlockLAnalysis <em>SBlock LAnalysis</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SFailure Events LAnalysis List</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SFailure Events LAnalysis List</em>' reference list.
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSBlockLAnalysis_SFailureEventsLAnalysisList()
	 * @see org.polarsys.esf.esflocalanalysis.IAbstractSFailureEventLAnalysis#getSBlockLAnalysis
	 * @model opposite="sBlockLAnalysis" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<IAbstractSFailureEventLAnalysis> getSFailureEventsLAnalysisList();

	/**
	 * Returns the value of the '<em><b>SPorts LAnalysis List</b></em>' reference list.
	 * The list contents are of type {@link org.polarsys.esf.esflocalanalysis.ISPortLAnalysis}.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esflocalanalysis.ISPortLAnalysis#getSBlockLAnalysis <em>SBlock LAnalysis</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SPorts LAnalysis List</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SPorts LAnalysis List</em>' reference list.
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSBlockLAnalysis_SPortsLAnalysisList()
	 * @see org.polarsys.esf.esflocalanalysis.ISPortLAnalysis#getSBlockLAnalysis
	 * @model opposite="sBlockLAnalysis" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<ISPortLAnalysis> getSPortsLAnalysisList();

	/**
	 * Returns the value of the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Base Class</em>' reference.
	 * @see #setBase_Class(org.eclipse.uml2.uml.Class)
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSBlockLAnalysis_Base_Class()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	org.eclipse.uml2.uml.Class getBase_Class();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getBase_Class <em>Base Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Base Class</em>' reference.
	 * @see #getBase_Class()
	 * @generated
	 */
	void setBase_Class(org.eclipse.uml2.uml.Class value);

	/**
	 * Returns the value of the '<em><b>SLogical Gates LAnalysis List</b></em>' reference list.
	 * The list contents are of type {@link org.polarsys.esf.esflocalanalysis.IAbstractSLogicalGateLAnalysis}.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esflocalanalysis.IAbstractSLogicalGateLAnalysis#getSBlockLAnalysis <em>SBlock LAnalysis</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SLogical Gates LAnalysis List</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SLogical Gates LAnalysis List</em>' reference list.
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSBlockLAnalysis_SLogicalGatesLAnalysisList()
	 * @see org.polarsys.esf.esflocalanalysis.IAbstractSLogicalGateLAnalysis#getSBlockLAnalysis
	 * @model opposite="sBlockLAnalysis" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<IAbstractSLogicalGateLAnalysis> getSLogicalGatesLAnalysisList();

	/**
	 * Returns the value of the '<em><b>SFailure Modes LAnalysis List</b></em>' reference list.
	 * The list contents are of type {@link org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis}.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis#getSBlockLAnalysis <em>SBlock LAnalysis</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SFailure Modes LAnalysis List</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SFailure Modes LAnalysis List</em>' reference list.
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSBlockLAnalysis_SFailureModesLAnalysisList()
	 * @see org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis#getSBlockLAnalysis
	 * @model opposite="sBlockLAnalysis" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<IAbstractSFailureModeLAnalysis> getSFailureModesLAnalysisList();

	/**
	 * Returns the value of the '<em><b>SBarriers LAnalysis List</b></em>' reference list.
	 * The list contents are of type {@link org.polarsys.esf.esflocalanalysis.ISBarrierLAnalysis}.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esflocalanalysis.ISBarrierLAnalysis#getSBlockLAnalysis <em>SBlock LAnalysis</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SBarriers LAnalysis List</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SBarriers LAnalysis List</em>' reference list.
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSBlockLAnalysis_SBarriersLAnalysisList()
	 * @see org.polarsys.esf.esflocalanalysis.ISBarrierLAnalysis#getSBlockLAnalysis
	 * @model opposite="sBlockLAnalysis" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<ISBarrierLAnalysis> getSBarriersLAnalysisList();

	/**
	 * Returns the value of the '<em><b>SLocal Analysis</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esflocalanalysis.ISLocalAnalysis#getSBlocksLAnalysisList <em>SBlocks LAnalysis List</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SLocal Analysis</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SLocal Analysis</em>' reference.
	 * @see #setSLocalAnalysis(ISLocalAnalysis)
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSBlockLAnalysis_SLocalAnalysis()
	 * @see org.polarsys.esf.esflocalanalysis.ISLocalAnalysis#getSBlocksLAnalysisList
	 * @model opposite="sBlocksLAnalysisList" required="true" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	ISLocalAnalysis getSLocalAnalysis();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSLocalAnalysis <em>SLocal Analysis</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>SLocal Analysis</em>' reference.
	 * @see #getSLocalAnalysis()
	 * @generated
	 */
	void setSLocalAnalysis(ISLocalAnalysis value);

	/**
	 * Returns the value of the '<em><b>SPropagation Links List</b></em>' reference list.
	 * The list contents are of type {@link org.polarsys.esf.esflocalanalysis.ISPropagationLink}.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esflocalanalysis.ISPropagationLink#getSBlockLAnalysis <em>SBlock LAnalysis</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SPropagation Links List</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SPropagation Links List</em>' reference list.
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSBlockLAnalysis_SPropagationLinksList()
	 * @see org.polarsys.esf.esflocalanalysis.ISPropagationLink#getSBlockLAnalysis
	 * @model opposite="sBlockLAnalysis" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<ISPropagationLink> getSPropagationLinksList();

} // ISBlockLAnalysis
