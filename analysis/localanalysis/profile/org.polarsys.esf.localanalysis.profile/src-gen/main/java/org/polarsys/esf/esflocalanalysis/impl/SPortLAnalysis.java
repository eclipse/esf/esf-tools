/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esflocalanalysis.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.uml2.uml.Port;
import org.polarsys.esf.esfarchitectureconcepts.ISPort;
import org.polarsys.esf.esfarchitectureconcepts.SDirection;
import org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage;
import org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis;
import org.polarsys.esf.esflocalanalysis.ISPortLAnalysis;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SPort LAnalysis</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SPortLAnalysis#getSBlockLAnalysis <em>SBlock LAnalysis</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SPortLAnalysis#getSPort <em>SPort</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SPortLAnalysis#getBase_Port <em>Base Port</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SPortLAnalysis#getSDirectionManual <em>SDirection Manual</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SPortLAnalysis#getSDirectionLAnalysis <em>SDirection LAnalysis</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SPortLAnalysis
		extends AbstractSFailureModeOwner
		implements ISPortLAnalysis {

	/**
	 * The cached value of the '{@link #getSPort() <em>SPort</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getSPort()
	 * @generated
	 * @ordered
	 */
	protected ISPort sPort;

	/**
	 * The cached value of the '{@link #getBase_Port() <em>Base Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getBase_Port()
	 * @generated
	 * @ordered
	 */
	protected Port base_Port;

	/**
	 * The default value of the '{@link #getSDirectionManual() <em>SDirection Manual</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getSDirectionManual()
	 * @generated
	 * @ordered
	 */
	protected static final SDirection SDIRECTION_MANUAL_EDEFAULT = SDirection.UNDEFINED;

	/**
	 * The cached value of the '{@link #getSDirectionManual() <em>SDirection Manual</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getSDirectionManual()
	 * @generated
	 * @ordered
	 */
	protected SDirection sDirectionManual = SDIRECTION_MANUAL_EDEFAULT;

	/**
	 * The default value of the '{@link #getSDirectionLAnalysis() <em>SDirection LAnalysis</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getSDirectionLAnalysis()
	 * @generated
	 * @ordered
	 */
	protected static final SDirection SDIRECTION_LANALYSIS_EDEFAULT = SDirection.IN;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected SPortLAnalysis() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IESFLocalAnalysisPackage.Literals.SPORT_LANALYSIS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISBlockLAnalysis getSBlockLAnalysis() {
		ISBlockLAnalysis sBlockLAnalysis = basicGetSBlockLAnalysis();
		return sBlockLAnalysis != null && sBlockLAnalysis.eIsProxy() ? (ISBlockLAnalysis) eResolveProxy((InternalEObject) sBlockLAnalysis) : sBlockLAnalysis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ISBlockLAnalysis basicGetSBlockLAnalysis() {
		// TODO: implement this method to return the 'SBlock LAnalysis' reference
		// -> do not perform proxy resolution
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setSBlockLAnalysis(ISBlockLAnalysis newSBlockLAnalysis) {
		// TODO: implement this method to set the 'SBlock LAnalysis' reference
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISPort getSPort() {
		if (sPort != null && sPort.eIsProxy()) {
			InternalEObject oldSPort = (InternalEObject) sPort;
			sPort = (ISPort) eResolveProxy(oldSPort);
			if (sPort != oldSPort) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IESFLocalAnalysisPackage.SPORT_LANALYSIS__SPORT, oldSPort, sPort));
			}
		}
		return sPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ISPort basicGetSPort() {
		return sPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setSPort(ISPort newSPort) {
		ISPort oldSPort = sPort;
		sPort = newSPort;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFLocalAnalysisPackage.SPORT_LANALYSIS__SPORT, oldSPort, sPort));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Port getBase_Port() {
		if (base_Port != null && base_Port.eIsProxy()) {
			InternalEObject oldBase_Port = (InternalEObject) base_Port;
			base_Port = (Port) eResolveProxy(oldBase_Port);
			if (base_Port != oldBase_Port) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IESFLocalAnalysisPackage.SPORT_LANALYSIS__BASE_PORT, oldBase_Port, base_Port));
			}
		}
		return base_Port;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Port basicGetBase_Port() {
		return base_Port;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setBase_Port(Port newBase_Port) {
		Port oldBase_Port = base_Port;
		base_Port = newBase_Port;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFLocalAnalysisPackage.SPORT_LANALYSIS__BASE_PORT, oldBase_Port, base_Port));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public SDirection getSDirectionManual() {
		return sDirectionManual;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setSDirectionManual(SDirection newSDirectionManual) {
		SDirection oldSDirectionManual = sDirectionManual;
		sDirectionManual = newSDirectionManual == null ? SDIRECTION_MANUAL_EDEFAULT : newSDirectionManual;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFLocalAnalysisPackage.SPORT_LANALYSIS__SDIRECTION_MANUAL, oldSDirectionManual, sDirectionManual));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public SDirection getSDirectionLAnalysis() {
		// TODO: implement this method to return the 'SDirection LAnalysis' attribute
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.SPORT_LANALYSIS__SBLOCK_LANALYSIS:
			if (resolve)
				return getSBlockLAnalysis();
			return basicGetSBlockLAnalysis();
		case IESFLocalAnalysisPackage.SPORT_LANALYSIS__SPORT:
			if (resolve)
				return getSPort();
			return basicGetSPort();
		case IESFLocalAnalysisPackage.SPORT_LANALYSIS__BASE_PORT:
			if (resolve)
				return getBase_Port();
			return basicGetBase_Port();
		case IESFLocalAnalysisPackage.SPORT_LANALYSIS__SDIRECTION_MANUAL:
			return getSDirectionManual();
		case IESFLocalAnalysisPackage.SPORT_LANALYSIS__SDIRECTION_LANALYSIS:
			return getSDirectionLAnalysis();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.SPORT_LANALYSIS__SBLOCK_LANALYSIS:
			setSBlockLAnalysis((ISBlockLAnalysis) newValue);
			return;
		case IESFLocalAnalysisPackage.SPORT_LANALYSIS__SPORT:
			setSPort((ISPort) newValue);
			return;
		case IESFLocalAnalysisPackage.SPORT_LANALYSIS__BASE_PORT:
			setBase_Port((Port) newValue);
			return;
		case IESFLocalAnalysisPackage.SPORT_LANALYSIS__SDIRECTION_MANUAL:
			setSDirectionManual((SDirection) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.SPORT_LANALYSIS__SBLOCK_LANALYSIS:
			setSBlockLAnalysis((ISBlockLAnalysis) null);
			return;
		case IESFLocalAnalysisPackage.SPORT_LANALYSIS__SPORT:
			setSPort((ISPort) null);
			return;
		case IESFLocalAnalysisPackage.SPORT_LANALYSIS__BASE_PORT:
			setBase_Port((Port) null);
			return;
		case IESFLocalAnalysisPackage.SPORT_LANALYSIS__SDIRECTION_MANUAL:
			setSDirectionManual(SDIRECTION_MANUAL_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.SPORT_LANALYSIS__SBLOCK_LANALYSIS:
			return basicGetSBlockLAnalysis() != null;
		case IESFLocalAnalysisPackage.SPORT_LANALYSIS__SPORT:
			return sPort != null;
		case IESFLocalAnalysisPackage.SPORT_LANALYSIS__BASE_PORT:
			return base_Port != null;
		case IESFLocalAnalysisPackage.SPORT_LANALYSIS__SDIRECTION_MANUAL:
			return sDirectionManual != SDIRECTION_MANUAL_EDEFAULT;
		case IESFLocalAnalysisPackage.SPORT_LANALYSIS__SDIRECTION_LANALYSIS:
			return getSDirectionLAnalysis() != SDIRECTION_LANALYSIS_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sDirectionManual: "); //$NON-NLS-1$
		result.append(sDirectionManual);
		result.append(')');
		return result.toString();
	}

} // SPortLAnalysis
