/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esflocalanalysis.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage;
import org.polarsys.esf.esflocalanalysis.ISSystemEvent;
import org.polarsys.esf.esflocalanalysis.ISSystemEventType;
import org.polarsys.esf.esflocalanalysis.ISSystemEventsLibrary;

import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SSystem Event Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SSystemEventType#getBase_Class <em>Base Class</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SSystemEventType#getSFailureEvent <em>SFailure Event</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SSystemEventType#getInstancesList <em>Instances List</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SSystemEventType#getSSystemEventsLibrary <em>SSystem Events Library</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SSystemEventType
		extends AbstractSLocalAnalysisElement
		implements ISSystemEventType {

	/**
	 * The cached value of the '{@link #getBase_Class() <em>Base Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getBase_Class()
	 * @generated
	 * @ordered
	 */
	protected org.eclipse.uml2.uml.Class base_Class;

	/**
	 * The cached value of the '{@link #getSFailureEvent() <em>SFailure Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getSFailureEvent()
	 * @generated
	 * @ordered
	 */
	protected ISFailureEvent sFailureEvent;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected SSystemEventType() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IESFLocalAnalysisPackage.Literals.SSYSTEM_EVENT_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public org.eclipse.uml2.uml.Class getBase_Class() {
		if (base_Class != null && base_Class.eIsProxy()) {
			InternalEObject oldBase_Class = (InternalEObject) base_Class;
			base_Class = (org.eclipse.uml2.uml.Class) eResolveProxy(oldBase_Class);
			if (base_Class != oldBase_Class) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IESFLocalAnalysisPackage.SSYSTEM_EVENT_TYPE__BASE_CLASS, oldBase_Class, base_Class));
			}
		}
		return base_Class;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public org.eclipse.uml2.uml.Class basicGetBase_Class() {
		return base_Class;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setBase_Class(org.eclipse.uml2.uml.Class newBase_Class) {
		org.eclipse.uml2.uml.Class oldBase_Class = base_Class;
		base_Class = newBase_Class;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFLocalAnalysisPackage.SSYSTEM_EVENT_TYPE__BASE_CLASS, oldBase_Class, base_Class));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISFailureEvent getSFailureEvent() {
		if (sFailureEvent != null && sFailureEvent.eIsProxy()) {
			InternalEObject oldSFailureEvent = (InternalEObject) sFailureEvent;
			sFailureEvent = (ISFailureEvent) eResolveProxy(oldSFailureEvent);
			if (sFailureEvent != oldSFailureEvent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IESFLocalAnalysisPackage.SSYSTEM_EVENT_TYPE__SFAILURE_EVENT, oldSFailureEvent, sFailureEvent));
			}
		}
		return sFailureEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ISFailureEvent basicGetSFailureEvent() {
		return sFailureEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setSFailureEvent(ISFailureEvent newSFailureEvent) {
		ISFailureEvent oldSFailureEvent = sFailureEvent;
		sFailureEvent = newSFailureEvent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFLocalAnalysisPackage.SSYSTEM_EVENT_TYPE__SFAILURE_EVENT, oldSFailureEvent, sFailureEvent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EList<ISSystemEvent> getInstancesList() {
		// TODO: implement this method to return the 'Instances List' reference list
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISSystemEventsLibrary getSSystemEventsLibrary() {
		ISSystemEventsLibrary sSystemEventsLibrary = basicGetSSystemEventsLibrary();
		return sSystemEventsLibrary != null && sSystemEventsLibrary.eIsProxy() ? (ISSystemEventsLibrary) eResolveProxy((InternalEObject) sSystemEventsLibrary) : sSystemEventsLibrary;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ISSystemEventsLibrary basicGetSSystemEventsLibrary() {
		// TODO: implement this method to return the 'SSystem Events Library' reference
		// -> do not perform proxy resolution
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setSSystemEventsLibrary(ISSystemEventsLibrary newSSystemEventsLibrary) {
		// TODO: implement this method to set the 'SSystem Events Library' reference
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.SSYSTEM_EVENT_TYPE__BASE_CLASS:
			if (resolve)
				return getBase_Class();
			return basicGetBase_Class();
		case IESFLocalAnalysisPackage.SSYSTEM_EVENT_TYPE__SFAILURE_EVENT:
			if (resolve)
				return getSFailureEvent();
			return basicGetSFailureEvent();
		case IESFLocalAnalysisPackage.SSYSTEM_EVENT_TYPE__INSTANCES_LIST:
			return getInstancesList();
		case IESFLocalAnalysisPackage.SSYSTEM_EVENT_TYPE__SSYSTEM_EVENTS_LIBRARY:
			if (resolve)
				return getSSystemEventsLibrary();
			return basicGetSSystemEventsLibrary();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.SSYSTEM_EVENT_TYPE__BASE_CLASS:
			setBase_Class((org.eclipse.uml2.uml.Class) newValue);
			return;
		case IESFLocalAnalysisPackage.SSYSTEM_EVENT_TYPE__SFAILURE_EVENT:
			setSFailureEvent((ISFailureEvent) newValue);
			return;
		case IESFLocalAnalysisPackage.SSYSTEM_EVENT_TYPE__INSTANCES_LIST:
			getInstancesList().clear();
			getInstancesList().addAll((Collection<? extends ISSystemEvent>) newValue);
			return;
		case IESFLocalAnalysisPackage.SSYSTEM_EVENT_TYPE__SSYSTEM_EVENTS_LIBRARY:
			setSSystemEventsLibrary((ISSystemEventsLibrary) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.SSYSTEM_EVENT_TYPE__BASE_CLASS:
			setBase_Class((org.eclipse.uml2.uml.Class) null);
			return;
		case IESFLocalAnalysisPackage.SSYSTEM_EVENT_TYPE__SFAILURE_EVENT:
			setSFailureEvent((ISFailureEvent) null);
			return;
		case IESFLocalAnalysisPackage.SSYSTEM_EVENT_TYPE__INSTANCES_LIST:
			getInstancesList().clear();
			return;
		case IESFLocalAnalysisPackage.SSYSTEM_EVENT_TYPE__SSYSTEM_EVENTS_LIBRARY:
			setSSystemEventsLibrary((ISSystemEventsLibrary) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.SSYSTEM_EVENT_TYPE__BASE_CLASS:
			return base_Class != null;
		case IESFLocalAnalysisPackage.SSYSTEM_EVENT_TYPE__SFAILURE_EVENT:
			return sFailureEvent != null;
		case IESFLocalAnalysisPackage.SSYSTEM_EVENT_TYPE__INSTANCES_LIST:
			return !getInstancesList().isEmpty();
		case IESFLocalAnalysisPackage.SSYSTEM_EVENT_TYPE__SSYSTEM_EVENTS_LIBRARY:
			return basicGetSSystemEventsLibrary() != null;
		}
		return super.eIsSet(featureID);
	}

} // SSystemEventType
