/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esflocalanalysis;

import org.eclipse.emf.common.util.EList;

import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureMode;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract SFailure Mode LAnalysis</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis#getSBlockLAnalysis <em>SBlock LAnalysis</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis#getSFailureMode <em>SFailure Mode</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis#getSFearedEventsFamiliesList <em>SFeared Events Families List</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis#getSFearedEventsList <em>SFeared Events List</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis#getOwner <em>Owner</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getAbstractSFailureModeLAnalysis()
 * @model abstract="true"
 * @generated
 */
public interface IAbstractSFailureModeLAnalysis
		extends IAbstractSPropagationElement {

	/**
	 * Returns the value of the '<em><b>SBlock LAnalysis</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSFailureModesLAnalysisList <em>SFailure Modes LAnalysis List</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SBlock LAnalysis</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SBlock LAnalysis</em>' reference.
	 * @see #setSBlockLAnalysis(ISBlockLAnalysis)
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getAbstractSFailureModeLAnalysis_SBlockLAnalysis()
	 * @see org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSFailureModesLAnalysisList
	 * @model opposite="sFailureModesLAnalysisList" required="true" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	ISBlockLAnalysis getSBlockLAnalysis();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis#getSBlockLAnalysis <em>SBlock LAnalysis</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>SBlock LAnalysis</em>' reference.
	 * @see #getSBlockLAnalysis()
	 * @generated
	 */
	void setSBlockLAnalysis(ISBlockLAnalysis value);

	/**
	 * Returns the value of the '<em><b>SFailure Mode</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SFailure Mode</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SFailure Mode</em>' reference.
	 * @see #setSFailureMode(ISFailureMode)
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getAbstractSFailureModeLAnalysis_SFailureMode()
	 * @model ordered="false"
	 * @generated
	 */
	ISFailureMode getSFailureMode();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis#getSFailureMode <em>SFailure Mode</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>SFailure Mode</em>' reference.
	 * @see #getSFailureMode()
	 * @generated
	 */
	void setSFailureMode(ISFailureMode value);

	/**
	 * Returns the value of the '<em><b>SFeared Events Families List</b></em>' reference list.
	 * The list contents are of type {@link org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SFeared Events Families List</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SFeared Events Families List</em>' reference list.
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getAbstractSFailureModeLAnalysis_SFearedEventsFamiliesList()
	 * @model ordered="false"
	 * @generated
	 */
	EList<ISFearedEventsFamily> getSFearedEventsFamiliesList();

	/**
	 * Returns the value of the '<em><b>SFeared Events List</b></em>' reference list.
	 * The list contents are of type {@link org.polarsys.esf.esflocalanalysis.ISFearedEvent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SFeared Events List</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SFeared Events List</em>' reference list.
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getAbstractSFailureModeLAnalysis_SFearedEventsList()
	 * @model ordered="false"
	 * @generated
	 */
	EList<ISFearedEvent> getSFearedEventsList();

	/**
	 * Returns the value of the '<em><b>Owner</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeOwner#getSFailureModesLAnalysisList <em>SFailure Modes LAnalysis List</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owner</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Owner</em>' reference.
	 * @see #setOwner(IAbstractSFailureModeOwner)
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getAbstractSFailureModeLAnalysis_Owner()
	 * @see org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeOwner#getSFailureModesLAnalysisList
	 * @model opposite="sFailureModesLAnalysisList" required="true" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	IAbstractSFailureModeOwner getOwner();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis#getOwner <em>Owner</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Owner</em>' reference.
	 * @see #getOwner()
	 * @generated
	 */
	void setOwner(IAbstractSFailureModeOwner value);

} // IAbstractSFailureModeLAnalysis
