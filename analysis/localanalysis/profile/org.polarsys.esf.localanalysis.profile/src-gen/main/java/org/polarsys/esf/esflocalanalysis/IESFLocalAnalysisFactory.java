/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esflocalanalysis;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * 
 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage
 * @generated
 */
public interface IESFLocalAnalysisFactory
		extends EFactory {

	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	IESFLocalAnalysisFactory eINSTANCE = org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisFactory.init();

	/**
	 * Returns a new object of class '<em>SPort LAnalysis</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>SPort LAnalysis</em>'.
	 * @generated
	 */
	ISPortLAnalysis createSPortLAnalysis();

	/**
	 * Returns a new object of class '<em>SPropagation Link</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>SPropagation Link</em>'.
	 * @generated
	 */
	ISPropagationLink createSPropagationLink();

	/**
	 * Returns a new object of class '<em>SBlock LAnalysis</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>SBlock LAnalysis</em>'.
	 * @generated
	 */
	ISBlockLAnalysis createSBlockLAnalysis();

	/**
	 * Returns a new object of class '<em>SBarrier LAnalysis</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>SBarrier LAnalysis</em>'.
	 * @generated
	 */
	ISBarrierLAnalysis createSBarrierLAnalysis();

	/**
	 * Returns a new object of class '<em>SLocal Analysis</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>SLocal Analysis</em>'.
	 * @generated
	 */
	ISLocalAnalysis createSLocalAnalysis();

	/**
	 * Returns a new object of class '<em>SSystem Events Library</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>SSystem Events Library</em>'.
	 * @generated
	 */
	ISSystemEventsLibrary createSSystemEventsLibrary();

	/**
	 * Returns a new object of class '<em>SSystem Event Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>SSystem Event Type</em>'.
	 * @generated
	 */
	ISSystemEventType createSSystemEventType();

	/**
	 * Returns a new object of class '<em>SSystem Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>SSystem Event</em>'.
	 * @generated
	 */
	ISSystemEvent createSSystemEvent();

	/**
	 * Returns a new object of class '<em>SFeared Events Library</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>SFeared Events Library</em>'.
	 * @generated
	 */
	ISFearedEventsLibrary createSFearedEventsLibrary();

	/**
	 * Returns a new object of class '<em>SFeared Events Family</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>SFeared Events Family</em>'.
	 * @generated
	 */
	ISFearedEventsFamily createSFearedEventsFamily();

	/**
	 * Returns a new object of class '<em>SFeared Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>SFeared Event</em>'.
	 * @generated
	 */
	ISFearedEvent createSFearedEvent();

	/**
	 * Returns a new object of class '<em>SDysfunctional Association</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>SDysfunctional Association</em>'.
	 * @generated
	 */
	ISDysfunctionalAssociation createSDysfunctionalAssociation();

	/**
	 * Returns a new object of class '<em>SAnd Gate LAnalysis</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>SAnd Gate LAnalysis</em>'.
	 * @generated
	 */
	ISAndGateLAnalysis createSAndGateLAnalysis();

	/**
	 * Returns a new object of class '<em>SOr Gate LAnalysis</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>SOr Gate LAnalysis</em>'.
	 * @generated
	 */
	ISOrGateLAnalysis createSOrGateLAnalysis();

	/**
	 * Returns a new object of class '<em>SUntimely Failure Mode</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>SUntimely Failure Mode</em>'.
	 * @generated
	 */
	ISUntimelyFailureMode createSUntimelyFailureMode();

	/**
	 * Returns a new object of class '<em>SAbsent Failure Mode</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>SAbsent Failure Mode</em>'.
	 * @generated
	 */
	ISAbsentFailureMode createSAbsentFailureMode();

	/**
	 * Returns a new object of class '<em>SErroneous Failure Mode</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>SErroneous Failure Mode</em>'.
	 * @generated
	 */
	ISErroneousFailureMode createSErroneousFailureMode();

	/**
	 * Returns a new object of class '<em>SLocal Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>SLocal Event</em>'.
	 * @generated
	 */
	ISLocalEvent createSLocalEvent();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the package supported by this factory.
	 * @generated
	 */
	IESFLocalAnalysisPackage getESFLocalAnalysisPackage();

} // IESFLocalAnalysisFactory
