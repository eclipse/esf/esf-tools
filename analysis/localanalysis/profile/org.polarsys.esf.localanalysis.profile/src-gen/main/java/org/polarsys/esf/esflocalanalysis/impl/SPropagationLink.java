/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esflocalanalysis.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.uml2.uml.Dependency;

import org.polarsys.esf.esflocalanalysis.IAbstractSPropagationElement;
import org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage;
import org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis;
import org.polarsys.esf.esflocalanalysis.ISPropagationLink;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SPropagation Link</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SPropagationLink#getElementTarget <em>Element Target</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SPropagationLink#getSBlockLAnalysis <em>SBlock LAnalysis</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SPropagationLink#getBase_Dependency <em>Base Dependency</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SPropagationLink#getElementSource <em>Element Source</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SPropagationLink
		extends AbstractSLocalAnalysisElement
		implements ISPropagationLink {

	/**
	 * The cached value of the '{@link #getBase_Dependency() <em>Base Dependency</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getBase_Dependency()
	 * @generated
	 * @ordered
	 */
	protected Dependency base_Dependency;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected SPropagationLink() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IESFLocalAnalysisPackage.Literals.SPROPAGATION_LINK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public IAbstractSPropagationElement getElementTarget() {
		IAbstractSPropagationElement elementTarget = basicGetElementTarget();
		return elementTarget != null && elementTarget.eIsProxy() ? (IAbstractSPropagationElement) eResolveProxy((InternalEObject) elementTarget) : elementTarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public IAbstractSPropagationElement basicGetElementTarget() {
		// TODO: implement this method to return the 'Element Target' reference
		// -> do not perform proxy resolution
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setElementTarget(IAbstractSPropagationElement newElementTarget) {
		// TODO: implement this method to set the 'Element Target' reference
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISBlockLAnalysis getSBlockLAnalysis() {
		ISBlockLAnalysis sBlockLAnalysis = basicGetSBlockLAnalysis();
		return sBlockLAnalysis != null && sBlockLAnalysis.eIsProxy() ? (ISBlockLAnalysis) eResolveProxy((InternalEObject) sBlockLAnalysis) : sBlockLAnalysis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ISBlockLAnalysis basicGetSBlockLAnalysis() {
		// TODO: implement this method to return the 'SBlock LAnalysis' reference
		// -> do not perform proxy resolution
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setSBlockLAnalysis(ISBlockLAnalysis newSBlockLAnalysis) {
		// TODO: implement this method to set the 'SBlock LAnalysis' reference
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Dependency getBase_Dependency() {
		if (base_Dependency != null && base_Dependency.eIsProxy()) {
			InternalEObject oldBase_Dependency = (InternalEObject) base_Dependency;
			base_Dependency = (Dependency) eResolveProxy(oldBase_Dependency);
			if (base_Dependency != oldBase_Dependency) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IESFLocalAnalysisPackage.SPROPAGATION_LINK__BASE_DEPENDENCY, oldBase_Dependency, base_Dependency));
			}
		}
		return base_Dependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Dependency basicGetBase_Dependency() {
		return base_Dependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setBase_Dependency(Dependency newBase_Dependency) {
		Dependency oldBase_Dependency = base_Dependency;
		base_Dependency = newBase_Dependency;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFLocalAnalysisPackage.SPROPAGATION_LINK__BASE_DEPENDENCY, oldBase_Dependency, base_Dependency));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public IAbstractSPropagationElement getElementSource() {
		IAbstractSPropagationElement elementSource = basicGetElementSource();
		return elementSource != null && elementSource.eIsProxy() ? (IAbstractSPropagationElement) eResolveProxy((InternalEObject) elementSource) : elementSource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public IAbstractSPropagationElement basicGetElementSource() {
		// TODO: implement this method to return the 'Element Source' reference
		// -> do not perform proxy resolution
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setElementSource(IAbstractSPropagationElement newElementSource) {
		// TODO: implement this method to set the 'Element Source' reference
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.SPROPAGATION_LINK__ELEMENT_TARGET:
			if (resolve)
				return getElementTarget();
			return basicGetElementTarget();
		case IESFLocalAnalysisPackage.SPROPAGATION_LINK__SBLOCK_LANALYSIS:
			if (resolve)
				return getSBlockLAnalysis();
			return basicGetSBlockLAnalysis();
		case IESFLocalAnalysisPackage.SPROPAGATION_LINK__BASE_DEPENDENCY:
			if (resolve)
				return getBase_Dependency();
			return basicGetBase_Dependency();
		case IESFLocalAnalysisPackage.SPROPAGATION_LINK__ELEMENT_SOURCE:
			if (resolve)
				return getElementSource();
			return basicGetElementSource();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.SPROPAGATION_LINK__ELEMENT_TARGET:
			setElementTarget((IAbstractSPropagationElement) newValue);
			return;
		case IESFLocalAnalysisPackage.SPROPAGATION_LINK__SBLOCK_LANALYSIS:
			setSBlockLAnalysis((ISBlockLAnalysis) newValue);
			return;
		case IESFLocalAnalysisPackage.SPROPAGATION_LINK__BASE_DEPENDENCY:
			setBase_Dependency((Dependency) newValue);
			return;
		case IESFLocalAnalysisPackage.SPROPAGATION_LINK__ELEMENT_SOURCE:
			setElementSource((IAbstractSPropagationElement) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.SPROPAGATION_LINK__ELEMENT_TARGET:
			setElementTarget((IAbstractSPropagationElement) null);
			return;
		case IESFLocalAnalysisPackage.SPROPAGATION_LINK__SBLOCK_LANALYSIS:
			setSBlockLAnalysis((ISBlockLAnalysis) null);
			return;
		case IESFLocalAnalysisPackage.SPROPAGATION_LINK__BASE_DEPENDENCY:
			setBase_Dependency((Dependency) null);
			return;
		case IESFLocalAnalysisPackage.SPROPAGATION_LINK__ELEMENT_SOURCE:
			setElementSource((IAbstractSPropagationElement) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.SPROPAGATION_LINK__ELEMENT_TARGET:
			return basicGetElementTarget() != null;
		case IESFLocalAnalysisPackage.SPROPAGATION_LINK__SBLOCK_LANALYSIS:
			return basicGetSBlockLAnalysis() != null;
		case IESFLocalAnalysisPackage.SPROPAGATION_LINK__BASE_DEPENDENCY:
			return base_Dependency != null;
		case IESFLocalAnalysisPackage.SPROPAGATION_LINK__ELEMENT_SOURCE:
			return basicGetElementSource() != null;
		}
		return super.eIsSet(featureID);
	}

} // SPropagationLink
