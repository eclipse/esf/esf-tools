/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esflocalanalysis.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis;
import org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage;
import org.polarsys.esf.esflocalanalysis.ISFearedEvent;
import org.polarsys.esf.esflocalanalysis.ISFearedEventsLibrary;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SFeared Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SFearedEvent#isSelected <em>Is Selected</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SFearedEvent#getBase_Class <em>Base Class</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SFearedEvent#getSFailureModesLAnalysisList <em>SFailure Modes LAnalysis List</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SFearedEvent#getSFearedEventsLibrary <em>SFeared Events Library</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SFearedEvent
		extends AbstractSLocalAnalysisElement
		implements ISFearedEvent {

	/**
	 * The default value of the '{@link #isSelected() <em>Is Selected</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #isSelected()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_SELECTED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #getBase_Class() <em>Base Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getBase_Class()
	 * @generated
	 * @ordered
	 */
	protected org.eclipse.uml2.uml.Class base_Class;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected SFearedEvent() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IESFLocalAnalysisPackage.Literals.SFEARED_EVENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean isSelected() {
		// TODO: implement this method to return the 'Is Selected' attribute
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setIsSelected(boolean newIsSelected) {
		// TODO: implement this method to set the 'Is Selected' attribute
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public org.eclipse.uml2.uml.Class getBase_Class() {
		if (base_Class != null && base_Class.eIsProxy()) {
			InternalEObject oldBase_Class = (InternalEObject) base_Class;
			base_Class = (org.eclipse.uml2.uml.Class) eResolveProxy(oldBase_Class);
			if (base_Class != oldBase_Class) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IESFLocalAnalysisPackage.SFEARED_EVENT__BASE_CLASS, oldBase_Class, base_Class));
			}
		}
		return base_Class;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public org.eclipse.uml2.uml.Class basicGetBase_Class() {
		return base_Class;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setBase_Class(org.eclipse.uml2.uml.Class newBase_Class) {
		org.eclipse.uml2.uml.Class oldBase_Class = base_Class;
		base_Class = newBase_Class;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFLocalAnalysisPackage.SFEARED_EVENT__BASE_CLASS, oldBase_Class, base_Class));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EList<IAbstractSFailureModeLAnalysis> getSFailureModesLAnalysisList() {
		// TODO: implement this method to return the 'SFailure Modes LAnalysis List' reference list
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISFearedEventsLibrary getSFearedEventsLibrary() {
		ISFearedEventsLibrary sFearedEventsLibrary = basicGetSFearedEventsLibrary();
		return sFearedEventsLibrary != null && sFearedEventsLibrary.eIsProxy() ? (ISFearedEventsLibrary) eResolveProxy((InternalEObject) sFearedEventsLibrary) : sFearedEventsLibrary;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ISFearedEventsLibrary basicGetSFearedEventsLibrary() {
		// TODO: implement this method to return the 'SFeared Events Library' reference
		// -> do not perform proxy resolution
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setSFearedEventsLibrary(ISFearedEventsLibrary newSFearedEventsLibrary) {
		// TODO: implement this method to set the 'SFeared Events Library' reference
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.SFEARED_EVENT__IS_SELECTED:
			return isSelected();
		case IESFLocalAnalysisPackage.SFEARED_EVENT__BASE_CLASS:
			if (resolve)
				return getBase_Class();
			return basicGetBase_Class();
		case IESFLocalAnalysisPackage.SFEARED_EVENT__SFAILURE_MODES_LANALYSIS_LIST:
			return getSFailureModesLAnalysisList();
		case IESFLocalAnalysisPackage.SFEARED_EVENT__SFEARED_EVENTS_LIBRARY:
			if (resolve)
				return getSFearedEventsLibrary();
			return basicGetSFearedEventsLibrary();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.SFEARED_EVENT__IS_SELECTED:
			setIsSelected((Boolean) newValue);
			return;
		case IESFLocalAnalysisPackage.SFEARED_EVENT__BASE_CLASS:
			setBase_Class((org.eclipse.uml2.uml.Class) newValue);
			return;
		case IESFLocalAnalysisPackage.SFEARED_EVENT__SFAILURE_MODES_LANALYSIS_LIST:
			getSFailureModesLAnalysisList().clear();
			getSFailureModesLAnalysisList().addAll((Collection<? extends IAbstractSFailureModeLAnalysis>) newValue);
			return;
		case IESFLocalAnalysisPackage.SFEARED_EVENT__SFEARED_EVENTS_LIBRARY:
			setSFearedEventsLibrary((ISFearedEventsLibrary) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.SFEARED_EVENT__IS_SELECTED:
			setIsSelected(IS_SELECTED_EDEFAULT);
			return;
		case IESFLocalAnalysisPackage.SFEARED_EVENT__BASE_CLASS:
			setBase_Class((org.eclipse.uml2.uml.Class) null);
			return;
		case IESFLocalAnalysisPackage.SFEARED_EVENT__SFAILURE_MODES_LANALYSIS_LIST:
			getSFailureModesLAnalysisList().clear();
			return;
		case IESFLocalAnalysisPackage.SFEARED_EVENT__SFEARED_EVENTS_LIBRARY:
			setSFearedEventsLibrary((ISFearedEventsLibrary) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.SFEARED_EVENT__IS_SELECTED:
			return isSelected() != IS_SELECTED_EDEFAULT;
		case IESFLocalAnalysisPackage.SFEARED_EVENT__BASE_CLASS:
			return base_Class != null;
		case IESFLocalAnalysisPackage.SFEARED_EVENT__SFAILURE_MODES_LANALYSIS_LIST:
			return !getSFailureModesLAnalysisList().isEmpty();
		case IESFLocalAnalysisPackage.SFEARED_EVENT__SFEARED_EVENTS_LIBRARY:
			return basicGetSFearedEventsLibrary() != null;
		}
		return super.eIsSet(featureID);
	}

} // SFearedEvent
