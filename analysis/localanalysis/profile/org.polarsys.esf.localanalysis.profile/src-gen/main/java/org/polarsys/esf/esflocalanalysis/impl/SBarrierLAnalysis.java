/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esflocalanalysis.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.uml2.uml.Property;

import org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage;
import org.polarsys.esf.esflocalanalysis.ISBarrierLAnalysis;
import org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis;

import org.polarsys.esf.esfsafetyconcepts.srecommendations.ISBarrier;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SBarrier LAnalysis</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SBarrierLAnalysis#getSBarrier <em>SBarrier</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SBarrierLAnalysis#getBase_Property <em>Base Property</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SBarrierLAnalysis#getSBlockLAnalysis <em>SBlock LAnalysis</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SBarrierLAnalysis
		extends AbstractSFailureModeOwner
		implements ISBarrierLAnalysis {

	/**
	 * The cached value of the '{@link #getSBarrier() <em>SBarrier</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getSBarrier()
	 * @generated
	 * @ordered
	 */
	protected ISBarrier sBarrier;

	/**
	 * The cached value of the '{@link #getBase_Property() <em>Base Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getBase_Property()
	 * @generated
	 * @ordered
	 */
	protected Property base_Property;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected SBarrierLAnalysis() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IESFLocalAnalysisPackage.Literals.SBARRIER_LANALYSIS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISBarrier getSBarrier() {
		if (sBarrier != null && sBarrier.eIsProxy()) {
			InternalEObject oldSBarrier = (InternalEObject) sBarrier;
			sBarrier = (ISBarrier) eResolveProxy(oldSBarrier);
			if (sBarrier != oldSBarrier) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IESFLocalAnalysisPackage.SBARRIER_LANALYSIS__SBARRIER, oldSBarrier, sBarrier));
			}
		}
		return sBarrier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ISBarrier basicGetSBarrier() {
		return sBarrier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setSBarrier(ISBarrier newSBarrier) {
		ISBarrier oldSBarrier = sBarrier;
		sBarrier = newSBarrier;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFLocalAnalysisPackage.SBARRIER_LANALYSIS__SBARRIER, oldSBarrier, sBarrier));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Property getBase_Property() {
		if (base_Property != null && base_Property.eIsProxy()) {
			InternalEObject oldBase_Property = (InternalEObject) base_Property;
			base_Property = (Property) eResolveProxy(oldBase_Property);
			if (base_Property != oldBase_Property) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IESFLocalAnalysisPackage.SBARRIER_LANALYSIS__BASE_PROPERTY, oldBase_Property, base_Property));
			}
		}
		return base_Property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Property basicGetBase_Property() {
		return base_Property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setBase_Property(Property newBase_Property) {
		Property oldBase_Property = base_Property;
		base_Property = newBase_Property;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFLocalAnalysisPackage.SBARRIER_LANALYSIS__BASE_PROPERTY, oldBase_Property, base_Property));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISBlockLAnalysis getSBlockLAnalysis() {
		ISBlockLAnalysis sBlockLAnalysis = basicGetSBlockLAnalysis();
		return sBlockLAnalysis != null && sBlockLAnalysis.eIsProxy() ? (ISBlockLAnalysis) eResolveProxy((InternalEObject) sBlockLAnalysis) : sBlockLAnalysis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ISBlockLAnalysis basicGetSBlockLAnalysis() {
		// TODO: implement this method to return the 'SBlock LAnalysis' reference
		// -> do not perform proxy resolution
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setSBlockLAnalysis(ISBlockLAnalysis newSBlockLAnalysis) {
		// TODO: implement this method to set the 'SBlock LAnalysis' reference
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.SBARRIER_LANALYSIS__SBARRIER:
			if (resolve)
				return getSBarrier();
			return basicGetSBarrier();
		case IESFLocalAnalysisPackage.SBARRIER_LANALYSIS__BASE_PROPERTY:
			if (resolve)
				return getBase_Property();
			return basicGetBase_Property();
		case IESFLocalAnalysisPackage.SBARRIER_LANALYSIS__SBLOCK_LANALYSIS:
			if (resolve)
				return getSBlockLAnalysis();
			return basicGetSBlockLAnalysis();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.SBARRIER_LANALYSIS__SBARRIER:
			setSBarrier((ISBarrier) newValue);
			return;
		case IESFLocalAnalysisPackage.SBARRIER_LANALYSIS__BASE_PROPERTY:
			setBase_Property((Property) newValue);
			return;
		case IESFLocalAnalysisPackage.SBARRIER_LANALYSIS__SBLOCK_LANALYSIS:
			setSBlockLAnalysis((ISBlockLAnalysis) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.SBARRIER_LANALYSIS__SBARRIER:
			setSBarrier((ISBarrier) null);
			return;
		case IESFLocalAnalysisPackage.SBARRIER_LANALYSIS__BASE_PROPERTY:
			setBase_Property((Property) null);
			return;
		case IESFLocalAnalysisPackage.SBARRIER_LANALYSIS__SBLOCK_LANALYSIS:
			setSBlockLAnalysis((ISBlockLAnalysis) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.SBARRIER_LANALYSIS__SBARRIER:
			return sBarrier != null;
		case IESFLocalAnalysisPackage.SBARRIER_LANALYSIS__BASE_PROPERTY:
			return base_Property != null;
		case IESFLocalAnalysisPackage.SBARRIER_LANALYSIS__SBLOCK_LANALYSIS:
			return basicGetSBlockLAnalysis() != null;
		}
		return super.eIsSet(featureID);
	}

} // SBarrierLAnalysis
