/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esflocalanalysis.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.polarsys.esf.esfcore.IAbstractSElement;
import org.polarsys.esf.esfcore.IAbstractSSafetyAnalysis;
import org.polarsys.esf.esfcore.IAbstractSSafetyConcept;

import org.polarsys.esf.esflocalanalysis.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * 
 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage
 * @generated
 */
public class ESFLocalAnalysisSwitch<T>
		extends Switch<T> {

	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected static IESFLocalAnalysisPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ESFLocalAnalysisSwitch() {
		if (modelPackage == null) {
			modelPackage = IESFLocalAnalysisPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param ePackage
	 *            the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case IESFLocalAnalysisPackage.ABSTRACT_SLOCAL_ANALYSIS_ELEMENT: {
			IAbstractSLocalAnalysisElement abstractSLocalAnalysisElement = (IAbstractSLocalAnalysisElement) theEObject;
			T result = caseAbstractSLocalAnalysisElement(abstractSLocalAnalysisElement);
			if (result == null)
				result = caseAbstractSSafetyConcept(abstractSLocalAnalysisElement);
			if (result == null)
				result = caseAbstractSElement(abstractSLocalAnalysisElement);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IESFLocalAnalysisPackage.SPORT_LANALYSIS: {
			ISPortLAnalysis sPortLAnalysis = (ISPortLAnalysis) theEObject;
			T result = caseSPortLAnalysis(sPortLAnalysis);
			if (result == null)
				result = caseAbstractSFailureModeOwner(sPortLAnalysis);
			if (result == null)
				result = caseAbstractSLocalAnalysisElement(sPortLAnalysis);
			if (result == null)
				result = caseAbstractSSafetyConcept(sPortLAnalysis);
			if (result == null)
				result = caseAbstractSElement(sPortLAnalysis);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_MODE_OWNER: {
			IAbstractSFailureModeOwner abstractSFailureModeOwner = (IAbstractSFailureModeOwner) theEObject;
			T result = caseAbstractSFailureModeOwner(abstractSFailureModeOwner);
			if (result == null)
				result = caseAbstractSLocalAnalysisElement(abstractSFailureModeOwner);
			if (result == null)
				result = caseAbstractSSafetyConcept(abstractSFailureModeOwner);
			if (result == null)
				result = caseAbstractSElement(abstractSFailureModeOwner);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_MODE_LANALYSIS: {
			IAbstractSFailureModeLAnalysis abstractSFailureModeLAnalysis = (IAbstractSFailureModeLAnalysis) theEObject;
			T result = caseAbstractSFailureModeLAnalysis(abstractSFailureModeLAnalysis);
			if (result == null)
				result = caseAbstractSPropagationElement(abstractSFailureModeLAnalysis);
			if (result == null)
				result = caseAbstractSLocalAnalysisElement(abstractSFailureModeLAnalysis);
			if (result == null)
				result = caseAbstractSSafetyConcept(abstractSFailureModeLAnalysis);
			if (result == null)
				result = caseAbstractSElement(abstractSFailureModeLAnalysis);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IESFLocalAnalysisPackage.ABSTRACT_SPROPAGATION_ELEMENT: {
			IAbstractSPropagationElement abstractSPropagationElement = (IAbstractSPropagationElement) theEObject;
			T result = caseAbstractSPropagationElement(abstractSPropagationElement);
			if (result == null)
				result = caseAbstractSLocalAnalysisElement(abstractSPropagationElement);
			if (result == null)
				result = caseAbstractSSafetyConcept(abstractSPropagationElement);
			if (result == null)
				result = caseAbstractSElement(abstractSPropagationElement);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IESFLocalAnalysisPackage.SPROPAGATION_LINK: {
			ISPropagationLink sPropagationLink = (ISPropagationLink) theEObject;
			T result = caseSPropagationLink(sPropagationLink);
			if (result == null)
				result = caseAbstractSLocalAnalysisElement(sPropagationLink);
			if (result == null)
				result = caseAbstractSSafetyConcept(sPropagationLink);
			if (result == null)
				result = caseAbstractSElement(sPropagationLink);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS: {
			ISBlockLAnalysis sBlockLAnalysis = (ISBlockLAnalysis) theEObject;
			T result = caseSBlockLAnalysis(sBlockLAnalysis);
			if (result == null)
				result = caseAbstractSLocalAnalysisElement(sBlockLAnalysis);
			if (result == null)
				result = caseAbstractSSafetyConcept(sBlockLAnalysis);
			if (result == null)
				result = caseAbstractSElement(sBlockLAnalysis);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_EVENT_LANALYSIS: {
			IAbstractSFailureEventLAnalysis abstractSFailureEventLAnalysis = (IAbstractSFailureEventLAnalysis) theEObject;
			T result = caseAbstractSFailureEventLAnalysis(abstractSFailureEventLAnalysis);
			if (result == null)
				result = caseAbstractSPropagationElement(abstractSFailureEventLAnalysis);
			if (result == null)
				result = caseAbstractSLocalAnalysisElement(abstractSFailureEventLAnalysis);
			if (result == null)
				result = caseAbstractSSafetyConcept(abstractSFailureEventLAnalysis);
			if (result == null)
				result = caseAbstractSElement(abstractSFailureEventLAnalysis);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IESFLocalAnalysisPackage.ABSTRACT_SLOGICAL_GATE_LANALYSIS: {
			IAbstractSLogicalGateLAnalysis abstractSLogicalGateLAnalysis = (IAbstractSLogicalGateLAnalysis) theEObject;
			T result = caseAbstractSLogicalGateLAnalysis(abstractSLogicalGateLAnalysis);
			if (result == null)
				result = caseAbstractSPropagationElement(abstractSLogicalGateLAnalysis);
			if (result == null)
				result = caseAbstractSLocalAnalysisElement(abstractSLogicalGateLAnalysis);
			if (result == null)
				result = caseAbstractSSafetyConcept(abstractSLogicalGateLAnalysis);
			if (result == null)
				result = caseAbstractSElement(abstractSLogicalGateLAnalysis);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IESFLocalAnalysisPackage.SBARRIER_LANALYSIS: {
			ISBarrierLAnalysis sBarrierLAnalysis = (ISBarrierLAnalysis) theEObject;
			T result = caseSBarrierLAnalysis(sBarrierLAnalysis);
			if (result == null)
				result = caseAbstractSFailureModeOwner(sBarrierLAnalysis);
			if (result == null)
				result = caseAbstractSLocalAnalysisElement(sBarrierLAnalysis);
			if (result == null)
				result = caseAbstractSSafetyConcept(sBarrierLAnalysis);
			if (result == null)
				result = caseAbstractSElement(sBarrierLAnalysis);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IESFLocalAnalysisPackage.SLOCAL_ANALYSIS: {
			ISLocalAnalysis sLocalAnalysis = (ISLocalAnalysis) theEObject;
			T result = caseSLocalAnalysis(sLocalAnalysis);
			if (result == null)
				result = caseAbstractSSafetyAnalysis(sLocalAnalysis);
			if (result == null)
				result = caseAbstractSElement(sLocalAnalysis);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IESFLocalAnalysisPackage.SSYSTEM_EVENTS_LIBRARY: {
			ISSystemEventsLibrary sSystemEventsLibrary = (ISSystemEventsLibrary) theEObject;
			T result = caseSSystemEventsLibrary(sSystemEventsLibrary);
			if (result == null)
				result = caseAbstractSLocalAnalysisElement(sSystemEventsLibrary);
			if (result == null)
				result = caseAbstractSSafetyConcept(sSystemEventsLibrary);
			if (result == null)
				result = caseAbstractSElement(sSystemEventsLibrary);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IESFLocalAnalysisPackage.SSYSTEM_EVENT_TYPE: {
			ISSystemEventType sSystemEventType = (ISSystemEventType) theEObject;
			T result = caseSSystemEventType(sSystemEventType);
			if (result == null)
				result = caseAbstractSLocalAnalysisElement(sSystemEventType);
			if (result == null)
				result = caseAbstractSSafetyConcept(sSystemEventType);
			if (result == null)
				result = caseAbstractSElement(sSystemEventType);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IESFLocalAnalysisPackage.SSYSTEM_EVENT: {
			ISSystemEvent sSystemEvent = (ISSystemEvent) theEObject;
			T result = caseSSystemEvent(sSystemEvent);
			if (result == null)
				result = caseAbstractSFailureEventLAnalysis(sSystemEvent);
			if (result == null)
				result = caseAbstractSPropagationElement(sSystemEvent);
			if (result == null)
				result = caseAbstractSLocalAnalysisElement(sSystemEvent);
			if (result == null)
				result = caseAbstractSSafetyConcept(sSystemEvent);
			if (result == null)
				result = caseAbstractSElement(sSystemEvent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IESFLocalAnalysisPackage.SFEARED_EVENTS_LIBRARY: {
			ISFearedEventsLibrary sFearedEventsLibrary = (ISFearedEventsLibrary) theEObject;
			T result = caseSFearedEventsLibrary(sFearedEventsLibrary);
			if (result == null)
				result = caseAbstractSLocalAnalysisElement(sFearedEventsLibrary);
			if (result == null)
				result = caseAbstractSSafetyConcept(sFearedEventsLibrary);
			if (result == null)
				result = caseAbstractSElement(sFearedEventsLibrary);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IESFLocalAnalysisPackage.SFEARED_EVENTS_FAMILY: {
			ISFearedEventsFamily sFearedEventsFamily = (ISFearedEventsFamily) theEObject;
			T result = caseSFearedEventsFamily(sFearedEventsFamily);
			if (result == null)
				result = caseAbstractSLocalAnalysisElement(sFearedEventsFamily);
			if (result == null)
				result = caseAbstractSSafetyConcept(sFearedEventsFamily);
			if (result == null)
				result = caseAbstractSElement(sFearedEventsFamily);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IESFLocalAnalysisPackage.SFEARED_EVENT: {
			ISFearedEvent sFearedEvent = (ISFearedEvent) theEObject;
			T result = caseSFearedEvent(sFearedEvent);
			if (result == null)
				result = caseAbstractSLocalAnalysisElement(sFearedEvent);
			if (result == null)
				result = caseAbstractSSafetyConcept(sFearedEvent);
			if (result == null)
				result = caseAbstractSElement(sFearedEvent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IESFLocalAnalysisPackage.SDYSFUNCTIONAL_ASSOCIATION: {
			ISDysfunctionalAssociation sDysfunctionalAssociation = (ISDysfunctionalAssociation) theEObject;
			T result = caseSDysfunctionalAssociation(sDysfunctionalAssociation);
			if (result == null)
				result = caseAbstractSLocalAnalysisElement(sDysfunctionalAssociation);
			if (result == null)
				result = caseAbstractSSafetyConcept(sDysfunctionalAssociation);
			if (result == null)
				result = caseAbstractSElement(sDysfunctionalAssociation);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IESFLocalAnalysisPackage.SAND_GATE_LANALYSIS: {
			ISAndGateLAnalysis sAndGateLAnalysis = (ISAndGateLAnalysis) theEObject;
			T result = caseSAndGateLAnalysis(sAndGateLAnalysis);
			if (result == null)
				result = caseAbstractSLogicalGateLAnalysis(sAndGateLAnalysis);
			if (result == null)
				result = caseAbstractSPropagationElement(sAndGateLAnalysis);
			if (result == null)
				result = caseAbstractSLocalAnalysisElement(sAndGateLAnalysis);
			if (result == null)
				result = caseAbstractSSafetyConcept(sAndGateLAnalysis);
			if (result == null)
				result = caseAbstractSElement(sAndGateLAnalysis);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IESFLocalAnalysisPackage.SOR_GATE_LANALYSIS: {
			ISOrGateLAnalysis sOrGateLAnalysis = (ISOrGateLAnalysis) theEObject;
			T result = caseSOrGateLAnalysis(sOrGateLAnalysis);
			if (result == null)
				result = caseAbstractSLogicalGateLAnalysis(sOrGateLAnalysis);
			if (result == null)
				result = caseAbstractSPropagationElement(sOrGateLAnalysis);
			if (result == null)
				result = caseAbstractSLocalAnalysisElement(sOrGateLAnalysis);
			if (result == null)
				result = caseAbstractSSafetyConcept(sOrGateLAnalysis);
			if (result == null)
				result = caseAbstractSElement(sOrGateLAnalysis);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IESFLocalAnalysisPackage.SUNTIMELY_FAILURE_MODE: {
			ISUntimelyFailureMode sUntimelyFailureMode = (ISUntimelyFailureMode) theEObject;
			T result = caseSUntimelyFailureMode(sUntimelyFailureMode);
			if (result == null)
				result = caseAbstractSFailureModeLAnalysis(sUntimelyFailureMode);
			if (result == null)
				result = caseAbstractSPropagationElement(sUntimelyFailureMode);
			if (result == null)
				result = caseAbstractSLocalAnalysisElement(sUntimelyFailureMode);
			if (result == null)
				result = caseAbstractSSafetyConcept(sUntimelyFailureMode);
			if (result == null)
				result = caseAbstractSElement(sUntimelyFailureMode);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IESFLocalAnalysisPackage.SABSENT_FAILURE_MODE: {
			ISAbsentFailureMode sAbsentFailureMode = (ISAbsentFailureMode) theEObject;
			T result = caseSAbsentFailureMode(sAbsentFailureMode);
			if (result == null)
				result = caseAbstractSFailureModeLAnalysis(sAbsentFailureMode);
			if (result == null)
				result = caseAbstractSPropagationElement(sAbsentFailureMode);
			if (result == null)
				result = caseAbstractSLocalAnalysisElement(sAbsentFailureMode);
			if (result == null)
				result = caseAbstractSSafetyConcept(sAbsentFailureMode);
			if (result == null)
				result = caseAbstractSElement(sAbsentFailureMode);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IESFLocalAnalysisPackage.SERRONEOUS_FAILURE_MODE: {
			ISErroneousFailureMode sErroneousFailureMode = (ISErroneousFailureMode) theEObject;
			T result = caseSErroneousFailureMode(sErroneousFailureMode);
			if (result == null)
				result = caseAbstractSFailureModeLAnalysis(sErroneousFailureMode);
			if (result == null)
				result = caseAbstractSPropagationElement(sErroneousFailureMode);
			if (result == null)
				result = caseAbstractSLocalAnalysisElement(sErroneousFailureMode);
			if (result == null)
				result = caseAbstractSSafetyConcept(sErroneousFailureMode);
			if (result == null)
				result = caseAbstractSElement(sErroneousFailureMode);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IESFLocalAnalysisPackage.SLOCAL_EVENT: {
			ISLocalEvent sLocalEvent = (ISLocalEvent) theEObject;
			T result = caseSLocalEvent(sLocalEvent);
			if (result == null)
				result = caseAbstractSFailureEventLAnalysis(sLocalEvent);
			if (result == null)
				result = caseAbstractSPropagationElement(sLocalEvent);
			if (result == null)
				result = caseAbstractSLocalAnalysisElement(sLocalEvent);
			if (result == null)
				result = caseAbstractSSafetyConcept(sLocalEvent);
			if (result == null)
				result = caseAbstractSElement(sLocalEvent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract SLocal Analysis Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract SLocal Analysis Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractSLocalAnalysisElement(IAbstractSLocalAnalysisElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SPort LAnalysis</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SPort LAnalysis</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSPortLAnalysis(ISPortLAnalysis object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract SFailure Mode Owner</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract SFailure Mode Owner</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractSFailureModeOwner(IAbstractSFailureModeOwner object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract SFailure Mode LAnalysis</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract SFailure Mode LAnalysis</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractSFailureModeLAnalysis(IAbstractSFailureModeLAnalysis object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract SPropagation Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract SPropagation Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractSPropagationElement(IAbstractSPropagationElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SPropagation Link</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SPropagation Link</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSPropagationLink(ISPropagationLink object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SBlock LAnalysis</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SBlock LAnalysis</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSBlockLAnalysis(ISBlockLAnalysis object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract SFailure Event LAnalysis</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract SFailure Event LAnalysis</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractSFailureEventLAnalysis(IAbstractSFailureEventLAnalysis object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract SLogical Gate LAnalysis</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract SLogical Gate LAnalysis</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractSLogicalGateLAnalysis(IAbstractSLogicalGateLAnalysis object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SBarrier LAnalysis</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SBarrier LAnalysis</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSBarrierLAnalysis(ISBarrierLAnalysis object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SLocal Analysis</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SLocal Analysis</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSLocalAnalysis(ISLocalAnalysis object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SSystem Events Library</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SSystem Events Library</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSSystemEventsLibrary(ISSystemEventsLibrary object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SSystem Event Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SSystem Event Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSSystemEventType(ISSystemEventType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SSystem Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SSystem Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSSystemEvent(ISSystemEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SFeared Events Library</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SFeared Events Library</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSFearedEventsLibrary(ISFearedEventsLibrary object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SFeared Events Family</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SFeared Events Family</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSFearedEventsFamily(ISFearedEventsFamily object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SFeared Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SFeared Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSFearedEvent(ISFearedEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SDysfunctional Association</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SDysfunctional Association</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSDysfunctionalAssociation(ISDysfunctionalAssociation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SAnd Gate LAnalysis</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SAnd Gate LAnalysis</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSAndGateLAnalysis(ISAndGateLAnalysis object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SOr Gate LAnalysis</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SOr Gate LAnalysis</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSOrGateLAnalysis(ISOrGateLAnalysis object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SUntimely Failure Mode</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SUntimely Failure Mode</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSUntimelyFailureMode(ISUntimelyFailureMode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SAbsent Failure Mode</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SAbsent Failure Mode</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSAbsentFailureMode(ISAbsentFailureMode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SErroneous Failure Mode</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SErroneous Failure Mode</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSErroneousFailureMode(ISErroneousFailureMode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SLocal Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SLocal Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSLocalEvent(ISLocalEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract SElement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract SElement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractSElement(IAbstractSElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract SSafety Concept</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract SSafety Concept</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractSSafetyConcept(IAbstractSSafetyConcept object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract SSafety Analysis</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract SSafety Analysis</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractSSafetyAnalysis(IAbstractSSafetyAnalysis object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} // ESFLocalAnalysisSwitch
