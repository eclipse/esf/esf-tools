/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esflocalanalysis;

import org.eclipse.uml2.uml.Connector;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SDysfunctional Association</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISDysfunctionalAssociation#getSFailureModeLAnalysis <em>SFailure Mode LAnalysis</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISDysfunctionalAssociation#getBase_Connector <em>Base Connector</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISDysfunctionalAssociation#getSFailureModeOwner <em>SFailure Mode Owner</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSDysfunctionalAssociation()
 * @model annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='SDysfunctionalAssociation '"
 * @generated
 */
public interface ISDysfunctionalAssociation
		extends IAbstractSLocalAnalysisElement {

	/**
	 * Returns the value of the '<em><b>SFailure Mode LAnalysis</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SFailure Mode LAnalysis</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SFailure Mode LAnalysis</em>' reference.
	 * @see #setSFailureModeLAnalysis(IAbstractSFailureModeLAnalysis)
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSDysfunctionalAssociation_SFailureModeLAnalysis()
	 * @model required="true" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	IAbstractSFailureModeLAnalysis getSFailureModeLAnalysis();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esflocalanalysis.ISDysfunctionalAssociation#getSFailureModeLAnalysis <em>SFailure Mode LAnalysis</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>SFailure Mode LAnalysis</em>' reference.
	 * @see #getSFailureModeLAnalysis()
	 * @generated
	 */
	void setSFailureModeLAnalysis(IAbstractSFailureModeLAnalysis value);

	/**
	 * Returns the value of the '<em><b>Base Connector</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Connector</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Base Connector</em>' reference.
	 * @see #setBase_Connector(Connector)
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSDysfunctionalAssociation_Base_Connector()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Connector getBase_Connector();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esflocalanalysis.ISDysfunctionalAssociation#getBase_Connector <em>Base Connector</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Base Connector</em>' reference.
	 * @see #getBase_Connector()
	 * @generated
	 */
	void setBase_Connector(Connector value);

	/**
	 * Returns the value of the '<em><b>SFailure Mode Owner</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SFailure Mode Owner</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SFailure Mode Owner</em>' reference.
	 * @see #setSFailureModeOwner(IAbstractSFailureModeOwner)
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSDysfunctionalAssociation_SFailureModeOwner()
	 * @model required="true" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	IAbstractSFailureModeOwner getSFailureModeOwner();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esflocalanalysis.ISDysfunctionalAssociation#getSFailureModeOwner <em>SFailure Mode Owner</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>SFailure Mode Owner</em>' reference.
	 * @see #getSFailureModeOwner()
	 * @generated
	 */
	void setSFailureModeOwner(IAbstractSFailureModeOwner value);

} // ISDysfunctionalAssociation
