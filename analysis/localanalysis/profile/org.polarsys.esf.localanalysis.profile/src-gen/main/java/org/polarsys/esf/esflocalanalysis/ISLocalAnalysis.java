/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esflocalanalysis;

import org.eclipse.emf.common.util.EList;

import org.polarsys.esf.esfcore.IAbstractSSafetyAnalysis;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SLocal Analysis</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISLocalAnalysis#getBase_Package <em>Base Package</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISLocalAnalysis#getSSystemEventsLibrary <em>SSystem Events Library</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISLocalAnalysis#getSFearedEventsLibrary <em>SFeared Events Library</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISLocalAnalysis#getSBlocksLAnalysisList <em>SBlocks LAnalysis List</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSLocalAnalysis()
 * @model
 * @generated
 */
public interface ISLocalAnalysis
		extends IAbstractSSafetyAnalysis {

	/**
	 * Returns the value of the '<em><b>Base Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Package</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Base Package</em>' reference.
	 * @see #setBase_Package(org.eclipse.uml2.uml.Package)
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSLocalAnalysis_Base_Package()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	org.eclipse.uml2.uml.Package getBase_Package();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esflocalanalysis.ISLocalAnalysis#getBase_Package <em>Base Package</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Base Package</em>' reference.
	 * @see #getBase_Package()
	 * @generated
	 */
	void setBase_Package(org.eclipse.uml2.uml.Package value);

	/**
	 * Returns the value of the '<em><b>SSystem Events Library</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esflocalanalysis.ISSystemEventsLibrary#getSLocalAnalysis <em>SLocal Analysis</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SSystem Events Library</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SSystem Events Library</em>' reference.
	 * @see #setSSystemEventsLibrary(ISSystemEventsLibrary)
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSLocalAnalysis_SSystemEventsLibrary()
	 * @see org.polarsys.esf.esflocalanalysis.ISSystemEventsLibrary#getSLocalAnalysis
	 * @model opposite="sLocalAnalysis" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	ISSystemEventsLibrary getSSystemEventsLibrary();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esflocalanalysis.ISLocalAnalysis#getSSystemEventsLibrary <em>SSystem Events Library</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>SSystem Events Library</em>' reference.
	 * @see #getSSystemEventsLibrary()
	 * @generated
	 */
	void setSSystemEventsLibrary(ISSystemEventsLibrary value);

	/**
	 * Returns the value of the '<em><b>SFeared Events Library</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esflocalanalysis.ISFearedEventsLibrary#getSLocalAnalysis <em>SLocal Analysis</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SFeared Events Library</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SFeared Events Library</em>' reference.
	 * @see #setSFearedEventsLibrary(ISFearedEventsLibrary)
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSLocalAnalysis_SFearedEventsLibrary()
	 * @see org.polarsys.esf.esflocalanalysis.ISFearedEventsLibrary#getSLocalAnalysis
	 * @model opposite="sLocalAnalysis" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	ISFearedEventsLibrary getSFearedEventsLibrary();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esflocalanalysis.ISLocalAnalysis#getSFearedEventsLibrary <em>SFeared Events Library</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>SFeared Events Library</em>' reference.
	 * @see #getSFearedEventsLibrary()
	 * @generated
	 */
	void setSFearedEventsLibrary(ISFearedEventsLibrary value);

	/**
	 * Returns the value of the '<em><b>SBlocks LAnalysis List</b></em>' reference list.
	 * The list contents are of type {@link org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis}.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSLocalAnalysis <em>SLocal Analysis</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SBlocks LAnalysis List</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SBlocks LAnalysis List</em>' reference list.
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSLocalAnalysis_SBlocksLAnalysisList()
	 * @see org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSLocalAnalysis
	 * @model opposite="sLocalAnalysis" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<ISBlockLAnalysis> getSBlocksLAnalysisList();

} // ISLocalAnalysis
