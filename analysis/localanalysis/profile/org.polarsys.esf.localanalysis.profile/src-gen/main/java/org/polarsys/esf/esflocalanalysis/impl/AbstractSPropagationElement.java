/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esflocalanalysis.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.polarsys.esf.esflocalanalysis.IAbstractSPropagationElement;
import org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage;
import org.polarsys.esf.esflocalanalysis.ISPropagationLink;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract SPropagation Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.AbstractSPropagationElement#getOutSPropagationLinksList <em>Out SPropagation Links List</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.AbstractSPropagationElement#getInSPropagationLinksList <em>In SPropagation Links List</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AbstractSPropagationElement
		extends AbstractSLocalAnalysisElement
		implements IAbstractSPropagationElement {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected AbstractSPropagationElement() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IESFLocalAnalysisPackage.Literals.ABSTRACT_SPROPAGATION_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EList<ISPropagationLink> getOutSPropagationLinksList() {
		// TODO: implement this method to return the 'Out SPropagation Links List' reference list
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EList<ISPropagationLink> getInSPropagationLinksList() {
		// TODO: implement this method to return the 'In SPropagation Links List' reference list
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.ABSTRACT_SPROPAGATION_ELEMENT__OUT_SPROPAGATION_LINKS_LIST:
			return getOutSPropagationLinksList();
		case IESFLocalAnalysisPackage.ABSTRACT_SPROPAGATION_ELEMENT__IN_SPROPAGATION_LINKS_LIST:
			return getInSPropagationLinksList();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.ABSTRACT_SPROPAGATION_ELEMENT__OUT_SPROPAGATION_LINKS_LIST:
			getOutSPropagationLinksList().clear();
			getOutSPropagationLinksList().addAll((Collection<? extends ISPropagationLink>) newValue);
			return;
		case IESFLocalAnalysisPackage.ABSTRACT_SPROPAGATION_ELEMENT__IN_SPROPAGATION_LINKS_LIST:
			getInSPropagationLinksList().clear();
			getInSPropagationLinksList().addAll((Collection<? extends ISPropagationLink>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.ABSTRACT_SPROPAGATION_ELEMENT__OUT_SPROPAGATION_LINKS_LIST:
			getOutSPropagationLinksList().clear();
			return;
		case IESFLocalAnalysisPackage.ABSTRACT_SPROPAGATION_ELEMENT__IN_SPROPAGATION_LINKS_LIST:
			getInSPropagationLinksList().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.ABSTRACT_SPROPAGATION_ELEMENT__OUT_SPROPAGATION_LINKS_LIST:
			return !getOutSPropagationLinksList().isEmpty();
		case IESFLocalAnalysisPackage.ABSTRACT_SPROPAGATION_ELEMENT__IN_SPROPAGATION_LINKS_LIST:
			return !getInSPropagationLinksList().isEmpty();
		}
		return super.eIsSet(featureID);
	}

} // AbstractSPropagationElement
