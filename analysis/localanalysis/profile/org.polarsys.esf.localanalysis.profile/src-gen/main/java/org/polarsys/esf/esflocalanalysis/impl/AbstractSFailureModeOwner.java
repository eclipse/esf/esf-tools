/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esflocalanalysis.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis;
import org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeOwner;
import org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract SFailure Mode Owner</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.AbstractSFailureModeOwner#getSFailureModesLAnalysisList <em>SFailure Modes LAnalysis List</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AbstractSFailureModeOwner
		extends AbstractSLocalAnalysisElement
		implements IAbstractSFailureModeOwner {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected AbstractSFailureModeOwner() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IESFLocalAnalysisPackage.Literals.ABSTRACT_SFAILURE_MODE_OWNER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EList<IAbstractSFailureModeLAnalysis> getSFailureModesLAnalysisList() {
		// TODO: implement this method to return the 'SFailure Modes LAnalysis List' reference list
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_MODE_OWNER__SFAILURE_MODES_LANALYSIS_LIST:
			return getSFailureModesLAnalysisList();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_MODE_OWNER__SFAILURE_MODES_LANALYSIS_LIST:
			getSFailureModesLAnalysisList().clear();
			getSFailureModesLAnalysisList().addAll((Collection<? extends IAbstractSFailureModeLAnalysis>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_MODE_OWNER__SFAILURE_MODES_LANALYSIS_LIST:
			getSFailureModesLAnalysisList().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_MODE_OWNER__SFAILURE_MODES_LANALYSIS_LIST:
			return !getSFailureModesLAnalysisList().isEmpty();
		}
		return super.eIsSet(featureID);
	}

} // AbstractSFailureModeOwner
