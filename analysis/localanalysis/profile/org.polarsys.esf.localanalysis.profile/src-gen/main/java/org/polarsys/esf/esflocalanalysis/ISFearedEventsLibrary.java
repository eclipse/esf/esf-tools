/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esflocalanalysis;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SFeared Events Library</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISFearedEventsLibrary#getBase_Package <em>Base Package</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISFearedEventsLibrary#getSFearedEventsFamiliesList <em>SFeared Events Families List</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISFearedEventsLibrary#getSFearedEventsList <em>SFeared Events List</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISFearedEventsLibrary#getSLocalAnalysis <em>SLocal Analysis</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSFearedEventsLibrary()
 * @model
 * @generated
 */
public interface ISFearedEventsLibrary
		extends IAbstractSLocalAnalysisElement {

	/**
	 * Returns the value of the '<em><b>Base Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Package</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Base Package</em>' reference.
	 * @see #setBase_Package(org.eclipse.uml2.uml.Package)
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSFearedEventsLibrary_Base_Package()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	org.eclipse.uml2.uml.Package getBase_Package();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esflocalanalysis.ISFearedEventsLibrary#getBase_Package <em>Base Package</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Base Package</em>' reference.
	 * @see #getBase_Package()
	 * @generated
	 */
	void setBase_Package(org.eclipse.uml2.uml.Package value);

	/**
	 * Returns the value of the '<em><b>SFeared Events Families List</b></em>' reference list.
	 * The list contents are of type {@link org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily}.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily#getSFearedEventsLibrary <em>SFeared Events Library</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SFeared Events Families List</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SFeared Events Families List</em>' reference list.
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSFearedEventsLibrary_SFearedEventsFamiliesList()
	 * @see org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily#getSFearedEventsLibrary
	 * @model opposite="sFearedEventsLibrary" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<ISFearedEventsFamily> getSFearedEventsFamiliesList();

	/**
	 * Returns the value of the '<em><b>SFeared Events List</b></em>' reference list.
	 * The list contents are of type {@link org.polarsys.esf.esflocalanalysis.ISFearedEvent}.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esflocalanalysis.ISFearedEvent#getSFearedEventsLibrary <em>SFeared Events Library</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SFeared Events List</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SFeared Events List</em>' reference list.
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSFearedEventsLibrary_SFearedEventsList()
	 * @see org.polarsys.esf.esflocalanalysis.ISFearedEvent#getSFearedEventsLibrary
	 * @model opposite="sFearedEventsLibrary" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<ISFearedEvent> getSFearedEventsList();

	/**
	 * Returns the value of the '<em><b>SLocal Analysis</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esflocalanalysis.ISLocalAnalysis#getSFearedEventsLibrary <em>SFeared Events Library</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SLocal Analysis</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SLocal Analysis</em>' reference.
	 * @see #setSLocalAnalysis(ISLocalAnalysis)
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSFearedEventsLibrary_SLocalAnalysis()
	 * @see org.polarsys.esf.esflocalanalysis.ISLocalAnalysis#getSFearedEventsLibrary
	 * @model opposite="sFearedEventsLibrary" required="true" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	ISLocalAnalysis getSLocalAnalysis();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esflocalanalysis.ISFearedEventsLibrary#getSLocalAnalysis <em>SLocal Analysis</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>SLocal Analysis</em>' reference.
	 * @see #getSLocalAnalysis()
	 * @generated
	 */
	void setSLocalAnalysis(ISLocalAnalysis value);

} // ISFearedEventsLibrary
