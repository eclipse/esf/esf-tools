/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esflocalanalysis.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.polarsys.esf.esfarchitectureconcepts.ISBlock;

import org.polarsys.esf.esflocalanalysis.IAbstractSFailureEventLAnalysis;
import org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis;
import org.polarsys.esf.esflocalanalysis.IAbstractSLogicalGateLAnalysis;
import org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage;
import org.polarsys.esf.esflocalanalysis.ISBarrierLAnalysis;
import org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis;
import org.polarsys.esf.esflocalanalysis.ISLocalAnalysis;
import org.polarsys.esf.esflocalanalysis.ISPortLAnalysis;
import org.polarsys.esf.esflocalanalysis.ISPropagationLink;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SBlock LAnalysis</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SBlockLAnalysis#getSBlock <em>SBlock</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SBlockLAnalysis#getSFailureEventsLAnalysisList <em>SFailure Events LAnalysis List</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SBlockLAnalysis#getSPortsLAnalysisList <em>SPorts LAnalysis List</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SBlockLAnalysis#getBase_Class <em>Base Class</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SBlockLAnalysis#getSLogicalGatesLAnalysisList <em>SLogical Gates LAnalysis List</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SBlockLAnalysis#getSFailureModesLAnalysisList <em>SFailure Modes LAnalysis List</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SBlockLAnalysis#getSBarriersLAnalysisList <em>SBarriers LAnalysis List</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SBlockLAnalysis#getSLocalAnalysis <em>SLocal Analysis</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SBlockLAnalysis#getSPropagationLinksList <em>SPropagation Links List</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SBlockLAnalysis
		extends AbstractSLocalAnalysisElement
		implements ISBlockLAnalysis {

	/**
	 * The cached value of the '{@link #getSBlock() <em>SBlock</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getSBlock()
	 * @generated
	 * @ordered
	 */
	protected ISBlock sBlock;

	/**
	 * The cached value of the '{@link #getBase_Class() <em>Base Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getBase_Class()
	 * @generated
	 * @ordered
	 */
	protected org.eclipse.uml2.uml.Class base_Class;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected SBlockLAnalysis() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IESFLocalAnalysisPackage.Literals.SBLOCK_LANALYSIS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISBlock getSBlock() {
		if (sBlock != null && sBlock.eIsProxy()) {
			InternalEObject oldSBlock = (InternalEObject) sBlock;
			sBlock = (ISBlock) eResolveProxy(oldSBlock);
			if (sBlock != oldSBlock) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__SBLOCK, oldSBlock, sBlock));
			}
		}
		return sBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ISBlock basicGetSBlock() {
		return sBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setSBlock(ISBlock newSBlock) {
		ISBlock oldSBlock = sBlock;
		sBlock = newSBlock;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__SBLOCK, oldSBlock, sBlock));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EList<IAbstractSFailureEventLAnalysis> getSFailureEventsLAnalysisList() {
		// TODO: implement this method to return the 'SFailure Events LAnalysis List' reference list
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EList<ISPortLAnalysis> getSPortsLAnalysisList() {
		// TODO: implement this method to return the 'SPorts LAnalysis List' reference list
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public org.eclipse.uml2.uml.Class getBase_Class() {
		if (base_Class != null && base_Class.eIsProxy()) {
			InternalEObject oldBase_Class = (InternalEObject) base_Class;
			base_Class = (org.eclipse.uml2.uml.Class) eResolveProxy(oldBase_Class);
			if (base_Class != oldBase_Class) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__BASE_CLASS, oldBase_Class, base_Class));
			}
		}
		return base_Class;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public org.eclipse.uml2.uml.Class basicGetBase_Class() {
		return base_Class;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setBase_Class(org.eclipse.uml2.uml.Class newBase_Class) {
		org.eclipse.uml2.uml.Class oldBase_Class = base_Class;
		base_Class = newBase_Class;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__BASE_CLASS, oldBase_Class, base_Class));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EList<IAbstractSLogicalGateLAnalysis> getSLogicalGatesLAnalysisList() {
		// TODO: implement this method to return the 'SLogical Gates LAnalysis List' reference list
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EList<IAbstractSFailureModeLAnalysis> getSFailureModesLAnalysisList() {
		// TODO: implement this method to return the 'SFailure Modes LAnalysis List' reference list
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EList<ISBarrierLAnalysis> getSBarriersLAnalysisList() {
		// TODO: implement this method to return the 'SBarriers LAnalysis List' reference list
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISLocalAnalysis getSLocalAnalysis() {
		ISLocalAnalysis sLocalAnalysis = basicGetSLocalAnalysis();
		return sLocalAnalysis != null && sLocalAnalysis.eIsProxy() ? (ISLocalAnalysis) eResolveProxy((InternalEObject) sLocalAnalysis) : sLocalAnalysis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ISLocalAnalysis basicGetSLocalAnalysis() {
		// TODO: implement this method to return the 'SLocal Analysis' reference
		// -> do not perform proxy resolution
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setSLocalAnalysis(ISLocalAnalysis newSLocalAnalysis) {
		// TODO: implement this method to set the 'SLocal Analysis' reference
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EList<ISPropagationLink> getSPropagationLinksList() {
		// TODO: implement this method to return the 'SPropagation Links List' reference list
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__SBLOCK:
			if (resolve)
				return getSBlock();
			return basicGetSBlock();
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__SFAILURE_EVENTS_LANALYSIS_LIST:
			return getSFailureEventsLAnalysisList();
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__SPORTS_LANALYSIS_LIST:
			return getSPortsLAnalysisList();
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__BASE_CLASS:
			if (resolve)
				return getBase_Class();
			return basicGetBase_Class();
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__SLOGICAL_GATES_LANALYSIS_LIST:
			return getSLogicalGatesLAnalysisList();
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__SFAILURE_MODES_LANALYSIS_LIST:
			return getSFailureModesLAnalysisList();
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__SBARRIERS_LANALYSIS_LIST:
			return getSBarriersLAnalysisList();
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__SLOCAL_ANALYSIS:
			if (resolve)
				return getSLocalAnalysis();
			return basicGetSLocalAnalysis();
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__SPROPAGATION_LINKS_LIST:
			return getSPropagationLinksList();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__SBLOCK:
			setSBlock((ISBlock) newValue);
			return;
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__SFAILURE_EVENTS_LANALYSIS_LIST:
			getSFailureEventsLAnalysisList().clear();
			getSFailureEventsLAnalysisList().addAll((Collection<? extends IAbstractSFailureEventLAnalysis>) newValue);
			return;
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__SPORTS_LANALYSIS_LIST:
			getSPortsLAnalysisList().clear();
			getSPortsLAnalysisList().addAll((Collection<? extends ISPortLAnalysis>) newValue);
			return;
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__BASE_CLASS:
			setBase_Class((org.eclipse.uml2.uml.Class) newValue);
			return;
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__SLOGICAL_GATES_LANALYSIS_LIST:
			getSLogicalGatesLAnalysisList().clear();
			getSLogicalGatesLAnalysisList().addAll((Collection<? extends IAbstractSLogicalGateLAnalysis>) newValue);
			return;
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__SFAILURE_MODES_LANALYSIS_LIST:
			getSFailureModesLAnalysisList().clear();
			getSFailureModesLAnalysisList().addAll((Collection<? extends IAbstractSFailureModeLAnalysis>) newValue);
			return;
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__SBARRIERS_LANALYSIS_LIST:
			getSBarriersLAnalysisList().clear();
			getSBarriersLAnalysisList().addAll((Collection<? extends ISBarrierLAnalysis>) newValue);
			return;
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__SLOCAL_ANALYSIS:
			setSLocalAnalysis((ISLocalAnalysis) newValue);
			return;
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__SPROPAGATION_LINKS_LIST:
			getSPropagationLinksList().clear();
			getSPropagationLinksList().addAll((Collection<? extends ISPropagationLink>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__SBLOCK:
			setSBlock((ISBlock) null);
			return;
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__SFAILURE_EVENTS_LANALYSIS_LIST:
			getSFailureEventsLAnalysisList().clear();
			return;
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__SPORTS_LANALYSIS_LIST:
			getSPortsLAnalysisList().clear();
			return;
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__BASE_CLASS:
			setBase_Class((org.eclipse.uml2.uml.Class) null);
			return;
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__SLOGICAL_GATES_LANALYSIS_LIST:
			getSLogicalGatesLAnalysisList().clear();
			return;
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__SFAILURE_MODES_LANALYSIS_LIST:
			getSFailureModesLAnalysisList().clear();
			return;
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__SBARRIERS_LANALYSIS_LIST:
			getSBarriersLAnalysisList().clear();
			return;
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__SLOCAL_ANALYSIS:
			setSLocalAnalysis((ISLocalAnalysis) null);
			return;
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__SPROPAGATION_LINKS_LIST:
			getSPropagationLinksList().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__SBLOCK:
			return sBlock != null;
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__SFAILURE_EVENTS_LANALYSIS_LIST:
			return !getSFailureEventsLAnalysisList().isEmpty();
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__SPORTS_LANALYSIS_LIST:
			return !getSPortsLAnalysisList().isEmpty();
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__BASE_CLASS:
			return base_Class != null;
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__SLOGICAL_GATES_LANALYSIS_LIST:
			return !getSLogicalGatesLAnalysisList().isEmpty();
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__SFAILURE_MODES_LANALYSIS_LIST:
			return !getSFailureModesLAnalysisList().isEmpty();
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__SBARRIERS_LANALYSIS_LIST:
			return !getSBarriersLAnalysisList().isEmpty();
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__SLOCAL_ANALYSIS:
			return basicGetSLocalAnalysis() != null;
		case IESFLocalAnalysisPackage.SBLOCK_LANALYSIS__SPROPAGATION_LINKS_LIST:
			return !getSPropagationLinksList().isEmpty();
		}
		return super.eIsSet(featureID);
	}

} // SBlockLAnalysis
