/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esflocalanalysis;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SFeared Events Family</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily#getBase_Class <em>Base Class</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily#getSFearedEventsList <em>SFeared Events List</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily#isSelected <em>Is Selected</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily#getSubFamiliesList <em>Sub Families List</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily#getAllSFearedEventsList <em>All SFeared Events List</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily#getSFailureModesLAnalysisList <em>SFailure Modes LAnalysis List</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily#getSFearedEventsLibrary <em>SFeared Events Library</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSFearedEventsFamily()
 * @model
 * @generated
 */
public interface ISFearedEventsFamily
		extends IAbstractSLocalAnalysisElement {

	/**
	 * Returns the value of the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Base Class</em>' reference.
	 * @see #setBase_Class(org.eclipse.uml2.uml.Class)
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSFearedEventsFamily_Base_Class()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	org.eclipse.uml2.uml.Class getBase_Class();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily#getBase_Class <em>Base Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Base Class</em>' reference.
	 * @see #getBase_Class()
	 * @generated
	 */
	void setBase_Class(org.eclipse.uml2.uml.Class value);

	/**
	 * Returns the value of the '<em><b>SFeared Events List</b></em>' reference list.
	 * The list contents are of type {@link org.polarsys.esf.esflocalanalysis.ISFearedEvent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SFeared Events List</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SFeared Events List</em>' reference list.
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSFearedEventsFamily_SFearedEventsList()
	 * @model transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<ISFearedEvent> getSFearedEventsList();

	/**
	 * Returns the value of the '<em><b>Is Selected</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Selected</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Is Selected</em>' attribute.
	 * @see #setIsSelected(boolean)
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSFearedEventsFamily_IsSelected()
	 * @model default="false" dataType="org.eclipse.uml2.types.Boolean" required="true" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	boolean isSelected();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily#isSelected <em>Is Selected</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Is Selected</em>' attribute.
	 * @see #isSelected()
	 * @generated
	 */
	void setIsSelected(boolean value);

	/**
	 * Returns the value of the '<em><b>Sub Families List</b></em>' reference list.
	 * The list contents are of type {@link org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Families List</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Sub Families List</em>' reference list.
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSFearedEventsFamily_SubFamiliesList()
	 * @model transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<ISFearedEventsFamily> getSubFamiliesList();

	/**
	 * Returns the value of the '<em><b>All SFeared Events List</b></em>' reference list.
	 * The list contents are of type {@link org.polarsys.esf.esflocalanalysis.ISFearedEvent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All SFeared Events List</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>All SFeared Events List</em>' reference list.
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSFearedEventsFamily_AllSFearedEventsList()
	 * @model transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<ISFearedEvent> getAllSFearedEventsList();

	/**
	 * Returns the value of the '<em><b>SFailure Modes LAnalysis List</b></em>' reference list.
	 * The list contents are of type {@link org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SFailure Modes LAnalysis List</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SFailure Modes LAnalysis List</em>' reference list.
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSFearedEventsFamily_SFailureModesLAnalysisList()
	 * @model transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<IAbstractSFailureModeLAnalysis> getSFailureModesLAnalysisList();

	/**
	 * Returns the value of the '<em><b>SFeared Events Library</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esflocalanalysis.ISFearedEventsLibrary#getSFearedEventsFamiliesList <em>SFeared Events Families List</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SFeared Events Library</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SFeared Events Library</em>' reference.
	 * @see #setSFearedEventsLibrary(ISFearedEventsLibrary)
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSFearedEventsFamily_SFearedEventsLibrary()
	 * @see org.polarsys.esf.esflocalanalysis.ISFearedEventsLibrary#getSFearedEventsFamiliesList
	 * @model opposite="sFearedEventsFamiliesList" required="true" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	ISFearedEventsLibrary getSFearedEventsLibrary();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily#getSFearedEventsLibrary <em>SFeared Events Library</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>SFeared Events Library</em>' reference.
	 * @see #getSFearedEventsLibrary()
	 * @generated
	 */
	void setSFearedEventsLibrary(ISFearedEventsLibrary value);

} // ISFearedEventsFamily
