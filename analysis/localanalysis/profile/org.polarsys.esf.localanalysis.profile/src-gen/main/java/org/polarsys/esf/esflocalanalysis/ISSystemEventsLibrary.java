/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esflocalanalysis;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SSystem Events Library</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISSystemEventsLibrary#getBase_Package <em>Base Package</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISSystemEventsLibrary#getSSystemEventTypesList <em>SSystem Event Types List</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISSystemEventsLibrary#getSLocalAnalysis <em>SLocal Analysis</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSSystemEventsLibrary()
 * @model
 * @generated
 */
public interface ISSystemEventsLibrary
		extends IAbstractSLocalAnalysisElement {

	/**
	 * Returns the value of the '<em><b>Base Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Package</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Base Package</em>' reference.
	 * @see #setBase_Package(org.eclipse.uml2.uml.Package)
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSSystemEventsLibrary_Base_Package()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	org.eclipse.uml2.uml.Package getBase_Package();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esflocalanalysis.ISSystemEventsLibrary#getBase_Package <em>Base Package</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Base Package</em>' reference.
	 * @see #getBase_Package()
	 * @generated
	 */
	void setBase_Package(org.eclipse.uml2.uml.Package value);

	/**
	 * Returns the value of the '<em><b>SSystem Event Types List</b></em>' reference list.
	 * The list contents are of type {@link org.polarsys.esf.esflocalanalysis.ISSystemEventType}.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esflocalanalysis.ISSystemEventType#getSSystemEventsLibrary <em>SSystem Events Library</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SSystem Event Types List</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SSystem Event Types List</em>' reference list.
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSSystemEventsLibrary_SSystemEventTypesList()
	 * @see org.polarsys.esf.esflocalanalysis.ISSystemEventType#getSSystemEventsLibrary
	 * @model opposite="sSystemEventsLibrary" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<ISSystemEventType> getSSystemEventTypesList();

	/**
	 * Returns the value of the '<em><b>SLocal Analysis</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esflocalanalysis.ISLocalAnalysis#getSSystemEventsLibrary <em>SSystem Events Library</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SLocal Analysis</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SLocal Analysis</em>' reference.
	 * @see #setSLocalAnalysis(ISLocalAnalysis)
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSSystemEventsLibrary_SLocalAnalysis()
	 * @see org.polarsys.esf.esflocalanalysis.ISLocalAnalysis#getSSystemEventsLibrary
	 * @model opposite="sSystemEventsLibrary" required="true" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	ISLocalAnalysis getSLocalAnalysis();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esflocalanalysis.ISSystemEventsLibrary#getSLocalAnalysis <em>SLocal Analysis</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>SLocal Analysis</em>' reference.
	 * @see #getSLocalAnalysis()
	 * @generated
	 */
	void setSLocalAnalysis(ISLocalAnalysis value);

} // ISSystemEventsLibrary
