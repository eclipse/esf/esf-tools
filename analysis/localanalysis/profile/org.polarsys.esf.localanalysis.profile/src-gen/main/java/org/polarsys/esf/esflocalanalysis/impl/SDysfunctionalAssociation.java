/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esflocalanalysis.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.uml2.uml.Connector;

import org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis;
import org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeOwner;
import org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage;
import org.polarsys.esf.esflocalanalysis.ISDysfunctionalAssociation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SDysfunctional Association</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SDysfunctionalAssociation#getSFailureModeLAnalysis <em>SFailure Mode LAnalysis</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SDysfunctionalAssociation#getBase_Connector <em>Base Connector</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.SDysfunctionalAssociation#getSFailureModeOwner <em>SFailure Mode Owner</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SDysfunctionalAssociation
		extends AbstractSLocalAnalysisElement
		implements ISDysfunctionalAssociation {

	/**
	 * The cached value of the '{@link #getBase_Connector() <em>Base Connector</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getBase_Connector()
	 * @generated
	 * @ordered
	 */
	protected Connector base_Connector;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected SDysfunctionalAssociation() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IESFLocalAnalysisPackage.Literals.SDYSFUNCTIONAL_ASSOCIATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public IAbstractSFailureModeLAnalysis getSFailureModeLAnalysis() {
		IAbstractSFailureModeLAnalysis sFailureModeLAnalysis = basicGetSFailureModeLAnalysis();
		return sFailureModeLAnalysis != null && sFailureModeLAnalysis.eIsProxy() ? (IAbstractSFailureModeLAnalysis) eResolveProxy((InternalEObject) sFailureModeLAnalysis) : sFailureModeLAnalysis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public IAbstractSFailureModeLAnalysis basicGetSFailureModeLAnalysis() {
		// TODO: implement this method to return the 'SFailure Mode LAnalysis' reference
		// -> do not perform proxy resolution
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setSFailureModeLAnalysis(IAbstractSFailureModeLAnalysis newSFailureModeLAnalysis) {
		// TODO: implement this method to set the 'SFailure Mode LAnalysis' reference
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Connector getBase_Connector() {
		if (base_Connector != null && base_Connector.eIsProxy()) {
			InternalEObject oldBase_Connector = (InternalEObject) base_Connector;
			base_Connector = (Connector) eResolveProxy(oldBase_Connector);
			if (base_Connector != oldBase_Connector) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IESFLocalAnalysisPackage.SDYSFUNCTIONAL_ASSOCIATION__BASE_CONNECTOR, oldBase_Connector, base_Connector));
			}
		}
		return base_Connector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Connector basicGetBase_Connector() {
		return base_Connector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setBase_Connector(Connector newBase_Connector) {
		Connector oldBase_Connector = base_Connector;
		base_Connector = newBase_Connector;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFLocalAnalysisPackage.SDYSFUNCTIONAL_ASSOCIATION__BASE_CONNECTOR, oldBase_Connector, base_Connector));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public IAbstractSFailureModeOwner getSFailureModeOwner() {
		IAbstractSFailureModeOwner sFailureModeOwner = basicGetSFailureModeOwner();
		return sFailureModeOwner != null && sFailureModeOwner.eIsProxy() ? (IAbstractSFailureModeOwner) eResolveProxy((InternalEObject) sFailureModeOwner) : sFailureModeOwner;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public IAbstractSFailureModeOwner basicGetSFailureModeOwner() {
		// TODO: implement this method to return the 'SFailure Mode Owner' reference
		// -> do not perform proxy resolution
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setSFailureModeOwner(IAbstractSFailureModeOwner newSFailureModeOwner) {
		// TODO: implement this method to set the 'SFailure Mode Owner' reference
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.SDYSFUNCTIONAL_ASSOCIATION__SFAILURE_MODE_LANALYSIS:
			if (resolve)
				return getSFailureModeLAnalysis();
			return basicGetSFailureModeLAnalysis();
		case IESFLocalAnalysisPackage.SDYSFUNCTIONAL_ASSOCIATION__BASE_CONNECTOR:
			if (resolve)
				return getBase_Connector();
			return basicGetBase_Connector();
		case IESFLocalAnalysisPackage.SDYSFUNCTIONAL_ASSOCIATION__SFAILURE_MODE_OWNER:
			if (resolve)
				return getSFailureModeOwner();
			return basicGetSFailureModeOwner();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.SDYSFUNCTIONAL_ASSOCIATION__SFAILURE_MODE_LANALYSIS:
			setSFailureModeLAnalysis((IAbstractSFailureModeLAnalysis) newValue);
			return;
		case IESFLocalAnalysisPackage.SDYSFUNCTIONAL_ASSOCIATION__BASE_CONNECTOR:
			setBase_Connector((Connector) newValue);
			return;
		case IESFLocalAnalysisPackage.SDYSFUNCTIONAL_ASSOCIATION__SFAILURE_MODE_OWNER:
			setSFailureModeOwner((IAbstractSFailureModeOwner) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.SDYSFUNCTIONAL_ASSOCIATION__SFAILURE_MODE_LANALYSIS:
			setSFailureModeLAnalysis((IAbstractSFailureModeLAnalysis) null);
			return;
		case IESFLocalAnalysisPackage.SDYSFUNCTIONAL_ASSOCIATION__BASE_CONNECTOR:
			setBase_Connector((Connector) null);
			return;
		case IESFLocalAnalysisPackage.SDYSFUNCTIONAL_ASSOCIATION__SFAILURE_MODE_OWNER:
			setSFailureModeOwner((IAbstractSFailureModeOwner) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.SDYSFUNCTIONAL_ASSOCIATION__SFAILURE_MODE_LANALYSIS:
			return basicGetSFailureModeLAnalysis() != null;
		case IESFLocalAnalysisPackage.SDYSFUNCTIONAL_ASSOCIATION__BASE_CONNECTOR:
			return base_Connector != null;
		case IESFLocalAnalysisPackage.SDYSFUNCTIONAL_ASSOCIATION__SFAILURE_MODE_OWNER:
			return basicGetSFailureModeOwner() != null;
		}
		return super.eIsSet(featureID);
	}

} // SDysfunctionalAssociation
