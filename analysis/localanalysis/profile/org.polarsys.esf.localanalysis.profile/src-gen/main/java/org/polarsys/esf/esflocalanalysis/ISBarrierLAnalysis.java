/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esflocalanalysis;

import org.eclipse.uml2.uml.Property;

import org.polarsys.esf.esfsafetyconcepts.srecommendations.ISBarrier;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SBarrier LAnalysis</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISBarrierLAnalysis#getSBarrier <em>SBarrier</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISBarrierLAnalysis#getBase_Property <em>Base Property</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.ISBarrierLAnalysis#getSBlockLAnalysis <em>SBlock LAnalysis</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSBarrierLAnalysis()
 * @model
 * @generated
 */
public interface ISBarrierLAnalysis
		extends IAbstractSFailureModeOwner {

	/**
	 * Returns the value of the '<em><b>SBarrier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SBarrier</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SBarrier</em>' reference.
	 * @see #setSBarrier(ISBarrier)
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSBarrierLAnalysis_SBarrier()
	 * @model ordered="false"
	 * @generated
	 */
	ISBarrier getSBarrier();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esflocalanalysis.ISBarrierLAnalysis#getSBarrier <em>SBarrier</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>SBarrier</em>' reference.
	 * @see #getSBarrier()
	 * @generated
	 */
	void setSBarrier(ISBarrier value);

	/**
	 * Returns the value of the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Property</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Base Property</em>' reference.
	 * @see #setBase_Property(Property)
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSBarrierLAnalysis_Base_Property()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Property getBase_Property();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esflocalanalysis.ISBarrierLAnalysis#getBase_Property <em>Base Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Base Property</em>' reference.
	 * @see #getBase_Property()
	 * @generated
	 */
	void setBase_Property(Property value);

	/**
	 * Returns the value of the '<em><b>SBlock LAnalysis</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSBarriersLAnalysisList <em>SBarriers LAnalysis List</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SBlock LAnalysis</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SBlock LAnalysis</em>' reference.
	 * @see #setSBlockLAnalysis(ISBlockLAnalysis)
	 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage#getSBarrierLAnalysis_SBlockLAnalysis()
	 * @see org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSBarriersLAnalysisList
	 * @model opposite="sBarriersLAnalysisList" required="true" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	ISBlockLAnalysis getSBlockLAnalysis();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esflocalanalysis.ISBarrierLAnalysis#getSBlockLAnalysis <em>SBlock LAnalysis</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>SBlock LAnalysis</em>' reference.
	 * @see #getSBlockLAnalysis()
	 * @generated
	 */
	void setSBlockLAnalysis(ISBlockLAnalysis value);

} // ISBarrierLAnalysis
