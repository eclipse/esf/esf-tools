/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esflocalanalysis;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.polarsys.esf.esfcore.IESFCorePackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each operation of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * 
 * @see org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='ESFLocalAnalysis'"
 * @generated
 */
public interface IESFLocalAnalysisPackage
		extends EPackage {

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	String eNAME = "esflocalanalysis"; //$NON-NLS-1$

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	String eNS_URI = "http://www.polarsys.org/esf/0.7.0/ESFLocalAnalysis"; //$NON-NLS-1$

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	String eNS_PREFIX = "ESFLocalAnalysis"; //$NON-NLS-1$

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	IESFLocalAnalysisPackage eINSTANCE = org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage.init();

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esflocalanalysis.impl.AbstractSLocalAnalysisElement <em>Abstract SLocal Analysis Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.polarsys.esf.esflocalanalysis.impl.AbstractSLocalAnalysisElement
	 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getAbstractSLocalAnalysisElement()
	 * @generated
	 */
	int ABSTRACT_SLOCAL_ANALYSIS_ELEMENT = 0;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__UUID = IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT__UUID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__NAME = IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__DESCRIPTION = IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__SARCHITECTURE_ELEMENTS_LIST = IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT__SARCHITECTURE_ELEMENTS_LIST;

	/**
	 * The number of structural features of the '<em>Abstract SLocal Analysis Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT = IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Abstract SLocal Analysis Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_OPERATION_COUNT = IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esflocalanalysis.impl.AbstractSFailureModeOwner <em>Abstract SFailure Mode Owner</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.polarsys.esf.esflocalanalysis.impl.AbstractSFailureModeOwner
	 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getAbstractSFailureModeOwner()
	 * @generated
	 */
	int ABSTRACT_SFAILURE_MODE_OWNER = 2;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SFAILURE_MODE_OWNER__UUID = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SFAILURE_MODE_OWNER__NAME = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SFAILURE_MODE_OWNER__DESCRIPTION = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SFAILURE_MODE_OWNER__SARCHITECTURE_ELEMENTS_LIST = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__SARCHITECTURE_ELEMENTS_LIST;

	/**
	 * The feature id for the '<em><b>SFailure Modes LAnalysis List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SFAILURE_MODE_OWNER__SFAILURE_MODES_LANALYSIS_LIST = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Abstract SFailure Mode Owner</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SFAILURE_MODE_OWNER_FEATURE_COUNT = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Abstract SFailure Mode Owner</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SFAILURE_MODE_OWNER_OPERATION_COUNT = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esflocalanalysis.impl.SPortLAnalysis <em>SPort LAnalysis</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.polarsys.esf.esflocalanalysis.impl.SPortLAnalysis
	 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getSPortLAnalysis()
	 * @generated
	 */
	int SPORT_LANALYSIS = 1;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT_LANALYSIS__UUID = ABSTRACT_SFAILURE_MODE_OWNER__UUID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT_LANALYSIS__NAME = ABSTRACT_SFAILURE_MODE_OWNER__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT_LANALYSIS__DESCRIPTION = ABSTRACT_SFAILURE_MODE_OWNER__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT_LANALYSIS__SARCHITECTURE_ELEMENTS_LIST = ABSTRACT_SFAILURE_MODE_OWNER__SARCHITECTURE_ELEMENTS_LIST;

	/**
	 * The feature id for the '<em><b>SFailure Modes LAnalysis List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT_LANALYSIS__SFAILURE_MODES_LANALYSIS_LIST = ABSTRACT_SFAILURE_MODE_OWNER__SFAILURE_MODES_LANALYSIS_LIST;

	/**
	 * The feature id for the '<em><b>SBlock LAnalysis</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT_LANALYSIS__SBLOCK_LANALYSIS = ABSTRACT_SFAILURE_MODE_OWNER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>SPort</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT_LANALYSIS__SPORT = ABSTRACT_SFAILURE_MODE_OWNER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Base Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT_LANALYSIS__BASE_PORT = ABSTRACT_SFAILURE_MODE_OWNER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>SDirection Manual</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT_LANALYSIS__SDIRECTION_MANUAL = ABSTRACT_SFAILURE_MODE_OWNER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>SDirection LAnalysis</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT_LANALYSIS__SDIRECTION_LANALYSIS = ABSTRACT_SFAILURE_MODE_OWNER_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>SPort LAnalysis</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT_LANALYSIS_FEATURE_COUNT = ABSTRACT_SFAILURE_MODE_OWNER_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>SPort LAnalysis</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT_LANALYSIS_OPERATION_COUNT = ABSTRACT_SFAILURE_MODE_OWNER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esflocalanalysis.impl.AbstractSPropagationElement <em>Abstract SPropagation Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.polarsys.esf.esflocalanalysis.impl.AbstractSPropagationElement
	 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getAbstractSPropagationElement()
	 * @generated
	 */
	int ABSTRACT_SPROPAGATION_ELEMENT = 4;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SPROPAGATION_ELEMENT__UUID = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SPROPAGATION_ELEMENT__NAME = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SPROPAGATION_ELEMENT__DESCRIPTION = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SPROPAGATION_ELEMENT__SARCHITECTURE_ELEMENTS_LIST = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__SARCHITECTURE_ELEMENTS_LIST;

	/**
	 * The feature id for the '<em><b>Out SPropagation Links List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SPROPAGATION_ELEMENT__OUT_SPROPAGATION_LINKS_LIST = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>In SPropagation Links List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SPROPAGATION_ELEMENT__IN_SPROPAGATION_LINKS_LIST = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Abstract SPropagation Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SPROPAGATION_ELEMENT_FEATURE_COUNT = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Abstract SPropagation Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SPROPAGATION_ELEMENT_OPERATION_COUNT = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esflocalanalysis.impl.AbstractSFailureModeLAnalysis <em>Abstract SFailure Mode LAnalysis</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.polarsys.esf.esflocalanalysis.impl.AbstractSFailureModeLAnalysis
	 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getAbstractSFailureModeLAnalysis()
	 * @generated
	 */
	int ABSTRACT_SFAILURE_MODE_LANALYSIS = 3;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SFAILURE_MODE_LANALYSIS__UUID = ABSTRACT_SPROPAGATION_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SFAILURE_MODE_LANALYSIS__NAME = ABSTRACT_SPROPAGATION_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SFAILURE_MODE_LANALYSIS__DESCRIPTION = ABSTRACT_SPROPAGATION_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SFAILURE_MODE_LANALYSIS__SARCHITECTURE_ELEMENTS_LIST = ABSTRACT_SPROPAGATION_ELEMENT__SARCHITECTURE_ELEMENTS_LIST;

	/**
	 * The feature id for the '<em><b>Out SPropagation Links List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SFAILURE_MODE_LANALYSIS__OUT_SPROPAGATION_LINKS_LIST = ABSTRACT_SPROPAGATION_ELEMENT__OUT_SPROPAGATION_LINKS_LIST;

	/**
	 * The feature id for the '<em><b>In SPropagation Links List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SFAILURE_MODE_LANALYSIS__IN_SPROPAGATION_LINKS_LIST = ABSTRACT_SPROPAGATION_ELEMENT__IN_SPROPAGATION_LINKS_LIST;

	/**
	 * The feature id for the '<em><b>SBlock LAnalysis</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SFAILURE_MODE_LANALYSIS__SBLOCK_LANALYSIS = ABSTRACT_SPROPAGATION_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>SFailure Mode</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SFAILURE_MODE_LANALYSIS__SFAILURE_MODE = ABSTRACT_SPROPAGATION_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>SFeared Events Families List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SFAILURE_MODE_LANALYSIS__SFEARED_EVENTS_FAMILIES_LIST = ABSTRACT_SPROPAGATION_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>SFeared Events List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SFAILURE_MODE_LANALYSIS__SFEARED_EVENTS_LIST = ABSTRACT_SPROPAGATION_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SFAILURE_MODE_LANALYSIS__OWNER = ABSTRACT_SPROPAGATION_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Abstract SFailure Mode LAnalysis</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SFAILURE_MODE_LANALYSIS_FEATURE_COUNT = ABSTRACT_SPROPAGATION_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Abstract SFailure Mode LAnalysis</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SFAILURE_MODE_LANALYSIS_OPERATION_COUNT = ABSTRACT_SPROPAGATION_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esflocalanalysis.impl.SPropagationLink <em>SPropagation Link</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.polarsys.esf.esflocalanalysis.impl.SPropagationLink
	 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getSPropagationLink()
	 * @generated
	 */
	int SPROPAGATION_LINK = 5;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPROPAGATION_LINK__UUID = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPROPAGATION_LINK__NAME = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPROPAGATION_LINK__DESCRIPTION = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPROPAGATION_LINK__SARCHITECTURE_ELEMENTS_LIST = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__SARCHITECTURE_ELEMENTS_LIST;

	/**
	 * The feature id for the '<em><b>Element Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPROPAGATION_LINK__ELEMENT_TARGET = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>SBlock LAnalysis</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPROPAGATION_LINK__SBLOCK_LANALYSIS = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Base Dependency</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPROPAGATION_LINK__BASE_DEPENDENCY = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Element Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPROPAGATION_LINK__ELEMENT_SOURCE = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>SPropagation Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPROPAGATION_LINK_FEATURE_COUNT = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>SPropagation Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPROPAGATION_LINK_OPERATION_COUNT = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esflocalanalysis.impl.SBlockLAnalysis <em>SBlock LAnalysis</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.polarsys.esf.esflocalanalysis.impl.SBlockLAnalysis
	 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getSBlockLAnalysis()
	 * @generated
	 */
	int SBLOCK_LANALYSIS = 6;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBLOCK_LANALYSIS__UUID = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBLOCK_LANALYSIS__NAME = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBLOCK_LANALYSIS__DESCRIPTION = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBLOCK_LANALYSIS__SARCHITECTURE_ELEMENTS_LIST = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__SARCHITECTURE_ELEMENTS_LIST;

	/**
	 * The feature id for the '<em><b>SBlock</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBLOCK_LANALYSIS__SBLOCK = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>SFailure Events LAnalysis List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBLOCK_LANALYSIS__SFAILURE_EVENTS_LANALYSIS_LIST = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>SPorts LAnalysis List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBLOCK_LANALYSIS__SPORTS_LANALYSIS_LIST = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBLOCK_LANALYSIS__BASE_CLASS = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>SLogical Gates LAnalysis List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBLOCK_LANALYSIS__SLOGICAL_GATES_LANALYSIS_LIST = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>SFailure Modes LAnalysis List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBLOCK_LANALYSIS__SFAILURE_MODES_LANALYSIS_LIST = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>SBarriers LAnalysis List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBLOCK_LANALYSIS__SBARRIERS_LANALYSIS_LIST = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>SLocal Analysis</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBLOCK_LANALYSIS__SLOCAL_ANALYSIS = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>SPropagation Links List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBLOCK_LANALYSIS__SPROPAGATION_LINKS_LIST = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The number of structural features of the '<em>SBlock LAnalysis</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBLOCK_LANALYSIS_FEATURE_COUNT = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 9;

	/**
	 * The number of operations of the '<em>SBlock LAnalysis</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBLOCK_LANALYSIS_OPERATION_COUNT = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esflocalanalysis.impl.AbstractSFailureEventLAnalysis <em>Abstract SFailure Event LAnalysis</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.polarsys.esf.esflocalanalysis.impl.AbstractSFailureEventLAnalysis
	 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getAbstractSFailureEventLAnalysis()
	 * @generated
	 */
	int ABSTRACT_SFAILURE_EVENT_LANALYSIS = 7;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SFAILURE_EVENT_LANALYSIS__UUID = ABSTRACT_SPROPAGATION_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SFAILURE_EVENT_LANALYSIS__NAME = ABSTRACT_SPROPAGATION_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SFAILURE_EVENT_LANALYSIS__DESCRIPTION = ABSTRACT_SPROPAGATION_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SFAILURE_EVENT_LANALYSIS__SARCHITECTURE_ELEMENTS_LIST = ABSTRACT_SPROPAGATION_ELEMENT__SARCHITECTURE_ELEMENTS_LIST;

	/**
	 * The feature id for the '<em><b>Out SPropagation Links List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SFAILURE_EVENT_LANALYSIS__OUT_SPROPAGATION_LINKS_LIST = ABSTRACT_SPROPAGATION_ELEMENT__OUT_SPROPAGATION_LINKS_LIST;

	/**
	 * The feature id for the '<em><b>In SPropagation Links List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SFAILURE_EVENT_LANALYSIS__IN_SPROPAGATION_LINKS_LIST = ABSTRACT_SPROPAGATION_ELEMENT__IN_SPROPAGATION_LINKS_LIST;

	/**
	 * The feature id for the '<em><b>SFailure Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SFAILURE_EVENT_LANALYSIS__SFAILURE_EVENT = ABSTRACT_SPROPAGATION_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>SBlock LAnalysis</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SFAILURE_EVENT_LANALYSIS__SBLOCK_LANALYSIS = ABSTRACT_SPROPAGATION_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Abstract SFailure Event LAnalysis</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SFAILURE_EVENT_LANALYSIS_FEATURE_COUNT = ABSTRACT_SPROPAGATION_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Abstract SFailure Event LAnalysis</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SFAILURE_EVENT_LANALYSIS_OPERATION_COUNT = ABSTRACT_SPROPAGATION_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esflocalanalysis.impl.AbstractSLogicalGateLAnalysis <em>Abstract SLogical Gate LAnalysis</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.polarsys.esf.esflocalanalysis.impl.AbstractSLogicalGateLAnalysis
	 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getAbstractSLogicalGateLAnalysis()
	 * @generated
	 */
	int ABSTRACT_SLOGICAL_GATE_LANALYSIS = 8;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SLOGICAL_GATE_LANALYSIS__UUID = ABSTRACT_SPROPAGATION_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SLOGICAL_GATE_LANALYSIS__NAME = ABSTRACT_SPROPAGATION_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SLOGICAL_GATE_LANALYSIS__DESCRIPTION = ABSTRACT_SPROPAGATION_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SLOGICAL_GATE_LANALYSIS__SARCHITECTURE_ELEMENTS_LIST = ABSTRACT_SPROPAGATION_ELEMENT__SARCHITECTURE_ELEMENTS_LIST;

	/**
	 * The feature id for the '<em><b>Out SPropagation Links List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SLOGICAL_GATE_LANALYSIS__OUT_SPROPAGATION_LINKS_LIST = ABSTRACT_SPROPAGATION_ELEMENT__OUT_SPROPAGATION_LINKS_LIST;

	/**
	 * The feature id for the '<em><b>In SPropagation Links List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SLOGICAL_GATE_LANALYSIS__IN_SPROPAGATION_LINKS_LIST = ABSTRACT_SPROPAGATION_ELEMENT__IN_SPROPAGATION_LINKS_LIST;

	/**
	 * The feature id for the '<em><b>SBlock LAnalysis</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SLOGICAL_GATE_LANALYSIS__SBLOCK_LANALYSIS = ABSTRACT_SPROPAGATION_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Abstract SLogical Gate LAnalysis</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SLOGICAL_GATE_LANALYSIS_FEATURE_COUNT = ABSTRACT_SPROPAGATION_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Abstract SLogical Gate LAnalysis</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SLOGICAL_GATE_LANALYSIS_OPERATION_COUNT = ABSTRACT_SPROPAGATION_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esflocalanalysis.impl.SBarrierLAnalysis <em>SBarrier LAnalysis</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.polarsys.esf.esflocalanalysis.impl.SBarrierLAnalysis
	 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getSBarrierLAnalysis()
	 * @generated
	 */
	int SBARRIER_LANALYSIS = 9;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBARRIER_LANALYSIS__UUID = ABSTRACT_SFAILURE_MODE_OWNER__UUID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBARRIER_LANALYSIS__NAME = ABSTRACT_SFAILURE_MODE_OWNER__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBARRIER_LANALYSIS__DESCRIPTION = ABSTRACT_SFAILURE_MODE_OWNER__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBARRIER_LANALYSIS__SARCHITECTURE_ELEMENTS_LIST = ABSTRACT_SFAILURE_MODE_OWNER__SARCHITECTURE_ELEMENTS_LIST;

	/**
	 * The feature id for the '<em><b>SFailure Modes LAnalysis List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBARRIER_LANALYSIS__SFAILURE_MODES_LANALYSIS_LIST = ABSTRACT_SFAILURE_MODE_OWNER__SFAILURE_MODES_LANALYSIS_LIST;

	/**
	 * The feature id for the '<em><b>SBarrier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBARRIER_LANALYSIS__SBARRIER = ABSTRACT_SFAILURE_MODE_OWNER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBARRIER_LANALYSIS__BASE_PROPERTY = ABSTRACT_SFAILURE_MODE_OWNER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>SBlock LAnalysis</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBARRIER_LANALYSIS__SBLOCK_LANALYSIS = ABSTRACT_SFAILURE_MODE_OWNER_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>SBarrier LAnalysis</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBARRIER_LANALYSIS_FEATURE_COUNT = ABSTRACT_SFAILURE_MODE_OWNER_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>SBarrier LAnalysis</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBARRIER_LANALYSIS_OPERATION_COUNT = ABSTRACT_SFAILURE_MODE_OWNER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esflocalanalysis.impl.SLocalAnalysis <em>SLocal Analysis</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.polarsys.esf.esflocalanalysis.impl.SLocalAnalysis
	 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getSLocalAnalysis()
	 * @generated
	 */
	int SLOCAL_ANALYSIS = 10;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SLOCAL_ANALYSIS__UUID = IESFCorePackage.ABSTRACT_SSAFETY_ANALYSIS__UUID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SLOCAL_ANALYSIS__NAME = IESFCorePackage.ABSTRACT_SSAFETY_ANALYSIS__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SLOCAL_ANALYSIS__DESCRIPTION = IESFCorePackage.ABSTRACT_SSAFETY_ANALYSIS__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>SSafety Concepts List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SLOCAL_ANALYSIS__SSAFETY_CONCEPTS_LIST = IESFCorePackage.ABSTRACT_SSAFETY_ANALYSIS__SSAFETY_CONCEPTS_LIST;

	/**
	 * The feature id for the '<em><b>Base Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SLOCAL_ANALYSIS__BASE_PACKAGE = IESFCorePackage.ABSTRACT_SSAFETY_ANALYSIS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>SSystem Events Library</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SLOCAL_ANALYSIS__SSYSTEM_EVENTS_LIBRARY = IESFCorePackage.ABSTRACT_SSAFETY_ANALYSIS_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>SFeared Events Library</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SLOCAL_ANALYSIS__SFEARED_EVENTS_LIBRARY = IESFCorePackage.ABSTRACT_SSAFETY_ANALYSIS_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>SBlocks LAnalysis List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SLOCAL_ANALYSIS__SBLOCKS_LANALYSIS_LIST = IESFCorePackage.ABSTRACT_SSAFETY_ANALYSIS_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>SLocal Analysis</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SLOCAL_ANALYSIS_FEATURE_COUNT = IESFCorePackage.ABSTRACT_SSAFETY_ANALYSIS_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>SLocal Analysis</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SLOCAL_ANALYSIS_OPERATION_COUNT = IESFCorePackage.ABSTRACT_SSAFETY_ANALYSIS_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esflocalanalysis.impl.SSystemEventsLibrary <em>SSystem Events Library</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.polarsys.esf.esflocalanalysis.impl.SSystemEventsLibrary
	 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getSSystemEventsLibrary()
	 * @generated
	 */
	int SSYSTEM_EVENTS_LIBRARY = 11;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSYSTEM_EVENTS_LIBRARY__UUID = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSYSTEM_EVENTS_LIBRARY__NAME = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSYSTEM_EVENTS_LIBRARY__DESCRIPTION = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSYSTEM_EVENTS_LIBRARY__SARCHITECTURE_ELEMENTS_LIST = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__SARCHITECTURE_ELEMENTS_LIST;

	/**
	 * The feature id for the '<em><b>Base Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSYSTEM_EVENTS_LIBRARY__BASE_PACKAGE = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>SSystem Event Types List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSYSTEM_EVENTS_LIBRARY__SSYSTEM_EVENT_TYPES_LIST = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>SLocal Analysis</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSYSTEM_EVENTS_LIBRARY__SLOCAL_ANALYSIS = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>SSystem Events Library</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSYSTEM_EVENTS_LIBRARY_FEATURE_COUNT = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>SSystem Events Library</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSYSTEM_EVENTS_LIBRARY_OPERATION_COUNT = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esflocalanalysis.impl.SSystemEventType <em>SSystem Event Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.polarsys.esf.esflocalanalysis.impl.SSystemEventType
	 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getSSystemEventType()
	 * @generated
	 */
	int SSYSTEM_EVENT_TYPE = 12;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSYSTEM_EVENT_TYPE__UUID = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSYSTEM_EVENT_TYPE__NAME = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSYSTEM_EVENT_TYPE__DESCRIPTION = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSYSTEM_EVENT_TYPE__SARCHITECTURE_ELEMENTS_LIST = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__SARCHITECTURE_ELEMENTS_LIST;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSYSTEM_EVENT_TYPE__BASE_CLASS = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>SFailure Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSYSTEM_EVENT_TYPE__SFAILURE_EVENT = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Instances List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSYSTEM_EVENT_TYPE__INSTANCES_LIST = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>SSystem Events Library</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSYSTEM_EVENT_TYPE__SSYSTEM_EVENTS_LIBRARY = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>SSystem Event Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSYSTEM_EVENT_TYPE_FEATURE_COUNT = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>SSystem Event Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSYSTEM_EVENT_TYPE_OPERATION_COUNT = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esflocalanalysis.impl.SSystemEvent <em>SSystem Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.polarsys.esf.esflocalanalysis.impl.SSystemEvent
	 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getSSystemEvent()
	 * @generated
	 */
	int SSYSTEM_EVENT = 13;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSYSTEM_EVENT__UUID = ABSTRACT_SFAILURE_EVENT_LANALYSIS__UUID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSYSTEM_EVENT__NAME = ABSTRACT_SFAILURE_EVENT_LANALYSIS__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSYSTEM_EVENT__DESCRIPTION = ABSTRACT_SFAILURE_EVENT_LANALYSIS__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSYSTEM_EVENT__SARCHITECTURE_ELEMENTS_LIST = ABSTRACT_SFAILURE_EVENT_LANALYSIS__SARCHITECTURE_ELEMENTS_LIST;

	/**
	 * The feature id for the '<em><b>Out SPropagation Links List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSYSTEM_EVENT__OUT_SPROPAGATION_LINKS_LIST = ABSTRACT_SFAILURE_EVENT_LANALYSIS__OUT_SPROPAGATION_LINKS_LIST;

	/**
	 * The feature id for the '<em><b>In SPropagation Links List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSYSTEM_EVENT__IN_SPROPAGATION_LINKS_LIST = ABSTRACT_SFAILURE_EVENT_LANALYSIS__IN_SPROPAGATION_LINKS_LIST;

	/**
	 * The feature id for the '<em><b>SFailure Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSYSTEM_EVENT__SFAILURE_EVENT = ABSTRACT_SFAILURE_EVENT_LANALYSIS__SFAILURE_EVENT;

	/**
	 * The feature id for the '<em><b>SBlock LAnalysis</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSYSTEM_EVENT__SBLOCK_LANALYSIS = ABSTRACT_SFAILURE_EVENT_LANALYSIS__SBLOCK_LANALYSIS;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSYSTEM_EVENT__BASE_PROPERTY = ABSTRACT_SFAILURE_EVENT_LANALYSIS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSYSTEM_EVENT__TYPE = ABSTRACT_SFAILURE_EVENT_LANALYSIS_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>SSystem Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSYSTEM_EVENT_FEATURE_COUNT = ABSTRACT_SFAILURE_EVENT_LANALYSIS_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>SSystem Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSYSTEM_EVENT_OPERATION_COUNT = ABSTRACT_SFAILURE_EVENT_LANALYSIS_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esflocalanalysis.impl.SFearedEventsLibrary <em>SFeared Events Library</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.polarsys.esf.esflocalanalysis.impl.SFearedEventsLibrary
	 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getSFearedEventsLibrary()
	 * @generated
	 */
	int SFEARED_EVENTS_LIBRARY = 14;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SFEARED_EVENTS_LIBRARY__UUID = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SFEARED_EVENTS_LIBRARY__NAME = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SFEARED_EVENTS_LIBRARY__DESCRIPTION = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SFEARED_EVENTS_LIBRARY__SARCHITECTURE_ELEMENTS_LIST = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__SARCHITECTURE_ELEMENTS_LIST;

	/**
	 * The feature id for the '<em><b>Base Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SFEARED_EVENTS_LIBRARY__BASE_PACKAGE = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>SFeared Events Families List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SFEARED_EVENTS_LIBRARY__SFEARED_EVENTS_FAMILIES_LIST = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>SFeared Events List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SFEARED_EVENTS_LIBRARY__SFEARED_EVENTS_LIST = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>SLocal Analysis</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SFEARED_EVENTS_LIBRARY__SLOCAL_ANALYSIS = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>SFeared Events Library</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SFEARED_EVENTS_LIBRARY_FEATURE_COUNT = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>SFeared Events Library</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SFEARED_EVENTS_LIBRARY_OPERATION_COUNT = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esflocalanalysis.impl.SFearedEventsFamily <em>SFeared Events Family</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.polarsys.esf.esflocalanalysis.impl.SFearedEventsFamily
	 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getSFearedEventsFamily()
	 * @generated
	 */
	int SFEARED_EVENTS_FAMILY = 15;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SFEARED_EVENTS_FAMILY__UUID = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SFEARED_EVENTS_FAMILY__NAME = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SFEARED_EVENTS_FAMILY__DESCRIPTION = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SFEARED_EVENTS_FAMILY__SARCHITECTURE_ELEMENTS_LIST = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__SARCHITECTURE_ELEMENTS_LIST;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SFEARED_EVENTS_FAMILY__BASE_CLASS = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>SFeared Events List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SFEARED_EVENTS_FAMILY__SFEARED_EVENTS_LIST = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Is Selected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SFEARED_EVENTS_FAMILY__IS_SELECTED = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Sub Families List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SFEARED_EVENTS_FAMILY__SUB_FAMILIES_LIST = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>All SFeared Events List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SFEARED_EVENTS_FAMILY__ALL_SFEARED_EVENTS_LIST = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>SFailure Modes LAnalysis List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SFEARED_EVENTS_FAMILY__SFAILURE_MODES_LANALYSIS_LIST = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>SFeared Events Library</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SFEARED_EVENTS_FAMILY__SFEARED_EVENTS_LIBRARY = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>SFeared Events Family</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SFEARED_EVENTS_FAMILY_FEATURE_COUNT = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The number of operations of the '<em>SFeared Events Family</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SFEARED_EVENTS_FAMILY_OPERATION_COUNT = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esflocalanalysis.impl.SFearedEvent <em>SFeared Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.polarsys.esf.esflocalanalysis.impl.SFearedEvent
	 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getSFearedEvent()
	 * @generated
	 */
	int SFEARED_EVENT = 16;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SFEARED_EVENT__UUID = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SFEARED_EVENT__NAME = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SFEARED_EVENT__DESCRIPTION = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SFEARED_EVENT__SARCHITECTURE_ELEMENTS_LIST = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__SARCHITECTURE_ELEMENTS_LIST;

	/**
	 * The feature id for the '<em><b>Is Selected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SFEARED_EVENT__IS_SELECTED = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SFEARED_EVENT__BASE_CLASS = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>SFailure Modes LAnalysis List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SFEARED_EVENT__SFAILURE_MODES_LANALYSIS_LIST = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>SFeared Events Library</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SFEARED_EVENT__SFEARED_EVENTS_LIBRARY = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>SFeared Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SFEARED_EVENT_FEATURE_COUNT = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>SFeared Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SFEARED_EVENT_OPERATION_COUNT = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esflocalanalysis.impl.SDysfunctionalAssociation <em>SDysfunctional Association</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.polarsys.esf.esflocalanalysis.impl.SDysfunctionalAssociation
	 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getSDysfunctionalAssociation()
	 * @generated
	 */
	int SDYSFUNCTIONAL_ASSOCIATION = 17;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SDYSFUNCTIONAL_ASSOCIATION__UUID = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SDYSFUNCTIONAL_ASSOCIATION__NAME = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SDYSFUNCTIONAL_ASSOCIATION__DESCRIPTION = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SDYSFUNCTIONAL_ASSOCIATION__SARCHITECTURE_ELEMENTS_LIST = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT__SARCHITECTURE_ELEMENTS_LIST;

	/**
	 * The feature id for the '<em><b>SFailure Mode LAnalysis</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SDYSFUNCTIONAL_ASSOCIATION__SFAILURE_MODE_LANALYSIS = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Base Connector</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SDYSFUNCTIONAL_ASSOCIATION__BASE_CONNECTOR = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>SFailure Mode Owner</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SDYSFUNCTIONAL_ASSOCIATION__SFAILURE_MODE_OWNER = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>SDysfunctional Association</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SDYSFUNCTIONAL_ASSOCIATION_FEATURE_COUNT = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>SDysfunctional Association</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SDYSFUNCTIONAL_ASSOCIATION_OPERATION_COUNT = ABSTRACT_SLOCAL_ANALYSIS_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esflocalanalysis.impl.SAndGateLAnalysis <em>SAnd Gate LAnalysis</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.polarsys.esf.esflocalanalysis.impl.SAndGateLAnalysis
	 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getSAndGateLAnalysis()
	 * @generated
	 */
	int SAND_GATE_LANALYSIS = 18;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SAND_GATE_LANALYSIS__UUID = ABSTRACT_SLOGICAL_GATE_LANALYSIS__UUID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SAND_GATE_LANALYSIS__NAME = ABSTRACT_SLOGICAL_GATE_LANALYSIS__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SAND_GATE_LANALYSIS__DESCRIPTION = ABSTRACT_SLOGICAL_GATE_LANALYSIS__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SAND_GATE_LANALYSIS__SARCHITECTURE_ELEMENTS_LIST = ABSTRACT_SLOGICAL_GATE_LANALYSIS__SARCHITECTURE_ELEMENTS_LIST;

	/**
	 * The feature id for the '<em><b>Out SPropagation Links List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SAND_GATE_LANALYSIS__OUT_SPROPAGATION_LINKS_LIST = ABSTRACT_SLOGICAL_GATE_LANALYSIS__OUT_SPROPAGATION_LINKS_LIST;

	/**
	 * The feature id for the '<em><b>In SPropagation Links List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SAND_GATE_LANALYSIS__IN_SPROPAGATION_LINKS_LIST = ABSTRACT_SLOGICAL_GATE_LANALYSIS__IN_SPROPAGATION_LINKS_LIST;

	/**
	 * The feature id for the '<em><b>SBlock LAnalysis</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SAND_GATE_LANALYSIS__SBLOCK_LANALYSIS = ABSTRACT_SLOGICAL_GATE_LANALYSIS__SBLOCK_LANALYSIS;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SAND_GATE_LANALYSIS__BASE_PROPERTY = ABSTRACT_SLOGICAL_GATE_LANALYSIS_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>SAnd Gate LAnalysis</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SAND_GATE_LANALYSIS_FEATURE_COUNT = ABSTRACT_SLOGICAL_GATE_LANALYSIS_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>SAnd Gate LAnalysis</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SAND_GATE_LANALYSIS_OPERATION_COUNT = ABSTRACT_SLOGICAL_GATE_LANALYSIS_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esflocalanalysis.impl.SOrGateLAnalysis <em>SOr Gate LAnalysis</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.polarsys.esf.esflocalanalysis.impl.SOrGateLAnalysis
	 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getSOrGateLAnalysis()
	 * @generated
	 */
	int SOR_GATE_LANALYSIS = 19;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SOR_GATE_LANALYSIS__UUID = ABSTRACT_SLOGICAL_GATE_LANALYSIS__UUID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SOR_GATE_LANALYSIS__NAME = ABSTRACT_SLOGICAL_GATE_LANALYSIS__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SOR_GATE_LANALYSIS__DESCRIPTION = ABSTRACT_SLOGICAL_GATE_LANALYSIS__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SOR_GATE_LANALYSIS__SARCHITECTURE_ELEMENTS_LIST = ABSTRACT_SLOGICAL_GATE_LANALYSIS__SARCHITECTURE_ELEMENTS_LIST;

	/**
	 * The feature id for the '<em><b>Out SPropagation Links List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SOR_GATE_LANALYSIS__OUT_SPROPAGATION_LINKS_LIST = ABSTRACT_SLOGICAL_GATE_LANALYSIS__OUT_SPROPAGATION_LINKS_LIST;

	/**
	 * The feature id for the '<em><b>In SPropagation Links List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SOR_GATE_LANALYSIS__IN_SPROPAGATION_LINKS_LIST = ABSTRACT_SLOGICAL_GATE_LANALYSIS__IN_SPROPAGATION_LINKS_LIST;

	/**
	 * The feature id for the '<em><b>SBlock LAnalysis</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SOR_GATE_LANALYSIS__SBLOCK_LANALYSIS = ABSTRACT_SLOGICAL_GATE_LANALYSIS__SBLOCK_LANALYSIS;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SOR_GATE_LANALYSIS__BASE_PROPERTY = ABSTRACT_SLOGICAL_GATE_LANALYSIS_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>SOr Gate LAnalysis</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SOR_GATE_LANALYSIS_FEATURE_COUNT = ABSTRACT_SLOGICAL_GATE_LANALYSIS_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>SOr Gate LAnalysis</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SOR_GATE_LANALYSIS_OPERATION_COUNT = ABSTRACT_SLOGICAL_GATE_LANALYSIS_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esflocalanalysis.impl.SUntimelyFailureMode <em>SUntimely Failure Mode</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.polarsys.esf.esflocalanalysis.impl.SUntimelyFailureMode
	 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getSUntimelyFailureMode()
	 * @generated
	 */
	int SUNTIMELY_FAILURE_MODE = 20;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SUNTIMELY_FAILURE_MODE__UUID = ABSTRACT_SFAILURE_MODE_LANALYSIS__UUID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SUNTIMELY_FAILURE_MODE__NAME = ABSTRACT_SFAILURE_MODE_LANALYSIS__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SUNTIMELY_FAILURE_MODE__DESCRIPTION = ABSTRACT_SFAILURE_MODE_LANALYSIS__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SUNTIMELY_FAILURE_MODE__SARCHITECTURE_ELEMENTS_LIST = ABSTRACT_SFAILURE_MODE_LANALYSIS__SARCHITECTURE_ELEMENTS_LIST;

	/**
	 * The feature id for the '<em><b>Out SPropagation Links List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SUNTIMELY_FAILURE_MODE__OUT_SPROPAGATION_LINKS_LIST = ABSTRACT_SFAILURE_MODE_LANALYSIS__OUT_SPROPAGATION_LINKS_LIST;

	/**
	 * The feature id for the '<em><b>In SPropagation Links List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SUNTIMELY_FAILURE_MODE__IN_SPROPAGATION_LINKS_LIST = ABSTRACT_SFAILURE_MODE_LANALYSIS__IN_SPROPAGATION_LINKS_LIST;

	/**
	 * The feature id for the '<em><b>SBlock LAnalysis</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SUNTIMELY_FAILURE_MODE__SBLOCK_LANALYSIS = ABSTRACT_SFAILURE_MODE_LANALYSIS__SBLOCK_LANALYSIS;

	/**
	 * The feature id for the '<em><b>SFailure Mode</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SUNTIMELY_FAILURE_MODE__SFAILURE_MODE = ABSTRACT_SFAILURE_MODE_LANALYSIS__SFAILURE_MODE;

	/**
	 * The feature id for the '<em><b>SFeared Events Families List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SUNTIMELY_FAILURE_MODE__SFEARED_EVENTS_FAMILIES_LIST = ABSTRACT_SFAILURE_MODE_LANALYSIS__SFEARED_EVENTS_FAMILIES_LIST;

	/**
	 * The feature id for the '<em><b>SFeared Events List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SUNTIMELY_FAILURE_MODE__SFEARED_EVENTS_LIST = ABSTRACT_SFAILURE_MODE_LANALYSIS__SFEARED_EVENTS_LIST;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SUNTIMELY_FAILURE_MODE__OWNER = ABSTRACT_SFAILURE_MODE_LANALYSIS__OWNER;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SUNTIMELY_FAILURE_MODE__BASE_PROPERTY = ABSTRACT_SFAILURE_MODE_LANALYSIS_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>SUntimely Failure Mode</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SUNTIMELY_FAILURE_MODE_FEATURE_COUNT = ABSTRACT_SFAILURE_MODE_LANALYSIS_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>SUntimely Failure Mode</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SUNTIMELY_FAILURE_MODE_OPERATION_COUNT = ABSTRACT_SFAILURE_MODE_LANALYSIS_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esflocalanalysis.impl.SAbsentFailureMode <em>SAbsent Failure Mode</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.polarsys.esf.esflocalanalysis.impl.SAbsentFailureMode
	 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getSAbsentFailureMode()
	 * @generated
	 */
	int SABSENT_FAILURE_MODE = 21;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SABSENT_FAILURE_MODE__UUID = ABSTRACT_SFAILURE_MODE_LANALYSIS__UUID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SABSENT_FAILURE_MODE__NAME = ABSTRACT_SFAILURE_MODE_LANALYSIS__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SABSENT_FAILURE_MODE__DESCRIPTION = ABSTRACT_SFAILURE_MODE_LANALYSIS__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SABSENT_FAILURE_MODE__SARCHITECTURE_ELEMENTS_LIST = ABSTRACT_SFAILURE_MODE_LANALYSIS__SARCHITECTURE_ELEMENTS_LIST;

	/**
	 * The feature id for the '<em><b>Out SPropagation Links List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SABSENT_FAILURE_MODE__OUT_SPROPAGATION_LINKS_LIST = ABSTRACT_SFAILURE_MODE_LANALYSIS__OUT_SPROPAGATION_LINKS_LIST;

	/**
	 * The feature id for the '<em><b>In SPropagation Links List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SABSENT_FAILURE_MODE__IN_SPROPAGATION_LINKS_LIST = ABSTRACT_SFAILURE_MODE_LANALYSIS__IN_SPROPAGATION_LINKS_LIST;

	/**
	 * The feature id for the '<em><b>SBlock LAnalysis</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SABSENT_FAILURE_MODE__SBLOCK_LANALYSIS = ABSTRACT_SFAILURE_MODE_LANALYSIS__SBLOCK_LANALYSIS;

	/**
	 * The feature id for the '<em><b>SFailure Mode</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SABSENT_FAILURE_MODE__SFAILURE_MODE = ABSTRACT_SFAILURE_MODE_LANALYSIS__SFAILURE_MODE;

	/**
	 * The feature id for the '<em><b>SFeared Events Families List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SABSENT_FAILURE_MODE__SFEARED_EVENTS_FAMILIES_LIST = ABSTRACT_SFAILURE_MODE_LANALYSIS__SFEARED_EVENTS_FAMILIES_LIST;

	/**
	 * The feature id for the '<em><b>SFeared Events List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SABSENT_FAILURE_MODE__SFEARED_EVENTS_LIST = ABSTRACT_SFAILURE_MODE_LANALYSIS__SFEARED_EVENTS_LIST;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SABSENT_FAILURE_MODE__OWNER = ABSTRACT_SFAILURE_MODE_LANALYSIS__OWNER;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SABSENT_FAILURE_MODE__BASE_PROPERTY = ABSTRACT_SFAILURE_MODE_LANALYSIS_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>SAbsent Failure Mode</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SABSENT_FAILURE_MODE_FEATURE_COUNT = ABSTRACT_SFAILURE_MODE_LANALYSIS_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>SAbsent Failure Mode</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SABSENT_FAILURE_MODE_OPERATION_COUNT = ABSTRACT_SFAILURE_MODE_LANALYSIS_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esflocalanalysis.impl.SErroneousFailureMode <em>SErroneous Failure Mode</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.polarsys.esf.esflocalanalysis.impl.SErroneousFailureMode
	 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getSErroneousFailureMode()
	 * @generated
	 */
	int SERRONEOUS_FAILURE_MODE = 22;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SERRONEOUS_FAILURE_MODE__UUID = ABSTRACT_SFAILURE_MODE_LANALYSIS__UUID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SERRONEOUS_FAILURE_MODE__NAME = ABSTRACT_SFAILURE_MODE_LANALYSIS__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SERRONEOUS_FAILURE_MODE__DESCRIPTION = ABSTRACT_SFAILURE_MODE_LANALYSIS__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SERRONEOUS_FAILURE_MODE__SARCHITECTURE_ELEMENTS_LIST = ABSTRACT_SFAILURE_MODE_LANALYSIS__SARCHITECTURE_ELEMENTS_LIST;

	/**
	 * The feature id for the '<em><b>Out SPropagation Links List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SERRONEOUS_FAILURE_MODE__OUT_SPROPAGATION_LINKS_LIST = ABSTRACT_SFAILURE_MODE_LANALYSIS__OUT_SPROPAGATION_LINKS_LIST;

	/**
	 * The feature id for the '<em><b>In SPropagation Links List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SERRONEOUS_FAILURE_MODE__IN_SPROPAGATION_LINKS_LIST = ABSTRACT_SFAILURE_MODE_LANALYSIS__IN_SPROPAGATION_LINKS_LIST;

	/**
	 * The feature id for the '<em><b>SBlock LAnalysis</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SERRONEOUS_FAILURE_MODE__SBLOCK_LANALYSIS = ABSTRACT_SFAILURE_MODE_LANALYSIS__SBLOCK_LANALYSIS;

	/**
	 * The feature id for the '<em><b>SFailure Mode</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SERRONEOUS_FAILURE_MODE__SFAILURE_MODE = ABSTRACT_SFAILURE_MODE_LANALYSIS__SFAILURE_MODE;

	/**
	 * The feature id for the '<em><b>SFeared Events Families List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SERRONEOUS_FAILURE_MODE__SFEARED_EVENTS_FAMILIES_LIST = ABSTRACT_SFAILURE_MODE_LANALYSIS__SFEARED_EVENTS_FAMILIES_LIST;

	/**
	 * The feature id for the '<em><b>SFeared Events List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SERRONEOUS_FAILURE_MODE__SFEARED_EVENTS_LIST = ABSTRACT_SFAILURE_MODE_LANALYSIS__SFEARED_EVENTS_LIST;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SERRONEOUS_FAILURE_MODE__OWNER = ABSTRACT_SFAILURE_MODE_LANALYSIS__OWNER;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SERRONEOUS_FAILURE_MODE__BASE_PROPERTY = ABSTRACT_SFAILURE_MODE_LANALYSIS_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>SErroneous Failure Mode</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SERRONEOUS_FAILURE_MODE_FEATURE_COUNT = ABSTRACT_SFAILURE_MODE_LANALYSIS_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>SErroneous Failure Mode</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SERRONEOUS_FAILURE_MODE_OPERATION_COUNT = ABSTRACT_SFAILURE_MODE_LANALYSIS_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esflocalanalysis.impl.SLocalEvent <em>SLocal Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.polarsys.esf.esflocalanalysis.impl.SLocalEvent
	 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getSLocalEvent()
	 * @generated
	 */
	int SLOCAL_EVENT = 23;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SLOCAL_EVENT__UUID = ABSTRACT_SFAILURE_EVENT_LANALYSIS__UUID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SLOCAL_EVENT__NAME = ABSTRACT_SFAILURE_EVENT_LANALYSIS__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SLOCAL_EVENT__DESCRIPTION = ABSTRACT_SFAILURE_EVENT_LANALYSIS__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SLOCAL_EVENT__SARCHITECTURE_ELEMENTS_LIST = ABSTRACT_SFAILURE_EVENT_LANALYSIS__SARCHITECTURE_ELEMENTS_LIST;

	/**
	 * The feature id for the '<em><b>Out SPropagation Links List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SLOCAL_EVENT__OUT_SPROPAGATION_LINKS_LIST = ABSTRACT_SFAILURE_EVENT_LANALYSIS__OUT_SPROPAGATION_LINKS_LIST;

	/**
	 * The feature id for the '<em><b>In SPropagation Links List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SLOCAL_EVENT__IN_SPROPAGATION_LINKS_LIST = ABSTRACT_SFAILURE_EVENT_LANALYSIS__IN_SPROPAGATION_LINKS_LIST;

	/**
	 * The feature id for the '<em><b>SFailure Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SLOCAL_EVENT__SFAILURE_EVENT = ABSTRACT_SFAILURE_EVENT_LANALYSIS__SFAILURE_EVENT;

	/**
	 * The feature id for the '<em><b>SBlock LAnalysis</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SLOCAL_EVENT__SBLOCK_LANALYSIS = ABSTRACT_SFAILURE_EVENT_LANALYSIS__SBLOCK_LANALYSIS;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SLOCAL_EVENT__BASE_PROPERTY = ABSTRACT_SFAILURE_EVENT_LANALYSIS_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>SLocal Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SLOCAL_EVENT_FEATURE_COUNT = ABSTRACT_SFAILURE_EVENT_LANALYSIS_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>SLocal Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SLOCAL_EVENT_OPERATION_COUNT = ABSTRACT_SFAILURE_EVENT_LANALYSIS_OPERATION_COUNT + 0;

	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.esflocalanalysis.IAbstractSLocalAnalysisElement <em>Abstract SLocal Analysis Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Abstract SLocal Analysis Element</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.IAbstractSLocalAnalysisElement
	 * @generated
	 */
	EClass getAbstractSLocalAnalysisElement();

	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.esflocalanalysis.ISPortLAnalysis <em>SPort LAnalysis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>SPort LAnalysis</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISPortLAnalysis
	 * @generated
	 */
	EClass getSPortLAnalysis();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISPortLAnalysis#getSBlockLAnalysis <em>SBlock LAnalysis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>SBlock LAnalysis</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISPortLAnalysis#getSBlockLAnalysis()
	 * @see #getSPortLAnalysis()
	 * @generated
	 */
	EReference getSPortLAnalysis_SBlockLAnalysis();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISPortLAnalysis#getSPort <em>SPort</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>SPort</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISPortLAnalysis#getSPort()
	 * @see #getSPortLAnalysis()
	 * @generated
	 */
	EReference getSPortLAnalysis_SPort();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISPortLAnalysis#getBase_Port <em>Base Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Base Port</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISPortLAnalysis#getBase_Port()
	 * @see #getSPortLAnalysis()
	 * @generated
	 */
	EReference getSPortLAnalysis_Base_Port();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.esflocalanalysis.ISPortLAnalysis#getSDirectionManual <em>SDirection Manual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>SDirection Manual</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISPortLAnalysis#getSDirectionManual()
	 * @see #getSPortLAnalysis()
	 * @generated
	 */
	EAttribute getSPortLAnalysis_SDirectionManual();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.esflocalanalysis.ISPortLAnalysis#getSDirectionLAnalysis <em>SDirection LAnalysis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>SDirection LAnalysis</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISPortLAnalysis#getSDirectionLAnalysis()
	 * @see #getSPortLAnalysis()
	 * @generated
	 */
	EAttribute getSPortLAnalysis_SDirectionLAnalysis();

	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeOwner <em>Abstract SFailure Mode Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Abstract SFailure Mode Owner</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeOwner
	 * @generated
	 */
	EClass getAbstractSFailureModeOwner();

	/**
	 * Returns the meta object for the reference list '{@link org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeOwner#getSFailureModesLAnalysisList <em>SFailure Modes LAnalysis List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>SFailure Modes LAnalysis List</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeOwner#getSFailureModesLAnalysisList()
	 * @see #getAbstractSFailureModeOwner()
	 * @generated
	 */
	EReference getAbstractSFailureModeOwner_SFailureModesLAnalysisList();

	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis <em>Abstract SFailure Mode LAnalysis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Abstract SFailure Mode LAnalysis</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis
	 * @generated
	 */
	EClass getAbstractSFailureModeLAnalysis();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis#getSBlockLAnalysis <em>SBlock LAnalysis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>SBlock LAnalysis</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis#getSBlockLAnalysis()
	 * @see #getAbstractSFailureModeLAnalysis()
	 * @generated
	 */
	EReference getAbstractSFailureModeLAnalysis_SBlockLAnalysis();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis#getSFailureMode <em>SFailure Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>SFailure Mode</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis#getSFailureMode()
	 * @see #getAbstractSFailureModeLAnalysis()
	 * @generated
	 */
	EReference getAbstractSFailureModeLAnalysis_SFailureMode();

	/**
	 * Returns the meta object for the reference list '{@link org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis#getSFearedEventsFamiliesList <em>SFeared Events Families List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>SFeared Events Families List</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis#getSFearedEventsFamiliesList()
	 * @see #getAbstractSFailureModeLAnalysis()
	 * @generated
	 */
	EReference getAbstractSFailureModeLAnalysis_SFearedEventsFamiliesList();

	/**
	 * Returns the meta object for the reference list '{@link org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis#getSFearedEventsList <em>SFeared Events List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>SFeared Events List</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis#getSFearedEventsList()
	 * @see #getAbstractSFailureModeLAnalysis()
	 * @generated
	 */
	EReference getAbstractSFailureModeLAnalysis_SFearedEventsList();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Owner</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis#getOwner()
	 * @see #getAbstractSFailureModeLAnalysis()
	 * @generated
	 */
	EReference getAbstractSFailureModeLAnalysis_Owner();

	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.esflocalanalysis.IAbstractSPropagationElement <em>Abstract SPropagation Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Abstract SPropagation Element</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.IAbstractSPropagationElement
	 * @generated
	 */
	EClass getAbstractSPropagationElement();

	/**
	 * Returns the meta object for the reference list '{@link org.polarsys.esf.esflocalanalysis.IAbstractSPropagationElement#getOutSPropagationLinksList <em>Out SPropagation Links List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>Out SPropagation Links List</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.IAbstractSPropagationElement#getOutSPropagationLinksList()
	 * @see #getAbstractSPropagationElement()
	 * @generated
	 */
	EReference getAbstractSPropagationElement_OutSPropagationLinksList();

	/**
	 * Returns the meta object for the reference list '{@link org.polarsys.esf.esflocalanalysis.IAbstractSPropagationElement#getInSPropagationLinksList <em>In SPropagation Links List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>In SPropagation Links List</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.IAbstractSPropagationElement#getInSPropagationLinksList()
	 * @see #getAbstractSPropagationElement()
	 * @generated
	 */
	EReference getAbstractSPropagationElement_InSPropagationLinksList();

	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.esflocalanalysis.ISPropagationLink <em>SPropagation Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>SPropagation Link</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISPropagationLink
	 * @generated
	 */
	EClass getSPropagationLink();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISPropagationLink#getElementTarget <em>Element Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Element Target</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISPropagationLink#getElementTarget()
	 * @see #getSPropagationLink()
	 * @generated
	 */
	EReference getSPropagationLink_ElementTarget();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISPropagationLink#getSBlockLAnalysis <em>SBlock LAnalysis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>SBlock LAnalysis</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISPropagationLink#getSBlockLAnalysis()
	 * @see #getSPropagationLink()
	 * @generated
	 */
	EReference getSPropagationLink_SBlockLAnalysis();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISPropagationLink#getBase_Dependency <em>Base Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Base Dependency</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISPropagationLink#getBase_Dependency()
	 * @see #getSPropagationLink()
	 * @generated
	 */
	EReference getSPropagationLink_Base_Dependency();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISPropagationLink#getElementSource <em>Element Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Element Source</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISPropagationLink#getElementSource()
	 * @see #getSPropagationLink()
	 * @generated
	 */
	EReference getSPropagationLink_ElementSource();

	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis <em>SBlock LAnalysis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>SBlock LAnalysis</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis
	 * @generated
	 */
	EClass getSBlockLAnalysis();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSBlock <em>SBlock</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>SBlock</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSBlock()
	 * @see #getSBlockLAnalysis()
	 * @generated
	 */
	EReference getSBlockLAnalysis_SBlock();

	/**
	 * Returns the meta object for the reference list '{@link org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSFailureEventsLAnalysisList <em>SFailure Events LAnalysis List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>SFailure Events LAnalysis List</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSFailureEventsLAnalysisList()
	 * @see #getSBlockLAnalysis()
	 * @generated
	 */
	EReference getSBlockLAnalysis_SFailureEventsLAnalysisList();

	/**
	 * Returns the meta object for the reference list '{@link org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSPortsLAnalysisList <em>SPorts LAnalysis List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>SPorts LAnalysis List</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSPortsLAnalysisList()
	 * @see #getSBlockLAnalysis()
	 * @generated
	 */
	EReference getSBlockLAnalysis_SPortsLAnalysisList();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getBase_Class <em>Base Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Base Class</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getBase_Class()
	 * @see #getSBlockLAnalysis()
	 * @generated
	 */
	EReference getSBlockLAnalysis_Base_Class();

	/**
	 * Returns the meta object for the reference list '{@link org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSLogicalGatesLAnalysisList <em>SLogical Gates LAnalysis List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>SLogical Gates LAnalysis List</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSLogicalGatesLAnalysisList()
	 * @see #getSBlockLAnalysis()
	 * @generated
	 */
	EReference getSBlockLAnalysis_SLogicalGatesLAnalysisList();

	/**
	 * Returns the meta object for the reference list '{@link org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSFailureModesLAnalysisList <em>SFailure Modes LAnalysis List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>SFailure Modes LAnalysis List</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSFailureModesLAnalysisList()
	 * @see #getSBlockLAnalysis()
	 * @generated
	 */
	EReference getSBlockLAnalysis_SFailureModesLAnalysisList();

	/**
	 * Returns the meta object for the reference list '{@link org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSBarriersLAnalysisList <em>SBarriers LAnalysis List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>SBarriers LAnalysis List</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSBarriersLAnalysisList()
	 * @see #getSBlockLAnalysis()
	 * @generated
	 */
	EReference getSBlockLAnalysis_SBarriersLAnalysisList();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSLocalAnalysis <em>SLocal Analysis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>SLocal Analysis</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSLocalAnalysis()
	 * @see #getSBlockLAnalysis()
	 * @generated
	 */
	EReference getSBlockLAnalysis_SLocalAnalysis();

	/**
	 * Returns the meta object for the reference list '{@link org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSPropagationLinksList <em>SPropagation Links List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>SPropagation Links List</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis#getSPropagationLinksList()
	 * @see #getSBlockLAnalysis()
	 * @generated
	 */
	EReference getSBlockLAnalysis_SPropagationLinksList();

	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.esflocalanalysis.IAbstractSFailureEventLAnalysis <em>Abstract SFailure Event LAnalysis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Abstract SFailure Event LAnalysis</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.IAbstractSFailureEventLAnalysis
	 * @generated
	 */
	EClass getAbstractSFailureEventLAnalysis();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.IAbstractSFailureEventLAnalysis#getSFailureEvent <em>SFailure Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>SFailure Event</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.IAbstractSFailureEventLAnalysis#getSFailureEvent()
	 * @see #getAbstractSFailureEventLAnalysis()
	 * @generated
	 */
	EReference getAbstractSFailureEventLAnalysis_SFailureEvent();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.IAbstractSFailureEventLAnalysis#getSBlockLAnalysis <em>SBlock LAnalysis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>SBlock LAnalysis</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.IAbstractSFailureEventLAnalysis#getSBlockLAnalysis()
	 * @see #getAbstractSFailureEventLAnalysis()
	 * @generated
	 */
	EReference getAbstractSFailureEventLAnalysis_SBlockLAnalysis();

	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.esflocalanalysis.IAbstractSLogicalGateLAnalysis <em>Abstract SLogical Gate LAnalysis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Abstract SLogical Gate LAnalysis</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.IAbstractSLogicalGateLAnalysis
	 * @generated
	 */
	EClass getAbstractSLogicalGateLAnalysis();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.IAbstractSLogicalGateLAnalysis#getSBlockLAnalysis <em>SBlock LAnalysis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>SBlock LAnalysis</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.IAbstractSLogicalGateLAnalysis#getSBlockLAnalysis()
	 * @see #getAbstractSLogicalGateLAnalysis()
	 * @generated
	 */
	EReference getAbstractSLogicalGateLAnalysis_SBlockLAnalysis();

	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.esflocalanalysis.ISBarrierLAnalysis <em>SBarrier LAnalysis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>SBarrier LAnalysis</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISBarrierLAnalysis
	 * @generated
	 */
	EClass getSBarrierLAnalysis();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISBarrierLAnalysis#getSBarrier <em>SBarrier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>SBarrier</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISBarrierLAnalysis#getSBarrier()
	 * @see #getSBarrierLAnalysis()
	 * @generated
	 */
	EReference getSBarrierLAnalysis_SBarrier();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISBarrierLAnalysis#getBase_Property <em>Base Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Base Property</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISBarrierLAnalysis#getBase_Property()
	 * @see #getSBarrierLAnalysis()
	 * @generated
	 */
	EReference getSBarrierLAnalysis_Base_Property();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISBarrierLAnalysis#getSBlockLAnalysis <em>SBlock LAnalysis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>SBlock LAnalysis</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISBarrierLAnalysis#getSBlockLAnalysis()
	 * @see #getSBarrierLAnalysis()
	 * @generated
	 */
	EReference getSBarrierLAnalysis_SBlockLAnalysis();

	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.esflocalanalysis.ISLocalAnalysis <em>SLocal Analysis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>SLocal Analysis</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISLocalAnalysis
	 * @generated
	 */
	EClass getSLocalAnalysis();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISLocalAnalysis#getBase_Package <em>Base Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Base Package</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISLocalAnalysis#getBase_Package()
	 * @see #getSLocalAnalysis()
	 * @generated
	 */
	EReference getSLocalAnalysis_Base_Package();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISLocalAnalysis#getSSystemEventsLibrary <em>SSystem Events Library</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>SSystem Events Library</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISLocalAnalysis#getSSystemEventsLibrary()
	 * @see #getSLocalAnalysis()
	 * @generated
	 */
	EReference getSLocalAnalysis_SSystemEventsLibrary();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISLocalAnalysis#getSFearedEventsLibrary <em>SFeared Events Library</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>SFeared Events Library</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISLocalAnalysis#getSFearedEventsLibrary()
	 * @see #getSLocalAnalysis()
	 * @generated
	 */
	EReference getSLocalAnalysis_SFearedEventsLibrary();

	/**
	 * Returns the meta object for the reference list '{@link org.polarsys.esf.esflocalanalysis.ISLocalAnalysis#getSBlocksLAnalysisList <em>SBlocks LAnalysis List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>SBlocks LAnalysis List</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISLocalAnalysis#getSBlocksLAnalysisList()
	 * @see #getSLocalAnalysis()
	 * @generated
	 */
	EReference getSLocalAnalysis_SBlocksLAnalysisList();

	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.esflocalanalysis.ISSystemEventsLibrary <em>SSystem Events Library</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>SSystem Events Library</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISSystemEventsLibrary
	 * @generated
	 */
	EClass getSSystemEventsLibrary();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISSystemEventsLibrary#getBase_Package <em>Base Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Base Package</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISSystemEventsLibrary#getBase_Package()
	 * @see #getSSystemEventsLibrary()
	 * @generated
	 */
	EReference getSSystemEventsLibrary_Base_Package();

	/**
	 * Returns the meta object for the reference list '{@link org.polarsys.esf.esflocalanalysis.ISSystemEventsLibrary#getSSystemEventTypesList <em>SSystem Event Types List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>SSystem Event Types List</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISSystemEventsLibrary#getSSystemEventTypesList()
	 * @see #getSSystemEventsLibrary()
	 * @generated
	 */
	EReference getSSystemEventsLibrary_SSystemEventTypesList();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISSystemEventsLibrary#getSLocalAnalysis <em>SLocal Analysis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>SLocal Analysis</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISSystemEventsLibrary#getSLocalAnalysis()
	 * @see #getSSystemEventsLibrary()
	 * @generated
	 */
	EReference getSSystemEventsLibrary_SLocalAnalysis();

	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.esflocalanalysis.ISSystemEventType <em>SSystem Event Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>SSystem Event Type</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISSystemEventType
	 * @generated
	 */
	EClass getSSystemEventType();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISSystemEventType#getBase_Class <em>Base Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Base Class</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISSystemEventType#getBase_Class()
	 * @see #getSSystemEventType()
	 * @generated
	 */
	EReference getSSystemEventType_Base_Class();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISSystemEventType#getSFailureEvent <em>SFailure Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>SFailure Event</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISSystemEventType#getSFailureEvent()
	 * @see #getSSystemEventType()
	 * @generated
	 */
	EReference getSSystemEventType_SFailureEvent();

	/**
	 * Returns the meta object for the reference list '{@link org.polarsys.esf.esflocalanalysis.ISSystemEventType#getInstancesList <em>Instances List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>Instances List</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISSystemEventType#getInstancesList()
	 * @see #getSSystemEventType()
	 * @generated
	 */
	EReference getSSystemEventType_InstancesList();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISSystemEventType#getSSystemEventsLibrary <em>SSystem Events Library</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>SSystem Events Library</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISSystemEventType#getSSystemEventsLibrary()
	 * @see #getSSystemEventType()
	 * @generated
	 */
	EReference getSSystemEventType_SSystemEventsLibrary();

	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.esflocalanalysis.ISSystemEvent <em>SSystem Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>SSystem Event</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISSystemEvent
	 * @generated
	 */
	EClass getSSystemEvent();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISSystemEvent#getBase_Property <em>Base Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Base Property</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISSystemEvent#getBase_Property()
	 * @see #getSSystemEvent()
	 * @generated
	 */
	EReference getSSystemEvent_Base_Property();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISSystemEvent#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISSystemEvent#getType()
	 * @see #getSSystemEvent()
	 * @generated
	 */
	EReference getSSystemEvent_Type();

	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.esflocalanalysis.ISFearedEventsLibrary <em>SFeared Events Library</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>SFeared Events Library</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISFearedEventsLibrary
	 * @generated
	 */
	EClass getSFearedEventsLibrary();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISFearedEventsLibrary#getBase_Package <em>Base Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Base Package</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISFearedEventsLibrary#getBase_Package()
	 * @see #getSFearedEventsLibrary()
	 * @generated
	 */
	EReference getSFearedEventsLibrary_Base_Package();

	/**
	 * Returns the meta object for the reference list '{@link org.polarsys.esf.esflocalanalysis.ISFearedEventsLibrary#getSFearedEventsFamiliesList <em>SFeared Events Families List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>SFeared Events Families List</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISFearedEventsLibrary#getSFearedEventsFamiliesList()
	 * @see #getSFearedEventsLibrary()
	 * @generated
	 */
	EReference getSFearedEventsLibrary_SFearedEventsFamiliesList();

	/**
	 * Returns the meta object for the reference list '{@link org.polarsys.esf.esflocalanalysis.ISFearedEventsLibrary#getSFearedEventsList <em>SFeared Events List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>SFeared Events List</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISFearedEventsLibrary#getSFearedEventsList()
	 * @see #getSFearedEventsLibrary()
	 * @generated
	 */
	EReference getSFearedEventsLibrary_SFearedEventsList();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISFearedEventsLibrary#getSLocalAnalysis <em>SLocal Analysis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>SLocal Analysis</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISFearedEventsLibrary#getSLocalAnalysis()
	 * @see #getSFearedEventsLibrary()
	 * @generated
	 */
	EReference getSFearedEventsLibrary_SLocalAnalysis();

	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily <em>SFeared Events Family</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>SFeared Events Family</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily
	 * @generated
	 */
	EClass getSFearedEventsFamily();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily#getBase_Class <em>Base Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Base Class</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily#getBase_Class()
	 * @see #getSFearedEventsFamily()
	 * @generated
	 */
	EReference getSFearedEventsFamily_Base_Class();

	/**
	 * Returns the meta object for the reference list '{@link org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily#getSFearedEventsList <em>SFeared Events List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>SFeared Events List</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily#getSFearedEventsList()
	 * @see #getSFearedEventsFamily()
	 * @generated
	 */
	EReference getSFearedEventsFamily_SFearedEventsList();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily#isSelected <em>Is Selected</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Is Selected</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily#isSelected()
	 * @see #getSFearedEventsFamily()
	 * @generated
	 */
	EAttribute getSFearedEventsFamily_IsSelected();

	/**
	 * Returns the meta object for the reference list '{@link org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily#getSubFamiliesList <em>Sub Families List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>Sub Families List</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily#getSubFamiliesList()
	 * @see #getSFearedEventsFamily()
	 * @generated
	 */
	EReference getSFearedEventsFamily_SubFamiliesList();

	/**
	 * Returns the meta object for the reference list '{@link org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily#getAllSFearedEventsList <em>All SFeared Events List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>All SFeared Events List</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily#getAllSFearedEventsList()
	 * @see #getSFearedEventsFamily()
	 * @generated
	 */
	EReference getSFearedEventsFamily_AllSFearedEventsList();

	/**
	 * Returns the meta object for the reference list '{@link org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily#getSFailureModesLAnalysisList <em>SFailure Modes LAnalysis List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>SFailure Modes LAnalysis List</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily#getSFailureModesLAnalysisList()
	 * @see #getSFearedEventsFamily()
	 * @generated
	 */
	EReference getSFearedEventsFamily_SFailureModesLAnalysisList();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily#getSFearedEventsLibrary <em>SFeared Events Library</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>SFeared Events Library</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily#getSFearedEventsLibrary()
	 * @see #getSFearedEventsFamily()
	 * @generated
	 */
	EReference getSFearedEventsFamily_SFearedEventsLibrary();

	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.esflocalanalysis.ISFearedEvent <em>SFeared Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>SFeared Event</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISFearedEvent
	 * @generated
	 */
	EClass getSFearedEvent();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.esflocalanalysis.ISFearedEvent#isSelected <em>Is Selected</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Is Selected</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISFearedEvent#isSelected()
	 * @see #getSFearedEvent()
	 * @generated
	 */
	EAttribute getSFearedEvent_IsSelected();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISFearedEvent#getBase_Class <em>Base Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Base Class</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISFearedEvent#getBase_Class()
	 * @see #getSFearedEvent()
	 * @generated
	 */
	EReference getSFearedEvent_Base_Class();

	/**
	 * Returns the meta object for the reference list '{@link org.polarsys.esf.esflocalanalysis.ISFearedEvent#getSFailureModesLAnalysisList <em>SFailure Modes LAnalysis List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>SFailure Modes LAnalysis List</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISFearedEvent#getSFailureModesLAnalysisList()
	 * @see #getSFearedEvent()
	 * @generated
	 */
	EReference getSFearedEvent_SFailureModesLAnalysisList();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISFearedEvent#getSFearedEventsLibrary <em>SFeared Events Library</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>SFeared Events Library</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISFearedEvent#getSFearedEventsLibrary()
	 * @see #getSFearedEvent()
	 * @generated
	 */
	EReference getSFearedEvent_SFearedEventsLibrary();

	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.esflocalanalysis.ISDysfunctionalAssociation <em>SDysfunctional Association</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>SDysfunctional Association</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISDysfunctionalAssociation
	 * @generated
	 */
	EClass getSDysfunctionalAssociation();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISDysfunctionalAssociation#getSFailureModeLAnalysis <em>SFailure Mode LAnalysis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>SFailure Mode LAnalysis</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISDysfunctionalAssociation#getSFailureModeLAnalysis()
	 * @see #getSDysfunctionalAssociation()
	 * @generated
	 */
	EReference getSDysfunctionalAssociation_SFailureModeLAnalysis();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISDysfunctionalAssociation#getBase_Connector <em>Base Connector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Base Connector</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISDysfunctionalAssociation#getBase_Connector()
	 * @see #getSDysfunctionalAssociation()
	 * @generated
	 */
	EReference getSDysfunctionalAssociation_Base_Connector();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISDysfunctionalAssociation#getSFailureModeOwner <em>SFailure Mode Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>SFailure Mode Owner</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISDysfunctionalAssociation#getSFailureModeOwner()
	 * @see #getSDysfunctionalAssociation()
	 * @generated
	 */
	EReference getSDysfunctionalAssociation_SFailureModeOwner();

	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.esflocalanalysis.ISAndGateLAnalysis <em>SAnd Gate LAnalysis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>SAnd Gate LAnalysis</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISAndGateLAnalysis
	 * @generated
	 */
	EClass getSAndGateLAnalysis();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISAndGateLAnalysis#getBase_Property <em>Base Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Base Property</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISAndGateLAnalysis#getBase_Property()
	 * @see #getSAndGateLAnalysis()
	 * @generated
	 */
	EReference getSAndGateLAnalysis_Base_Property();

	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.esflocalanalysis.ISOrGateLAnalysis <em>SOr Gate LAnalysis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>SOr Gate LAnalysis</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISOrGateLAnalysis
	 * @generated
	 */
	EClass getSOrGateLAnalysis();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISOrGateLAnalysis#getBase_Property <em>Base Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Base Property</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISOrGateLAnalysis#getBase_Property()
	 * @see #getSOrGateLAnalysis()
	 * @generated
	 */
	EReference getSOrGateLAnalysis_Base_Property();

	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.esflocalanalysis.ISUntimelyFailureMode <em>SUntimely Failure Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>SUntimely Failure Mode</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISUntimelyFailureMode
	 * @generated
	 */
	EClass getSUntimelyFailureMode();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISUntimelyFailureMode#getBase_Property <em>Base Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Base Property</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISUntimelyFailureMode#getBase_Property()
	 * @see #getSUntimelyFailureMode()
	 * @generated
	 */
	EReference getSUntimelyFailureMode_Base_Property();

	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.esflocalanalysis.ISAbsentFailureMode <em>SAbsent Failure Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>SAbsent Failure Mode</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISAbsentFailureMode
	 * @generated
	 */
	EClass getSAbsentFailureMode();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISAbsentFailureMode#getBase_Property <em>Base Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Base Property</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISAbsentFailureMode#getBase_Property()
	 * @see #getSAbsentFailureMode()
	 * @generated
	 */
	EReference getSAbsentFailureMode_Base_Property();

	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.esflocalanalysis.ISErroneousFailureMode <em>SErroneous Failure Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>SErroneous Failure Mode</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISErroneousFailureMode
	 * @generated
	 */
	EClass getSErroneousFailureMode();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISErroneousFailureMode#getBase_Property <em>Base Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Base Property</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISErroneousFailureMode#getBase_Property()
	 * @see #getSErroneousFailureMode()
	 * @generated
	 */
	EReference getSErroneousFailureMode_Base_Property();

	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.esflocalanalysis.ISLocalEvent <em>SLocal Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>SLocal Event</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISLocalEvent
	 * @generated
	 */
	EClass getSLocalEvent();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esflocalanalysis.ISLocalEvent#getBase_Property <em>Base Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Base Property</em>'.
	 * @see org.polarsys.esf.esflocalanalysis.ISLocalEvent#getBase_Property()
	 * @see #getSLocalEvent()
	 * @generated
	 */
	EReference getSLocalEvent_Base_Property();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	IESFLocalAnalysisFactory getESFLocalAnalysisFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each operation of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	interface Literals {

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esflocalanalysis.impl.AbstractSLocalAnalysisElement <em>Abstract SLocal Analysis Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see org.polarsys.esf.esflocalanalysis.impl.AbstractSLocalAnalysisElement
		 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getAbstractSLocalAnalysisElement()
		 * @generated
		 */
		EClass ABSTRACT_SLOCAL_ANALYSIS_ELEMENT = eINSTANCE.getAbstractSLocalAnalysisElement();

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esflocalanalysis.impl.SPortLAnalysis <em>SPort LAnalysis</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see org.polarsys.esf.esflocalanalysis.impl.SPortLAnalysis
		 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getSPortLAnalysis()
		 * @generated
		 */
		EClass SPORT_LANALYSIS = eINSTANCE.getSPortLAnalysis();

		/**
		 * The meta object literal for the '<em><b>SBlock LAnalysis</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SPORT_LANALYSIS__SBLOCK_LANALYSIS = eINSTANCE.getSPortLAnalysis_SBlockLAnalysis();

		/**
		 * The meta object literal for the '<em><b>SPort</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SPORT_LANALYSIS__SPORT = eINSTANCE.getSPortLAnalysis_SPort();

		/**
		 * The meta object literal for the '<em><b>Base Port</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SPORT_LANALYSIS__BASE_PORT = eINSTANCE.getSPortLAnalysis_Base_Port();

		/**
		 * The meta object literal for the '<em><b>SDirection Manual</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute SPORT_LANALYSIS__SDIRECTION_MANUAL = eINSTANCE.getSPortLAnalysis_SDirectionManual();

		/**
		 * The meta object literal for the '<em><b>SDirection LAnalysis</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute SPORT_LANALYSIS__SDIRECTION_LANALYSIS = eINSTANCE.getSPortLAnalysis_SDirectionLAnalysis();

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esflocalanalysis.impl.AbstractSFailureModeOwner <em>Abstract SFailure Mode Owner</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see org.polarsys.esf.esflocalanalysis.impl.AbstractSFailureModeOwner
		 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getAbstractSFailureModeOwner()
		 * @generated
		 */
		EClass ABSTRACT_SFAILURE_MODE_OWNER = eINSTANCE.getAbstractSFailureModeOwner();

		/**
		 * The meta object literal for the '<em><b>SFailure Modes LAnalysis List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference ABSTRACT_SFAILURE_MODE_OWNER__SFAILURE_MODES_LANALYSIS_LIST = eINSTANCE.getAbstractSFailureModeOwner_SFailureModesLAnalysisList();

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esflocalanalysis.impl.AbstractSFailureModeLAnalysis <em>Abstract SFailure Mode LAnalysis</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see org.polarsys.esf.esflocalanalysis.impl.AbstractSFailureModeLAnalysis
		 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getAbstractSFailureModeLAnalysis()
		 * @generated
		 */
		EClass ABSTRACT_SFAILURE_MODE_LANALYSIS = eINSTANCE.getAbstractSFailureModeLAnalysis();

		/**
		 * The meta object literal for the '<em><b>SBlock LAnalysis</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference ABSTRACT_SFAILURE_MODE_LANALYSIS__SBLOCK_LANALYSIS = eINSTANCE.getAbstractSFailureModeLAnalysis_SBlockLAnalysis();

		/**
		 * The meta object literal for the '<em><b>SFailure Mode</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference ABSTRACT_SFAILURE_MODE_LANALYSIS__SFAILURE_MODE = eINSTANCE.getAbstractSFailureModeLAnalysis_SFailureMode();

		/**
		 * The meta object literal for the '<em><b>SFeared Events Families List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference ABSTRACT_SFAILURE_MODE_LANALYSIS__SFEARED_EVENTS_FAMILIES_LIST = eINSTANCE.getAbstractSFailureModeLAnalysis_SFearedEventsFamiliesList();

		/**
		 * The meta object literal for the '<em><b>SFeared Events List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference ABSTRACT_SFAILURE_MODE_LANALYSIS__SFEARED_EVENTS_LIST = eINSTANCE.getAbstractSFailureModeLAnalysis_SFearedEventsList();

		/**
		 * The meta object literal for the '<em><b>Owner</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference ABSTRACT_SFAILURE_MODE_LANALYSIS__OWNER = eINSTANCE.getAbstractSFailureModeLAnalysis_Owner();

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esflocalanalysis.impl.AbstractSPropagationElement <em>Abstract SPropagation Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see org.polarsys.esf.esflocalanalysis.impl.AbstractSPropagationElement
		 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getAbstractSPropagationElement()
		 * @generated
		 */
		EClass ABSTRACT_SPROPAGATION_ELEMENT = eINSTANCE.getAbstractSPropagationElement();

		/**
		 * The meta object literal for the '<em><b>Out SPropagation Links List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference ABSTRACT_SPROPAGATION_ELEMENT__OUT_SPROPAGATION_LINKS_LIST = eINSTANCE.getAbstractSPropagationElement_OutSPropagationLinksList();

		/**
		 * The meta object literal for the '<em><b>In SPropagation Links List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference ABSTRACT_SPROPAGATION_ELEMENT__IN_SPROPAGATION_LINKS_LIST = eINSTANCE.getAbstractSPropagationElement_InSPropagationLinksList();

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esflocalanalysis.impl.SPropagationLink <em>SPropagation Link</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see org.polarsys.esf.esflocalanalysis.impl.SPropagationLink
		 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getSPropagationLink()
		 * @generated
		 */
		EClass SPROPAGATION_LINK = eINSTANCE.getSPropagationLink();

		/**
		 * The meta object literal for the '<em><b>Element Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SPROPAGATION_LINK__ELEMENT_TARGET = eINSTANCE.getSPropagationLink_ElementTarget();

		/**
		 * The meta object literal for the '<em><b>SBlock LAnalysis</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SPROPAGATION_LINK__SBLOCK_LANALYSIS = eINSTANCE.getSPropagationLink_SBlockLAnalysis();

		/**
		 * The meta object literal for the '<em><b>Base Dependency</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SPROPAGATION_LINK__BASE_DEPENDENCY = eINSTANCE.getSPropagationLink_Base_Dependency();

		/**
		 * The meta object literal for the '<em><b>Element Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SPROPAGATION_LINK__ELEMENT_SOURCE = eINSTANCE.getSPropagationLink_ElementSource();

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esflocalanalysis.impl.SBlockLAnalysis <em>SBlock LAnalysis</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see org.polarsys.esf.esflocalanalysis.impl.SBlockLAnalysis
		 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getSBlockLAnalysis()
		 * @generated
		 */
		EClass SBLOCK_LANALYSIS = eINSTANCE.getSBlockLAnalysis();

		/**
		 * The meta object literal for the '<em><b>SBlock</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SBLOCK_LANALYSIS__SBLOCK = eINSTANCE.getSBlockLAnalysis_SBlock();

		/**
		 * The meta object literal for the '<em><b>SFailure Events LAnalysis List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SBLOCK_LANALYSIS__SFAILURE_EVENTS_LANALYSIS_LIST = eINSTANCE.getSBlockLAnalysis_SFailureEventsLAnalysisList();

		/**
		 * The meta object literal for the '<em><b>SPorts LAnalysis List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SBLOCK_LANALYSIS__SPORTS_LANALYSIS_LIST = eINSTANCE.getSBlockLAnalysis_SPortsLAnalysisList();

		/**
		 * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SBLOCK_LANALYSIS__BASE_CLASS = eINSTANCE.getSBlockLAnalysis_Base_Class();

		/**
		 * The meta object literal for the '<em><b>SLogical Gates LAnalysis List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SBLOCK_LANALYSIS__SLOGICAL_GATES_LANALYSIS_LIST = eINSTANCE.getSBlockLAnalysis_SLogicalGatesLAnalysisList();

		/**
		 * The meta object literal for the '<em><b>SFailure Modes LAnalysis List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SBLOCK_LANALYSIS__SFAILURE_MODES_LANALYSIS_LIST = eINSTANCE.getSBlockLAnalysis_SFailureModesLAnalysisList();

		/**
		 * The meta object literal for the '<em><b>SBarriers LAnalysis List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SBLOCK_LANALYSIS__SBARRIERS_LANALYSIS_LIST = eINSTANCE.getSBlockLAnalysis_SBarriersLAnalysisList();

		/**
		 * The meta object literal for the '<em><b>SLocal Analysis</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SBLOCK_LANALYSIS__SLOCAL_ANALYSIS = eINSTANCE.getSBlockLAnalysis_SLocalAnalysis();

		/**
		 * The meta object literal for the '<em><b>SPropagation Links List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SBLOCK_LANALYSIS__SPROPAGATION_LINKS_LIST = eINSTANCE.getSBlockLAnalysis_SPropagationLinksList();

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esflocalanalysis.impl.AbstractSFailureEventLAnalysis <em>Abstract SFailure Event LAnalysis</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see org.polarsys.esf.esflocalanalysis.impl.AbstractSFailureEventLAnalysis
		 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getAbstractSFailureEventLAnalysis()
		 * @generated
		 */
		EClass ABSTRACT_SFAILURE_EVENT_LANALYSIS = eINSTANCE.getAbstractSFailureEventLAnalysis();

		/**
		 * The meta object literal for the '<em><b>SFailure Event</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference ABSTRACT_SFAILURE_EVENT_LANALYSIS__SFAILURE_EVENT = eINSTANCE.getAbstractSFailureEventLAnalysis_SFailureEvent();

		/**
		 * The meta object literal for the '<em><b>SBlock LAnalysis</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference ABSTRACT_SFAILURE_EVENT_LANALYSIS__SBLOCK_LANALYSIS = eINSTANCE.getAbstractSFailureEventLAnalysis_SBlockLAnalysis();

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esflocalanalysis.impl.AbstractSLogicalGateLAnalysis <em>Abstract SLogical Gate LAnalysis</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see org.polarsys.esf.esflocalanalysis.impl.AbstractSLogicalGateLAnalysis
		 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getAbstractSLogicalGateLAnalysis()
		 * @generated
		 */
		EClass ABSTRACT_SLOGICAL_GATE_LANALYSIS = eINSTANCE.getAbstractSLogicalGateLAnalysis();

		/**
		 * The meta object literal for the '<em><b>SBlock LAnalysis</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference ABSTRACT_SLOGICAL_GATE_LANALYSIS__SBLOCK_LANALYSIS = eINSTANCE.getAbstractSLogicalGateLAnalysis_SBlockLAnalysis();

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esflocalanalysis.impl.SBarrierLAnalysis <em>SBarrier LAnalysis</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see org.polarsys.esf.esflocalanalysis.impl.SBarrierLAnalysis
		 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getSBarrierLAnalysis()
		 * @generated
		 */
		EClass SBARRIER_LANALYSIS = eINSTANCE.getSBarrierLAnalysis();

		/**
		 * The meta object literal for the '<em><b>SBarrier</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SBARRIER_LANALYSIS__SBARRIER = eINSTANCE.getSBarrierLAnalysis_SBarrier();

		/**
		 * The meta object literal for the '<em><b>Base Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SBARRIER_LANALYSIS__BASE_PROPERTY = eINSTANCE.getSBarrierLAnalysis_Base_Property();

		/**
		 * The meta object literal for the '<em><b>SBlock LAnalysis</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SBARRIER_LANALYSIS__SBLOCK_LANALYSIS = eINSTANCE.getSBarrierLAnalysis_SBlockLAnalysis();

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esflocalanalysis.impl.SLocalAnalysis <em>SLocal Analysis</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see org.polarsys.esf.esflocalanalysis.impl.SLocalAnalysis
		 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getSLocalAnalysis()
		 * @generated
		 */
		EClass SLOCAL_ANALYSIS = eINSTANCE.getSLocalAnalysis();

		/**
		 * The meta object literal for the '<em><b>Base Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SLOCAL_ANALYSIS__BASE_PACKAGE = eINSTANCE.getSLocalAnalysis_Base_Package();

		/**
		 * The meta object literal for the '<em><b>SSystem Events Library</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SLOCAL_ANALYSIS__SSYSTEM_EVENTS_LIBRARY = eINSTANCE.getSLocalAnalysis_SSystemEventsLibrary();

		/**
		 * The meta object literal for the '<em><b>SFeared Events Library</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SLOCAL_ANALYSIS__SFEARED_EVENTS_LIBRARY = eINSTANCE.getSLocalAnalysis_SFearedEventsLibrary();

		/**
		 * The meta object literal for the '<em><b>SBlocks LAnalysis List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SLOCAL_ANALYSIS__SBLOCKS_LANALYSIS_LIST = eINSTANCE.getSLocalAnalysis_SBlocksLAnalysisList();

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esflocalanalysis.impl.SSystemEventsLibrary <em>SSystem Events Library</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see org.polarsys.esf.esflocalanalysis.impl.SSystemEventsLibrary
		 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getSSystemEventsLibrary()
		 * @generated
		 */
		EClass SSYSTEM_EVENTS_LIBRARY = eINSTANCE.getSSystemEventsLibrary();

		/**
		 * The meta object literal for the '<em><b>Base Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SSYSTEM_EVENTS_LIBRARY__BASE_PACKAGE = eINSTANCE.getSSystemEventsLibrary_Base_Package();

		/**
		 * The meta object literal for the '<em><b>SSystem Event Types List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SSYSTEM_EVENTS_LIBRARY__SSYSTEM_EVENT_TYPES_LIST = eINSTANCE.getSSystemEventsLibrary_SSystemEventTypesList();

		/**
		 * The meta object literal for the '<em><b>SLocal Analysis</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SSYSTEM_EVENTS_LIBRARY__SLOCAL_ANALYSIS = eINSTANCE.getSSystemEventsLibrary_SLocalAnalysis();

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esflocalanalysis.impl.SSystemEventType <em>SSystem Event Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see org.polarsys.esf.esflocalanalysis.impl.SSystemEventType
		 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getSSystemEventType()
		 * @generated
		 */
		EClass SSYSTEM_EVENT_TYPE = eINSTANCE.getSSystemEventType();

		/**
		 * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SSYSTEM_EVENT_TYPE__BASE_CLASS = eINSTANCE.getSSystemEventType_Base_Class();

		/**
		 * The meta object literal for the '<em><b>SFailure Event</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SSYSTEM_EVENT_TYPE__SFAILURE_EVENT = eINSTANCE.getSSystemEventType_SFailureEvent();

		/**
		 * The meta object literal for the '<em><b>Instances List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SSYSTEM_EVENT_TYPE__INSTANCES_LIST = eINSTANCE.getSSystemEventType_InstancesList();

		/**
		 * The meta object literal for the '<em><b>SSystem Events Library</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SSYSTEM_EVENT_TYPE__SSYSTEM_EVENTS_LIBRARY = eINSTANCE.getSSystemEventType_SSystemEventsLibrary();

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esflocalanalysis.impl.SSystemEvent <em>SSystem Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see org.polarsys.esf.esflocalanalysis.impl.SSystemEvent
		 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getSSystemEvent()
		 * @generated
		 */
		EClass SSYSTEM_EVENT = eINSTANCE.getSSystemEvent();

		/**
		 * The meta object literal for the '<em><b>Base Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SSYSTEM_EVENT__BASE_PROPERTY = eINSTANCE.getSSystemEvent_Base_Property();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SSYSTEM_EVENT__TYPE = eINSTANCE.getSSystemEvent_Type();

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esflocalanalysis.impl.SFearedEventsLibrary <em>SFeared Events Library</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see org.polarsys.esf.esflocalanalysis.impl.SFearedEventsLibrary
		 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getSFearedEventsLibrary()
		 * @generated
		 */
		EClass SFEARED_EVENTS_LIBRARY = eINSTANCE.getSFearedEventsLibrary();

		/**
		 * The meta object literal for the '<em><b>Base Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SFEARED_EVENTS_LIBRARY__BASE_PACKAGE = eINSTANCE.getSFearedEventsLibrary_Base_Package();

		/**
		 * The meta object literal for the '<em><b>SFeared Events Families List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SFEARED_EVENTS_LIBRARY__SFEARED_EVENTS_FAMILIES_LIST = eINSTANCE.getSFearedEventsLibrary_SFearedEventsFamiliesList();

		/**
		 * The meta object literal for the '<em><b>SFeared Events List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SFEARED_EVENTS_LIBRARY__SFEARED_EVENTS_LIST = eINSTANCE.getSFearedEventsLibrary_SFearedEventsList();

		/**
		 * The meta object literal for the '<em><b>SLocal Analysis</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SFEARED_EVENTS_LIBRARY__SLOCAL_ANALYSIS = eINSTANCE.getSFearedEventsLibrary_SLocalAnalysis();

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esflocalanalysis.impl.SFearedEventsFamily <em>SFeared Events Family</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see org.polarsys.esf.esflocalanalysis.impl.SFearedEventsFamily
		 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getSFearedEventsFamily()
		 * @generated
		 */
		EClass SFEARED_EVENTS_FAMILY = eINSTANCE.getSFearedEventsFamily();

		/**
		 * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SFEARED_EVENTS_FAMILY__BASE_CLASS = eINSTANCE.getSFearedEventsFamily_Base_Class();

		/**
		 * The meta object literal for the '<em><b>SFeared Events List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SFEARED_EVENTS_FAMILY__SFEARED_EVENTS_LIST = eINSTANCE.getSFearedEventsFamily_SFearedEventsList();

		/**
		 * The meta object literal for the '<em><b>Is Selected</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute SFEARED_EVENTS_FAMILY__IS_SELECTED = eINSTANCE.getSFearedEventsFamily_IsSelected();

		/**
		 * The meta object literal for the '<em><b>Sub Families List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SFEARED_EVENTS_FAMILY__SUB_FAMILIES_LIST = eINSTANCE.getSFearedEventsFamily_SubFamiliesList();

		/**
		 * The meta object literal for the '<em><b>All SFeared Events List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SFEARED_EVENTS_FAMILY__ALL_SFEARED_EVENTS_LIST = eINSTANCE.getSFearedEventsFamily_AllSFearedEventsList();

		/**
		 * The meta object literal for the '<em><b>SFailure Modes LAnalysis List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SFEARED_EVENTS_FAMILY__SFAILURE_MODES_LANALYSIS_LIST = eINSTANCE.getSFearedEventsFamily_SFailureModesLAnalysisList();

		/**
		 * The meta object literal for the '<em><b>SFeared Events Library</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SFEARED_EVENTS_FAMILY__SFEARED_EVENTS_LIBRARY = eINSTANCE.getSFearedEventsFamily_SFearedEventsLibrary();

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esflocalanalysis.impl.SFearedEvent <em>SFeared Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see org.polarsys.esf.esflocalanalysis.impl.SFearedEvent
		 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getSFearedEvent()
		 * @generated
		 */
		EClass SFEARED_EVENT = eINSTANCE.getSFearedEvent();

		/**
		 * The meta object literal for the '<em><b>Is Selected</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute SFEARED_EVENT__IS_SELECTED = eINSTANCE.getSFearedEvent_IsSelected();

		/**
		 * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SFEARED_EVENT__BASE_CLASS = eINSTANCE.getSFearedEvent_Base_Class();

		/**
		 * The meta object literal for the '<em><b>SFailure Modes LAnalysis List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SFEARED_EVENT__SFAILURE_MODES_LANALYSIS_LIST = eINSTANCE.getSFearedEvent_SFailureModesLAnalysisList();

		/**
		 * The meta object literal for the '<em><b>SFeared Events Library</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SFEARED_EVENT__SFEARED_EVENTS_LIBRARY = eINSTANCE.getSFearedEvent_SFearedEventsLibrary();

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esflocalanalysis.impl.SDysfunctionalAssociation <em>SDysfunctional Association</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see org.polarsys.esf.esflocalanalysis.impl.SDysfunctionalAssociation
		 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getSDysfunctionalAssociation()
		 * @generated
		 */
		EClass SDYSFUNCTIONAL_ASSOCIATION = eINSTANCE.getSDysfunctionalAssociation();

		/**
		 * The meta object literal for the '<em><b>SFailure Mode LAnalysis</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SDYSFUNCTIONAL_ASSOCIATION__SFAILURE_MODE_LANALYSIS = eINSTANCE.getSDysfunctionalAssociation_SFailureModeLAnalysis();

		/**
		 * The meta object literal for the '<em><b>Base Connector</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SDYSFUNCTIONAL_ASSOCIATION__BASE_CONNECTOR = eINSTANCE.getSDysfunctionalAssociation_Base_Connector();

		/**
		 * The meta object literal for the '<em><b>SFailure Mode Owner</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SDYSFUNCTIONAL_ASSOCIATION__SFAILURE_MODE_OWNER = eINSTANCE.getSDysfunctionalAssociation_SFailureModeOwner();

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esflocalanalysis.impl.SAndGateLAnalysis <em>SAnd Gate LAnalysis</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see org.polarsys.esf.esflocalanalysis.impl.SAndGateLAnalysis
		 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getSAndGateLAnalysis()
		 * @generated
		 */
		EClass SAND_GATE_LANALYSIS = eINSTANCE.getSAndGateLAnalysis();

		/**
		 * The meta object literal for the '<em><b>Base Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SAND_GATE_LANALYSIS__BASE_PROPERTY = eINSTANCE.getSAndGateLAnalysis_Base_Property();

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esflocalanalysis.impl.SOrGateLAnalysis <em>SOr Gate LAnalysis</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see org.polarsys.esf.esflocalanalysis.impl.SOrGateLAnalysis
		 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getSOrGateLAnalysis()
		 * @generated
		 */
		EClass SOR_GATE_LANALYSIS = eINSTANCE.getSOrGateLAnalysis();

		/**
		 * The meta object literal for the '<em><b>Base Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SOR_GATE_LANALYSIS__BASE_PROPERTY = eINSTANCE.getSOrGateLAnalysis_Base_Property();

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esflocalanalysis.impl.SUntimelyFailureMode <em>SUntimely Failure Mode</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see org.polarsys.esf.esflocalanalysis.impl.SUntimelyFailureMode
		 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getSUntimelyFailureMode()
		 * @generated
		 */
		EClass SUNTIMELY_FAILURE_MODE = eINSTANCE.getSUntimelyFailureMode();

		/**
		 * The meta object literal for the '<em><b>Base Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SUNTIMELY_FAILURE_MODE__BASE_PROPERTY = eINSTANCE.getSUntimelyFailureMode_Base_Property();

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esflocalanalysis.impl.SAbsentFailureMode <em>SAbsent Failure Mode</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see org.polarsys.esf.esflocalanalysis.impl.SAbsentFailureMode
		 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getSAbsentFailureMode()
		 * @generated
		 */
		EClass SABSENT_FAILURE_MODE = eINSTANCE.getSAbsentFailureMode();

		/**
		 * The meta object literal for the '<em><b>Base Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SABSENT_FAILURE_MODE__BASE_PROPERTY = eINSTANCE.getSAbsentFailureMode_Base_Property();

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esflocalanalysis.impl.SErroneousFailureMode <em>SErroneous Failure Mode</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see org.polarsys.esf.esflocalanalysis.impl.SErroneousFailureMode
		 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getSErroneousFailureMode()
		 * @generated
		 */
		EClass SERRONEOUS_FAILURE_MODE = eINSTANCE.getSErroneousFailureMode();

		/**
		 * The meta object literal for the '<em><b>Base Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SERRONEOUS_FAILURE_MODE__BASE_PROPERTY = eINSTANCE.getSErroneousFailureMode_Base_Property();

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esflocalanalysis.impl.SLocalEvent <em>SLocal Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see org.polarsys.esf.esflocalanalysis.impl.SLocalEvent
		 * @see org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage#getSLocalEvent()
		 * @generated
		 */
		EClass SLOCAL_EVENT = eINSTANCE.getSLocalEvent();

		/**
		 * The meta object literal for the '<em><b>Base Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SLOCAL_EVENT__BASE_PROPERTY = eINSTANCE.getSLocalEvent_Base_Property();

	}

} // IESFLocalAnalysisPackage
