/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esflocalanalysis.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.polarsys.esf.esflocalanalysis.IAbstractSFailureEventLAnalysis;
import org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisPackage;
import org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis;

import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract SFailure Event LAnalysis</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.AbstractSFailureEventLAnalysis#getSFailureEvent <em>SFailure Event</em>}</li>
 * <li>{@link org.polarsys.esf.esflocalanalysis.impl.AbstractSFailureEventLAnalysis#getSBlockLAnalysis <em>SBlock LAnalysis</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AbstractSFailureEventLAnalysis
		extends AbstractSPropagationElement
		implements IAbstractSFailureEventLAnalysis {

	/**
	 * The cached value of the '{@link #getSFailureEvent() <em>SFailure Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getSFailureEvent()
	 * @generated
	 * @ordered
	 */
	protected ISFailureEvent sFailureEvent;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected AbstractSFailureEventLAnalysis() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IESFLocalAnalysisPackage.Literals.ABSTRACT_SFAILURE_EVENT_LANALYSIS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISFailureEvent getSFailureEvent() {
		if (sFailureEvent != null && sFailureEvent.eIsProxy()) {
			InternalEObject oldSFailureEvent = (InternalEObject) sFailureEvent;
			sFailureEvent = (ISFailureEvent) eResolveProxy(oldSFailureEvent);
			if (sFailureEvent != oldSFailureEvent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_EVENT_LANALYSIS__SFAILURE_EVENT, oldSFailureEvent, sFailureEvent));
			}
		}
		return sFailureEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ISFailureEvent basicGetSFailureEvent() {
		return sFailureEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setSFailureEvent(ISFailureEvent newSFailureEvent) {
		ISFailureEvent oldSFailureEvent = sFailureEvent;
		sFailureEvent = newSFailureEvent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_EVENT_LANALYSIS__SFAILURE_EVENT, oldSFailureEvent, sFailureEvent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISBlockLAnalysis getSBlockLAnalysis() {
		ISBlockLAnalysis sBlockLAnalysis = basicGetSBlockLAnalysis();
		return sBlockLAnalysis != null && sBlockLAnalysis.eIsProxy() ? (ISBlockLAnalysis) eResolveProxy((InternalEObject) sBlockLAnalysis) : sBlockLAnalysis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ISBlockLAnalysis basicGetSBlockLAnalysis() {
		// TODO: implement this method to return the 'SBlock LAnalysis' reference
		// -> do not perform proxy resolution
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setSBlockLAnalysis(ISBlockLAnalysis newSBlockLAnalysis) {
		// TODO: implement this method to set the 'SBlock LAnalysis' reference
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_EVENT_LANALYSIS__SFAILURE_EVENT:
			if (resolve)
				return getSFailureEvent();
			return basicGetSFailureEvent();
		case IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_EVENT_LANALYSIS__SBLOCK_LANALYSIS:
			if (resolve)
				return getSBlockLAnalysis();
			return basicGetSBlockLAnalysis();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_EVENT_LANALYSIS__SFAILURE_EVENT:
			setSFailureEvent((ISFailureEvent) newValue);
			return;
		case IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_EVENT_LANALYSIS__SBLOCK_LANALYSIS:
			setSBlockLAnalysis((ISBlockLAnalysis) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_EVENT_LANALYSIS__SFAILURE_EVENT:
			setSFailureEvent((ISFailureEvent) null);
			return;
		case IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_EVENT_LANALYSIS__SBLOCK_LANALYSIS:
			setSBlockLAnalysis((ISBlockLAnalysis) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_EVENT_LANALYSIS__SFAILURE_EVENT:
			return sFailureEvent != null;
		case IESFLocalAnalysisPackage.ABSTRACT_SFAILURE_EVENT_LANALYSIS__SBLOCK_LANALYSIS:
			return basicGetSBlockLAnalysis() != null;
		}
		return super.eIsSet(featureID);
	}

} // AbstractSFailureEventLAnalysis
