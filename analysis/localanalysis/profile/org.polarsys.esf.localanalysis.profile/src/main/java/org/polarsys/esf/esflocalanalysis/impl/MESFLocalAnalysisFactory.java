/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.esflocalanalysis.impl;

import org.polarsys.esf.esfarchitectureconcepts.impl.ESFArchitectureConceptsFactory;
import org.polarsys.esf.esflocalanalysis.IESFLocalAnalysisFactory;
import org.polarsys.esf.esflocalanalysis.IMESFLocalAnalysisFactory;
import org.polarsys.esf.esflocalanalysis.IMSAbsentFailureMode;
import org.polarsys.esf.esflocalanalysis.IMSAndGateLAnalysis;
import org.polarsys.esf.esflocalanalysis.IMSBarrierLAnalysis;
import org.polarsys.esf.esflocalanalysis.IMSBlockLAnalysis;
import org.polarsys.esf.esflocalanalysis.IMSDysfunctionalAssociation;
import org.polarsys.esf.esflocalanalysis.IMSErroneousFailureMode;
import org.polarsys.esf.esflocalanalysis.IMSFearedEvent;
import org.polarsys.esf.esflocalanalysis.IMSFearedEventsFamily;
import org.polarsys.esf.esflocalanalysis.IMSFearedEventsLibrary;
import org.polarsys.esf.esflocalanalysis.IMSLocalAnalysis;
import org.polarsys.esf.esflocalanalysis.IMSLocalEvent;
import org.polarsys.esf.esflocalanalysis.IMSOrGateLAnalysis;
import org.polarsys.esf.esflocalanalysis.IMSPortLAnalysis;
import org.polarsys.esf.esflocalanalysis.IMSPropagationLink;
import org.polarsys.esf.esflocalanalysis.IMSSystemEvent;
import org.polarsys.esf.esflocalanalysis.IMSSystemEventType;
import org.polarsys.esf.esflocalanalysis.IMSSystemEventsLibrary;
import org.polarsys.esf.esflocalanalysis.IMSUntimelyFailureMode;

/**
 * This factory overrides the generated factory {@link ESFArchitectureConceptsFactory}
 * and returns the new generated interfaces.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 186 $
 */
public class MESFLocalAnalysisFactory
	extends ESFLocalAnalysisFactory
	implements IMESFLocalAnalysisFactory {

	/**
	 * Default constructor.
	 */
	public MESFLocalAnalysisFactory() {
		super();
	}

	/**
	 * @return The initialised factory
	 */
	public static IMESFLocalAnalysisFactory init() {
		IESFLocalAnalysisFactory vFactory = ESFLocalAnalysisFactory.init();

		// Check if the factory returned by the standard implementation can suit,
		// otherwise create a new instance
		if (!(vFactory instanceof IMESFLocalAnalysisFactory)) {
			vFactory = new MESFLocalAnalysisFactory();
		}

		return (IMESFLocalAnalysisFactory) vFactory;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IMSAbsentFailureMode createSAbsentFailureMode() {
		IMSAbsentFailureMode vSAbsentFailureMode = new MSAbsentFailureMode();
		return vSAbsentFailureMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IMSAndGateLAnalysis createSAndGateLAnalysis() {
		IMSAndGateLAnalysis vSAndGateLAnalysis = new MSAndGateLAnalysis();
		return vSAndGateLAnalysis;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IMSBarrierLAnalysis createSBarrierLAnalysis() {
		IMSBarrierLAnalysis vSBarrierLAnalysis = new MSBarrierLAnalysis();
		return vSBarrierLAnalysis;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IMSBlockLAnalysis createSBlockLAnalysis() {
		IMSBlockLAnalysis vSBlockLAnalysis = new MSBlockLAnalysis();
		return vSBlockLAnalysis;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IMSDysfunctionalAssociation createSDysfunctionalAssociation() {
		IMSDysfunctionalAssociation vSDysfunctionalAssociation = new MSDysfunctionalAssociation();
		return vSDysfunctionalAssociation;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IMSErroneousFailureMode createSErroneousFailureMode() {
		IMSErroneousFailureMode vSErroneousFailureMode = new MSErroneousFailureMode();
		return vSErroneousFailureMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IMSLocalAnalysis createSLocalAnalysis() {
		IMSLocalAnalysis vSLocalAnalysis = new MSLocalAnalysis();
		return vSLocalAnalysis;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IMSLocalEvent createSLocalEvent() {
		IMSLocalEvent vSLocalEvent = new MSLocalEvent();
		return vSLocalEvent;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IMSOrGateLAnalysis createSOrGateLAnalysis() {
		IMSOrGateLAnalysis vSOrGateLAnalysis = new MSOrGateLAnalysis();
		return vSOrGateLAnalysis;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IMSPortLAnalysis createSPortLAnalysis() {
		IMSPortLAnalysis vSPortLAnalysis = new MSPortLAnalysis();
		return vSPortLAnalysis;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IMSPropagationLink createSPropagationLink() {
		IMSPropagationLink vSPropagationLink = new MSPropagationLink();
		return vSPropagationLink;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IMSSystemEvent createSSystemEvent() {
		IMSSystemEvent vSSystemEvent = new MSSystemEvent();
		return vSSystemEvent;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IMSSystemEventsLibrary createSSystemEventsLibrary() {
		IMSSystemEventsLibrary vSSystemEventsLibrary = new MSSystemEventsLibrary();
		return vSSystemEventsLibrary;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IMSSystemEventType createSSystemEventType() {
		IMSSystemEventType vSSystemEventType = new MSSystemEventType();
		return vSSystemEventType;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IMSUntimelyFailureMode createSUntimelyFailureMode() {
		IMSUntimelyFailureMode vSUltimelyFailureMode = new MSUntimelyFailureMode();
		return vSUltimelyFailureMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IMSFearedEvent createSFearedEvent() {
		IMSFearedEvent vSFearedEvent = new MSFearedEvent();
		return vSFearedEvent;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IMSFearedEventsFamily createSFearedEventsFamily() {
		IMSFearedEventsFamily vSFearedEventSFamily = new MSFearedEventsFamily();
		return vSFearedEventSFamily;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IMSFearedEventsLibrary createSFearedEventsLibrary() {
		IMSFearedEventsLibrary vSFearedEventsLibrary = new MSFearedEventsLibrary();
		return vSFearedEventsLibrary;
	}
}
