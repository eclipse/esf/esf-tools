/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.esflocalanalysis;

import org.polarsys.esf.esflocalanalysis.impl.MESFLocalAnalysisFactory;

/**
 * This interface can override the generated interface {@link IESFLocalAnalysisFactory}.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 186 $
 */
public interface IMESFLocalAnalysisFactory
	extends IESFLocalAnalysisFactory {

	/**
	 * Specialise the eINSTANCE initialisation with the new interface type
	 * (overridden in the override_factory extension).
	 */
	IMESFLocalAnalysisFactory INSTANCE = MESFLocalAnalysisFactory.init();

	@Override
	IMSAbsentFailureMode createSAbsentFailureMode();

	@Override
	IMSAndGateLAnalysis createSAndGateLAnalysis();

	@Override
	IMSBarrierLAnalysis createSBarrierLAnalysis();

	@Override
	IMSBlockLAnalysis createSBlockLAnalysis();

	@Override
	IMSDysfunctionalAssociation createSDysfunctionalAssociation();

	@Override
	IMSErroneousFailureMode createSErroneousFailureMode();

	@Override
	IMSLocalAnalysis createSLocalAnalysis();

	@Override
	IMSLocalEvent createSLocalEvent();

	@Override
	IMSOrGateLAnalysis createSOrGateLAnalysis();

	@Override
	IMSPortLAnalysis createSPortLAnalysis();

	@Override
	IMSPropagationLink createSPropagationLink();

	@Override
	IMSSystemEvent createSSystemEvent();

	@Override
	IMSSystemEventsLibrary createSSystemEventsLibrary();

	@Override
	IMSSystemEventType createSSystemEventType();

	@Override
	IMSUntimelyFailureMode createSUntimelyFailureMode();

	@Override
	IMSFearedEvent createSFearedEvent();

	@Override
	IMSFearedEventsFamily createSFearedEventsFamily();

	@Override
	IMSFearedEventsLibrary createSFearedEventsLibrary();
}
