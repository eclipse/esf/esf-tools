/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.esflocalanalysis.impl;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreEList.UnmodifiableEList;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.esfcore.impl.GenericAbstractSElement;
import org.polarsys.esf.esflocalanalysis.IAbstractSFailureEventLAnalysis;
import org.polarsys.esf.esflocalanalysis.IMSSystemEventType;
import org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis;
import org.polarsys.esf.esflocalanalysis.ISLocalAnalysis;
import org.polarsys.esf.esflocalanalysis.ISSystemEvent;
import org.polarsys.esf.esflocalanalysis.ISSystemEventType;
import org.polarsys.esf.esflocalanalysis.ISSystemEventsLibrary;

/**
 * This class can override the generated class {@link SSystemEventType} and will be
 * used by the custom factory.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class MSSystemEventType
	extends SSystemEventType
	implements IMSSystemEventType {

	/**
	 * Default constructor.
	 */
	public MSSystemEventType() {
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return GenericAbstractSElement.getName(getBase_Class());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getUUID() {
		return GenericAbstractSElement.getUUID(getBase_Class());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EList<ISSystemEvent> getInstancesList() {
		Class vBase = getBase_Class();
		EList<ISSystemEvent> vInstancesList = new BasicEList<ISSystemEvent>();
		ISLocalAnalysis vSLocalAnalysis = null;

		if (vBase != null) {
			Element vGrandOwner = vBase.getOwner().getOwner();
			if (vGrandOwner instanceof Package) {
				EObject vStereotypeApplication = UMLUtil.getStereotypeApplication(vGrandOwner, ISLocalAnalysis.class);
				if (vStereotypeApplication != null) {
					vSLocalAnalysis = (ISLocalAnalysis) vStereotypeApplication;
				}
			}

			if (vSLocalAnalysis != null) {
				for (ISBlockLAnalysis vSBlockLAnalysis : vSLocalAnalysis.getSBlocksLAnalysisList()) {
					for (IAbstractSFailureEventLAnalysis vSFailureEvent : vSBlockLAnalysis
						.getSFailureEventsLAnalysisList()) {
						if (vSFailureEvent instanceof ISSystemEvent) {
							ISSystemEvent vSSystemEvent = (ISSystemEvent) vSFailureEvent;
							ISSystemEventType vType = vSSystemEvent.getType();
							if (vType != null && vType.equals(this)) {
								vInstancesList.add(vSSystemEvent);
							}
						}
					}
				}
			}
		}

		UnmodifiableEList<ISSystemEvent> vUInstancesList =
			new UnmodifiableEList<ISSystemEvent>(
				this,
				ESFLocalAnalysisPackage.eINSTANCE.getSSystemEventType_InstancesList(),
				vInstancesList.size(),
				vInstancesList.toArray());

		return vUInstancesList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ISSystemEventsLibrary basicGetSSystemEventsLibrary() {
		Class vBase = getBase_Class();
		ISSystemEventsLibrary vSSystemEventsLibrary = null;

		if (vBase != null) {
			Element vOwner = vBase.getOwner();
			EObject vStereotypeApplication = UMLUtil.getStereotypeApplication(vOwner, ISSystemEventsLibrary.class);
			if (vStereotypeApplication != null) {
				vSSystemEventsLibrary = (ISSystemEventsLibrary) vStereotypeApplication;
			}
		}
		return vSSystemEventsLibrary;
	}
}
