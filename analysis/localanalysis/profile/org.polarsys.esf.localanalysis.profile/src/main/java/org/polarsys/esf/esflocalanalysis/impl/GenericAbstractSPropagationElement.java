/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esflocalanalysis.impl;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.DirectedRelationship;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.esflocalanalysis.ISPropagationLink;

/**
 * This class implements the derived attributes of the generated class {@link AbstractSPropagationElement}.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public final class GenericAbstractSPropagationElement {

	/**
	 * Default constructor, private as it's a utility class.
	 */
	private GenericAbstractSPropagationElement() {
		// Nothing to do
	}

	/**
	 * Get List of Out SPropagationLinks.
	 *
	 * @param pBaseProperty The element base of a SPropagationElement
	 * @return The List of Out SPropagationLinks
	 */
	public static EList<ISPropagationLink> getOutSPropagationLinksList(final Property pBaseProperty) {
		EList<ISPropagationLink> vOutSPropagationLinksList = new BasicEList<ISPropagationLink>();

		if (pBaseProperty != null) {
			for (Dependency vDependency : pBaseProperty.getClientDependencies()) {
				EObject vStereotypeApplication =
					UMLUtil.getStereotypeApplication(vDependency, ISPropagationLink.class);
				if (vStereotypeApplication != null) {
					vOutSPropagationLinksList.add((ISPropagationLink) vStereotypeApplication);
				}
			}
		}

		return vOutSPropagationLinksList;
	}

	/**
	 * Get List of In SPropagationLinks.
	 *
	 * @param pBaseProperty The element base a SPropagationElement
	 * @return The List of In SPropagationLinks
	 */
	public static EList<ISPropagationLink> getInSPropagationLinksList(final Property pBaseProperty) {
		EList<ISPropagationLink> vInSPropagationLinksList = new BasicEList<ISPropagationLink>();

		if (pBaseProperty != null) {
			for (DirectedRelationship vRelationship : pBaseProperty.getTargetDirectedRelationships()) {
				EObject vStereotypeApplication =
					UMLUtil.getStereotypeApplication(vRelationship, ISPropagationLink.class);
				if (vStereotypeApplication != null) {
					vInSPropagationLinksList.add((ISPropagationLink) vStereotypeApplication);
				}
			}
		}

		return vInSPropagationLinksList;
	}

}
