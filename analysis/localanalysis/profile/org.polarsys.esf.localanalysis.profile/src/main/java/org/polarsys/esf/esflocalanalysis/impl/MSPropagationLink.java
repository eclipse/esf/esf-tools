/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.esflocalanalysis.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.esfcore.impl.GenericAbstractSElement;
import org.polarsys.esf.esflocalanalysis.IAbstractSPropagationElement;
import org.polarsys.esf.esflocalanalysis.IMSPropagationLink;
import org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis;
import org.polarsys.esf.localanalysis.profile.set.ESFLocalAnalysisSet;

/**
 * This class can override the generated class {@link SPropagationLink} and will be
 * used by the custom factory.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class MSPropagationLink
	extends SPropagationLink
	implements IMSPropagationLink {

	/**
	 * Default constructor.
	 */
	public MSPropagationLink() {
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return GenericAbstractSElement.getName(getBase_Dependency());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getUUID() {
		return GenericAbstractSElement.getUUID(getBase_Dependency());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ISBlockLAnalysis basicGetSBlockLAnalysis() {
		ISBlockLAnalysis vSBlockLAnalysis = null;
		Dependency vBase = getBase_Dependency();

		if (vBase != null) {
			NamedElement vElement = vBase.getClients().get(0);
			if (vElement != null) {
				EObject vStereotypeApplication =
					UMLUtil.getStereotypeApplication(vElement.getOwner(), ISBlockLAnalysis.class);
				if (vStereotypeApplication != null) {
					vSBlockLAnalysis = (ISBlockLAnalysis) vStereotypeApplication;
				}
			}
		}
		return vSBlockLAnalysis;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IAbstractSPropagationElement basicGetElementSource() {
		IAbstractSPropagationElement vElementSource = null;
		Dependency vBase = getBase_Dependency();

		if (vBase != null) {
			Iterator<NamedElement> vIterator = vBase.getClients().iterator();

			while ((vElementSource == null) && vIterator.hasNext()) {
				NamedElement vElement = vIterator.next();
				EObject vStereotypeApplication = getStereotypeApplicationOfSPropagationElement(vElement);

				if (vStereotypeApplication != null) {
					vElementSource = (IAbstractSPropagationElement) vStereotypeApplication;
				}
			}
		}
		return vElementSource;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IAbstractSPropagationElement basicGetElementTarget() {
		IAbstractSPropagationElement vElementTarget = null;
		Dependency vBase = getBase_Dependency();

		if (vBase != null) {
			Iterator<NamedElement> vIterator = vBase.getSuppliers().iterator();

			while ((vElementTarget == null) && vIterator.hasNext()) {
				NamedElement vElement = vIterator.next();
				EObject vStereotypeApplication = getStereotypeApplicationOfSPropagationElement(vElement);

				if (vStereotypeApplication != null) {
					vElementTarget = (IAbstractSPropagationElement) vStereotypeApplication;
				}
			}

		}
		return vElementTarget;
	}

	/**
	 * Get the Stereotype Application of SPropagationElement.
	 *
	 * @param pElement The UML Element
	 * @return The Stereotype Application
	 */
	private EObject getStereotypeApplicationOfSPropagationElement(final Element pElement) {
		EObject vStereotypeApplication = null;
		List<java.lang.Class<? extends IAbstractSPropagationElement>> vSPropagationElementsList =
			new ArrayList<java.lang.Class<? extends IAbstractSPropagationElement>>();

		vSPropagationElementsList.addAll(ESFLocalAnalysisSet.SLOGICALGATES_LIST);
		vSPropagationElementsList.addAll(ESFLocalAnalysisSet.SFAILUREEVENTS_LIST);
		vSPropagationElementsList.addAll(ESFLocalAnalysisSet.SFAILUREMODES_LIST);

		Iterator<java.lang.Class<? extends IAbstractSPropagationElement>> vIterator =
			vSPropagationElementsList.iterator();

		while ((vStereotypeApplication == null) && vIterator.hasNext()) {
			java.lang.Class<? extends IAbstractSPropagationElement> vSPropagationElement = vIterator.next();
			if (UMLUtil.getStereotypeApplication(pElement, vSPropagationElement) != null) {
				vStereotypeApplication = UMLUtil.getStereotypeApplication(pElement, vSPropagationElement);
			}
		}
		return vStereotypeApplication;
	}
}
