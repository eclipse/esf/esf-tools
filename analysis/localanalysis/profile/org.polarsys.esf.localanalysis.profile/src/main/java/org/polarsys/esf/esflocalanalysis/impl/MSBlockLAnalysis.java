/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.esflocalanalysis.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreEList.UnmodifiableEList;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Relationship;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.esfcore.impl.GenericAbstractSElement;
import org.polarsys.esf.esflocalanalysis.IAbstractSFailureEventLAnalysis;
import org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis;
import org.polarsys.esf.esflocalanalysis.IAbstractSLogicalGateLAnalysis;
import org.polarsys.esf.esflocalanalysis.IAbstractSPropagationElement;
import org.polarsys.esf.esflocalanalysis.IMSBlockLAnalysis;
import org.polarsys.esf.esflocalanalysis.ISBarrierLAnalysis;
import org.polarsys.esf.esflocalanalysis.ISLocalAnalysis;
import org.polarsys.esf.esflocalanalysis.ISPortLAnalysis;
import org.polarsys.esf.esflocalanalysis.ISPropagationLink;
import org.polarsys.esf.localanalysis.profile.set.ESFLocalAnalysisSet;

/**
 * This class can override the generated class {@link SBlockLAnalysis} and will be
 * used by the custom factory.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class MSBlockLAnalysis
	extends SBlockLAnalysis
	implements IMSBlockLAnalysis {

	/**
	 * Default constructor.
	 */
	public MSBlockLAnalysis() {
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return GenericAbstractSElement.getName(getBase_Class());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getUUID() {
		return GenericAbstractSElement.getUUID(getBase_Class());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EList<IAbstractSFailureEventLAnalysis> getSFailureEventsLAnalysisList() {
		EList<IAbstractSFailureEventLAnalysis> vSFailureEventsLAnalysisList =
			new BasicEList<IAbstractSFailureEventLAnalysis>();
		Class vBase = getBase_Class();

		if (vBase != null) {
			/**
			 * Retrieve all SLocalEvents and SSystemEvents.
			 */
			for (Element vElement : vBase.getOwnedElements()) {
				Iterator<java.lang.Class<? extends IAbstractSFailureEventLAnalysis>> vIterator =
					ESFLocalAnalysisSet.SFAILUREEVENTS_LIST.iterator();
				EObject vStereotypeApplication = null;
				while (vStereotypeApplication == null && vIterator.hasNext()) {
					java.lang.Class<? extends IAbstractSFailureEventLAnalysis> vSFailureEventLAnalysis =
						vIterator.next();
					vStereotypeApplication = UMLUtil.getStereotypeApplication(vElement, vSFailureEventLAnalysis);
				}
				if (vStereotypeApplication != null) {
					vSFailureEventsLAnalysisList.add((IAbstractSFailureEventLAnalysis) vStereotypeApplication);
				}
			}
		}

		UnmodifiableEList<IAbstractSFailureEventLAnalysis> vUSFailureEventsLAnalysisList =
			new UnmodifiableEList<IAbstractSFailureEventLAnalysis>(
				this,
				ESFLocalAnalysisPackage.eINSTANCE.getSBlockLAnalysis_SFailureEventsLAnalysisList(),
				vSFailureEventsLAnalysisList.size(),
				vSFailureEventsLAnalysisList.toArray());

		return vUSFailureEventsLAnalysisList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EList<ISPropagationLink> getSPropagationLinksList() {
		EList<ISPropagationLink> vSPropagationLinksList = new BasicEList<ISPropagationLink>();
		Class vBase = getBase_Class();

		if (vBase != null) {
			/**
			 * Retrieve all SPropagationLinks.
			 */
			for (Element vElement : vBase.getOwnedElements()) {
				if (isPropagationElement(vElement)) {
					for (Relationship vRelationship : vElement.getRelationships()) {
						EObject vStereotypeApplication =
							UMLUtil.getStereotypeApplication(vRelationship, ISPropagationLink.class);
						if (vStereotypeApplication != null) {
							/* Verify if the propagationLink exists in the list */
							ISPropagationLink vSPropagationLink = (ISPropagationLink) vStereotypeApplication;
							if (!vSPropagationLinksList.contains(vSPropagationLink)) {
								vSPropagationLinksList.add(vSPropagationLink);
							}
						}
					}
				}
			}
		}

		UnmodifiableEList<ISPropagationLink> vUSPropagationLinksList = new UnmodifiableEList<ISPropagationLink>(
			this,
			ESFLocalAnalysisPackage.eINSTANCE.getSBlockLAnalysis_SPropagationLinksList(),
			vSPropagationLinksList.size(),
			vSPropagationLinksList.toArray());
		return vUSPropagationLinksList;
	}

	/**
	 * Check if the UML Element is a Propagation Element by stereotype application.
	 *
	 * @param pElement The UML Element
	 * @return True if the element is a Propagation Element
	 */
	private boolean isPropagationElement(final Element pElement) {
		boolean vIsPropagationElement = false;
		List<java.lang.Class<? extends IAbstractSPropagationElement>> vSPropagationElementsList =
			new ArrayList<java.lang.Class<? extends IAbstractSPropagationElement>>();

		vSPropagationElementsList.addAll(ESFLocalAnalysisSet.SLOGICALGATES_LIST);
		vSPropagationElementsList.addAll(ESFLocalAnalysisSet.SFAILUREEVENTS_LIST);
		vSPropagationElementsList.addAll(ESFLocalAnalysisSet.SFAILUREMODES_LIST);

		Iterator<java.lang.Class<? extends IAbstractSPropagationElement>> vIterator =
			vSPropagationElementsList.iterator();

		while (!vIsPropagationElement && vIterator.hasNext()) {
			java.lang.Class<? extends IAbstractSPropagationElement> vSPropagationElement = vIterator.next();
			if (UMLUtil.getStereotypeApplication(pElement, vSPropagationElement) != null) {
				vIsPropagationElement = true;
			}
		}
		return vIsPropagationElement;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EList<ISPortLAnalysis> getSPortsLAnalysisList() {
		EList<ISPortLAnalysis> vSPortsLAnalysisList = new BasicEList<ISPortLAnalysis>();
		Class vBase = getBase_Class();

		if (vBase != null) {
			/**
			 * Retrieve all SPortsLAnalysis.
			 */
			for (Element vElement : vBase.getOwnedElements()) {
				EObject vStereotypeApplication = UMLUtil.getStereotypeApplication(vElement, ISPortLAnalysis.class);
				if (vStereotypeApplication != null) {
					vSPortsLAnalysisList.add((ISPortLAnalysis) vStereotypeApplication);
				}
			}
		}

		UnmodifiableEList<ISPortLAnalysis> vUSPortsLAnalysisList = new UnmodifiableEList<ISPortLAnalysis>(
			this,
			ESFLocalAnalysisPackage.eINSTANCE.getSBlockLAnalysis_SPortsLAnalysisList(),
			vSPortsLAnalysisList.size(),
			vSPortsLAnalysisList.toArray());
		return vUSPortsLAnalysisList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EList<IAbstractSLogicalGateLAnalysis> getSLogicalGatesLAnalysisList() {
		EList<IAbstractSLogicalGateLAnalysis> vSLogicalGatesLAnalysisList =
			new BasicEList<IAbstractSLogicalGateLAnalysis>();
		Class vBase = getBase_Class();

		if (vBase != null) {
			/**
			 * Retrieve all SOrGatesLAnalysis and SAndGatesLAnalysis.
			 */
			for (Element vElement : vBase.getOwnedElements()) {
				Iterator<java.lang.Class<? extends IAbstractSLogicalGateLAnalysis>> vIterator =
					ESFLocalAnalysisSet.SLOGICALGATES_LIST.iterator();
				EObject vStereotypeApplication = null;
				while (vStereotypeApplication == null && vIterator.hasNext()) {
					java.lang.Class<? extends IAbstractSLogicalGateLAnalysis> vSLogicalGateLAnalysis = vIterator.next();
					vStereotypeApplication = UMLUtil.getStereotypeApplication(vElement, vSLogicalGateLAnalysis);
				}
				if (vStereotypeApplication != null) {
					vSLogicalGatesLAnalysisList.add((IAbstractSLogicalGateLAnalysis) vStereotypeApplication);
				}
			}
		}

		UnmodifiableEList<IAbstractSLogicalGateLAnalysis> vUSLogicalGatesLAnalysisList =
			new UnmodifiableEList<IAbstractSLogicalGateLAnalysis>(
				this,
				ESFLocalAnalysisPackage.eINSTANCE.getSBlockLAnalysis_SLogicalGatesLAnalysisList(),
				vSLogicalGatesLAnalysisList.size(),
				vSLogicalGatesLAnalysisList.toArray());
		return vUSLogicalGatesLAnalysisList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EList<IAbstractSFailureModeLAnalysis> getSFailureModesLAnalysisList() {
		EList<IAbstractSFailureModeLAnalysis> vSFailureModesLAnalysisList =
			new BasicEList<IAbstractSFailureModeLAnalysis>();
		Class vBase = getBase_Class();

		if (vBase != null) {
			/**
			 * Retrieve all SUltimelyFailureModes, SErroneousFailureModes and SAbsentFailureModes.
			 */
			for (Element vElement : vBase.getOwnedElements()) {
				Iterator<java.lang.Class<? extends IAbstractSFailureModeLAnalysis>> vIterator =
					ESFLocalAnalysisSet.SFAILUREMODES_LIST.iterator();
				EObject vStereotypeApplication = null;
				while (vStereotypeApplication == null && vIterator.hasNext()) {
					java.lang.Class<? extends IAbstractSFailureModeLAnalysis> vSFailureModeLAnalysis = vIterator.next();
					vStereotypeApplication = UMLUtil.getStereotypeApplication(vElement, vSFailureModeLAnalysis);
				}
				if (vStereotypeApplication != null) {
					vSFailureModesLAnalysisList.add((IAbstractSFailureModeLAnalysis) vStereotypeApplication);
				}
			}
		}

		UnmodifiableEList<IAbstractSFailureModeLAnalysis> vUSFailureModesLAnalysisList =
			new UnmodifiableEList<IAbstractSFailureModeLAnalysis>(
				this,
				ESFLocalAnalysisPackage.eINSTANCE.getSBlockLAnalysis_SFailureModesLAnalysisList(),
				vSFailureModesLAnalysisList.size(),
				vSFailureModesLAnalysisList.toArray());
		return vUSFailureModesLAnalysisList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EList<ISBarrierLAnalysis> getSBarriersLAnalysisList() {
		EList<ISBarrierLAnalysis> vSBarriersLAnalysisList = new BasicEList<ISBarrierLAnalysis>();
		Class vBase = getBase_Class();

		if (vBase != null) {
			/**
			 * Retrieve all SBarriersLAnalysis.
			 */
			for (Element vElement : vBase.getOwnedElements()) {
				EObject vStereotypeApplication = UMLUtil.getStereotypeApplication(vElement, ISBarrierLAnalysis.class);
				if (vStereotypeApplication != null) {
					vSBarriersLAnalysisList.add((ISBarrierLAnalysis) vStereotypeApplication);
				}
			}
		}

		UnmodifiableEList<ISBarrierLAnalysis> vUSBarriersLAnalysisList = new UnmodifiableEList<ISBarrierLAnalysis>(
			this,
			ESFLocalAnalysisPackage.eINSTANCE.getSBlockLAnalysis_SBarriersLAnalysisList(),
			vSBarriersLAnalysisList.size(),
			vSBarriersLAnalysisList.toArray());
		return vUSBarriersLAnalysisList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ISLocalAnalysis basicGetSLocalAnalysis() {
		Class vBase = getBase_Class();
		ISLocalAnalysis vLocalAnalysis = null;

		if (vBase != null) {
			Element vOwner = vBase.getOwner();
			EObject vStereotypeApplication = UMLUtil.getStereotypeApplication(vOwner, ISLocalAnalysis.class);
			if (vStereotypeApplication != null) {
				vLocalAnalysis = (ISLocalAnalysis) vStereotypeApplication;
			}
		}
		return vLocalAnalysis;
	}
}
