/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.esflocalanalysis.impl;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreEList.UnmodifiableEList;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.esfcore.impl.GenericAbstractSElement;
import org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis;
import org.polarsys.esf.esflocalanalysis.IMSBarrierLAnalysis;
import org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis;

/**
 * This class can override the generated class {@link SBarrierLAnalysis} and will be
 * used by the custom factory.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class MSBarrierLAnalysis
	extends SBarrierLAnalysis
	implements IMSBarrierLAnalysis {

	/**
	 * Default constructor.
	 */
	public MSBarrierLAnalysis() {
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return GenericAbstractSElement.getName(getBase_Property());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getUUID() {
		return GenericAbstractSElement.getUUID(getBase_Property());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ISBlockLAnalysis basicGetSBlockLAnalysis() {
		Property vBase = getBase_Property();
		ISBlockLAnalysis vSBlockLAnalysis = null;
		if (vBase != null) {
			Element vOwner = vBase.getOwner();
			EObject vStereotypeApplication = UMLUtil.getStereotypeApplication(vOwner, ISBlockLAnalysis.class);
			if (vStereotypeApplication != null) {
				vSBlockLAnalysis = (ISBlockLAnalysis) vStereotypeApplication;
			}
		}
		return vSBlockLAnalysis;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EList<IAbstractSFailureModeLAnalysis> getSFailureModesLAnalysisList() {
		EList<IAbstractSFailureModeLAnalysis> vSFailureModesLAnalysisList =
			GenericAbstractSFailureModeOwner.getSFailureModesLAnalysisList(getBase_Property());

		UnmodifiableEList<IAbstractSFailureModeLAnalysis> vUSFailureModesLAnalysisList =
			new UnmodifiableEList<IAbstractSFailureModeLAnalysis>(
				this,
				ESFLocalAnalysisPackage.eINSTANCE.getAbstractSFailureModeOwner_SFailureModesLAnalysisList(),
				vSFailureModesLAnalysisList.size(),
				vSFailureModesLAnalysisList.toArray());

		return vUSFailureModesLAnalysisList;
	}
}
