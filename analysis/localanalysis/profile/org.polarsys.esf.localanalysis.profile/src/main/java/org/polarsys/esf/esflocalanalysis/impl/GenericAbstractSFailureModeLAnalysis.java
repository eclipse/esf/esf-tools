/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esflocalanalysis.impl;

import java.util.Iterator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeOwner;
import org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis;
import org.polarsys.esf.esflocalanalysis.ISDysfunctionalAssociation;

/**
 * This class implements the derived attributes of the generated class {@link AbstractSFailureModeLAnalysis}.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public final class GenericAbstractSFailureModeLAnalysis {

	/**
	 * Default constructor, private as it's a utility class.
	 */
	private GenericAbstractSFailureModeLAnalysis() {
		// Nothing to do
	}

	/**
	 * Get SBlockLAnalysis.
	 *
	 * @param pBaseProperty The element base of a SFailureModeLAnalysis
	 * @return The SBlockLAnalysis
	 */
	public static ISBlockLAnalysis getSBlockLAnalysis(final Property pBaseProperty) {
		ISBlockLAnalysis vSBlockLAnalysis = null;
		if (pBaseProperty != null) {
			Element vOwner = pBaseProperty.getOwner();
			EObject vStereotypeApplication = UMLUtil.getStereotypeApplication(vOwner, ISBlockLAnalysis.class);
			if (vStereotypeApplication != null) {
				vSBlockLAnalysis = (ISBlockLAnalysis) vStereotypeApplication;
			}
		}
		return vSBlockLAnalysis;
	}

	/**
	 * Get owner.
	 *
	 * @param pBaseProperty The element base of a SFailureModeLAnalysis
	 * @return The owner
	 */
	public static IAbstractSFailureModeOwner getOwner(final Property pBaseProperty) {
		IAbstractSFailureModeOwner vOwner = null;
		if (pBaseProperty != null) {
			/* Get all connectorEnds of property */
			Iterator<ConnectorEnd> vIterator = pBaseProperty.getEnds().iterator();
			while (vOwner == null && vIterator.hasNext()) {
				ConnectorEnd vConnectorEnd = (ConnectorEnd) vIterator.next();
				/* Get connector of the connectorEnd */
				Connector vConnector = (Connector) vConnectorEnd.getOwner();
				/* Verify if connector is a SDysfunctionalAssociation */
				if (UMLUtil.getStereotypeApplication(vConnector, ISDysfunctionalAssociation.class) != null) {
					ISDysfunctionalAssociation vSDysfunctionalAssociation = (ISDysfunctionalAssociation) UMLUtil
						.getStereotypeApplication(vConnector, ISDysfunctionalAssociation.class);
					vOwner = vSDysfunctionalAssociation.getSFailureModeOwner();
				}
			}
		}
		return vOwner;
	}
}
