/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.esflocalanalysis.impl;

import java.util.Iterator;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreEList.UnmodifiableEList;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.esfcore.impl.GenericAbstractSElement;
import org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis;
import org.polarsys.esf.esflocalanalysis.IMSFearedEventsFamily;
import org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis;
import org.polarsys.esf.esflocalanalysis.ISFearedEvent;
import org.polarsys.esf.esflocalanalysis.ISFearedEventsFamily;
import org.polarsys.esf.esflocalanalysis.ISFearedEventsLibrary;
import org.polarsys.esf.esflocalanalysis.ISLocalAnalysis;

/**
 * This class can override the generated class {@link SFearedEventsFamily} and will be
 * used by the custom factory.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class MSFearedEventsFamily
	extends SFearedEventsFamily
	implements IMSFearedEventsFamily {

	/**
	 * Default constructor.
	 */
	public MSFearedEventsFamily() {
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return GenericAbstractSElement.getName(getBase_Class());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getUUID() {
		return GenericAbstractSElement.getUUID(getBase_Class());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EList<ISFearedEvent> getSFearedEventsList() {
		EList<ISFearedEvent> vSFearedEventsList = new BasicEList<ISFearedEvent>();
		Class vBase = getBase_Class();

		if (vBase != null) {
			for (Element vElement : vBase.getOwnedElements()) {
				EObject vStereotypeApplication = UMLUtil.getStereotypeApplication(vElement, ISFearedEvent.class);
				if (vStereotypeApplication != null) {
					vSFearedEventsList.add((ISFearedEvent) vStereotypeApplication);
				}
			}
		}

		UnmodifiableEList<ISFearedEvent> vUSFearedEventsList = new UnmodifiableEList<ISFearedEvent>(
			this,
			ESFLocalAnalysisPackage.eINSTANCE.getSFearedEventsFamily_SFearedEventsList(),
			vSFearedEventsList.size(),
			vSFearedEventsList.toArray());

		return vUSFearedEventsList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EList<ISFearedEvent> getAllSFearedEventsList() {
		EList<ISFearedEvent> vAllSFearedEventsList = new BasicEList<ISFearedEvent>();
		Class vBase = getBase_Class();

		if (vBase != null) {
			for (Element vElement : vBase.getOwnedElements()) {
				EObject vStereotypeApplication = UMLUtil.getStereotypeApplication(vElement, ISFearedEvent.class);
				if (vStereotypeApplication != null) {
					vAllSFearedEventsList.add((ISFearedEvent) vStereotypeApplication);
				}
			}

			EList<ISFearedEventsFamily> vSubFamiliesList = getMySubFamiliesList(vBase);
			if (!vSubFamiliesList.isEmpty()) {
				ISFearedEventsFamily vHead = vSubFamiliesList.get(0);
				vSubFamiliesList.remove(0);
				vAllSFearedEventsList = getAllSFearedEventsList(vHead, vSubFamiliesList, vAllSFearedEventsList);
			}
		}

		UnmodifiableEList<ISFearedEvent> vUAllSFearedEventsList = new UnmodifiableEList<ISFearedEvent>(
			this,
			ESFLocalAnalysisPackage.eINSTANCE.getSFearedEventsFamily_SFearedEventsList(),
			vAllSFearedEventsList.size(),
			vAllSFearedEventsList.toArray());

		return vUAllSFearedEventsList;
	}

	/**
	 * Retrieve all direct and indirect owned SFearedEvents.
	 *
	 * @param pSubFamily The current sub-family
	 * @param pSubFamiliesToVisit The list of Sub-families to visit
	 * @param pAccOfSFearedEventsList The
	 * @return The List of SFearedEvents
	 */
	private EList<ISFearedEvent> getAllSFearedEventsList(
		final ISFearedEventsFamily pSubFamily,
		final EList<ISFearedEventsFamily> pSubFamiliesToVisit,
		final EList<ISFearedEvent> pAccOfSFearedEventsList) {
		EList<ISFearedEvent> vUpdateSFearedEventsList = new BasicEList<ISFearedEvent>();
		vUpdateSFearedEventsList.addAll(pAccOfSFearedEventsList);
		vUpdateSFearedEventsList.addAll(pSubFamily.getSFearedEventsList());

		if (!pSubFamiliesToVisit.isEmpty()) {
			EList<ISFearedEventsFamily> vUpdateSubFamiliesToVisit = new BasicEList<ISFearedEventsFamily>();
			vUpdateSubFamiliesToVisit.addAll(pSubFamiliesToVisit);
			vUpdateSubFamiliesToVisit.addAll(getMySubFamiliesList(pSubFamily.getBase_Class()));
			ISFearedEventsFamily vHead = vUpdateSubFamiliesToVisit.remove(0);

			vUpdateSFearedEventsList =
				getAllSFearedEventsList(vHead, vUpdateSubFamiliesToVisit, vUpdateSFearedEventsList);
		}

		return vUpdateSFearedEventsList;
	}

	/**
	 * Retrieve the owned SFearedEventsFamilies.
	 *
	 * @param pBase The element base of SFearedEventsFamily
	 * @return The List of SFearedEventsFamilies
	 */
	private EList<ISFearedEventsFamily> getMySubFamiliesList(final Class pBase) {
		EList<ISFearedEventsFamily> vMySubFamiliesList = new BasicEList<ISFearedEventsFamily>();

		for (Element vElement : pBase.getOwnedElements()) {
			EObject vStereotypeApplication = UMLUtil.getStereotypeApplication(vElement, ISFearedEventsFamily.class);
			if (vStereotypeApplication != null) {
				vMySubFamiliesList.add((ISFearedEventsFamily) vStereotypeApplication);
			}
		}

		return vMySubFamiliesList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EList<ISFearedEventsFamily> getSubFamiliesList() {
		EList<ISFearedEventsFamily> vSubFamiliesList = new BasicEList<ISFearedEventsFamily>();
		Class vBase = getBase_Class();

		if (vBase != null) {
			vSubFamiliesList.addAll(getMySubFamiliesList(vBase));
		}

		UnmodifiableEList<ISFearedEventsFamily> vUSubFamiliesList = new UnmodifiableEList<ISFearedEventsFamily>(
			this,
			ESFLocalAnalysisPackage.eINSTANCE.getSFearedEventsFamily_SubFamiliesList(),
			vSubFamiliesList.size(),
			vSubFamiliesList.toArray());

		return vUSubFamiliesList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isSelected() {
		boolean vIsSelected = false;

		if (!getSFailureModesLAnalysisList().isEmpty()) {
			vIsSelected = true;
		}

		return vIsSelected;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EList<IAbstractSFailureModeLAnalysis> getSFailureModesLAnalysisList() {
		EList<IAbstractSFailureModeLAnalysis> vSFailureModesLAnalysisList =
			new BasicEList<IAbstractSFailureModeLAnalysis>();
		Class vBase = getBase_Class();

		if (vBase != null) {
			ISFearedEventsLibrary vSFearedEventsLibrary = getSFearedEventsLibrary();
			ISLocalAnalysis vSLocalAnalysis = vSFearedEventsLibrary.getSLocalAnalysis();

			if (vSLocalAnalysis != null) {
				// Retrieve all SFailureModesLAnalysis of the SLocalAnalysis for each SBlockLAnalysis
				// and take all which contain the SFearedEventsFamily (this).
				for (ISBlockLAnalysis vSBlockLAnalysis : vSLocalAnalysis.getSBlocksLAnalysisList()) {
					for (IAbstractSFailureModeLAnalysis vSFailureModeLAnalysis : vSBlockLAnalysis
						.getSFailureModesLAnalysisList()) {
						Iterator<ISFearedEventsFamily> vIterator =
							vSFailureModeLAnalysis.getSFearedEventsFamiliesList().iterator();
						Boolean vFound = false;
						while (!vFound && vIterator.hasNext()) {
							ISFearedEventsFamily vSFearedEventsFamily = (ISFearedEventsFamily) vIterator.next();
							if (vSFearedEventsFamily.getBase_Class().equals(vBase)) {
								vFound = true;
								vSFailureModesLAnalysisList.add(vSFailureModeLAnalysis);
							}
						}
					}
				}
			}
		}

		UnmodifiableEList<IAbstractSFailureModeLAnalysis> vUSFailureModesLAnalysisList =
			new UnmodifiableEList<IAbstractSFailureModeLAnalysis>(
				this,
				ESFLocalAnalysisPackage.eINSTANCE.getSFearedEventsFamily_SFailureModesLAnalysisList(),
				vSFailureModesLAnalysisList.size(),
				vSFailureModesLAnalysisList.toArray());

		return vUSFailureModesLAnalysisList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ISFearedEventsLibrary basicGetSFearedEventsLibrary() {
		Class vBase = getBase_Class();
		ISFearedEventsLibrary vSFearedEventsLibrary = null;

		if (vBase != null) {
			vSFearedEventsLibrary = getSFearedEventsLibrary(vBase);
		}

		return vSFearedEventsLibrary;
	}

	/**
	 * Retrieve SFearedEventsLibrary.
	 *
	 * @param pElement The owned Element
	 * @return The SFearedEventsLibrary
	 */
	private ISFearedEventsLibrary getSFearedEventsLibrary(final Element pElement) {
		Element vOwner = pElement.getOwner();
		ISFearedEventsLibrary vSFearedEventsLibrary = null;

		EObject vStereotypeApplication = UMLUtil.getStereotypeApplication(vOwner, ISFearedEventsLibrary.class);
		if (vStereotypeApplication != null) {
			vSFearedEventsLibrary = (ISFearedEventsLibrary) vStereotypeApplication;
		} else {
			vSFearedEventsLibrary = getSFearedEventsLibrary(vOwner);
		}

		return vSFearedEventsLibrary;
	}
}
