/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.esflocalanalysis.impl;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.util.EcoreEList.UnmodifiableEList;
import org.polarsys.esf.esfcore.impl.GenericAbstractSElement;
import org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeOwner;
import org.polarsys.esf.esflocalanalysis.IMSAbsentFailureMode;
import org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis;
import org.polarsys.esf.esflocalanalysis.ISPropagationLink;

/**
 * This class can override the generated class {@link SAbsentFailureMode} and will be
 * used by the custom factory.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class MSAbsentFailureMode
	extends SAbsentFailureMode
	implements IMSAbsentFailureMode {

	/**
	 * Default constructor.
	 */
	public MSAbsentFailureMode() {
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return GenericAbstractSElement.getName(getBase_Property());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getUUID() {
		return GenericAbstractSElement.getUUID(getBase_Property());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ISBlockLAnalysis basicGetSBlockLAnalysis() {
		return GenericAbstractSFailureModeLAnalysis.getSBlockLAnalysis(getBase_Property());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IAbstractSFailureModeOwner basicGetOwner() {
		return GenericAbstractSFailureModeLAnalysis.getOwner(getBase_Property());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EList<ISPropagationLink> getInSPropagationLinksList() {
		EList<ISPropagationLink> vInSPropagationLinksList =
			GenericAbstractSPropagationElement.getInSPropagationLinksList(getBase_Property());

		UnmodifiableEList<ISPropagationLink> vUInSPropagationLinksList = new UnmodifiableEList<ISPropagationLink>(
			this,
			ESFLocalAnalysisPackage.eINSTANCE.getAbstractSPropagationElement_OutSPropagationLinksList(),
			vInSPropagationLinksList.size(),
			vInSPropagationLinksList.toArray());

		return vUInSPropagationLinksList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EList<ISPropagationLink> getOutSPropagationLinksList() {
		EList<ISPropagationLink> vOutSPropagationLinksList =
			GenericAbstractSPropagationElement.getOutSPropagationLinksList(getBase_Property());

		UnmodifiableEList<ISPropagationLink> vUOutSPropagationLinksList = new UnmodifiableEList<ISPropagationLink>(
			this,
			ESFLocalAnalysisPackage.eINSTANCE.getAbstractSPropagationElement_OutSPropagationLinksList(),
			vOutSPropagationLinksList.size(),
			vOutSPropagationLinksList.toArray());

		return vUOutSPropagationLinksList;
	}
}
