/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.localanalysis.profile.set;

import java.util.Arrays;
import java.util.List;

import org.polarsys.esf.esflocalanalysis.IAbstractSFailureEventLAnalysis;
import org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis;
import org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeOwner;
import org.polarsys.esf.esflocalanalysis.IAbstractSLogicalGateLAnalysis;
import org.polarsys.esf.esflocalanalysis.ISAbsentFailureMode;
import org.polarsys.esf.esflocalanalysis.ISAndGateLAnalysis;
import org.polarsys.esf.esflocalanalysis.ISBarrierLAnalysis;
import org.polarsys.esf.esflocalanalysis.ISErroneousFailureMode;
import org.polarsys.esf.esflocalanalysis.ISLocalEvent;
import org.polarsys.esf.esflocalanalysis.ISOrGateLAnalysis;
import org.polarsys.esf.esflocalanalysis.ISPortLAnalysis;
import org.polarsys.esf.esflocalanalysis.ISSystemEvent;
import org.polarsys.esf.esflocalanalysis.ISUntimelyFailureMode;

/**
 * ESFLocalAnalysis profile set.
 *
 * @author  $Author: ymunoz $
 * @version $Revision: 168 $
 */
public final class ESFLocalAnalysisSet {
	/** ESFLocalAnalysis profile path. */
	public static final String PROFILE_PATH =
		"pathmap://ESFLOCALANALYSIS_PROFILE/esflocalanalysis.profile.uml"; //$NON-NLS-1$

	/** ESFLocalAnalysis profile URI. */
	public static final String PROFILE_URI =
		"http://www.polarsys.org/esf/0.7.0/ESFLocalAnalysis"; //$NON-NLS-1$

	/** List of Logical Gates defined by ESFLocalAnalysis profile. */
	public static final List<java.lang.Class<? extends IAbstractSLogicalGateLAnalysis>> SLOGICALGATES_LIST =
		Arrays.asList(ISOrGateLAnalysis.class, ISAndGateLAnalysis.class);

	/** List of Failure Modes defined by ESFLocalAnalysis profile. */
	public static final List<java.lang.Class<? extends IAbstractSFailureModeLAnalysis>> SFAILUREMODES_LIST =
		Arrays.asList(ISAbsentFailureMode.class, ISErroneousFailureMode.class, ISUntimelyFailureMode.class);

	/** List of Failure Events defined by ESFLocalAnalysis profile. */
	public static final List<java.lang.Class<? extends IAbstractSFailureEventLAnalysis>> SFAILUREEVENTS_LIST =
		Arrays.asList(ISLocalEvent.class, ISSystemEvent.class);

	/** List of Failure Modes Owner defined by ESFLocalAnalysis profile. */
	public static final List<java.lang.Class<? extends IAbstractSFailureModeOwner>> SFAILUREMODESOWNER_LIST =
		Arrays.asList(ISPortLAnalysis.class, ISBarrierLAnalysis.class);

	/**
	 * Default constructor, private as it's a utility class.
	 */
	private ESFLocalAnalysisSet() {
	 // Nothing to do
	}
}
