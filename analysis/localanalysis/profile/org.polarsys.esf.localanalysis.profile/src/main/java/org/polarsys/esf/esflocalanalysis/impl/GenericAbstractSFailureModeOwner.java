/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esflocalanalysis.impl;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis;
import org.polarsys.esf.esflocalanalysis.ISDysfunctionalAssociation;

/**
 * This class implements the derived attributes of the generated class {@link AbstractSFailureModeOwner}.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public final class GenericAbstractSFailureModeOwner {

	/**
	 * Default constructor, private as it's a utility class.
	 */
	private GenericAbstractSFailureModeOwner() {
		// Nothing to do
	}

	/**
	 * Get List of SFailureModeLAnalysis.
	 *
	 * @param pBaseProperty The element base of a SFailureModeOwner
	 * @return The List of SFailureModeLAnalysis
	 */
	public static EList<IAbstractSFailureModeLAnalysis> getSFailureModesLAnalysisList(final Property pBaseProperty) {
		EList<IAbstractSFailureModeLAnalysis> vSFailureModesLAnalysisList =
			new BasicEList<IAbstractSFailureModeLAnalysis>();

		if (pBaseProperty != null) {
			/* Get all connectorEnds of property */
			for (ConnectorEnd vConnectorEnd : pBaseProperty.getEnds()) {
				/* Get connector of the connectorEnd */
				Connector vConnector = (Connector) vConnectorEnd.getOwner();
				/* Verify if connector is a SDysfunctionalAssociation */
				if (UMLUtil.getStereotypeApplication(vConnector, ISDysfunctionalAssociation.class) != null) {
					ISDysfunctionalAssociation vSDysfunctionalAssociation = (ISDysfunctionalAssociation) UMLUtil
						.getStereotypeApplication(vConnector, ISDysfunctionalAssociation.class);
					IAbstractSFailureModeLAnalysis vSFailureModeLAnalysis =
						vSDysfunctionalAssociation.getSFailureModeLAnalysis();
					if (vSFailureModeLAnalysis != null) {
						vSFailureModesLAnalysisList.add(vSFailureModeLAnalysis);
					}
				}
			}
		}

		return vSFailureModesLAnalysisList;
	}
}
