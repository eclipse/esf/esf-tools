/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.esflocalanalysis.impl;

import java.util.Iterator;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreEList.UnmodifiableEList;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.esfcore.impl.GenericAbstractSElement;
import org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis;
import org.polarsys.esf.esflocalanalysis.IMSFearedEvent;
import org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis;
import org.polarsys.esf.esflocalanalysis.ISFearedEvent;
import org.polarsys.esf.esflocalanalysis.ISFearedEventsLibrary;
import org.polarsys.esf.esflocalanalysis.ISLocalAnalysis;

/**
 * This class can override the generated class {@link SFearedEvent} and will be
 * used by the custom factory.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class MSFearedEvent
	extends SFearedEvent
	implements IMSFearedEvent {

	/**
	 * Default constructor.
	 */
	public MSFearedEvent() {
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return GenericAbstractSElement.getName(getBase_Class());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getUUID() {
		return GenericAbstractSElement.getUUID(getBase_Class());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isSelected() {
		boolean vIsSelected = false;

		if (!getSFailureModesLAnalysisList().isEmpty()) {
			vIsSelected = true;
		}

		return vIsSelected;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EList<IAbstractSFailureModeLAnalysis> getSFailureModesLAnalysisList() {
		EList<IAbstractSFailureModeLAnalysis> vSFailureModesLAnalysisList =
			new BasicEList<IAbstractSFailureModeLAnalysis>();
		Class vBase = getBase_Class();

		if (vBase != null) {
			ISFearedEventsLibrary vSFearedEventsLibrary = getSFearedEventsLibrary();
			ISLocalAnalysis vSLocalAnalysis = vSFearedEventsLibrary.getSLocalAnalysis();

			if (vSLocalAnalysis != null) {
				// Retrieve all SFailureModesLAnalysis of the SLocalAnalysis for each SBlockLAnalysis
				// and take all which contain the SFearedEvent (this).
				for (ISBlockLAnalysis vSBlockLAnalysis : vSLocalAnalysis.getSBlocksLAnalysisList()) {
					for (IAbstractSFailureModeLAnalysis vSFailureModeLAnalysis : vSBlockLAnalysis
						.getSFailureModesLAnalysisList()) {
						Iterator<ISFearedEvent> vIterator =
							vSFailureModeLAnalysis.getSFearedEventsList().iterator();
						Boolean vFound = false;
						while (!vFound && vIterator.hasNext()) {
							ISFearedEvent vSFearedEvent = (ISFearedEvent) vIterator.next();
							if (vSFearedEvent.getBase_Class().equals(vBase)) {
								vFound = true;
								vSFailureModesLAnalysisList.add(vSFailureModeLAnalysis);
							}
						}
					}
				}
			}
		}

		UnmodifiableEList<IAbstractSFailureModeLAnalysis> vUSFailureModesLAnalysisList =
			new UnmodifiableEList<IAbstractSFailureModeLAnalysis>(
				this,
				ESFLocalAnalysisPackage.eINSTANCE.getSFearedEvent_SFailureModesLAnalysisList(),
				vSFailureModesLAnalysisList.size(),
				vSFailureModesLAnalysisList.toArray());

		return vUSFailureModesLAnalysisList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ISFearedEventsLibrary basicGetSFearedEventsLibrary() {
		Class vBase = getBase_Class();
		ISFearedEventsLibrary vSFearedEventsLibrary = null;

		if (vBase != null) {
			vSFearedEventsLibrary = getSFearedEventsLibrary(vBase);
		}

		return vSFearedEventsLibrary;
	}

	/**
	 * Retrieve SFearedEventsLibrary.
	 *
	 * @param pElement The owned Element
	 * @return The SFearedEventsLibrary
	 */
	private ISFearedEventsLibrary getSFearedEventsLibrary(final Element pElement) {
		Element vOwner = pElement.getOwner();
		ISFearedEventsLibrary vSFearedEventsLibrary = null;

		EObject vStereotypeApplication = UMLUtil.getStereotypeApplication(vOwner, ISFearedEventsLibrary.class);
		if (vStereotypeApplication != null) {
			vSFearedEventsLibrary = (ISFearedEventsLibrary) vStereotypeApplication;
		} else {
			vSFearedEventsLibrary = getSFearedEventsLibrary(vOwner);
		}

		return vSFearedEventsLibrary;
	}
}
