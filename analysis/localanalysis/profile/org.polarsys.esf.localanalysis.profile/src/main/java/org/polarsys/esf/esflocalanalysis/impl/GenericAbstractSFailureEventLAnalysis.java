/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esflocalanalysis.impl;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis;

/**
 * This class implements the derived attributes of the generated class {@link AbstractSFailureEventLAnalysis}.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public final class GenericAbstractSFailureEventLAnalysis {

	/**
	 * Default constructor, private as it's a utility class.
	 */
	private GenericAbstractSFailureEventLAnalysis() {
		// Nothing to do
	}

	/**
	 * Get SBlockLAnalysis.
	 *
	 * @param pBaseProperty The element base of a SFailureEventLAnalysis
	 * @return The SBlockLAnalysis
	 */
	public static ISBlockLAnalysis getSBlockLAnalysis(final Property pBaseProperty) {
		ISBlockLAnalysis vSBlockLAnalysis = null;
		if (pBaseProperty != null) {
			Element vOwner = pBaseProperty.getOwner();
			EObject vStereotypeApplication = UMLUtil.getStereotypeApplication(vOwner, ISBlockLAnalysis.class);
			if (vStereotypeApplication != null) {
				vSBlockLAnalysis = (ISBlockLAnalysis) vStereotypeApplication;
			}
		}
		return vSBlockLAnalysis;
	}
}
