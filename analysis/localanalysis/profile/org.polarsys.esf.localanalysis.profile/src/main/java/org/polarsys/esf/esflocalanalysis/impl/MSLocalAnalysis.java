/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.esflocalanalysis.impl;

import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreEList.UnmodifiableEList;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.esfcore.impl.GenericAbstractSElement;
import org.polarsys.esf.esflocalanalysis.IMSLocalAnalysis;
import org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis;
import org.polarsys.esf.esflocalanalysis.ISFearedEventsLibrary;
import org.polarsys.esf.esflocalanalysis.ISSystemEventsLibrary;

/**
 * This class can override the generated class {@link SLocalAnalysis} and will be
 * used by the custom factory.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class MSLocalAnalysis
	extends SLocalAnalysis
	implements IMSLocalAnalysis {

	/**
	 * Default constructor.
	 */
	public MSLocalAnalysis() {
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return GenericAbstractSElement.getName(getBase_Package());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getUUID() {
		return GenericAbstractSElement.getUUID(getBase_Package());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EList<ISBlockLAnalysis> getSBlocksLAnalysisList() {
		EList<ISBlockLAnalysis> vSSafetyConceptsList = new BasicEList<ISBlockLAnalysis>();
		Package vBase = getBase_Package();

		if (vBase != null) {
			for (Element vElement : vBase.getOwnedElements()) {
				EObject vStereotypeApplication = UMLUtil.getStereotypeApplication(vElement, ISBlockLAnalysis.class);
				if (vStereotypeApplication != null) {
					vSSafetyConceptsList.add((ISBlockLAnalysis) vStereotypeApplication);
				}
			}
		}

		UnmodifiableEList<ISBlockLAnalysis> vUSSafetyConceptsList =
			new UnmodifiableEList<ISBlockLAnalysis>(
				this,
				ESFLocalAnalysisPackage.eINSTANCE.getSLocalAnalysis_SBlocksLAnalysisList(),
				vSSafetyConceptsList.size(),
				vSSafetyConceptsList.toArray());

		return vUSSafetyConceptsList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ISSystemEventsLibrary basicGetSSystemEventsLibrary() {
		Package vBase = getBase_Package();
		ISSystemEventsLibrary vSystemEventsLibrary = null;
		if (vBase != null) {
			Boolean vFound = false;
			List<Package> vPackagesList = vBase.getNestedPackages();
			Iterator<Package> vIterator = vPackagesList.iterator();

			while (vIterator.hasNext() && !vFound) {
				Package vPackage = vIterator.next();
				EObject vStereotypeApplication =
					UMLUtil.getStereotypeApplication(vPackage, ISSystemEventsLibrary.class);
				if (vStereotypeApplication != null) {
					vSystemEventsLibrary = (ISSystemEventsLibrary) vStereotypeApplication;
				}
			}
		}
		return vSystemEventsLibrary;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ISFearedEventsLibrary basicGetSFearedEventsLibrary() {
		Package vBase = getBase_Package();
		ISFearedEventsLibrary vSFearedEventsLibrary = null;
		if (vBase != null) {
			Boolean vFound = false;
			List<Package> vPackagesList = vBase.getNestedPackages();
			Iterator<Package> vIterator = vPackagesList.iterator();

			while (vIterator.hasNext() && !vFound) {
				Package vPackage = vIterator.next();
				EObject vStereotypeApplication =
					UMLUtil.getStereotypeApplication(vPackage, ISFearedEventsLibrary.class);
				if (vStereotypeApplication != null) {
					vSFearedEventsLibrary = (ISFearedEventsLibrary) vStereotypeApplication;
				}
			}
		}
		return vSFearedEventsLibrary;
	}
}
