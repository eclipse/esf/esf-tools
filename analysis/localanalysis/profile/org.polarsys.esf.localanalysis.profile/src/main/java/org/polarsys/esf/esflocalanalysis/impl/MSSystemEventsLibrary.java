/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.esflocalanalysis.impl;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreEList.UnmodifiableEList;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.esfcore.impl.GenericAbstractSElement;
import org.polarsys.esf.esflocalanalysis.IMSSystemEventsLibrary;
import org.polarsys.esf.esflocalanalysis.ISLocalAnalysis;
import org.polarsys.esf.esflocalanalysis.ISSystemEventType;

/**
 * This class can override the generated class {@link SSystemEventsLibrary} and will be
 * used by the custom factory.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class MSSystemEventsLibrary
	extends SSystemEventsLibrary
	implements IMSSystemEventsLibrary {

	/**
	 * Default constructor.
	 */
	public MSSystemEventsLibrary() {
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return GenericAbstractSElement.getName(getBase_Package());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getUUID() {
		return GenericAbstractSElement.getUUID(getBase_Package());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EList<ISSystemEventType> getSSystemEventTypesList() {
		EList<ISSystemEventType> vSSystemEventTypesList = new BasicEList<ISSystemEventType>();
		Package vBase = getBase_Package();

		if (vBase != null) {
			for (Element vElement : vBase.getOwnedElements()) {
				EObject vStereotypeApplication = UMLUtil.getStereotypeApplication(vElement, ISSystemEventType.class);
				if (vStereotypeApplication != null) {
					vSSystemEventTypesList.add((ISSystemEventType) vStereotypeApplication);
				}
			}
		}

		UnmodifiableEList<ISSystemEventType> vUSSystemEventTypesList =
			new UnmodifiableEList<ISSystemEventType>(
				this,
				ESFLocalAnalysisPackage.eINSTANCE.getSSystemEventsLibrary_SSystemEventTypesList(),
				vSSystemEventTypesList.size(),
				vSSystemEventTypesList.toArray());

		return vUSSystemEventTypesList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ISLocalAnalysis basicGetSLocalAnalysis() {
		Package vBase = getBase_Package();
		ISLocalAnalysis vLocalAnalysis = null;

		if (vBase != null) {
			Element vOwner = vBase.getOwner();
			EObject vStereotypeApplication = UMLUtil.getStereotypeApplication(vOwner, ISLocalAnalysis.class);
			if (vStereotypeApplication != null) {
				vLocalAnalysis = (ISLocalAnalysis) vStereotypeApplication;
			}
		}
		return vLocalAnalysis;
	}
}
