/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.esflocalanalysis.impl;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreEList.UnmodifiableEList;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.core.profile.esfarchitectureconcepts.PortDirection;
import org.polarsys.esf.esfarchitectureconcepts.SDirection;
import org.polarsys.esf.esfcore.impl.GenericAbstractSElement;
import org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis;
import org.polarsys.esf.esflocalanalysis.IMSPortLAnalysis;
import org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis;

/**
 * This class can override the generated class {@link SPortLAnalysis} and will be
 * used by the custom factory.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class MSPortLAnalysis
	extends SPortLAnalysis
	implements IMSPortLAnalysis {

	/**
	 * Default constructor.
	 */
	public MSPortLAnalysis() {
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return GenericAbstractSElement.getName(getBase_Port());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getUUID() {
		return GenericAbstractSElement.getUUID(getBase_Port());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ISBlockLAnalysis basicGetSBlockLAnalysis() {
		Property vBase = getBase_Port();
		ISBlockLAnalysis vSBlockLAnalysis = null;
		if (vBase != null) {
			Element vOwner = vBase.getOwner();
			EObject vStereotypeApplication = UMLUtil.getStereotypeApplication(vOwner, ISBlockLAnalysis.class);
			if (vStereotypeApplication != null) {
				vSBlockLAnalysis = (ISBlockLAnalysis) vStereotypeApplication;
			}
		}
		return vSBlockLAnalysis;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EList<IAbstractSFailureModeLAnalysis> getSFailureModesLAnalysisList() {
		EList<IAbstractSFailureModeLAnalysis> vSFailureModesLAnalysisList =
			GenericAbstractSFailureModeOwner.getSFailureModesLAnalysisList(getBase_Port());

		UnmodifiableEList<IAbstractSFailureModeLAnalysis> vUSFailureModesLAnalysisList =
			new UnmodifiableEList<IAbstractSFailureModeLAnalysis>(
				this,
				ESFLocalAnalysisPackage.eINSTANCE.getAbstractSFailureModeOwner_SFailureModesLAnalysisList(),
				vSFailureModesLAnalysisList.size(),
				vSFailureModesLAnalysisList.toArray());

		return vUSFailureModesLAnalysisList;
	}
	
	/**
	 *  {@inheritDoc}
	 */
	@Override
	public SDirection getSDirectionLAnalysis() {
		Port port = getBase_Port();
		SDirection direction = getSDirectionManual();
		if (direction == SDirection.UNDEFINED && port != null) {
			direction = PortDirection.getSDirection(port);
		}
		return direction;
	}
}
