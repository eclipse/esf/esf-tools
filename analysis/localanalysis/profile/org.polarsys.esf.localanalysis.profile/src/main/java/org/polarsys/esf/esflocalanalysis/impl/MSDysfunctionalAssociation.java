/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.esflocalanalysis.impl;

import java.util.Iterator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.esfcore.impl.GenericAbstractSElement;
import org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis;
import org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeOwner;
import org.polarsys.esf.esflocalanalysis.IMSDysfunctionalAssociation;
import org.polarsys.esf.localanalysis.profile.set.ESFLocalAnalysisSet;

/**
 * This class can override the generated class {@link SDysfunctionalAssociation} and will be
 * used by the custom factory.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class MSDysfunctionalAssociation
	extends SDysfunctionalAssociation
	implements IMSDysfunctionalAssociation {

	/**
	 * Default constructor.
	 */
	public MSDysfunctionalAssociation() {
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return GenericAbstractSElement.getName(getBase_Connector());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getUUID() {
		return GenericAbstractSElement.getUUID(getBase_Connector());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IAbstractSFailureModeOwner basicGetSFailureModeOwner() {
		Connector vBase = getBase_Connector();
		IAbstractSFailureModeOwner vSFailureModeOwner = null;

		if (vBase != null) {
			Iterator<ConnectorEnd> vIterator = vBase.getEnds().iterator();

			while ((vSFailureModeOwner == null) && vIterator.hasNext()) {
				ConnectorEnd vConnectorEnd = (ConnectorEnd) vIterator.next();
				EObject vStereotypeApplication = getStereotypeApplicationOfSFailureModeOwner(vConnectorEnd.getRole());

				if (vStereotypeApplication != null) {
					vSFailureModeOwner = (IAbstractSFailureModeOwner) vStereotypeApplication;
				}
			}

		}

		return vSFailureModeOwner;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IAbstractSFailureModeLAnalysis basicGetSFailureModeLAnalysis() {
		Connector vBase = getBase_Connector();
		IAbstractSFailureModeLAnalysis vSFailureMode = null;

		if (vBase != null) {
			Iterator<ConnectorEnd> vIterator = vBase.getEnds().iterator();

			while ((vSFailureMode == null) && vIterator.hasNext()) {
				ConnectorEnd vConnectorEnd = (ConnectorEnd) vIterator.next();
				EObject vStereotypeApplication =
					getStereotypeApplicationOfSFailureModeLAnalysis(vConnectorEnd.getRole());

				if (vStereotypeApplication != null) {
					vSFailureMode = (IAbstractSFailureModeLAnalysis) vStereotypeApplication;
				}
			}

		}

		return vSFailureMode;
	}

	/**
	 * Get the Stereotype Application of SFailureModeOwner.
	 *
	 * @param pElement The UML Element
	 * @return The Stereotype Application
	 */
	private EObject getStereotypeApplicationOfSFailureModeOwner(final Element pElement) {
		EObject vStereotypeApplication = null;

		Iterator<java.lang.Class<? extends IAbstractSFailureModeOwner>> vIterator =
			ESFLocalAnalysisSet.SFAILUREMODESOWNER_LIST.iterator();

		while ((vStereotypeApplication == null) && vIterator.hasNext()) {
			java.lang.Class<? extends IAbstractSFailureModeOwner> vSFailureModeOwner = vIterator.next();
			vStereotypeApplication = UMLUtil.getStereotypeApplication(pElement, vSFailureModeOwner);
		}
		return vStereotypeApplication;
	}

	/**
	 * Get the Stereotype Application of SFailureModeLAnalysis.
	 *
	 * @param pElement The UML Element
	 * @return The Stereotype Application
	 */
	private EObject getStereotypeApplicationOfSFailureModeLAnalysis(final Element pElement) {
		EObject vStereotypeApplication = null;

		Iterator<java.lang.Class<? extends IAbstractSFailureModeLAnalysis>> vIterator =
			ESFLocalAnalysisSet.SFAILUREMODES_LIST.iterator();

		while ((vStereotypeApplication == null) && vIterator.hasNext()) {
			java.lang.Class<? extends IAbstractSFailureModeLAnalysis> vSFailureMode = vIterator.next();
			vStereotypeApplication = UMLUtil.getStereotypeApplication(pElement, vSFailureMode);
		}
		return vStereotypeApplication;
	}
}
