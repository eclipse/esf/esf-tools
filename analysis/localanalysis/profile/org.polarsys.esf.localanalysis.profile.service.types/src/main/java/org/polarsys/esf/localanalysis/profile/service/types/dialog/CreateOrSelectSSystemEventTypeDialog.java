
/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.localanalysis.profile.service.types.dialog;

import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.papyrus.uml.diagram.common.dialogs.CreateOrSelectTypeDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.UMLPackage;
import org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage;
import org.polarsys.esf.localanalysis.profile.service.types.set.ESFLocalAnalysisTypesSet;

/**
 * Dialog for initialisation of SSystemeEvent (Property) type (the type is either selected or created).
 *
 * @author  $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class CreateOrSelectSSystemEventTypeDialog
	extends CreateOrSelectTypeDialog {

	/**
	 * Constructor for initialise of the dialog.
	 *
	 * @param pShell The parent shell
	 * @param pOwner The activity that owns the action
	 */
	public CreateOrSelectSSystemEventTypeDialog(
		final Shell pShell,
		final NamedElement pOwner) {
		super(
			pShell,
			pOwner,
			ElementTypeRegistry.getInstance().getType(ESFLocalAnalysisTypesSet.SSYSTEMEVENTTYPE_TYPE_ID),
			UMLPackage.eINSTANCE.getTypedElement_Type(),
			ESFLocalAnalysisPackage.eINSTANCE.getSSystemEventType(),
			ElementTypeRegistry.getInstance().getType(ESFLocalAnalysisTypesSet.SSYSTEMEVENTSLIBRARY_TYPE_ID),
			UMLPackage.eINSTANCE.getPackage_PackagedElement(),
			null);
	}

}
