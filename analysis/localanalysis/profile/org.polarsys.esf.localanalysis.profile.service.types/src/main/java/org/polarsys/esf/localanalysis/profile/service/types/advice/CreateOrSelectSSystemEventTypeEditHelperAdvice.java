/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.localanalysis.profile.service.types.advice;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.common.core.command.UnexecutableCommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.jface.window.Window;
import org.eclipse.papyrus.uml.diagram.common.dialogs.CreateOrSelectTypeDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.esflocalanalysis.ISLocalAnalysis;
import org.polarsys.esf.esflocalanalysis.ISSystemEventsLibrary;
import org.polarsys.esf.localanalysis.profile.service.types.command.ConfigureSSystemEventCommand;
import org.polarsys.esf.localanalysis.profile.service.types.dialog.CreateOrSelectSSystemEventTypeDialog;
import org.polarsys.esf.localanalysis.profile.tools.util.ESFLocalAnalysisUtil;

/**
 * Add a pop-up to create or select a SSystemEventType for the property type of the element (SSystemEvent) using this
 * advice.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public final class CreateOrSelectSSystemEventTypeEditHelperAdvice
	extends AbstractEditHelperAdvice {

	/** SystemEventsLibrary package name. */
	public static final String SYSTEMEVENTSLIBRARY_PACKAGE_NAME = "SystemEventsLibrary"; //$NON-NLS-1$

	/** Create SystemEventsLibrary package label. */
	public static final String CREATE_SYSTEMEVENTSLIBRARY_PACKAGE_LABEL = "Create SystemEventsLibrary package"; //$NON-NLS-1$

	/**
	 * Default constructor.
	 */
	public CreateOrSelectSSystemEventTypeEditHelperAdvice() {
		// Nothing to do
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected ICommand getBeforeConfigureCommand(final ConfigureRequest pRequest) {
		configureRequest(pRequest);
		return super.getBeforeConfigureCommand(pRequest);
	}

	/**
	 * @param pRequest ConfigureRequest
	 */
	protected void configureRequest(final ConfigureRequest pRequest) {
		Map<String, Object> vNewParameters = new HashMap<String, Object>();

		ICommand vConfigureCommand = null;

		Shell vShell = Display.getDefault().getActiveShell();
		Property vProperty = (Property) pRequest.getElementToConfigure();
		Class vClassOwner = (Class) vProperty.getOwner();
		Package vPackage = vClassOwner.getNearestPackage();

		EObject vStereotypeApplication = UMLUtil.getStereotypeApplication(vPackage, ISLocalAnalysis.class);

		if (vStereotypeApplication != null) {
			Package vTarget = getSSystemEventsLibraryPackage((ISLocalAnalysis) vStereotypeApplication);

			CreateOrSelectTypeDialog vDialog = new CreateOrSelectSSystemEventTypeDialog(vShell, vTarget);

			vDialog.open();
			if (vDialog.getReturnCode() == Window.OK) {

				final ICommand vTypeCreationCommand = vDialog.getNewTypeCreateCommand();
				final Type vSSystemEventType = (Type) vDialog.getExistingType();

				// Abort if type creation command exists but is not executable
				if ((vTypeCreationCommand != null) && (!vTypeCreationCommand.canExecute())) {
					vConfigureCommand = UnexecutableCommand.INSTANCE;
				} else {
					vConfigureCommand = CompositeCommand.compose(vConfigureCommand, vTypeCreationCommand);
				}

				// Create the configure command that will set the constraint property type
				ICommand vSetTypeCommand =
					new ConfigureSSystemEventCommand(pRequest, vSSystemEventType, vTypeCreationCommand);
				vConfigureCommand = CompositeCommand.compose(vConfigureCommand, vSetTypeCommand);
			} else {
				throw new OperationCanceledException("Creation cancelled by user"); //$NON-NLS-1$
			}

			vNewParameters.put(AfterConfigureCommandEditHelperAdvice.AFTER_CONFIGURE_COMMAND, vConfigureCommand);
			pRequest.addParameters(vNewParameters);
		}
	}

	/**
	 * Retrieve SSystemEventsLibrary package.
	 *
	 * @param pLocalAnalysis The SLocalAnalysis
	 * @return The SSystemEventsLibrary package
	 */
	private Package getSSystemEventsLibraryPackage(final ISLocalAnalysis pLocalAnalysis) {
		ISSystemEventsLibrary vSSystemEventsLibrary = pLocalAnalysis.getSSystemEventsLibrary();

		if (vSSystemEventsLibrary == null) {
			ESFLocalAnalysisUtil.createSSystemEventsLibrary(pLocalAnalysis.getBase_Package());
			vSSystemEventsLibrary = pLocalAnalysis.getSSystemEventsLibrary();
		}

		return vSSystemEventsLibrary.getBase_Package();
	}
}
