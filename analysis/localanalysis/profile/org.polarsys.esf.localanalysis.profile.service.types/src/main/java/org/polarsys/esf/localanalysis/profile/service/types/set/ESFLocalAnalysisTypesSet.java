/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.localanalysis.profile.service.types.set;

/**
 * ESFLocalAnalysis types set.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public final class ESFLocalAnalysisTypesSet {

	/** Specialized Types ID of SSystemEventslibrary. */
	public static final String SSYSTEMEVENTSLIBRARY_TYPE_ID =
		"org.polarsys.esf.ESFLocalAnalysis.SSystemEventsLibrary"; //$NON-NLS-1$

	/** Specialized Types ID of SSystemEventType. */
	public static final String SSYSTEMEVENTTYPE_TYPE_ID =
		"org.polarsys.esf.ESFLocalAnalysis.SSystemEventType"; //$NON-NLS-1$

	/**
	 * Default constructor, private as it's a utility class.
	 */
	private ESFLocalAnalysisTypesSet() {
		// Nothing to do
	}
}
