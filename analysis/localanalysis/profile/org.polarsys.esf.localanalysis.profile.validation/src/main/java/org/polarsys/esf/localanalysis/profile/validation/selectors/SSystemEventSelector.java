/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.localanalysis.profile.validation.selectors;

import org.eclipse.emf.validation.model.IClientSelector;
import org.polarsys.esf.esflocalanalysis.ISSystemEvent;

/**
 * Class to match the {@link org.eclipse.emf.ecore.EObject}s against a client context
 * ({@link ISSystemEvent}).
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class SSystemEventSelector
	implements IClientSelector {

	/**
	 * Default constructor.
	 */
	public SSystemEventSelector() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean selects(final Object pObject) {
		return pObject instanceof ISSystemEvent;
	}

}
