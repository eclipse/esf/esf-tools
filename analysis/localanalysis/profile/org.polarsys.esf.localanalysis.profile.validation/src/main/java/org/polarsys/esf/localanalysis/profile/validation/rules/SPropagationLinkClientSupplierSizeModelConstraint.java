/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.localanalysis.profile.validation.rules;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.esflocalanalysis.IAbstractSPropagationElement;
import org.polarsys.esf.esflocalanalysis.ISAbsentFailureMode;
import org.polarsys.esf.esflocalanalysis.ISAndGateLAnalysis;
import org.polarsys.esf.esflocalanalysis.ISErroneousFailureMode;
import org.polarsys.esf.esflocalanalysis.ISLocalEvent;
import org.polarsys.esf.esflocalanalysis.ISOrGateLAnalysis;
import org.polarsys.esf.esflocalanalysis.ISPropagationLink;
import org.polarsys.esf.esflocalanalysis.ISSystemEvent;
import org.polarsys.esf.esflocalanalysis.ISUntimelyFailureMode;
import org.polarsys.esf.localanalysis.profile.set.ESFLocalAnalysisSet;

/**
 * Class define the constraint of the Clients and the Suppliers size of the element base {@link Dependency} of
 * {@link ISPropagationLink}:
 * - The Clients could be an element stereotyped by: {@link ISLocalEvent}, {@link ISSystemEvent},
 * {@link ISOrGateLAnalysis}, {@link ISAndGateLAnalysi}s, {@link ISAbsentFailureMode}, {@link ISErroneousFailureMode} or
 * {@link ISUltimelyFailureMode}.
 * - The Suppliers could be an element stereotyped by: {@link ISOrGateLAnalysis}, {@link ISAndGateLAnalysis},
 * {@link ISAbsentFailureMode}, {@link ISErroneousFailureMode} or {@link ISUntimelyFailureMode}.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class SPropagationLinkClientSupplierSizeModelConstraint
	extends AbstractModelConstraint {

	/**
	 * Default constructor.
	 */
	public SPropagationLinkClientSupplierSizeModelConstraint() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IStatus validate(final IValidationContext pContext) {
		IStatus vStatus = Status.OK_STATUS;
		if (pContext.getTarget() instanceof ISPropagationLink) {
			ISPropagationLink vSPropagationLink = (ISPropagationLink) pContext.getTarget();
			Dependency vDependency = vSPropagationLink.getBase_Dependency();

			Boolean vHasCorrectClient = false;
			Boolean vHasCorrectSupplier = false;

			if (vDependency != null) {
				Iterator<NamedElement> vIterator = vDependency.getClients().iterator();

				while (vIterator.hasNext()) {
					NamedElement vElement = vIterator.next();
					EObject vStereotypeApplication = getStereotypeApplicationForClientElement(vElement);

					if (vStereotypeApplication != null) {
						vHasCorrectClient = true;
					}
				}

				vIterator = vDependency.getSuppliers().iterator();

				while (vIterator.hasNext()) {
					NamedElement vElement = vIterator.next();
					EObject vStereotypeApplication = getStereotypeApplicationForSupplierElement(vElement);

					if (vStereotypeApplication != null) {
						vHasCorrectSupplier = true;
					}
				}

				if (vHasCorrectClient && vHasCorrectSupplier) {
					vStatus = pContext.createSuccessStatus();
				} else {
					vStatus = pContext.createFailureStatus(pContext.getTarget());
				}
			}
		}
		return vStatus;
	}

	/**
	 * Get the Stereotype Application for Client element.
	 *
	 * @param pElement The UML Element
	 * @return The Stereotype Application
	 */
	private EObject getStereotypeApplicationForClientElement(final NamedElement pElement) {
		EObject vStereotypeApplication = null;
		List<java.lang.Class<? extends IAbstractSPropagationElement>> vSPropagationElementsList =
			new ArrayList<java.lang.Class<? extends IAbstractSPropagationElement>>();

		vSPropagationElementsList.addAll(ESFLocalAnalysisSet.SLOGICALGATES_LIST);
		vSPropagationElementsList.addAll(ESFLocalAnalysisSet.SFAILUREEVENTS_LIST);
		vSPropagationElementsList.addAll(ESFLocalAnalysisSet.SFAILUREMODES_LIST);

		vStereotypeApplication = getStereotypeApplicationOfSPropagationElement(pElement, vSPropagationElementsList);

		return vStereotypeApplication;
	}

	/**
	 * Get the Stereotype Application for Supplier element.
	 *
	 * @param pElement The UML Element
	 * @return The Stereotype Application
	 */
	private EObject getStereotypeApplicationForSupplierElement(final NamedElement pElement) {
		EObject vStereotypeApplication = null;
		List<java.lang.Class<? extends IAbstractSPropagationElement>> vSPropagationElementsList =
			new ArrayList<java.lang.Class<? extends IAbstractSPropagationElement>>();

		vSPropagationElementsList.addAll(ESFLocalAnalysisSet.SLOGICALGATES_LIST);
		vSPropagationElementsList.addAll(ESFLocalAnalysisSet.SFAILUREMODES_LIST);

		vStereotypeApplication = getStereotypeApplicationOfSPropagationElement(pElement, vSPropagationElementsList);

		return vStereotypeApplication;
	}

	/**
	 * Get the Stereotype Application of SPropagationElement.
	 *
	 * @param pElement The UML Element
	 * @param pSPropagationElementsList The List of SPropagationElements defined by ESFLocalAnalysis profile.
	 *
	 * @return The Stereotype Application
	 */
	private EObject getStereotypeApplicationOfSPropagationElement(
		final Element pElement,
		final List<Class<? extends IAbstractSPropagationElement>> pSPropagationElementsList) {
		EObject vStereotypeApplication = null;

		Iterator<java.lang.Class<? extends IAbstractSPropagationElement>> vIterator =
			pSPropagationElementsList.iterator();

		while ((vStereotypeApplication == null) && vIterator.hasNext()) {
			java.lang.Class<? extends IAbstractSPropagationElement> vSPropagationElement = vIterator.next();
			if (UMLUtil.getStereotypeApplication(pElement, vSPropagationElement) != null) {
				vStereotypeApplication = UMLUtil.getStereotypeApplication(pElement, vSPropagationElement);
			}
		}
		return vStereotypeApplication;
	}
}
