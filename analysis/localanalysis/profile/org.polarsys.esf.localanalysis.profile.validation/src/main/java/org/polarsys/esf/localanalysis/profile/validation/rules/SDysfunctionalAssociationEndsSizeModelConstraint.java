/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.localanalysis.profile.validation.rules;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.uml2.uml.Connector;
import org.polarsys.esf.esflocalanalysis.ISDysfunctionalAssociation;

/**
 * Class define the constraint of the Ends size of the element base {@link Connector} of
 * {@link ISDysfunctionalAssociation}:
 * The Ends size must be equal to two.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class SDysfunctionalAssociationEndsSizeModelConstraint
	extends AbstractModelConstraint {

	/**
	 * Default constructor.
	 */
	public SDysfunctionalAssociationEndsSizeModelConstraint() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IStatus validate(final IValidationContext pContext) {
		IStatus vStatus = Status.OK_STATUS;
		if (pContext.getTarget() instanceof ISDysfunctionalAssociation) {
			ISDysfunctionalAssociation vSDysfunctionalAssociation = (ISDysfunctionalAssociation) pContext.getTarget();
			Connector vConnector = vSDysfunctionalAssociation.getBase_Connector();

			if (vConnector != null) {
				if (vConnector.getEnds().size() == 2) {
					vStatus = pContext.createSuccessStatus();
				} else {
					vStatus = pContext.createFailureStatus(pContext.getTarget());
				}
			}
		}
		return vStatus;
	}
}
