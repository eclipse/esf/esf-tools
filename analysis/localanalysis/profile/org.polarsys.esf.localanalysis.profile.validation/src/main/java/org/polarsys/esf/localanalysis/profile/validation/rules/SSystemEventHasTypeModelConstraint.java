/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.localanalysis.profile.validation.rules;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.polarsys.esf.esflocalanalysis.ISSystemEvent;
import org.polarsys.esf.esflocalanalysis.ISSystemEventType;

/**
 * Class define the constraint of the type of the {@link ISSystemEvent}:
 * It must be typed by {@link ISSystemEventType}.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class SSystemEventHasTypeModelConstraint
	extends AbstractModelConstraint {

	/**
	 * Default constructor.
	 */
	public SSystemEventHasTypeModelConstraint() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IStatus validate(final IValidationContext pContext) {
		IStatus vStatus = Status.OK_STATUS;
		EObject vTarget = pContext.getTarget();
		ISSystemEventType vType = null;

		if (vTarget instanceof ISSystemEvent) {
			ISSystemEvent vSSystemEvent = (ISSystemEvent) vTarget;

			vType = vSSystemEvent.getType();

			if (vType != null) {
				vStatus = pContext.createSuccessStatus();
			} else {
				vStatus = pContext.createFailureStatus(pContext.getTarget());
			}
		}
		return vStatus;
	}
}
