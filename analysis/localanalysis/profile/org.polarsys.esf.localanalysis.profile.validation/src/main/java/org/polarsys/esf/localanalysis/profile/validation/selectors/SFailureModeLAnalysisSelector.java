/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.localanalysis.profile.validation.selectors;

import org.eclipse.emf.validation.model.IClientSelector;
import org.polarsys.esf.esflocalanalysis.ISAbsentFailureMode;
import org.polarsys.esf.esflocalanalysis.ISErroneousFailureMode;
import org.polarsys.esf.esflocalanalysis.ISUntimelyFailureMode;

/**
 * Class to match the {@link org.eclipse.emf.ecore.EObject}s against a client context
 * ({@link ISFailureModeLAnalysis}).
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class SFailureModeLAnalysisSelector
	implements IClientSelector {

	/**
	 * Default constructor.
	 */
	public SFailureModeLAnalysisSelector() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean selects(final Object pObject) {
		boolean vIsSFailureModeLAnalysis = false;
		if ((pObject instanceof ISAbsentFailureMode) || (pObject instanceof ISErroneousFailureMode)
			|| (pObject instanceof ISUntimelyFailureMode)) {
			vIsSFailureModeLAnalysis = true;
		}
		return vIsSFailureModeLAnalysis;
	}

}
