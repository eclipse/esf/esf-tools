/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.localanalysis.profile.validation.rules;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis;
import org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeOwner;
import org.polarsys.esf.esflocalanalysis.ISAbsentFailureMode;
import org.polarsys.esf.esflocalanalysis.ISErroneousFailureMode;
import org.polarsys.esf.esflocalanalysis.ISUntimelyFailureMode;

/**
 * Class define the constraint of the owner of the {@link ISAbsentFailureMode}, {@link ISErroneousFailureMode} or
 * {@link ISUntimelyFailureMode} :
 * It must be an {@link IAbstractSFailureModeOwner}.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class SFailureModeLAnalysisOwnerModelConstraint
	extends AbstractModelConstraint {

	/**
	 * Default constructor.
	 */
	public SFailureModeLAnalysisOwnerModelConstraint() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IStatus validate(final IValidationContext pContext) {
		IStatus vStatus = Status.OK_STATUS;
		EObject vTarget = pContext.getTarget();
		IAbstractSFailureModeOwner vOwner = null;

		if (vTarget instanceof IAbstractSFailureModeLAnalysis) {
			if (vTarget instanceof ISAbsentFailureMode) {
				ISAbsentFailureMode vSAbsentFailureMode = (ISAbsentFailureMode) vTarget;
				vOwner = vSAbsentFailureMode.getOwner();
			} else if (vTarget instanceof ISUntimelyFailureMode) {
				ISUntimelyFailureMode vSUltimelyFailureMode = (ISUntimelyFailureMode) vTarget;
				vOwner = vSUltimelyFailureMode.getOwner();
			} else if (vTarget instanceof ISErroneousFailureMode) {
				ISErroneousFailureMode vSErroneousFailureMode = (ISErroneousFailureMode) vTarget;
				vOwner = vSErroneousFailureMode.getOwner();
			}

			if (vOwner != null) {
				vStatus = pContext.createSuccessStatus();
			} else {
				vStatus = pContext.createFailureStatus(pContext.getTarget());
			}
		}
		return vStatus;
	}
}
