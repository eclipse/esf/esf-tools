/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.localanalysis.profile.validation.rules;

import java.util.Iterator;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeLAnalysis;
import org.polarsys.esf.esflocalanalysis.IAbstractSFailureModeOwner;
import org.polarsys.esf.esflocalanalysis.ISAbsentFailureMode;
import org.polarsys.esf.esflocalanalysis.ISBarrierLAnalysis;
import org.polarsys.esf.esflocalanalysis.ISDysfunctionalAssociation;
import org.polarsys.esf.esflocalanalysis.ISErroneousFailureMode;
import org.polarsys.esf.esflocalanalysis.ISPortLAnalysis;
import org.polarsys.esf.esflocalanalysis.ISUntimelyFailureMode;
import org.polarsys.esf.localanalysis.profile.set.ESFLocalAnalysisSet;

/**
 * Class define the constraint of Ends of the element base {@link Connector} of {@link ISDysfunctionalAssociation}:
 * The one End must be an element stereotyped by a "Failure Mode Owner": {@link ISPortLAnalysis} or
 * {@link ISBarrierLAnalysis}.
 * And the other end must be an element stereotyped by a "Failure Mode": {@link ISAbsentFailureMode},
 * {@link ISErroneousFailureMode} or {@link ISUntimelyFailureMode}.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class SDysfunctionalAssociationEndsModelConstraint
	extends AbstractModelConstraint {

	/**
	 * Default constructor.
	 */
	public SDysfunctionalAssociationEndsModelConstraint() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IStatus validate(final IValidationContext pContext) {
		IStatus vStatus = Status.OK_STATUS;
		if (pContext.getTarget() instanceof ISDysfunctionalAssociation) {
			ISDysfunctionalAssociation vSDysfunctionalAssociation = (ISDysfunctionalAssociation) pContext.getTarget();
			Connector vConnector = vSDysfunctionalAssociation.getBase_Connector();

			boolean vHasEndSFailureModeOwner = false;
			boolean vHasEndSFailureMode = false;

			if (vConnector != null) {
				Iterator<ConnectorEnd> vIterator = vConnector.getEnds().iterator();

				while (vIterator.hasNext()) {
					ConnectorEnd vConnectorEnd = (ConnectorEnd) vIterator.next();
					if (getStereotypeApplicationOfSFailureModeOwner(vConnectorEnd.getRole()) != null) {
						vHasEndSFailureModeOwner = true;
					} else if (getStereotypeApplicationOfSFailureModeLAnalysis(vConnectorEnd.getRole()) != null) {
						vHasEndSFailureMode = true;
					}
				}
			}

			if (vHasEndSFailureModeOwner && vHasEndSFailureMode) {
				vStatus = pContext.createSuccessStatus();
			} else {
				vStatus = pContext.createFailureStatus(pContext.getTarget());
			}
		}
		return vStatus;
	}

	/**
	 * Get the Stereotype Application of SFailureModeOwner.
	 *
	 * @param pElement The UML Element
	 * @return The Stereotype Application
	 */
	private EObject getStereotypeApplicationOfSFailureModeOwner(final Element pElement) {
		EObject vStereotypeApplication = null;

		Iterator<java.lang.Class<? extends IAbstractSFailureModeOwner>> vIterator =
			ESFLocalAnalysisSet.SFAILUREMODESOWNER_LIST.iterator();

		while ((vStereotypeApplication == null) && vIterator.hasNext()) {
			java.lang.Class<? extends IAbstractSFailureModeOwner> vSFailureModeOwner = vIterator.next();
			vStereotypeApplication = UMLUtil.getStereotypeApplication(pElement, vSFailureModeOwner);
		}
		return vStereotypeApplication;
	}

	/**
	 * Get the Stereotype Application of SFailureModeLAnalysis.
	 *
	 * @param pElement The UML Element
	 * @return The Stereotype Application
	 */
	private EObject getStereotypeApplicationOfSFailureModeLAnalysis(final Element pElement) {
		EObject vStereotypeApplication = null;

		Iterator<java.lang.Class<? extends IAbstractSFailureModeLAnalysis>> vIterator =
			ESFLocalAnalysisSet.SFAILUREMODES_LIST.iterator();

		while ((vStereotypeApplication == null) && vIterator.hasNext()) {
			java.lang.Class<? extends IAbstractSFailureModeLAnalysis> vSFailureMode = vIterator.next();
			vStereotypeApplication = UMLUtil.getStereotypeApplication(pElement, vSFailureMode);
		}
		return vStereotypeApplication;
	}
}
