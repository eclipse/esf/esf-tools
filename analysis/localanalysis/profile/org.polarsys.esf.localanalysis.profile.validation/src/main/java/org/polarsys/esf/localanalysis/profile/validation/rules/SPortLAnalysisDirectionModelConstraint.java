/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.localanalysis.profile.validation.rules;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.polarsys.esf.esfarchitectureconcepts.SDirection;
import org.polarsys.esf.esflocalanalysis.ISPortLAnalysis;

/**
 * Class define the constraint of the direction of the {@link ISPortLAnalysis}:
 * It should be IN or OUT. Not UNDEFINED.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class SPortLAnalysisDirectionModelConstraint
	extends AbstractModelConstraint {

	/**
	 * Default constructor.
	 */
	public SPortLAnalysisDirectionModelConstraint() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IStatus validate(final IValidationContext pContext) {
		IStatus vStatus = Status.OK_STATUS;
		if (pContext.getTarget() instanceof ISPortLAnalysis) {
			ISPortLAnalysis vSPortLAnalysis = (ISPortLAnalysis) pContext.getTarget();

			if (vSPortLAnalysis.getSDirectionLAnalysis().getValue() != SDirection.UNDEFINED_VALUE) {
				vStatus = pContext.createSuccessStatus();
			} else {
				vStatus = pContext.createFailureStatus(pContext.getTarget());
			}
		}
		return vStatus;
	}
}
