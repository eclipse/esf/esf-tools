/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.localanalysis.profile.validation.rules;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.uml2.uml.Dependency;
import org.polarsys.esf.esflocalanalysis.ISPropagationLink;

/**
 * Class define the constraint of the Clients and the Suppliers of the element base {@link Dependency} of
 * {@link ISPropagationLink}:
 * The Clients and the Suppliers size must be equal to one.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class SPropagationLinkClientSupplierModelConstraint
	extends AbstractModelConstraint {

	/**
	 * Default constructor.
	 */
	public SPropagationLinkClientSupplierModelConstraint() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IStatus validate(final IValidationContext pContext) {
		IStatus vStatus = Status.OK_STATUS;
		if (pContext.getTarget() instanceof ISPropagationLink) {
			ISPropagationLink vSPropagationLink = (ISPropagationLink) pContext.getTarget();
			Dependency vDependency = vSPropagationLink.getBase_Dependency();

			if (vDependency != null) {
				if ((vDependency.getClients().size() == 1) && (vDependency.getSuppliers().size() == 1)) {
					vStatus = pContext.createSuccessStatus();
				} else {
					vStatus = pContext.createFailureStatus(pContext.getTarget());
				}
			}
		}
		return vStatus;
	}
}
