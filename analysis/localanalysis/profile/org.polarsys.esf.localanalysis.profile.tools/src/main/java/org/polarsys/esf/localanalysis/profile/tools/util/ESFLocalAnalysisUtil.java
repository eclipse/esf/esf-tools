/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.localanalysis.profile.tools.util;

import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.papyrus.uml.tools.utils.PackageUtil;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.core.utils.ModelUtil;
import org.polarsys.esf.esfarchitectureconcepts.ISBlock;
import org.polarsys.esf.esflocalanalysis.ISBlockLAnalysis;
import org.polarsys.esf.esflocalanalysis.ISLocalAnalysis;
import org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage;
import org.polarsys.esf.esflocalanalysis.impl.SBlockLAnalysis;
import org.polarsys.esf.esflocalanalysis.impl.SFearedEvent;
import org.polarsys.esf.esflocalanalysis.impl.SFearedEventsFamily;
import org.polarsys.esf.esflocalanalysis.impl.SPortLAnalysis;
import org.polarsys.esf.esflocalanalysis.impl.SSystemEventType;
import org.polarsys.esf.localanalysis.profile.set.ESFLocalAnalysisSet;

/**
 * Utilities for working with ESFLocalAnalysis.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public final class ESFLocalAnalysisUtil {

	/** SFearedEventsLibrary package name. */
	public static final String SFEAREDEVENTSLIBRARY_PACKAGE_NAME = "FearedEventsLibrary"; //$NON-NLS-1$

	/** Create SFearedEventsLibrary label. */
	public static final String CREATE_SFEAREDEVENTSLIBRARY_LABEL = "Create SFearedEventsLibrary"; //$NON-NLS-1$

	/** Create SFearedEventsFamily label. */
	public static final String CREATE_SFEAREDEVENTSFAMILY_LABEL = "Create SFearedEventsFamily"; //$NON-NLS-1$

	/** Create SFearedEvent label. */
	public static final String CREATE_SFEAREDEVENT_LABEL = "Create SFearedEvent"; //$NON-NLS-1$

	/** SSystemEventsLibrary package name. */
	public static final String SSYSTEMEVENTSLIBRARY_PACKAGE_NAME = "SystemEventsLibrary"; //$NON-NLS-1$

	/** Create SSystemEventsLibrary label. */
	public static final String CREATE_SSYSTEMEVENTSLIBRARY_LABEL = "Create SSystemEventsLibrary"; //$NON-NLS-1$

	/** Create SSystemEventType label. */
	public static final String CREATE_SSYSTEMEVENTTYPE_LABEL = "Create SSystemEventType"; //$NON-NLS-1$

	/** SLocalAnalysis package name. */
	public static final String SLOCALANALYSIS_PACKAGE_NAME = "LocalAnalysis"; //$NON-NLS-1$

	/** Apply ESFLocalAnalysis profile label. */
	public static final String APPLY_ESFLOCALANALYSIS_PROFILE_LABEL = "Apply ESFLocalAnalysis profile"; //$NON-NLS-1$

	/** Create SLocalAnalysis label. */
	public static final String CREATE_SLOCALANALYSIS_LABEL = "Create SLocalAnalysis"; //$NON-NLS-1$

	/** Create SBlock_LA label. */
	public static final String CREATE_SBLOCK_LA_LABEL = "Create SBlock_LA"; //$NON-NLS-1$

	/**
	 * Default constructor.
	 */
	private ESFLocalAnalysisUtil() {
	}

	/**
	 * Create the System Events Library.
	 *
	 * @param pSLocalAnalysisPackage The LocalAnalysis package where the library must be created
	 */
	public static void createSSystemEventsLibrary(final Package pSLocalAnalysisPackage) {
		TransactionalEditingDomain vDomain = ModelUtil.getTransactionalEditingDomain(pSLocalAnalysisPackage);

		RecordingCommand vCreateSSystemEventsLibrary =
			new RecordingCommand(vDomain, CREATE_SSYSTEMEVENTSLIBRARY_LABEL) {

				@Override
				protected void doExecute() {
					// Create the object 'SystemEventsLibrary' package
					Package vSystemEventsLibrary =
						pSLocalAnalysisPackage.createNestedPackage(SSYSTEMEVENTSLIBRARY_PACKAGE_NAME);
					// Apply 'SSystemEventsLibrary' stereotype on 'SystemEventsLibrary' package
					UMLUtil.StereotypeApplicationHelper.getInstance(vSystemEventsLibrary).applyStereotype(
						vSystemEventsLibrary,
						ESFLocalAnalysisPackage.eINSTANCE.getSSystemEventsLibrary());
				}
			};

		// Verify if command can be executed
		if (vCreateSSystemEventsLibrary.canExecute()) {
			// Execute command
			vDomain.getCommandStack().execute(vCreateSSystemEventsLibrary);
		}
	}

	/**
	 * Create the SFearedEventsLibrary.
	 *
	 * @param pSLocalAnalysisPackage The LocalAnalysis package where the library must be created
	 */
	public static void createSFearedEventsLibrary(final Package pSLocalAnalysisPackage) {
		TransactionalEditingDomain vDomain = ModelUtil.getTransactionalEditingDomain(pSLocalAnalysisPackage);

		RecordingCommand vCreateSFearedEventsLibrary =
			new RecordingCommand(vDomain, CREATE_SFEAREDEVENTSLIBRARY_LABEL) {

				@Override
				protected void doExecute() {
					// Create the object 'FearedEventsLibrary' package
					Package vFearedEventsLibrary =
						pSLocalAnalysisPackage.createNestedPackage(SFEAREDEVENTSLIBRARY_PACKAGE_NAME);
					// Apply 'SFearedEventsLibrary' stereotype on 'FearedEventsLibrary' package
					UMLUtil.StereotypeApplicationHelper.getInstance(vFearedEventsLibrary).applyStereotype(
						vFearedEventsLibrary,
						ESFLocalAnalysisPackage.eINSTANCE.getSFearedEventsLibrary());
				}
			};

		// Verify if command can be executed
		if (vCreateSFearedEventsLibrary.canExecute()) {
			// Execute command
			vDomain.getCommandStack().execute(vCreateSFearedEventsLibrary);
		}
	}

	/**
	 * Create a new SSystemEventType element.
	 *
	 * @param pSSystemEventsLibrary The SSystemEventsLibrary package where the SystemEventTypes must be created
	 */
	public static void createSSystemEventType(final Package pSSystemEventsLibrary) {

		TransactionalEditingDomain vDomain = ModelUtil.getTransactionalEditingDomain(pSSystemEventsLibrary);

		RecordingCommand vCreateSSystemEventType = new RecordingCommand(vDomain, CREATE_SSYSTEMEVENTTYPE_LABEL) {

			@Override
			protected void doExecute() {
				String vName = SSystemEventType.class.getSimpleName() + pSSystemEventsLibrary.getOwnedElements().size();
				// Create a new SSystemEventType element
				Class vSSystemEventType = pSSystemEventsLibrary.createOwnedClass(vName, false);
				// Apply 'SSystemEventType' stereotype on the created class
				UMLUtil.StereotypeApplicationHelper.getInstance(vSSystemEventType)
					.applyStereotype(vSSystemEventType, ESFLocalAnalysisPackage.eINSTANCE.getSSystemEventType());
			}
		};

		// Verify if command can be executed
		if (vCreateSSystemEventType.canExecute()) {
			// Execute command
			vDomain.getCommandStack().execute(vCreateSSystemEventType);
		}
	}

	/**
	 * Create a new createSFearedEvent element.
	 *
	 * @param pParentElement The SFearedEventsLibrary/SFearedEventsFamily element (UML) where the SFearedEvent
	 * must be created
	 */
	public static void createSFearedEvent(final Element pParentElement) {

		TransactionalEditingDomain vDomain = ModelUtil.getTransactionalEditingDomain(pParentElement);

		RecordingCommand vCreateSFearedEvent = new RecordingCommand(vDomain, CREATE_SFEAREDEVENT_LABEL) {

			@Override
			protected void doExecute() {
				String vName = SFearedEvent.class.getSimpleName() + pParentElement.getOwnedElements().size();
				// Create a new SFearedEvent element
				Class vSFearedEventClass = UMLFactory.eINSTANCE.createClass();
				vSFearedEventClass.setName(vName);

				if (pParentElement instanceof Package) {
					((Package) pParentElement).getPackagedElements().add(vSFearedEventClass);
				} else if (pParentElement instanceof Class) {
					((Class) pParentElement).getNestedClassifiers().add(vSFearedEventClass);
				}

				if (vSFearedEventClass != null) {
					// Apply 'SFearedEvent' stereotype on the created class
					UMLUtil.StereotypeApplicationHelper.getInstance(vSFearedEventClass)
						.applyStereotype(vSFearedEventClass, ESFLocalAnalysisPackage.eINSTANCE.getSFearedEvent());
				}
			}
		};

		// Verify if command can be executed
		if (vCreateSFearedEvent.canExecute()) {
			// Execute command
			vDomain.getCommandStack().execute(vCreateSFearedEvent);
		}
	}

	/**
	 * Create a new createSFearedEventsFamily element.
	 *
	 * @param pParentElement The SFearedEventsLibrary/SFearedEventsFamily element (UML) where the SFearedEventsFamily
	 * must be created
	 */
	public static void createSFearedEventsFamily(final Element pParentElement) {

		TransactionalEditingDomain vDomain = ModelUtil.getTransactionalEditingDomain(pParentElement);

		RecordingCommand vCreateSFearedEventsFamily = new RecordingCommand(vDomain, CREATE_SFEAREDEVENTSFAMILY_LABEL) {

			@Override
			protected void doExecute() {
				String vName = SFearedEventsFamily.class.getSimpleName() + pParentElement.getOwnedElements().size();
				// Create a new SFearedEventsFamily element
				Class vSFearedEventsFamilyClass = UMLFactory.eINSTANCE.createClass();
				vSFearedEventsFamilyClass.setName(vName);

				if (pParentElement instanceof Package) {
					((Package) pParentElement).getPackagedElements().add(vSFearedEventsFamilyClass);
				} else if (pParentElement instanceof Class) {
					((Class) pParentElement).getNestedClassifiers().add(vSFearedEventsFamilyClass);
				}

				if (vSFearedEventsFamilyClass != null) {
					// Apply 'SFearedEventsFamily' stereotype on the created class
					UMLUtil.StereotypeApplicationHelper.getInstance(vSFearedEventsFamilyClass).applyStereotype(
						vSFearedEventsFamilyClass,
						ESFLocalAnalysisPackage.eINSTANCE.getSFearedEventsFamily());
				}
			}
		};

		// Verify if command can be executed
		if (vCreateSFearedEventsFamily.canExecute()) {
			// Execute command
			vDomain.getCommandStack().execute(vCreateSFearedEventsFamily);
		}
	}

	/**
	 * Create SLocalAnalysis.
	 *
	 * @param pSafetyArtifacts The SafetyArtifacts package
	 */
	public static void createSLocalAnalysis(final Package localAnalysisPkg) {
		TransactionalEditingDomain vDomain = ModelUtil.getTransactionalEditingDomain(localAnalysisPkg);

		RecordingCommand vCreateLocalAnalysisPackage = new RecordingCommand(vDomain, CREATE_SLOCALANALYSIS_LABEL) {

			@Override
			protected void doExecute() {
				// Verify and apply (if necessary) ESFLocalAnalysis profile
				if (localAnalysisPkg.getAppliedProfile(ESFLocalAnalysisPackage.eNS_PREFIX) == null) {
					ESFLocalAnalysisUtil.applyESFLocalAnalysisProfile(localAnalysisPkg);
				}		   
			}
		};

		// Verify if command can be executed
		if (vCreateLocalAnalysisPackage.canExecute()) {
			// Execute command
			vDomain.getCommandStack().execute(vCreateLocalAnalysisPackage);
		}
	}

	/**
	 * Create SBlockLAnalysis.
	 *
	 * @param block The SBlock to be local analyzed
	 */
	public static void createSBlockLAnalysis(final Class block) {
		TransactionalEditingDomain vDomain = ModelUtil.getTransactionalEditingDomain(block);
		RecordingCommand vCreateSBlockLA = new RecordingCommand(vDomain, CREATE_SBLOCK_LA_LABEL) {

			@Override
			protected void doExecute() {
				// Apply 'SBlockLAnalysis' stereotype on 'SBlockLA'
				StereotypeUtil.apply(block, SBlockLAnalysis.class);

				// Create the ports for SBlockLA
				for (Port port : block.getOwnedPorts()) {
					// Apply 'SPortLAnalysis' stereotype on 'SBlockLA'
					StereotypeUtil.apply(port, SPortLAnalysis.class);
				}
			}
		};

		// Verify if command can be executed
		if (vCreateSBlockLA.canExecute()) {
			// Execute command
			vDomain.getCommandStack().execute(vCreateSBlockLA);
		}
	}

	/**
	 * Apply ESFLocalAnalysis profile to the given model element.
	 *
	 * @param pModel The model target
	 */
	public static void applyESFLocalAnalysisProfile(final Package pModel) {
		TransactionalEditingDomain vDomain = ModelUtil.getTransactionalEditingDomain(pModel);

		RecordingCommand vApplyLAProfileCmd = new RecordingCommand(vDomain, APPLY_ESFLOCALANALYSIS_PROFILE_LABEL) {

			@Override
			protected void doExecute() {
				// Retrieve ESFLocalAnalysis profile and apply it
				Profile vESFLocalAnalysisProfile = (Profile) PackageUtil
					.loadPackage(URI.createURI(ESFLocalAnalysisSet.PROFILE_PATH), pModel.eResource().getResourceSet());

				if (vESFLocalAnalysisProfile != null) {
					PackageUtil.applyProfile(pModel, vESFLocalAnalysisProfile, true);
				}
			}
		};

		// Verify if command can be executed
		if (vApplyLAProfileCmd.canExecute()) {
			// Execute command
			vDomain.getCommandStack().execute(vApplyLAProfileCmd);
		}
	}

	/**
	 * Get SLocalAnalysis.
	 *
	 * @param pSafetyArtifacts The SafetyArtifacts package
	 * @return The SLocalAnalysis
	 */
	public static ISLocalAnalysis getSLocalAnalysis(final Package pSafetyArtifacts) {
		ISLocalAnalysis vSLocalAnalysis = ESFLocalAnalysisUtil.retrieveSLocalAnalysis(pSafetyArtifacts);

		if (vSLocalAnalysis == null) {
			ESFLocalAnalysisUtil.createSLocalAnalysis(pSafetyArtifacts);
			vSLocalAnalysis = retrieveSLocalAnalysis(pSafetyArtifacts);
		}

		return vSLocalAnalysis;
	}

	/**
	 * Retrieve SLocalAnalysis.
	 *
	 * @param pSafetyArtifacts The SafetyArtifacts package
	 * @return The SLocalAnalysis
	 */
	private static ISLocalAnalysis retrieveSLocalAnalysis(final Package pSafetyArtifacts) {
		ISLocalAnalysis vSLocalAnalysis = null;
		Boolean vFound = false;
		List<Package> vPackagesList = pSafetyArtifacts.getNestedPackages();
		Iterator<Package> vIterator = vPackagesList.iterator();

		while (vIterator.hasNext() && !vFound) {
			Package vPackage = vIterator.next();
			EObject vStereotypeApplication = UMLUtil.getStereotypeApplication(vPackage, ISLocalAnalysis.class);
			if (vStereotypeApplication != null) {
				vFound = true;
				vSLocalAnalysis = (ISLocalAnalysis) vStereotypeApplication;
			}
		}

		return vSLocalAnalysis;
	}

	/**
	 * Get the Local Analysis of a SBlock (SBlockLAnalysis).
	 *
	 * @param pSLocalAnalysis The SLocalAnalysis
	 * @param pSBlock The SBlock which is analyzed by SBlockLAnalysis
	 * @return The SBlockLAnalysis
	 */
	@Deprecated
	public static ISBlockLAnalysis getSBlockLAnalysisBySBlock(
		final ISLocalAnalysis pSLocalAnalysis,
		final ISBlock pSBlock) {
		ISBlockLAnalysis vSBlockLAnalysis = null;
		List<ISBlockLAnalysis> vSBlocksLAnalysisList = pSLocalAnalysis.getSBlocksLAnalysisList();
		Iterator<ISBlockLAnalysis> vIterator = vSBlocksLAnalysisList.iterator();

		while (vSBlockLAnalysis == null && vIterator.hasNext()) {
			vSBlockLAnalysis = vIterator.next();

			if (!vSBlockLAnalysis.getSBlock().equals(pSBlock)) {
				vSBlockLAnalysis = null;
			}
		}
		return vSBlockLAnalysis;
	}
}
