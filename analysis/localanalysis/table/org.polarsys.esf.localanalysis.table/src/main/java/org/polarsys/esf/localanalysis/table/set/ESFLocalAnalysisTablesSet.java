/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.localanalysis.table.set;

/**
 * ESFLocalAnalysis tables set.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public final class ESFLocalAnalysisTablesSet {

	/**
	 * SSystemEventslibrary Table ID.
	 * NOTE: The ID must be the same in:
	 *  - sSystemEventsLibraryTable.configuration
	 *  - sSystemEventsLibraryTable.nattableconfiguration
	 */
	public static final String SSYSTEMEVENTSLIBRARY_TABLE_ID =
		"SSystemEventsLibraryTable"; //$NON-NLS-1$

	/**
	 * SSystemEventslibrary Table name.
	 * NOTE: The name must be the same in:
	 *  - sSystemEventsLibraryTable.nattableconfiguration
	 */
	public static final String SSYSTEMEVENTSLIBRARY_TABLE_NAME =
		"SystemEventsLibraryTable"; //$NON-NLS-1$


	/**
	 * Default constructor, private as it's a utility class.
	 */
	private ESFLocalAnalysisTablesSet() {
		// Nothing to do
	}
}
