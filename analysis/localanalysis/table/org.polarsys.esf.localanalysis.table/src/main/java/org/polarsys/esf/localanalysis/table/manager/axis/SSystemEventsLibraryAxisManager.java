/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.localanalysis.table.manager.axis;

import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.ISpecializationType;
import org.eclipse.papyrus.uml.nattable.manager.axis.AbstractStereotypedElementUMLSynchronizedOnFeatureAxisManager;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.esflocalanalysis.ISSystemEventType;
import org.polarsys.esf.esflocalanalysis.impl.ESFLocalAnalysisPackage;
import org.polarsys.esf.localanalysis.profile.service.types.set.ESFLocalAnalysisTypesSet;

/**
 *
 * Axis Manager for SSystemEventsTable.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class SSystemEventsLibraryAxisManager
	extends AbstractStereotypedElementUMLSynchronizedOnFeatureAxisManager<ISSystemEventType> {

	/**
	 *
	 * Default constructor.
	 */
	public SSystemEventsLibraryAxisManager() {
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	public boolean canCreateAxisElement(final String pElementId) {
		ISpecializationType vType = (ISpecializationType) ElementTypeRegistry.getInstance()
			.getType(ESFLocalAnalysisTypesSet.SSYSTEMEVENTTYPE_TYPE_ID);
		return vType.getId().equals(pElementId);
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	protected Element getStereotypeBaseElement(final ISSystemEventType pStereotypeApplication) {
		return pStereotypeApplication.getBase_Class();
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	protected boolean isInstanceOfRequiredStereotypeApplication(final Object pObject) {
		return pObject instanceof ISSystemEventType;
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	protected boolean isAllowedAsBaseElement(final Element pElement) {
		return pElement instanceof Class;
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	protected ISSystemEventType getStereotypeApplication(final Element pElement) {
		return UMLUtil.getStereotypeApplication(pElement, ISSystemEventType.class);
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	protected String getStereotypeApplicationBasePropertyName() {
		return ESFLocalAnalysisPackage.eINSTANCE.getSSystemEventType().getName();
	}

}
