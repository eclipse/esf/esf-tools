/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.localanalysis.diagram.set;

/**
 * ESFLocalAnalysis diagram set.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public final class ESFLocalAnalysisDiagramSet {

	/**
	 * ESFLocalAnalysis Diagram name.
	 * NOTE: The name must be the same in:
	 *  - localAnalysisDiagram.configuration
	 */
	public static final String ESFLOCALANALYSIS_DIAGRAM_NAME =
		"Component fault analysis diagram"; //$NON-NLS-1$


	/**
	 * Default constructor, private as it's a utility class.
	 */
	private ESFLocalAnalysisDiagramSet() {
		// Nothing to do
	}
}
