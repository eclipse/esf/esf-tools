/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.fmea.profile.service.types.set;

/**
 * ESFFMEA types set.
 *
 * @author $Author: ogurcan $
 * @version $Revision: 168 $
 */
public final class ESFFMEATypesSet {

	/** Specialized Types ID of SBlockFMEA. */
	public static final String SBLOCKFMEA_TYPE_ID =
		"org.polarsys.esf.ESFFMEA.SBlockFMEA"; //$NON-NLS-1$

	/** Specialized Types ID of SFailureModeFMEA. */
	public static final String SFAILUREMODEFMEA_TYPE_ID =
		"org.polarsys.esf.ESFFMEA.SFailureModeFMEA"; //$NON-NLS-1$

	/**
	 * Default constructor, private as it's a utility class.
	 */
	private ESFFMEATypesSet() {
		// Nothing to do
	}
}
