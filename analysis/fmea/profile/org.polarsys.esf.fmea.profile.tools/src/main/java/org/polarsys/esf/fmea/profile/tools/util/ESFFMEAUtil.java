/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.fmea.profile.tools.util;

import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.papyrus.uml.tools.utils.PackageUtil;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.core.utils.ModelUtil;
import org.polarsys.esf.esfarchitectureconcepts.ISBlock;
import org.polarsys.esf.esffmea.ISBlockFMEA;
import org.polarsys.esf.esffmea.ISFMEA;
import org.polarsys.esf.esffmea.impl.ESFFMEAPackage;
import org.polarsys.esf.esffmea.impl.SBlockFMEA;
import org.polarsys.esf.esffmea.impl.SFailureModeFMEA;
import org.polarsys.esf.fmea.profile.set.ESFFMEASet;

/**
 * Utilities for working with ESFFMEA.
 *
 * @author $Author: ogurcan $
 * @version $Revision: 168 $
 */
public final class ESFFMEAUtil {

	/** FMEA package name. */
	private static final String FMEA_PACKAGE_NAME = "FMEA"; //$NON-NLS-1$

	/** Apply ESFFMEA profile label. */
	private static final String APPLY_ESFFMEA_PROFILE_LABEL = "Apply ESFFMEA profile"; //$NON-NLS-1$

	/** Create FMEA package label. */
	private static final String CREATE_FMEA_PACKAGE_LABEL = "Create FMEA package"; //$NON-NLS-1$

	/** Create SBlock_FMEA label. */
	private static final String CREATE_SBLOCK_FMEA_LABEL = "Create SBlock_FMEA"; //$NON-NLS-1$

	/** Create SFailureMode_FMEA label. */
	private static final String CREATE_SFAILUREMODE_FMEA_LABEL = "Create SFailureMode_FMEA"; //$NON-NLS-1$

	/**
	 * Default constructor.
	 */
	private ESFFMEAUtil() {
	}

	/**
	 * Create SFMEA.
	 *
	 * @param pSafetyArtifacts The SafetyArtifacts package
	 */
	public static void createSFMEA(final Package pSafetyArtifacts) {
		TransactionalEditingDomain vDomain = ModelUtil.getTransactionalEditingDomain(pSafetyArtifacts);

		RecordingCommand vCreateFMEAPackage = new RecordingCommand(vDomain, CREATE_FMEA_PACKAGE_LABEL) {

			@Override
			protected void doExecute() {
				// Create the object 'FMEA' package
				Package vFMEA = pSafetyArtifacts.createNestedPackage(FMEA_PACKAGE_NAME);
				
				// Verify and apply (if necessary) ESFFMEA profile
				if (vFMEA.getAppliedProfile(ESFFMEAPackage.eNS_PREFIX) == null) {
					ESFFMEAUtil.applyESFFMEAProfile(vFMEA);
				}

				// Apply 'SFMEA' stereotype on 'FMEA' package
				UMLUtil.StereotypeApplicationHelper.getInstance(vFMEA)
					.applyStereotype(vFMEA, ESFFMEAPackage.eINSTANCE.getSFMEA());
			}
		};

		// Verify if command can be executed
		if (vCreateFMEAPackage.canExecute()) {
			// Execute command
			vDomain.getCommandStack().execute(vCreateFMEAPackage);
		}
	}

	/**
	 * Get SFMEA.
	 *
	 * @param pSafetyArtifacts The SafetyArtifacts package
	 * @return The SFMEA
	 */
	public static ISFMEA getSFMEA(final Package pSafetyArtifacts) {
		ISFMEA vSFMEA = retrieveSFMEA(pSafetyArtifacts);
		if (vSFMEA == null) {
			createSFMEA(pSafetyArtifacts);
			vSFMEA = retrieveSFMEA(pSafetyArtifacts);
		}

		return vSFMEA;
	}

	/**
	 * Retrieve SFMEA.
	 *
	 * @param pSafetyArtifacts The SafetyArtifacts package
	 * @return The SFMEA
	 */
	private static ISFMEA retrieveSFMEA(final Package pSafetyArtifacts) {
		ISFMEA vSFMEA = null;
		Boolean vFound = false;
		List<Package> vPackagesList = pSafetyArtifacts.getNestedPackages();
		Iterator<Package> vIterator = vPackagesList.iterator();

		while (vIterator.hasNext() && !vFound) {
			Package vPackage = vIterator.next();
			EObject vStereotypeApplication = UMLUtil.getStereotypeApplication(vPackage, ISFMEA.class);
			if (vStereotypeApplication != null) {
				vFound = true;
				vSFMEA = (ISFMEA) vStereotypeApplication;
			}
		}

		return vSFMEA;
	}

	/**
	 * Create SBlockFMEA.
	 *
	 * @param block The SBlock to be analyzed using fmea
	 */
	public static void createSBlockFMEA(final Class block) {
		TransactionalEditingDomain vDomain = ModelUtil.getTransactionalEditingDomain(block);

		RecordingCommand vCreateSBlockFMEA = new RecordingCommand(vDomain, CREATE_SBLOCK_FMEA_LABEL) {

			@Override
			protected void doExecute() {
				StereotypeUtil.applyApp(block, SBlockFMEA.class);
			}
		};

		// Verify if command can be executed
		if (vCreateSBlockFMEA.canExecute()) {
			// Execute command
			vDomain.getCommandStack().execute(vCreateSBlockFMEA);
		}
	}

	/**
	 * Create SFailureModeFMEA.
	 *
	 * @param pSBlockFMEA The SBlockFMEA to be analyzed using fmea
	 */
	public static void createSFailureModeFMEA(final Class pSBlockFMEA) {
		TransactionalEditingDomain vDomain = ModelUtil.getTransactionalEditingDomain(pSBlockFMEA.getPackage());
		RecordingCommand vCreateSFailureModeFMEA = new RecordingCommand(vDomain, CREATE_SFAILUREMODE_FMEA_LABEL) {

			@Override
			protected void doExecute() {
				// compute the name for the new property
				String vName = SFailureModeFMEA.class.getSimpleName() + pSBlockFMEA.getOwnedElements().size();
				// Create the object 'Property' of SBlock for FMEA
				Property vSFailureModeFMEAProperty = pSBlockFMEA.createOwnedAttribute(vName, null);

				// Apply 'SFailureModeFMEA' stereotype on 'vSFailureModeFMEAProperty'
				UMLUtil.StereotypeApplicationHelper.getInstance(vSFailureModeFMEAProperty)
					.applyStereotype(vSFailureModeFMEAProperty, ESFFMEAPackage.eINSTANCE.getSFailureModeFMEA());
			}
		};

		// Verify if command can be executed
		if (vCreateSFailureModeFMEA.canExecute()) {
			// Execute command
			vDomain.getCommandStack().execute(vCreateSFailureModeFMEA);
		}
	}

	/**
	 * Apply ESFFMEA profile to the given model element.
	 *
	 * @param pModel The model target
	 */
	public static void applyESFFMEAProfile(final Package pModel) {
		TransactionalEditingDomain vDomain = ModelUtil.getTransactionalEditingDomain(pModel);

		RecordingCommand vApplyFMEAProfileCmd = new RecordingCommand(vDomain, APPLY_ESFFMEA_PROFILE_LABEL) {

			@Override
			protected void doExecute() {
				// Retrieve ESFFMEA profile and apply it
				Profile vESFFMEAProfile = (Profile) PackageUtil
					.loadPackage(URI.createURI(ESFFMEASet.PROFILE_PATH), pModel.eResource().getResourceSet());

				if (vESFFMEAProfile != null) {
					PackageUtil.applyProfile(pModel, vESFFMEAProfile, true);
				}
			}
		};

		// Verify if command can be executed
		if (vApplyFMEAProfileCmd.canExecute()) {
			// Execute command
			vDomain.getCommandStack().execute(vApplyFMEAProfileCmd);
		}
	}

	/**
	 * Get the FMEA of a SBlock (SBlockFMEA).
	 *
	 * @param pSFMEA The SFMEA
	 * @param pSBlock The SBlock which is analysed by SBlockFMEA
	 * @return The SBlockFMEA
	 */
	public static ISBlockFMEA getSBlockFMEABySBlock(ISFMEA pSFMEA, ISBlock pSBlock) {
		ISBlockFMEA vSBlockFMEA = null;
		List<ISBlockFMEA> vSBlocksFMEAList = pSFMEA.getSBlocksFMEAList();
		Iterator<ISBlockFMEA> vIterator = vSBlocksFMEAList.iterator();

		while (vSBlockFMEA == null && vIterator.hasNext()) {
			vSBlockFMEA = vIterator.next();

			if (!vSBlockFMEA.getSBlock().equals(pSBlock)) {
				vSBlockFMEA = null;
			}
		}
		return vSBlockFMEA;
	}

}
