/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.fmea.impl;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreEList.UnmodifiableEList;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.esfcore.impl.GenericAbstractSElement;
import org.polarsys.esf.esffmea.ISFailureModeFMEA;
import org.polarsys.esf.esffmea.impl.ESFFMEAPackage;
import org.polarsys.esf.esffmea.impl.SBlockFMEA;
import org.polarsys.esf.fmea.IMSBlockFMEA;

/**
 * This class can override the generated class {@link SBlockFMEA} and will be
 * used by the custom factory.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class MSBlockFMEA
	extends SBlockFMEA
	implements IMSBlockFMEA {

	/**
	 * Default constructor.
	 */
	public MSBlockFMEA() {
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return GenericAbstractSElement.getName(getBase_Class());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getUUID() {
		return GenericAbstractSElement.getUUID(getBase_Class());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getCriticality() {
		EList<ISFailureModeFMEA> vSFailureModesFMEAList = getSFailureModesFMEAList();
		int vHighestCriticality = 0;

		if (!vSFailureModesFMEAList.isEmpty()) {
			for (ISFailureModeFMEA vSFailureModeFMEA : vSFailureModesFMEAList) {
				int vCriticalityFM = vSFailureModeFMEA.getFinalCriticality();
				if (vHighestCriticality < vCriticalityFM) {
					vHighestCriticality = vCriticalityFM;
				}
			}
		}
		return vHighestCriticality;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EList<ISFailureModeFMEA> getSFailureModesFMEAList() {
		EList<ISFailureModeFMEA> vSFailureModesFMEAList = new BasicEList<ISFailureModeFMEA>();
		Class vBase = getBase_Class();

		if (vBase != null) {
			/**
			 * Retrieve all SPortsLAnalysis.
			 */
			for (Element vElement : vBase.getOwnedElements()) {
				EObject vStereotypeApplication = UMLUtil.getStereotypeApplication(vElement, ISFailureModeFMEA.class);
				if (vStereotypeApplication != null) {
					vSFailureModesFMEAList.add((ISFailureModeFMEA) vStereotypeApplication);
				}
			}
		}

		UnmodifiableEList<ISFailureModeFMEA> vUSFailureModesFMEAList = new UnmodifiableEList<ISFailureModeFMEA>(
			this,
			ESFFMEAPackage.eINSTANCE.getSBlockFMEA_SFailureModesFMEAList(),
			vSFailureModesFMEAList.size(),
			vSFailureModesFMEAList.toArray());
		return vUSFailureModesFMEAList;
	}

}
