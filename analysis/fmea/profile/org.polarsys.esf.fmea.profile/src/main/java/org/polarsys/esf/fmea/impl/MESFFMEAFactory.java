/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.fmea.impl;

import org.polarsys.esf.esffmea.IESFFMEAFactory;
/**
 * This factory overrides the generated factory {@link ESFArchitectureConceptsFactory}
 * and returns the new generated interfaces.
 *
 * @author $Author: ogurcan $
 * @version $Revision: 186 $
 */
import org.polarsys.esf.esffmea.impl.ESFFMEAFactory;
import org.polarsys.esf.fmea.IMESFFMEAFactory;
import org.polarsys.esf.fmea.IMSBlockFMEA;
import org.polarsys.esf.fmea.IMSFMEA;
import org.polarsys.esf.fmea.IMSFailureModeFMEA;

/**
 * This factory overrides the generated factory {@link ESFFMEAFactory}
 * and returns the new generated interfaces.
 *
 * @author $Author: ogurcan $
 * @version $Revision: 168 $
 */
public class MESFFMEAFactory
	extends ESFFMEAFactory
	implements IMESFFMEAFactory {

	/**
	 * Default constructor.
	 */
	public MESFFMEAFactory() {
		super();
	}

	/**
	 * @return The initialised factory
	 */
	public static IMESFFMEAFactory init() {
		IESFFMEAFactory vFactory = ESFFMEAFactory.init();

		// Check if the factory returned by the standard implementation can suit,
		// otherwise create a new instance
		if (!(vFactory instanceof IMESFFMEAFactory)) {
			vFactory = new MESFFMEAFactory();
		}

		return (IMESFFMEAFactory) vFactory;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IMSFMEA createSFMEA() {
		IMSFMEA vFMEA = new MSFMEA();
		return vFMEA;
	}

	@Override
	public IMSBlockFMEA createSBlockFMEA() {
		IMSBlockFMEA vSBlockFMEA = new MSBlockFMEA();
		return vSBlockFMEA;
	}

	@Override
	public IMSFailureModeFMEA createSFailureModeFMEA() {
		IMSFailureModeFMEA vSFailureModeFMEA = new MSFailureModeFMEA();
		return vSFailureModeFMEA;
	}
}
