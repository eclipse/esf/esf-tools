/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.fmea.impl;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.esfcore.impl.GenericAbstractSElement;
import org.polarsys.esf.esffmea.ISBlockFMEA;
import org.polarsys.esf.esffmea.impl.SFailureModeFMEA;
import org.polarsys.esf.fmea.IMSFailureModeFMEA;

/**
 * This class can override the generated class {@link SFailureModeFMEA} and will be
 * used by the custom factory.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class MSFailureModeFMEA
	extends SFailureModeFMEA
	implements IMSFailureModeFMEA {

	/**
	 * Default constructor.
	 */
	public MSFailureModeFMEA() {
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return GenericAbstractSElement.getName(getBase_Property());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getUUID() {
		return GenericAbstractSElement.getUUID(getBase_Property());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ISBlockFMEA basicGetSBlockFMEA() {
		Property vBase = getBase_Property();
		ISBlockFMEA vSBlockFMEA = null;
		if (vBase != null) {
			Element vOwner = vBase.getOwner();
			EObject vStereotypeApplication = UMLUtil.getStereotypeApplication(vOwner, ISBlockFMEA.class);
			if (vStereotypeApplication != null) {
				vSBlockFMEA = (ISBlockFMEA) vStereotypeApplication;
			}
		}
		return vSBlockFMEA;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getFinalCriticality() {
		int vFinalCriticality = getFinalSeverity() * getFinalOccurence();

		if (isDetectable) {
			vFinalCriticality *= getFinalDetectability();
		}
		return vFinalCriticality;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getInitialCriticality() {
		int vInitialCriticality = getInitialSeverity() * getInitialOccurence();

		if (isDetectable) {
			vInitialCriticality *= getInitialDetectability();
		}
		return vInitialCriticality;
	}
}
