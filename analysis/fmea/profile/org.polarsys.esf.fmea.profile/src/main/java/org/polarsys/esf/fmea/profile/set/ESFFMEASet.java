/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.fmea.profile.set;


/**
 * ESFFMEA profile set.
 *
 * @author  $Author: ogurcan $
 * @version $Revision: 169 $
 */
public final class ESFFMEASet {
	/** ESFFMEA profile path. */
	public static final String PROFILE_PATH =
		"pathmap://ESFFMEA_PROFILE/esffmea.profile.uml"; //$NON-NLS-1$

	/** ESFFMEA profile URI. */
	public static final String PROFILE_URI =
		"http://www.polarsys.org/esf/0.7.0/ESFFMEA"; //$NON-NLS-1$

	/**
	 * Default constructor, private as it's a utility class.
	 */
	private ESFFMEASet() {
	 // Nothing to do
	}
}
