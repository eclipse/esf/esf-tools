/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.fmea.impl;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreEList.UnmodifiableEList;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.esfcore.impl.GenericAbstractSElement;
import org.polarsys.esf.esffmea.ISBlockFMEA;
import org.polarsys.esf.esffmea.impl.ESFFMEAPackage;
import org.polarsys.esf.esffmea.impl.SFMEA;
import org.polarsys.esf.fmea.IMSFMEA;

/**
 * This class can override the generated class {@link SFMEA} and will be
 * used by the custom factory.
 *
 * @author $Author: ogurcan $
 * @version $Revision: 168 $
 */
public class MSFMEA
	extends SFMEA
	implements IMSFMEA {

	/**
	 * Default constructor.
	 */
	public MSFMEA() {
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return GenericAbstractSElement.getName(getBase_Package());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getUUID() {
		return GenericAbstractSElement.getUUID(getBase_Package());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EList<ISBlockFMEA> getSBlocksFMEAList() {
		EList<ISBlockFMEA> vSSBlocksFMEAList = new BasicEList<ISBlockFMEA>();
		Package vBase = getBase_Package();

		if (vBase != null) {
			for (Element vElement : vBase.getOwnedElements()) {
				EObject vStereotypeApplication = UMLUtil.getStereotypeApplication(vElement, ISBlockFMEA.class);
				if (vStereotypeApplication != null) {
					vSSBlocksFMEAList.add((ISBlockFMEA) vStereotypeApplication);
				}
			}
		}

		UnmodifiableEList<ISBlockFMEA> vUSSBlocksFMEAList =
			new UnmodifiableEList<ISBlockFMEA>(
				this,
				ESFFMEAPackage.eINSTANCE.getSFMEA_SBlocksFMEAList(),
				vSSBlocksFMEAList.size(),
				vSSBlocksFMEAList.toArray());

		return vUSSBlocksFMEAList;
	}

}
