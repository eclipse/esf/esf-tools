/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.fmea;

import org.polarsys.esf.esffmea.IESFFMEAFactory;
import org.polarsys.esf.fmea.impl.MESFFMEAFactory;

/**
 * This interface can override the generated interface {@link IESFFMEAFactory}.
 *
 * @author $Author: ogurcan $
 * @version $Revision: 168 $
 */
public interface IMESFFMEAFactory
	extends IESFFMEAFactory {

	/**
	 * Specialise the eINSTANCE initialisation with the new interface type
	 * (overridden in the override_factory extension).
	 */
	IMESFFMEAFactory INSTANCE = MESFFMEAFactory.init();

	@Override
	IMSFMEA createSFMEA();

	@Override
	IMSBlockFMEA createSBlockFMEA();

	@Override
	IMSFailureModeFMEA createSFailureModeFMEA();
}
