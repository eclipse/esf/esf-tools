/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esffmea;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.polarsys.esf.esffmea.IESFFMEAPackage
 * @generated
 */
public interface IESFFMEAFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	IESFFMEAFactory eINSTANCE = org.polarsys.esf.esffmea.impl.ESFFMEAFactory.init();

	/**
	 * Returns a new object of class '<em>SFMEA</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SFMEA</em>'.
	 * @generated
	 */
	ISFMEA createSFMEA();

	/**
	 * Returns a new object of class '<em>SBlock FMEA</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SBlock FMEA</em>'.
	 * @generated
	 */
	ISBlockFMEA createSBlockFMEA();

	/**
	 * Returns a new object of class '<em>Abstract SFMEA Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Abstract SFMEA Element</em>'.
	 * @generated
	 */
	IAbstractSFMEAElement createAbstractSFMEAElement();

	/**
	 * Returns a new object of class '<em>SFailure Mode FMEA</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SFailure Mode FMEA</em>'.
	 * @generated
	 */
	ISFailureModeFMEA createSFailureModeFMEA();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	IESFFMEAPackage getESFFMEAPackage();

} //IESFFMEAFactory
