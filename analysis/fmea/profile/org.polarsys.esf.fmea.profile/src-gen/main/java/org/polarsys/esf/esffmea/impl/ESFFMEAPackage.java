/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esffmea.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.uml2.types.TypesPackage;

import org.eclipse.uml2.uml.UMLPackage;

import org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage;

import org.polarsys.esf.esfcore.IESFCorePackage;

import org.polarsys.esf.esffmea.IAbstractSFMEAElement;
import org.polarsys.esf.esffmea.IESFFMEAFactory;
import org.polarsys.esf.esffmea.IESFFMEAPackage;
import org.polarsys.esf.esffmea.ISBlockFMEA;
import org.polarsys.esf.esffmea.ISFMEA;
import org.polarsys.esf.esffmea.ISFailureModeFMEA;

import org.polarsys.esf.esfsafetyconcepts.IESFSafetyConceptsPackage;

import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ESFFMEAPackage extends EPackageImpl implements IESFFMEAPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sfmeaEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sBlockFMEAEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractSFMEAElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sFailureModeFMEAEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.polarsys.esf.esffmea.IESFFMEAPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ESFFMEAPackage() {
		super(eNS_URI, IESFFMEAFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link IESFFMEAPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static IESFFMEAPackage init() {
		if (isInited) return (IESFFMEAPackage)EPackage.Registry.INSTANCE.getEPackage(IESFFMEAPackage.eNS_URI);

		// Obtain or create and register package
		ESFFMEAPackage theESFFMEAPackage = (ESFFMEAPackage)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ESFFMEAPackage ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ESFFMEAPackage());

		isInited = true;

		// Initialize simple dependencies
		IESFArchitectureConceptsPackage.eINSTANCE.eClass();
		IESFSafetyConceptsPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theESFFMEAPackage.createPackageContents();

		// Initialize created meta-data
		theESFFMEAPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theESFFMEAPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(IESFFMEAPackage.eNS_URI, theESFFMEAPackage);
		return theESFFMEAPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSFMEA() {
		return sfmeaEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSFMEA_Base_Package() {
		return (EReference)sfmeaEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSFMEA_SBlocksFMEAList() {
		return (EReference)sfmeaEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSBlockFMEA() {
		return sBlockFMEAEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSBlockFMEA_SBlock() {
		return (EReference)sBlockFMEAEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSBlockFMEA_Criticality() {
		return (EAttribute)sBlockFMEAEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSBlockFMEA_Base_Class() {
		return (EReference)sBlockFMEAEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSBlockFMEA_SFailureModesFMEAList() {
		return (EReference)sBlockFMEAEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractSFMEAElement() {
		return abstractSFMEAElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSFailureModeFMEA() {
		return sFailureModeFMEAEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSFailureModeFMEA_Remarks() {
		return (EAttribute)sFailureModeFMEAEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSFailureModeFMEA_InitialCriticality() {
		return (EAttribute)sFailureModeFMEAEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSFailureModeFMEA_FinalCriticality() {
		return (EAttribute)sFailureModeFMEAEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSFailureModeFMEA_InitialOccurence() {
		return (EAttribute)sFailureModeFMEAEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSFailureModeFMEA_FinalOccurence() {
		return (EAttribute)sFailureModeFMEAEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSFailureModeFMEA_InitialSeverity() {
		return (EAttribute)sFailureModeFMEAEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSFailureModeFMEA_FinalSeverity() {
		return (EAttribute)sFailureModeFMEAEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSFailureModeFMEA_IsDetectable() {
		return (EAttribute)sFailureModeFMEAEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSFailureModeFMEA_InitialDetectability() {
		return (EAttribute)sFailureModeFMEAEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSFailureModeFMEA_Base_Property() {
		return (EReference)sFailureModeFMEAEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSFailureModeFMEA_SFailureMode() {
		return (EReference)sFailureModeFMEAEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSFailureModeFMEA_InitialPreventiveActions() {
		return (EAttribute)sFailureModeFMEAEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSFailureModeFMEA_FinalPreventiveActions() {
		return (EAttribute)sFailureModeFMEAEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSFailureModeFMEA_LocalEffects() {
		return (EAttribute)sFailureModeFMEAEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSFailureModeFMEA_Causes() {
		return (EAttribute)sFailureModeFMEAEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSFailureModeFMEA_FinalDetectability() {
		return (EAttribute)sFailureModeFMEAEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSFailureModeFMEA_SystemEffects() {
		return (EAttribute)sFailureModeFMEAEClass.getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSFailureModeFMEA_CustomerEffects() {
		return (EAttribute)sFailureModeFMEAEClass.getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSFailureModeFMEA_SBlockFMEA() {
		return (EReference)sFailureModeFMEAEClass.getEStructuralFeatures().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IESFFMEAFactory getESFFMEAFactory() {
		return (IESFFMEAFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		sfmeaEClass = createEClass(SFMEA);
		createEReference(sfmeaEClass, SFMEA__BASE_PACKAGE);
		createEReference(sfmeaEClass, SFMEA__SBLOCKS_FMEA_LIST);

		sBlockFMEAEClass = createEClass(SBLOCK_FMEA);
		createEReference(sBlockFMEAEClass, SBLOCK_FMEA__SBLOCK);
		createEAttribute(sBlockFMEAEClass, SBLOCK_FMEA__CRITICALITY);
		createEReference(sBlockFMEAEClass, SBLOCK_FMEA__BASE_CLASS);
		createEReference(sBlockFMEAEClass, SBLOCK_FMEA__SFAILURE_MODES_FMEA_LIST);

		abstractSFMEAElementEClass = createEClass(ABSTRACT_SFMEA_ELEMENT);

		sFailureModeFMEAEClass = createEClass(SFAILURE_MODE_FMEA);
		createEAttribute(sFailureModeFMEAEClass, SFAILURE_MODE_FMEA__REMARKS);
		createEAttribute(sFailureModeFMEAEClass, SFAILURE_MODE_FMEA__INITIAL_CRITICALITY);
		createEAttribute(sFailureModeFMEAEClass, SFAILURE_MODE_FMEA__FINAL_CRITICALITY);
		createEAttribute(sFailureModeFMEAEClass, SFAILURE_MODE_FMEA__INITIAL_OCCURENCE);
		createEAttribute(sFailureModeFMEAEClass, SFAILURE_MODE_FMEA__FINAL_OCCURENCE);
		createEAttribute(sFailureModeFMEAEClass, SFAILURE_MODE_FMEA__INITIAL_SEVERITY);
		createEAttribute(sFailureModeFMEAEClass, SFAILURE_MODE_FMEA__FINAL_SEVERITY);
		createEAttribute(sFailureModeFMEAEClass, SFAILURE_MODE_FMEA__IS_DETECTABLE);
		createEAttribute(sFailureModeFMEAEClass, SFAILURE_MODE_FMEA__INITIAL_DETECTABILITY);
		createEReference(sFailureModeFMEAEClass, SFAILURE_MODE_FMEA__BASE_PROPERTY);
		createEReference(sFailureModeFMEAEClass, SFAILURE_MODE_FMEA__SFAILURE_MODE);
		createEAttribute(sFailureModeFMEAEClass, SFAILURE_MODE_FMEA__INITIAL_PREVENTIVE_ACTIONS);
		createEAttribute(sFailureModeFMEAEClass, SFAILURE_MODE_FMEA__FINAL_PREVENTIVE_ACTIONS);
		createEAttribute(sFailureModeFMEAEClass, SFAILURE_MODE_FMEA__LOCAL_EFFECTS);
		createEAttribute(sFailureModeFMEAEClass, SFAILURE_MODE_FMEA__CAUSES);
		createEAttribute(sFailureModeFMEAEClass, SFAILURE_MODE_FMEA__FINAL_DETECTABILITY);
		createEAttribute(sFailureModeFMEAEClass, SFAILURE_MODE_FMEA__SYSTEM_EFFECTS);
		createEAttribute(sFailureModeFMEAEClass, SFAILURE_MODE_FMEA__CUSTOMER_EFFECTS);
		createEReference(sFailureModeFMEAEClass, SFAILURE_MODE_FMEA__SBLOCK_FMEA);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		IESFCorePackage theESFCorePackage = (IESFCorePackage)EPackage.Registry.INSTANCE.getEPackage(IESFCorePackage.eNS_URI);
		UMLPackage theUMLPackage = (UMLPackage)EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);
		IESFArchitectureConceptsPackage theESFArchitectureConceptsPackage = (IESFArchitectureConceptsPackage)EPackage.Registry.INSTANCE.getEPackage(IESFArchitectureConceptsPackage.eNS_URI);
		TypesPackage theTypesPackage = (TypesPackage)EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);
		ISDysfunctionsPackage theSDysfunctionsPackage = (ISDysfunctionsPackage)EPackage.Registry.INSTANCE.getEPackage(ISDysfunctionsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		sfmeaEClass.getESuperTypes().add(theESFCorePackage.getAbstractSSafetyAnalysis());
		sBlockFMEAEClass.getESuperTypes().add(this.getAbstractSFMEAElement());
		abstractSFMEAElementEClass.getESuperTypes().add(theESFCorePackage.getAbstractSSafetyConcept());
		sFailureModeFMEAEClass.getESuperTypes().add(this.getAbstractSFMEAElement());

		// Initialize classes, features, and operations; add parameters
		initEClass(sfmeaEClass, ISFMEA.class, "SFMEA", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSFMEA_Base_Package(), theUMLPackage.getPackage(), null, "base_Package", null, 1, 1, ISFMEA.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getSFMEA_SBlocksFMEAList(), this.getSBlockFMEA(), null, "sBlocksFMEAList", null, 0, -1, ISFMEA.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(sBlockFMEAEClass, ISBlockFMEA.class, "SBlockFMEA", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSBlockFMEA_SBlock(), theESFArchitectureConceptsPackage.getSBlock(), null, "sBlock", null, 1, 1, ISBlockFMEA.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getSBlockFMEA_Criticality(), theTypesPackage.getInteger(), "criticality", "0", 1, 1, ISBlockFMEA.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$ //$NON-NLS-2$
		initEReference(getSBlockFMEA_Base_Class(), theUMLPackage.getClass_(), null, "base_Class", null, 1, 1, ISBlockFMEA.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getSBlockFMEA_SFailureModesFMEAList(), this.getSFailureModeFMEA(), this.getSFailureModeFMEA_SBlockFMEA(), "sFailureModesFMEAList", null, 0, -1, ISBlockFMEA.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(abstractSFMEAElementEClass, IAbstractSFMEAElement.class, "AbstractSFMEAElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(sFailureModeFMEAEClass, ISFailureModeFMEA.class, "SFailureModeFMEA", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getSFailureModeFMEA_Remarks(), theTypesPackage.getString(), "remarks", null, 0, 1, ISFailureModeFMEA.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getSFailureModeFMEA_InitialCriticality(), theTypesPackage.getInteger(), "initialCriticality", "0", 1, 1, ISFailureModeFMEA.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$ //$NON-NLS-2$
		initEAttribute(getSFailureModeFMEA_FinalCriticality(), theTypesPackage.getInteger(), "finalCriticality", "0", 1, 1, ISFailureModeFMEA.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$ //$NON-NLS-2$
		initEAttribute(getSFailureModeFMEA_InitialOccurence(), theTypesPackage.getInteger(), "initialOccurence", "0", 1, 1, ISFailureModeFMEA.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$ //$NON-NLS-2$
		initEAttribute(getSFailureModeFMEA_FinalOccurence(), theTypesPackage.getInteger(), "finalOccurence", "0", 1, 1, ISFailureModeFMEA.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$ //$NON-NLS-2$
		initEAttribute(getSFailureModeFMEA_InitialSeverity(), theTypesPackage.getInteger(), "initialSeverity", "0", 1, 1, ISFailureModeFMEA.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$ //$NON-NLS-2$
		initEAttribute(getSFailureModeFMEA_FinalSeverity(), theTypesPackage.getInteger(), "finalSeverity", "0", 1, 1, ISFailureModeFMEA.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$ //$NON-NLS-2$
		initEAttribute(getSFailureModeFMEA_IsDetectable(), theTypesPackage.getBoolean(), "isDetectable", "false", 1, 1, ISFailureModeFMEA.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$ //$NON-NLS-2$
		initEAttribute(getSFailureModeFMEA_InitialDetectability(), theTypesPackage.getInteger(), "initialDetectability", "0", 1, 1, ISFailureModeFMEA.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$ //$NON-NLS-2$
		initEReference(getSFailureModeFMEA_Base_Property(), theUMLPackage.getProperty(), null, "base_Property", null, 1, 1, ISFailureModeFMEA.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getSFailureModeFMEA_SFailureMode(), theSDysfunctionsPackage.getSFailureMode(), null, "sFailureMode", null, 1, 1, ISFailureModeFMEA.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getSFailureModeFMEA_InitialPreventiveActions(), theTypesPackage.getString(), "initialPreventiveActions", null, 0, 1, ISFailureModeFMEA.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getSFailureModeFMEA_FinalPreventiveActions(), theTypesPackage.getString(), "finalPreventiveActions", null, 0, 1, ISFailureModeFMEA.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getSFailureModeFMEA_LocalEffects(), theTypesPackage.getString(), "localEffects", null, 0, 1, ISFailureModeFMEA.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getSFailureModeFMEA_Causes(), theTypesPackage.getString(), "causes", null, 0, 1, ISFailureModeFMEA.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getSFailureModeFMEA_FinalDetectability(), theTypesPackage.getInteger(), "finalDetectability", "0", 1, 1, ISFailureModeFMEA.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$ //$NON-NLS-2$
		initEAttribute(getSFailureModeFMEA_SystemEffects(), theTypesPackage.getString(), "systemEffects", null, 0, 1, ISFailureModeFMEA.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getSFailureModeFMEA_CustomerEffects(), theTypesPackage.getString(), "customerEffects", null, 0, 1, ISFailureModeFMEA.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getSFailureModeFMEA_SBlockFMEA(), this.getSBlockFMEA(), this.getSBlockFMEA_SFailureModesFMEAList(), "sBlockFMEA", null, 1, 1, ISFailureModeFMEA.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/uml2/2.0.0/UML
		createUMLAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/uml2/2.0.0/UML</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createUMLAnnotations() {
		String source = "http://www.eclipse.org/uml2/2.0.0/UML"; //$NON-NLS-1$	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "originalName", "ESFFMEA" //$NON-NLS-1$ //$NON-NLS-2$
		   });
	}

} //ESFFMEAPackage
