/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esffmea.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.uml2.uml.Property;

import org.polarsys.esf.esffmea.IESFFMEAPackage;
import org.polarsys.esf.esffmea.ISBlockFMEA;
import org.polarsys.esf.esffmea.ISFailureModeFMEA;

import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureMode;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SFailure Mode FMEA</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.polarsys.esf.esffmea.impl.SFailureModeFMEA#getRemarks <em>Remarks</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.impl.SFailureModeFMEA#getInitialCriticality <em>Initial Criticality</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.impl.SFailureModeFMEA#getFinalCriticality <em>Final Criticality</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.impl.SFailureModeFMEA#getInitialOccurence <em>Initial Occurence</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.impl.SFailureModeFMEA#getFinalOccurence <em>Final Occurence</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.impl.SFailureModeFMEA#getInitialSeverity <em>Initial Severity</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.impl.SFailureModeFMEA#getFinalSeverity <em>Final Severity</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.impl.SFailureModeFMEA#isDetectable <em>Is Detectable</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.impl.SFailureModeFMEA#getInitialDetectability <em>Initial Detectability</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.impl.SFailureModeFMEA#getBase_Property <em>Base Property</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.impl.SFailureModeFMEA#getSFailureMode <em>SFailure Mode</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.impl.SFailureModeFMEA#getInitialPreventiveActions <em>Initial Preventive Actions</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.impl.SFailureModeFMEA#getFinalPreventiveActions <em>Final Preventive Actions</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.impl.SFailureModeFMEA#getLocalEffects <em>Local Effects</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.impl.SFailureModeFMEA#getCauses <em>Causes</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.impl.SFailureModeFMEA#getFinalDetectability <em>Final Detectability</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.impl.SFailureModeFMEA#getSystemEffects <em>System Effects</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.impl.SFailureModeFMEA#getCustomerEffects <em>Customer Effects</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.impl.SFailureModeFMEA#getSBlockFMEA <em>SBlock FMEA</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SFailureModeFMEA extends AbstractSFMEAElement implements ISFailureModeFMEA {
	/**
	 * The default value of the '{@link #getRemarks() <em>Remarks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRemarks()
	 * @generated
	 * @ordered
	 */
	protected static final String REMARKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRemarks() <em>Remarks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRemarks()
	 * @generated
	 * @ordered
	 */
	protected String remarks = REMARKS_EDEFAULT;

	/**
	 * The default value of the '{@link #getInitialCriticality() <em>Initial Criticality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitialCriticality()
	 * @generated
	 * @ordered
	 */
	protected static final int INITIAL_CRITICALITY_EDEFAULT = 0;

	/**
	 * The default value of the '{@link #getFinalCriticality() <em>Final Criticality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFinalCriticality()
	 * @generated
	 * @ordered
	 */
	protected static final int FINAL_CRITICALITY_EDEFAULT = 0;

	/**
	 * The default value of the '{@link #getInitialOccurence() <em>Initial Occurence</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitialOccurence()
	 * @generated
	 * @ordered
	 */
	protected static final int INITIAL_OCCURENCE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getInitialOccurence() <em>Initial Occurence</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitialOccurence()
	 * @generated
	 * @ordered
	 */
	protected int initialOccurence = INITIAL_OCCURENCE_EDEFAULT;

	/**
	 * The default value of the '{@link #getFinalOccurence() <em>Final Occurence</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFinalOccurence()
	 * @generated
	 * @ordered
	 */
	protected static final int FINAL_OCCURENCE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getFinalOccurence() <em>Final Occurence</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFinalOccurence()
	 * @generated
	 * @ordered
	 */
	protected int finalOccurence = FINAL_OCCURENCE_EDEFAULT;

	/**
	 * The default value of the '{@link #getInitialSeverity() <em>Initial Severity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitialSeverity()
	 * @generated
	 * @ordered
	 */
	protected static final int INITIAL_SEVERITY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getInitialSeverity() <em>Initial Severity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitialSeverity()
	 * @generated
	 * @ordered
	 */
	protected int initialSeverity = INITIAL_SEVERITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getFinalSeverity() <em>Final Severity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFinalSeverity()
	 * @generated
	 * @ordered
	 */
	protected static final int FINAL_SEVERITY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getFinalSeverity() <em>Final Severity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFinalSeverity()
	 * @generated
	 * @ordered
	 */
	protected int finalSeverity = FINAL_SEVERITY_EDEFAULT;

	/**
	 * The default value of the '{@link #isDetectable() <em>Is Detectable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDetectable()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_DETECTABLE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isDetectable() <em>Is Detectable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDetectable()
	 * @generated
	 * @ordered
	 */
	protected boolean isDetectable = IS_DETECTABLE_EDEFAULT;

	/**
	 * The default value of the '{@link #getInitialDetectability() <em>Initial Detectability</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitialDetectability()
	 * @generated
	 * @ordered
	 */
	protected static final int INITIAL_DETECTABILITY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getInitialDetectability() <em>Initial Detectability</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitialDetectability()
	 * @generated
	 * @ordered
	 */
	protected int initialDetectability = INITIAL_DETECTABILITY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getBase_Property() <em>Base Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_Property()
	 * @generated
	 * @ordered
	 */
	protected Property base_Property;

	/**
	 * The cached value of the '{@link #getSFailureMode() <em>SFailure Mode</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSFailureMode()
	 * @generated
	 * @ordered
	 */
	protected ISFailureMode sFailureMode;

	/**
	 * The default value of the '{@link #getInitialPreventiveActions() <em>Initial Preventive Actions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitialPreventiveActions()
	 * @generated
	 * @ordered
	 */
	protected static final String INITIAL_PREVENTIVE_ACTIONS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInitialPreventiveActions() <em>Initial Preventive Actions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitialPreventiveActions()
	 * @generated
	 * @ordered
	 */
	protected String initialPreventiveActions = INITIAL_PREVENTIVE_ACTIONS_EDEFAULT;

	/**
	 * The default value of the '{@link #getFinalPreventiveActions() <em>Final Preventive Actions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFinalPreventiveActions()
	 * @generated
	 * @ordered
	 */
	protected static final String FINAL_PREVENTIVE_ACTIONS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFinalPreventiveActions() <em>Final Preventive Actions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFinalPreventiveActions()
	 * @generated
	 * @ordered
	 */
	protected String finalPreventiveActions = FINAL_PREVENTIVE_ACTIONS_EDEFAULT;

	/**
	 * The default value of the '{@link #getLocalEffects() <em>Local Effects</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalEffects()
	 * @generated
	 * @ordered
	 */
	protected static final String LOCAL_EFFECTS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLocalEffects() <em>Local Effects</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalEffects()
	 * @generated
	 * @ordered
	 */
	protected String localEffects = LOCAL_EFFECTS_EDEFAULT;

	/**
	 * The default value of the '{@link #getCauses() <em>Causes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCauses()
	 * @generated
	 * @ordered
	 */
	protected static final String CAUSES_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCauses() <em>Causes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCauses()
	 * @generated
	 * @ordered
	 */
	protected String causes = CAUSES_EDEFAULT;

	/**
	 * The default value of the '{@link #getFinalDetectability() <em>Final Detectability</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFinalDetectability()
	 * @generated
	 * @ordered
	 */
	protected static final int FINAL_DETECTABILITY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getFinalDetectability() <em>Final Detectability</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFinalDetectability()
	 * @generated
	 * @ordered
	 */
	protected int finalDetectability = FINAL_DETECTABILITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getSystemEffects() <em>System Effects</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSystemEffects()
	 * @generated
	 * @ordered
	 */
	protected static final String SYSTEM_EFFECTS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSystemEffects() <em>System Effects</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSystemEffects()
	 * @generated
	 * @ordered
	 */
	protected String systemEffects = SYSTEM_EFFECTS_EDEFAULT;

	/**
	 * The default value of the '{@link #getCustomerEffects() <em>Customer Effects</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCustomerEffects()
	 * @generated
	 * @ordered
	 */
	protected static final String CUSTOMER_EFFECTS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCustomerEffects() <em>Customer Effects</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCustomerEffects()
	 * @generated
	 * @ordered
	 */
	protected String customerEffects = CUSTOMER_EFFECTS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SFailureModeFMEA() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IESFFMEAPackage.Literals.SFAILURE_MODE_FMEA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRemarks(String newRemarks) {
		String oldRemarks = remarks;
		remarks = newRemarks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFFMEAPackage.SFAILURE_MODE_FMEA__REMARKS, oldRemarks, remarks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getInitialCriticality() {
		// TODO: implement this method to return the 'Initial Criticality' attribute
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitialCriticality(int newInitialCriticality) {
		// TODO: implement this method to set the 'Initial Criticality' attribute
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getFinalCriticality() {
		// TODO: implement this method to return the 'Final Criticality' attribute
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFinalCriticality(int newFinalCriticality) {
		// TODO: implement this method to set the 'Final Criticality' attribute
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getInitialOccurence() {
		return initialOccurence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitialOccurence(int newInitialOccurence) {
		int oldInitialOccurence = initialOccurence;
		initialOccurence = newInitialOccurence;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFFMEAPackage.SFAILURE_MODE_FMEA__INITIAL_OCCURENCE, oldInitialOccurence, initialOccurence));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getFinalOccurence() {
		return finalOccurence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFinalOccurence(int newFinalOccurence) {
		int oldFinalOccurence = finalOccurence;
		finalOccurence = newFinalOccurence;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFFMEAPackage.SFAILURE_MODE_FMEA__FINAL_OCCURENCE, oldFinalOccurence, finalOccurence));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getInitialSeverity() {
		return initialSeverity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitialSeverity(int newInitialSeverity) {
		int oldInitialSeverity = initialSeverity;
		initialSeverity = newInitialSeverity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFFMEAPackage.SFAILURE_MODE_FMEA__INITIAL_SEVERITY, oldInitialSeverity, initialSeverity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getFinalSeverity() {
		return finalSeverity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFinalSeverity(int newFinalSeverity) {
		int oldFinalSeverity = finalSeverity;
		finalSeverity = newFinalSeverity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFFMEAPackage.SFAILURE_MODE_FMEA__FINAL_SEVERITY, oldFinalSeverity, finalSeverity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isDetectable() {
		return isDetectable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsDetectable(boolean newIsDetectable) {
		boolean oldIsDetectable = isDetectable;
		isDetectable = newIsDetectable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFFMEAPackage.SFAILURE_MODE_FMEA__IS_DETECTABLE, oldIsDetectable, isDetectable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getInitialDetectability() {
		return initialDetectability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitialDetectability(int newInitialDetectability) {
		int oldInitialDetectability = initialDetectability;
		initialDetectability = newInitialDetectability;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFFMEAPackage.SFAILURE_MODE_FMEA__INITIAL_DETECTABILITY, oldInitialDetectability, initialDetectability));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Property getBase_Property() {
		if (base_Property != null && base_Property.eIsProxy()) {
			InternalEObject oldBase_Property = (InternalEObject)base_Property;
			base_Property = (Property)eResolveProxy(oldBase_Property);
			if (base_Property != oldBase_Property) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IESFFMEAPackage.SFAILURE_MODE_FMEA__BASE_PROPERTY, oldBase_Property, base_Property));
			}
		}
		return base_Property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Property basicGetBase_Property() {
		return base_Property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBase_Property(Property newBase_Property) {
		Property oldBase_Property = base_Property;
		base_Property = newBase_Property;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFFMEAPackage.SFAILURE_MODE_FMEA__BASE_PROPERTY, oldBase_Property, base_Property));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISFailureMode getSFailureMode() {
		if (sFailureMode != null && sFailureMode.eIsProxy()) {
			InternalEObject oldSFailureMode = (InternalEObject)sFailureMode;
			sFailureMode = (ISFailureMode)eResolveProxy(oldSFailureMode);
			if (sFailureMode != oldSFailureMode) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IESFFMEAPackage.SFAILURE_MODE_FMEA__SFAILURE_MODE, oldSFailureMode, sFailureMode));
			}
		}
		return sFailureMode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISFailureMode basicGetSFailureMode() {
		return sFailureMode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSFailureMode(ISFailureMode newSFailureMode) {
		ISFailureMode oldSFailureMode = sFailureMode;
		sFailureMode = newSFailureMode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFFMEAPackage.SFAILURE_MODE_FMEA__SFAILURE_MODE, oldSFailureMode, sFailureMode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getInitialPreventiveActions() {
		return initialPreventiveActions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitialPreventiveActions(String newInitialPreventiveActions) {
		String oldInitialPreventiveActions = initialPreventiveActions;
		initialPreventiveActions = newInitialPreventiveActions;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFFMEAPackage.SFAILURE_MODE_FMEA__INITIAL_PREVENTIVE_ACTIONS, oldInitialPreventiveActions, initialPreventiveActions));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFinalPreventiveActions() {
		return finalPreventiveActions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFinalPreventiveActions(String newFinalPreventiveActions) {
		String oldFinalPreventiveActions = finalPreventiveActions;
		finalPreventiveActions = newFinalPreventiveActions;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFFMEAPackage.SFAILURE_MODE_FMEA__FINAL_PREVENTIVE_ACTIONS, oldFinalPreventiveActions, finalPreventiveActions));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLocalEffects() {
		return localEffects;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocalEffects(String newLocalEffects) {
		String oldLocalEffects = localEffects;
		localEffects = newLocalEffects;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFFMEAPackage.SFAILURE_MODE_FMEA__LOCAL_EFFECTS, oldLocalEffects, localEffects));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCauses() {
		return causes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCauses(String newCauses) {
		String oldCauses = causes;
		causes = newCauses;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFFMEAPackage.SFAILURE_MODE_FMEA__CAUSES, oldCauses, causes));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getFinalDetectability() {
		return finalDetectability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFinalDetectability(int newFinalDetectability) {
		int oldFinalDetectability = finalDetectability;
		finalDetectability = newFinalDetectability;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFFMEAPackage.SFAILURE_MODE_FMEA__FINAL_DETECTABILITY, oldFinalDetectability, finalDetectability));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSystemEffects() {
		return systemEffects;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSystemEffects(String newSystemEffects) {
		String oldSystemEffects = systemEffects;
		systemEffects = newSystemEffects;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFFMEAPackage.SFAILURE_MODE_FMEA__SYSTEM_EFFECTS, oldSystemEffects, systemEffects));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCustomerEffects() {
		return customerEffects;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCustomerEffects(String newCustomerEffects) {
		String oldCustomerEffects = customerEffects;
		customerEffects = newCustomerEffects;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFFMEAPackage.SFAILURE_MODE_FMEA__CUSTOMER_EFFECTS, oldCustomerEffects, customerEffects));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISBlockFMEA getSBlockFMEA() {
		ISBlockFMEA sBlockFMEA = basicGetSBlockFMEA();
		return sBlockFMEA != null && sBlockFMEA.eIsProxy() ? (ISBlockFMEA)eResolveProxy((InternalEObject)sBlockFMEA) : sBlockFMEA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISBlockFMEA basicGetSBlockFMEA() {
		// TODO: implement this method to return the 'SBlock FMEA' reference
		// -> do not perform proxy resolution
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSBlockFMEA(ISBlockFMEA newSBlockFMEA) {
		// TODO: implement this method to set the 'SBlock FMEA' reference
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__REMARKS:
				return getRemarks();
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__INITIAL_CRITICALITY:
				return getInitialCriticality();
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__FINAL_CRITICALITY:
				return getFinalCriticality();
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__INITIAL_OCCURENCE:
				return getInitialOccurence();
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__FINAL_OCCURENCE:
				return getFinalOccurence();
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__INITIAL_SEVERITY:
				return getInitialSeverity();
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__FINAL_SEVERITY:
				return getFinalSeverity();
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__IS_DETECTABLE:
				return isDetectable();
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__INITIAL_DETECTABILITY:
				return getInitialDetectability();
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__BASE_PROPERTY:
				if (resolve) return getBase_Property();
				return basicGetBase_Property();
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__SFAILURE_MODE:
				if (resolve) return getSFailureMode();
				return basicGetSFailureMode();
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__INITIAL_PREVENTIVE_ACTIONS:
				return getInitialPreventiveActions();
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__FINAL_PREVENTIVE_ACTIONS:
				return getFinalPreventiveActions();
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__LOCAL_EFFECTS:
				return getLocalEffects();
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__CAUSES:
				return getCauses();
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__FINAL_DETECTABILITY:
				return getFinalDetectability();
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__SYSTEM_EFFECTS:
				return getSystemEffects();
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__CUSTOMER_EFFECTS:
				return getCustomerEffects();
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__SBLOCK_FMEA:
				if (resolve) return getSBlockFMEA();
				return basicGetSBlockFMEA();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__REMARKS:
				setRemarks((String)newValue);
				return;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__INITIAL_CRITICALITY:
				setInitialCriticality((Integer)newValue);
				return;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__FINAL_CRITICALITY:
				setFinalCriticality((Integer)newValue);
				return;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__INITIAL_OCCURENCE:
				setInitialOccurence((Integer)newValue);
				return;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__FINAL_OCCURENCE:
				setFinalOccurence((Integer)newValue);
				return;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__INITIAL_SEVERITY:
				setInitialSeverity((Integer)newValue);
				return;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__FINAL_SEVERITY:
				setFinalSeverity((Integer)newValue);
				return;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__IS_DETECTABLE:
				setIsDetectable((Boolean)newValue);
				return;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__INITIAL_DETECTABILITY:
				setInitialDetectability((Integer)newValue);
				return;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__BASE_PROPERTY:
				setBase_Property((Property)newValue);
				return;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__SFAILURE_MODE:
				setSFailureMode((ISFailureMode)newValue);
				return;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__INITIAL_PREVENTIVE_ACTIONS:
				setInitialPreventiveActions((String)newValue);
				return;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__FINAL_PREVENTIVE_ACTIONS:
				setFinalPreventiveActions((String)newValue);
				return;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__LOCAL_EFFECTS:
				setLocalEffects((String)newValue);
				return;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__CAUSES:
				setCauses((String)newValue);
				return;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__FINAL_DETECTABILITY:
				setFinalDetectability((Integer)newValue);
				return;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__SYSTEM_EFFECTS:
				setSystemEffects((String)newValue);
				return;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__CUSTOMER_EFFECTS:
				setCustomerEffects((String)newValue);
				return;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__SBLOCK_FMEA:
				setSBlockFMEA((ISBlockFMEA)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__REMARKS:
				setRemarks(REMARKS_EDEFAULT);
				return;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__INITIAL_CRITICALITY:
				setInitialCriticality(INITIAL_CRITICALITY_EDEFAULT);
				return;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__FINAL_CRITICALITY:
				setFinalCriticality(FINAL_CRITICALITY_EDEFAULT);
				return;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__INITIAL_OCCURENCE:
				setInitialOccurence(INITIAL_OCCURENCE_EDEFAULT);
				return;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__FINAL_OCCURENCE:
				setFinalOccurence(FINAL_OCCURENCE_EDEFAULT);
				return;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__INITIAL_SEVERITY:
				setInitialSeverity(INITIAL_SEVERITY_EDEFAULT);
				return;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__FINAL_SEVERITY:
				setFinalSeverity(FINAL_SEVERITY_EDEFAULT);
				return;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__IS_DETECTABLE:
				setIsDetectable(IS_DETECTABLE_EDEFAULT);
				return;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__INITIAL_DETECTABILITY:
				setInitialDetectability(INITIAL_DETECTABILITY_EDEFAULT);
				return;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__BASE_PROPERTY:
				setBase_Property((Property)null);
				return;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__SFAILURE_MODE:
				setSFailureMode((ISFailureMode)null);
				return;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__INITIAL_PREVENTIVE_ACTIONS:
				setInitialPreventiveActions(INITIAL_PREVENTIVE_ACTIONS_EDEFAULT);
				return;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__FINAL_PREVENTIVE_ACTIONS:
				setFinalPreventiveActions(FINAL_PREVENTIVE_ACTIONS_EDEFAULT);
				return;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__LOCAL_EFFECTS:
				setLocalEffects(LOCAL_EFFECTS_EDEFAULT);
				return;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__CAUSES:
				setCauses(CAUSES_EDEFAULT);
				return;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__FINAL_DETECTABILITY:
				setFinalDetectability(FINAL_DETECTABILITY_EDEFAULT);
				return;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__SYSTEM_EFFECTS:
				setSystemEffects(SYSTEM_EFFECTS_EDEFAULT);
				return;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__CUSTOMER_EFFECTS:
				setCustomerEffects(CUSTOMER_EFFECTS_EDEFAULT);
				return;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__SBLOCK_FMEA:
				setSBlockFMEA((ISBlockFMEA)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__REMARKS:
				return REMARKS_EDEFAULT == null ? remarks != null : !REMARKS_EDEFAULT.equals(remarks);
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__INITIAL_CRITICALITY:
				return getInitialCriticality() != INITIAL_CRITICALITY_EDEFAULT;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__FINAL_CRITICALITY:
				return getFinalCriticality() != FINAL_CRITICALITY_EDEFAULT;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__INITIAL_OCCURENCE:
				return initialOccurence != INITIAL_OCCURENCE_EDEFAULT;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__FINAL_OCCURENCE:
				return finalOccurence != FINAL_OCCURENCE_EDEFAULT;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__INITIAL_SEVERITY:
				return initialSeverity != INITIAL_SEVERITY_EDEFAULT;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__FINAL_SEVERITY:
				return finalSeverity != FINAL_SEVERITY_EDEFAULT;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__IS_DETECTABLE:
				return isDetectable != IS_DETECTABLE_EDEFAULT;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__INITIAL_DETECTABILITY:
				return initialDetectability != INITIAL_DETECTABILITY_EDEFAULT;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__BASE_PROPERTY:
				return base_Property != null;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__SFAILURE_MODE:
				return sFailureMode != null;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__INITIAL_PREVENTIVE_ACTIONS:
				return INITIAL_PREVENTIVE_ACTIONS_EDEFAULT == null ? initialPreventiveActions != null : !INITIAL_PREVENTIVE_ACTIONS_EDEFAULT.equals(initialPreventiveActions);
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__FINAL_PREVENTIVE_ACTIONS:
				return FINAL_PREVENTIVE_ACTIONS_EDEFAULT == null ? finalPreventiveActions != null : !FINAL_PREVENTIVE_ACTIONS_EDEFAULT.equals(finalPreventiveActions);
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__LOCAL_EFFECTS:
				return LOCAL_EFFECTS_EDEFAULT == null ? localEffects != null : !LOCAL_EFFECTS_EDEFAULT.equals(localEffects);
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__CAUSES:
				return CAUSES_EDEFAULT == null ? causes != null : !CAUSES_EDEFAULT.equals(causes);
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__FINAL_DETECTABILITY:
				return finalDetectability != FINAL_DETECTABILITY_EDEFAULT;
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__SYSTEM_EFFECTS:
				return SYSTEM_EFFECTS_EDEFAULT == null ? systemEffects != null : !SYSTEM_EFFECTS_EDEFAULT.equals(systemEffects);
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__CUSTOMER_EFFECTS:
				return CUSTOMER_EFFECTS_EDEFAULT == null ? customerEffects != null : !CUSTOMER_EFFECTS_EDEFAULT.equals(customerEffects);
			case IESFFMEAPackage.SFAILURE_MODE_FMEA__SBLOCK_FMEA:
				return basicGetSBlockFMEA() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (remarks: "); //$NON-NLS-1$
		result.append(remarks);
		result.append(", initialOccurence: "); //$NON-NLS-1$
		result.append(initialOccurence);
		result.append(", finalOccurence: "); //$NON-NLS-1$
		result.append(finalOccurence);
		result.append(", initialSeverity: "); //$NON-NLS-1$
		result.append(initialSeverity);
		result.append(", finalSeverity: "); //$NON-NLS-1$
		result.append(finalSeverity);
		result.append(", isDetectable: "); //$NON-NLS-1$
		result.append(isDetectable);
		result.append(", initialDetectability: "); //$NON-NLS-1$
		result.append(initialDetectability);
		result.append(", initialPreventiveActions: "); //$NON-NLS-1$
		result.append(initialPreventiveActions);
		result.append(", finalPreventiveActions: "); //$NON-NLS-1$
		result.append(finalPreventiveActions);
		result.append(", localEffects: "); //$NON-NLS-1$
		result.append(localEffects);
		result.append(", causes: "); //$NON-NLS-1$
		result.append(causes);
		result.append(", finalDetectability: "); //$NON-NLS-1$
		result.append(finalDetectability);
		result.append(", systemEffects: "); //$NON-NLS-1$
		result.append(systemEffects);
		result.append(", customerEffects: "); //$NON-NLS-1$
		result.append(customerEffects);
		result.append(')');
		return result.toString();
	}

} //SFailureModeFMEA
