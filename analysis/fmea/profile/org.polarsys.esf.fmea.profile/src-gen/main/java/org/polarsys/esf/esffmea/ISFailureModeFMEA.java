/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esffmea;

import org.eclipse.uml2.uml.Property;

import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureMode;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SFailure Mode FMEA</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getRemarks <em>Remarks</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getInitialCriticality <em>Initial Criticality</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getFinalCriticality <em>Final Criticality</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getInitialOccurence <em>Initial Occurence</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getFinalOccurence <em>Final Occurence</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getInitialSeverity <em>Initial Severity</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getFinalSeverity <em>Final Severity</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#isDetectable <em>Is Detectable</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getInitialDetectability <em>Initial Detectability</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getBase_Property <em>Base Property</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getSFailureMode <em>SFailure Mode</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getInitialPreventiveActions <em>Initial Preventive Actions</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getFinalPreventiveActions <em>Final Preventive Actions</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getLocalEffects <em>Local Effects</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getCauses <em>Causes</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getFinalDetectability <em>Final Detectability</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getSystemEffects <em>System Effects</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getCustomerEffects <em>Customer Effects</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getSBlockFMEA <em>SBlock FMEA</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esffmea.IESFFMEAPackage#getSFailureModeFMEA()
 * @model
 * @generated
 */
public interface ISFailureModeFMEA extends IAbstractSFMEAElement {
	/**
	 * Returns the value of the '<em><b>Remarks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Remarks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Remarks</em>' attribute.
	 * @see #setRemarks(String)
	 * @see org.polarsys.esf.esffmea.IESFFMEAPackage#getSFailureModeFMEA_Remarks()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getRemarks();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getRemarks <em>Remarks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Remarks</em>' attribute.
	 * @see #getRemarks()
	 * @generated
	 */
	void setRemarks(String value);

	/**
	 * Returns the value of the '<em><b>Initial Criticality</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initial Criticality</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial Criticality</em>' attribute.
	 * @see #setInitialCriticality(int)
	 * @see org.polarsys.esf.esffmea.IESFFMEAPackage#getSFailureModeFMEA_InitialCriticality()
	 * @model default="0" dataType="org.eclipse.uml2.types.Integer" required="true" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	int getInitialCriticality();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getInitialCriticality <em>Initial Criticality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initial Criticality</em>' attribute.
	 * @see #getInitialCriticality()
	 * @generated
	 */
	void setInitialCriticality(int value);

	/**
	 * Returns the value of the '<em><b>Final Criticality</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Final Criticality</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Final Criticality</em>' attribute.
	 * @see #setFinalCriticality(int)
	 * @see org.polarsys.esf.esffmea.IESFFMEAPackage#getSFailureModeFMEA_FinalCriticality()
	 * @model default="0" dataType="org.eclipse.uml2.types.Integer" required="true" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	int getFinalCriticality();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getFinalCriticality <em>Final Criticality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Final Criticality</em>' attribute.
	 * @see #getFinalCriticality()
	 * @generated
	 */
	void setFinalCriticality(int value);

	/**
	 * Returns the value of the '<em><b>Initial Occurence</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initial Occurence</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial Occurence</em>' attribute.
	 * @see #setInitialOccurence(int)
	 * @see org.polarsys.esf.esffmea.IESFFMEAPackage#getSFailureModeFMEA_InitialOccurence()
	 * @model default="0" dataType="org.eclipse.uml2.types.Integer" required="true" ordered="false"
	 * @generated
	 */
	int getInitialOccurence();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getInitialOccurence <em>Initial Occurence</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initial Occurence</em>' attribute.
	 * @see #getInitialOccurence()
	 * @generated
	 */
	void setInitialOccurence(int value);

	/**
	 * Returns the value of the '<em><b>Final Occurence</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Final Occurence</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Final Occurence</em>' attribute.
	 * @see #setFinalOccurence(int)
	 * @see org.polarsys.esf.esffmea.IESFFMEAPackage#getSFailureModeFMEA_FinalOccurence()
	 * @model default="0" dataType="org.eclipse.uml2.types.Integer" required="true" ordered="false"
	 * @generated
	 */
	int getFinalOccurence();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getFinalOccurence <em>Final Occurence</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Final Occurence</em>' attribute.
	 * @see #getFinalOccurence()
	 * @generated
	 */
	void setFinalOccurence(int value);

	/**
	 * Returns the value of the '<em><b>Initial Severity</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initial Severity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial Severity</em>' attribute.
	 * @see #setInitialSeverity(int)
	 * @see org.polarsys.esf.esffmea.IESFFMEAPackage#getSFailureModeFMEA_InitialSeverity()
	 * @model default="0" dataType="org.eclipse.uml2.types.Integer" required="true" ordered="false"
	 * @generated
	 */
	int getInitialSeverity();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getInitialSeverity <em>Initial Severity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initial Severity</em>' attribute.
	 * @see #getInitialSeverity()
	 * @generated
	 */
	void setInitialSeverity(int value);

	/**
	 * Returns the value of the '<em><b>Final Severity</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Final Severity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Final Severity</em>' attribute.
	 * @see #setFinalSeverity(int)
	 * @see org.polarsys.esf.esffmea.IESFFMEAPackage#getSFailureModeFMEA_FinalSeverity()
	 * @model default="0" dataType="org.eclipse.uml2.types.Integer" required="true" ordered="false"
	 * @generated
	 */
	int getFinalSeverity();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getFinalSeverity <em>Final Severity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Final Severity</em>' attribute.
	 * @see #getFinalSeverity()
	 * @generated
	 */
	void setFinalSeverity(int value);

	/**
	 * Returns the value of the '<em><b>Is Detectable</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Detectable</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Detectable</em>' attribute.
	 * @see #setIsDetectable(boolean)
	 * @see org.polarsys.esf.esffmea.IESFFMEAPackage#getSFailureModeFMEA_IsDetectable()
	 * @model default="false" dataType="org.eclipse.uml2.types.Boolean" required="true" ordered="false"
	 * @generated
	 */
	boolean isDetectable();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#isDetectable <em>Is Detectable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Detectable</em>' attribute.
	 * @see #isDetectable()
	 * @generated
	 */
	void setIsDetectable(boolean value);

	/**
	 * Returns the value of the '<em><b>Initial Detectability</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initial Detectability</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial Detectability</em>' attribute.
	 * @see #setInitialDetectability(int)
	 * @see org.polarsys.esf.esffmea.IESFFMEAPackage#getSFailureModeFMEA_InitialDetectability()
	 * @model default="0" dataType="org.eclipse.uml2.types.Integer" required="true" ordered="false"
	 * @generated
	 */
	int getInitialDetectability();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getInitialDetectability <em>Initial Detectability</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initial Detectability</em>' attribute.
	 * @see #getInitialDetectability()
	 * @generated
	 */
	void setInitialDetectability(int value);

	/**
	 * Returns the value of the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Property</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Property</em>' reference.
	 * @see #setBase_Property(Property)
	 * @see org.polarsys.esf.esffmea.IESFFMEAPackage#getSFailureModeFMEA_Base_Property()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Property getBase_Property();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getBase_Property <em>Base Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Property</em>' reference.
	 * @see #getBase_Property()
	 * @generated
	 */
	void setBase_Property(Property value);

	/**
	 * Returns the value of the '<em><b>SFailure Mode</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SFailure Mode</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SFailure Mode</em>' reference.
	 * @see #setSFailureMode(ISFailureMode)
	 * @see org.polarsys.esf.esffmea.IESFFMEAPackage#getSFailureModeFMEA_SFailureMode()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	ISFailureMode getSFailureMode();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getSFailureMode <em>SFailure Mode</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SFailure Mode</em>' reference.
	 * @see #getSFailureMode()
	 * @generated
	 */
	void setSFailureMode(ISFailureMode value);

	/**
	 * Returns the value of the '<em><b>Initial Preventive Actions</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initial Preventive Actions</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial Preventive Actions</em>' attribute.
	 * @see #setInitialPreventiveActions(String)
	 * @see org.polarsys.esf.esffmea.IESFFMEAPackage#getSFailureModeFMEA_InitialPreventiveActions()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getInitialPreventiveActions();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getInitialPreventiveActions <em>Initial Preventive Actions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initial Preventive Actions</em>' attribute.
	 * @see #getInitialPreventiveActions()
	 * @generated
	 */
	void setInitialPreventiveActions(String value);

	/**
	 * Returns the value of the '<em><b>Final Preventive Actions</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Final Preventive Actions</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Final Preventive Actions</em>' attribute.
	 * @see #setFinalPreventiveActions(String)
	 * @see org.polarsys.esf.esffmea.IESFFMEAPackage#getSFailureModeFMEA_FinalPreventiveActions()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getFinalPreventiveActions();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getFinalPreventiveActions <em>Final Preventive Actions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Final Preventive Actions</em>' attribute.
	 * @see #getFinalPreventiveActions()
	 * @generated
	 */
	void setFinalPreventiveActions(String value);

	/**
	 * Returns the value of the '<em><b>Local Effects</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Effects</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Effects</em>' attribute.
	 * @see #setLocalEffects(String)
	 * @see org.polarsys.esf.esffmea.IESFFMEAPackage#getSFailureModeFMEA_LocalEffects()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getLocalEffects();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getLocalEffects <em>Local Effects</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Local Effects</em>' attribute.
	 * @see #getLocalEffects()
	 * @generated
	 */
	void setLocalEffects(String value);

	/**
	 * Returns the value of the '<em><b>Causes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Causes</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Causes</em>' attribute.
	 * @see #setCauses(String)
	 * @see org.polarsys.esf.esffmea.IESFFMEAPackage#getSFailureModeFMEA_Causes()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getCauses();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getCauses <em>Causes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Causes</em>' attribute.
	 * @see #getCauses()
	 * @generated
	 */
	void setCauses(String value);

	/**
	 * Returns the value of the '<em><b>Final Detectability</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Final Detectability</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Final Detectability</em>' attribute.
	 * @see #setFinalDetectability(int)
	 * @see org.polarsys.esf.esffmea.IESFFMEAPackage#getSFailureModeFMEA_FinalDetectability()
	 * @model default="0" dataType="org.eclipse.uml2.types.Integer" required="true" ordered="false"
	 * @generated
	 */
	int getFinalDetectability();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getFinalDetectability <em>Final Detectability</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Final Detectability</em>' attribute.
	 * @see #getFinalDetectability()
	 * @generated
	 */
	void setFinalDetectability(int value);

	/**
	 * Returns the value of the '<em><b>System Effects</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>System Effects</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>System Effects</em>' attribute.
	 * @see #setSystemEffects(String)
	 * @see org.polarsys.esf.esffmea.IESFFMEAPackage#getSFailureModeFMEA_SystemEffects()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getSystemEffects();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getSystemEffects <em>System Effects</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>System Effects</em>' attribute.
	 * @see #getSystemEffects()
	 * @generated
	 */
	void setSystemEffects(String value);

	/**
	 * Returns the value of the '<em><b>Customer Effects</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Customer Effects</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Customer Effects</em>' attribute.
	 * @see #setCustomerEffects(String)
	 * @see org.polarsys.esf.esffmea.IESFFMEAPackage#getSFailureModeFMEA_CustomerEffects()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getCustomerEffects();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getCustomerEffects <em>Customer Effects</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Customer Effects</em>' attribute.
	 * @see #getCustomerEffects()
	 * @generated
	 */
	void setCustomerEffects(String value);

	/**
	 * Returns the value of the '<em><b>SBlock FMEA</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esffmea.ISBlockFMEA#getSFailureModesFMEAList <em>SFailure Modes FMEA List</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SBlock FMEA</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SBlock FMEA</em>' reference.
	 * @see #setSBlockFMEA(ISBlockFMEA)
	 * @see org.polarsys.esf.esffmea.IESFFMEAPackage#getSFailureModeFMEA_SBlockFMEA()
	 * @see org.polarsys.esf.esffmea.ISBlockFMEA#getSFailureModesFMEAList
	 * @model opposite="sFailureModesFMEAList" required="true" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	ISBlockFMEA getSBlockFMEA();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getSBlockFMEA <em>SBlock FMEA</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SBlock FMEA</em>' reference.
	 * @see #getSBlockFMEA()
	 * @generated
	 */
	void setSBlockFMEA(ISBlockFMEA value);

} // ISFailureModeFMEA
