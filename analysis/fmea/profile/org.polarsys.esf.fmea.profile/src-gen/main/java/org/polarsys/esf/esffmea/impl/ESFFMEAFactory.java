/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esffmea.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.polarsys.esf.esffmea.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ESFFMEAFactory extends EFactoryImpl implements IESFFMEAFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static IESFFMEAFactory init() {
		try {
			IESFFMEAFactory theESFFMEAFactory = (IESFFMEAFactory)EPackage.Registry.INSTANCE.getEFactory(IESFFMEAPackage.eNS_URI);
			if (theESFFMEAFactory != null) {
				return theESFFMEAFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ESFFMEAFactory();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ESFFMEAFactory() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case IESFFMEAPackage.SFMEA: return createSFMEA();
			case IESFFMEAPackage.SBLOCK_FMEA: return createSBlockFMEA();
			case IESFFMEAPackage.ABSTRACT_SFMEA_ELEMENT: return createAbstractSFMEAElement();
			case IESFFMEAPackage.SFAILURE_MODE_FMEA: return createSFailureModeFMEA();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier"); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISFMEA createSFMEA() {
		SFMEA sfmea = new SFMEA();
		return sfmea;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISBlockFMEA createSBlockFMEA() {
		SBlockFMEA sBlockFMEA = new SBlockFMEA();
		return sBlockFMEA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IAbstractSFMEAElement createAbstractSFMEAElement() {
		AbstractSFMEAElement abstractSFMEAElement = new AbstractSFMEAElement();
		return abstractSFMEAElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISFailureModeFMEA createSFailureModeFMEA() {
		SFailureModeFMEA sFailureModeFMEA = new SFailureModeFMEA();
		return sFailureModeFMEA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IESFFMEAPackage getESFFMEAPackage() {
		return (IESFFMEAPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static IESFFMEAPackage getPackage() {
		return IESFFMEAPackage.eINSTANCE;
	}

} //ESFFMEAFactory
