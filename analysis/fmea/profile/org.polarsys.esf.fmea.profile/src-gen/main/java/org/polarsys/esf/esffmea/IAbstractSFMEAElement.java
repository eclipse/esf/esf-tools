/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esffmea;

import org.polarsys.esf.esfcore.IAbstractSSafetyConcept;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract SFMEA Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.polarsys.esf.esffmea.IESFFMEAPackage#getAbstractSFMEAElement()
 * @model
 * @generated
 */
public interface IAbstractSFMEAElement extends IAbstractSSafetyConcept {
} // IAbstractSFMEAElement
