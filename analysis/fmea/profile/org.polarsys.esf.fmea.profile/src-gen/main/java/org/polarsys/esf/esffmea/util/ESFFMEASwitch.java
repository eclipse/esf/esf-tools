/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esffmea.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.polarsys.esf.esfcore.IAbstractSElement;
import org.polarsys.esf.esfcore.IAbstractSSafetyAnalysis;
import org.polarsys.esf.esfcore.IAbstractSSafetyConcept;

import org.polarsys.esf.esffmea.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.polarsys.esf.esffmea.IESFFMEAPackage
 * @generated
 */
public class ESFFMEASwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static IESFFMEAPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ESFFMEASwitch() {
		if (modelPackage == null) {
			modelPackage = IESFFMEAPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case IESFFMEAPackage.SFMEA: {
				ISFMEA sfmea = (ISFMEA)theEObject;
				T result = caseSFMEA(sfmea);
				if (result == null) result = caseAbstractSSafetyAnalysis(sfmea);
				if (result == null) result = caseAbstractSElement(sfmea);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case IESFFMEAPackage.SBLOCK_FMEA: {
				ISBlockFMEA sBlockFMEA = (ISBlockFMEA)theEObject;
				T result = caseSBlockFMEA(sBlockFMEA);
				if (result == null) result = caseAbstractSFMEAElement(sBlockFMEA);
				if (result == null) result = caseAbstractSSafetyConcept(sBlockFMEA);
				if (result == null) result = caseAbstractSElement(sBlockFMEA);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case IESFFMEAPackage.ABSTRACT_SFMEA_ELEMENT: {
				IAbstractSFMEAElement abstractSFMEAElement = (IAbstractSFMEAElement)theEObject;
				T result = caseAbstractSFMEAElement(abstractSFMEAElement);
				if (result == null) result = caseAbstractSSafetyConcept(abstractSFMEAElement);
				if (result == null) result = caseAbstractSElement(abstractSFMEAElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case IESFFMEAPackage.SFAILURE_MODE_FMEA: {
				ISFailureModeFMEA sFailureModeFMEA = (ISFailureModeFMEA)theEObject;
				T result = caseSFailureModeFMEA(sFailureModeFMEA);
				if (result == null) result = caseAbstractSFMEAElement(sFailureModeFMEA);
				if (result == null) result = caseAbstractSSafetyConcept(sFailureModeFMEA);
				if (result == null) result = caseAbstractSElement(sFailureModeFMEA);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SFMEA</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SFMEA</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSFMEA(ISFMEA object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SBlock FMEA</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SBlock FMEA</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSBlockFMEA(ISBlockFMEA object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract SFMEA Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract SFMEA Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractSFMEAElement(IAbstractSFMEAElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SFailure Mode FMEA</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SFailure Mode FMEA</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSFailureModeFMEA(ISFailureModeFMEA object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract SElement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract SElement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractSElement(IAbstractSElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract SSafety Analysis</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract SSafety Analysis</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractSSafetyAnalysis(IAbstractSSafetyAnalysis object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract SSafety Concept</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract SSafety Concept</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractSSafetyConcept(IAbstractSSafetyConcept object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //ESFFMEASwitch
