/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esffmea;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.polarsys.esf.esfcore.IESFCorePackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.polarsys.esf.esffmea.IESFFMEAFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='ESFFMEA'"
 * @generated
 */
public interface IESFFMEAPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "esffmea"; //$NON-NLS-1$

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.polarsys.org/esf/0.7.0/ESFFMEA"; //$NON-NLS-1$

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ESFFMEA"; //$NON-NLS-1$

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	IESFFMEAPackage eINSTANCE = org.polarsys.esf.esffmea.impl.ESFFMEAPackage.init();

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esffmea.impl.SFMEA <em>SFMEA</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polarsys.esf.esffmea.impl.SFMEA
	 * @see org.polarsys.esf.esffmea.impl.ESFFMEAPackage#getSFMEA()
	 * @generated
	 */
	int SFMEA = 0;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SFMEA__UUID = IESFCorePackage.ABSTRACT_SSAFETY_ANALYSIS__UUID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SFMEA__NAME = IESFCorePackage.ABSTRACT_SSAFETY_ANALYSIS__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SFMEA__DESCRIPTION = IESFCorePackage.ABSTRACT_SSAFETY_ANALYSIS__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>SSafety Concepts List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SFMEA__SSAFETY_CONCEPTS_LIST = IESFCorePackage.ABSTRACT_SSAFETY_ANALYSIS__SSAFETY_CONCEPTS_LIST;

	/**
	 * The feature id for the '<em><b>Base Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SFMEA__BASE_PACKAGE = IESFCorePackage.ABSTRACT_SSAFETY_ANALYSIS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>SBlocks FMEA List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SFMEA__SBLOCKS_FMEA_LIST = IESFCorePackage.ABSTRACT_SSAFETY_ANALYSIS_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>SFMEA</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SFMEA_FEATURE_COUNT = IESFCorePackage.ABSTRACT_SSAFETY_ANALYSIS_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>SFMEA</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SFMEA_OPERATION_COUNT = IESFCorePackage.ABSTRACT_SSAFETY_ANALYSIS_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esffmea.impl.AbstractSFMEAElement <em>Abstract SFMEA Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polarsys.esf.esffmea.impl.AbstractSFMEAElement
	 * @see org.polarsys.esf.esffmea.impl.ESFFMEAPackage#getAbstractSFMEAElement()
	 * @generated
	 */
	int ABSTRACT_SFMEA_ELEMENT = 2;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SFMEA_ELEMENT__UUID = IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT__UUID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SFMEA_ELEMENT__NAME = IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SFMEA_ELEMENT__DESCRIPTION = IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SFMEA_ELEMENT__SARCHITECTURE_ELEMENTS_LIST = IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT__SARCHITECTURE_ELEMENTS_LIST;

	/**
	 * The number of structural features of the '<em>Abstract SFMEA Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SFMEA_ELEMENT_FEATURE_COUNT = IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Abstract SFMEA Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SFMEA_ELEMENT_OPERATION_COUNT = IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esffmea.impl.SBlockFMEA <em>SBlock FMEA</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polarsys.esf.esffmea.impl.SBlockFMEA
	 * @see org.polarsys.esf.esffmea.impl.ESFFMEAPackage#getSBlockFMEA()
	 * @generated
	 */
	int SBLOCK_FMEA = 1;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SBLOCK_FMEA__UUID = ABSTRACT_SFMEA_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SBLOCK_FMEA__NAME = ABSTRACT_SFMEA_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SBLOCK_FMEA__DESCRIPTION = ABSTRACT_SFMEA_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SBLOCK_FMEA__SARCHITECTURE_ELEMENTS_LIST = ABSTRACT_SFMEA_ELEMENT__SARCHITECTURE_ELEMENTS_LIST;

	/**
	 * The feature id for the '<em><b>SBlock</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SBLOCK_FMEA__SBLOCK = ABSTRACT_SFMEA_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Criticality</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SBLOCK_FMEA__CRITICALITY = ABSTRACT_SFMEA_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SBLOCK_FMEA__BASE_CLASS = ABSTRACT_SFMEA_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>SFailure Modes FMEA List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SBLOCK_FMEA__SFAILURE_MODES_FMEA_LIST = ABSTRACT_SFMEA_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>SBlock FMEA</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SBLOCK_FMEA_FEATURE_COUNT = ABSTRACT_SFMEA_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>SBlock FMEA</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SBLOCK_FMEA_OPERATION_COUNT = ABSTRACT_SFMEA_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esffmea.impl.SFailureModeFMEA <em>SFailure Mode FMEA</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polarsys.esf.esffmea.impl.SFailureModeFMEA
	 * @see org.polarsys.esf.esffmea.impl.ESFFMEAPackage#getSFailureModeFMEA()
	 * @generated
	 */
	int SFAILURE_MODE_FMEA = 3;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SFAILURE_MODE_FMEA__UUID = ABSTRACT_SFMEA_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SFAILURE_MODE_FMEA__NAME = ABSTRACT_SFMEA_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SFAILURE_MODE_FMEA__DESCRIPTION = ABSTRACT_SFMEA_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SFAILURE_MODE_FMEA__SARCHITECTURE_ELEMENTS_LIST = ABSTRACT_SFMEA_ELEMENT__SARCHITECTURE_ELEMENTS_LIST;

	/**
	 * The feature id for the '<em><b>Remarks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SFAILURE_MODE_FMEA__REMARKS = ABSTRACT_SFMEA_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Initial Criticality</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SFAILURE_MODE_FMEA__INITIAL_CRITICALITY = ABSTRACT_SFMEA_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Final Criticality</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SFAILURE_MODE_FMEA__FINAL_CRITICALITY = ABSTRACT_SFMEA_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Initial Occurence</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SFAILURE_MODE_FMEA__INITIAL_OCCURENCE = ABSTRACT_SFMEA_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Final Occurence</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SFAILURE_MODE_FMEA__FINAL_OCCURENCE = ABSTRACT_SFMEA_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Initial Severity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SFAILURE_MODE_FMEA__INITIAL_SEVERITY = ABSTRACT_SFMEA_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Final Severity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SFAILURE_MODE_FMEA__FINAL_SEVERITY = ABSTRACT_SFMEA_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Is Detectable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SFAILURE_MODE_FMEA__IS_DETECTABLE = ABSTRACT_SFMEA_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Initial Detectability</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SFAILURE_MODE_FMEA__INITIAL_DETECTABILITY = ABSTRACT_SFMEA_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SFAILURE_MODE_FMEA__BASE_PROPERTY = ABSTRACT_SFMEA_ELEMENT_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>SFailure Mode</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SFAILURE_MODE_FMEA__SFAILURE_MODE = ABSTRACT_SFMEA_ELEMENT_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Initial Preventive Actions</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SFAILURE_MODE_FMEA__INITIAL_PREVENTIVE_ACTIONS = ABSTRACT_SFMEA_ELEMENT_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Final Preventive Actions</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SFAILURE_MODE_FMEA__FINAL_PREVENTIVE_ACTIONS = ABSTRACT_SFMEA_ELEMENT_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Local Effects</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SFAILURE_MODE_FMEA__LOCAL_EFFECTS = ABSTRACT_SFMEA_ELEMENT_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Causes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SFAILURE_MODE_FMEA__CAUSES = ABSTRACT_SFMEA_ELEMENT_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Final Detectability</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SFAILURE_MODE_FMEA__FINAL_DETECTABILITY = ABSTRACT_SFMEA_ELEMENT_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>System Effects</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SFAILURE_MODE_FMEA__SYSTEM_EFFECTS = ABSTRACT_SFMEA_ELEMENT_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>Customer Effects</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SFAILURE_MODE_FMEA__CUSTOMER_EFFECTS = ABSTRACT_SFMEA_ELEMENT_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>SBlock FMEA</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SFAILURE_MODE_FMEA__SBLOCK_FMEA = ABSTRACT_SFMEA_ELEMENT_FEATURE_COUNT + 18;

	/**
	 * The number of structural features of the '<em>SFailure Mode FMEA</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SFAILURE_MODE_FMEA_FEATURE_COUNT = ABSTRACT_SFMEA_ELEMENT_FEATURE_COUNT + 19;

	/**
	 * The number of operations of the '<em>SFailure Mode FMEA</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SFAILURE_MODE_FMEA_OPERATION_COUNT = ABSTRACT_SFMEA_ELEMENT_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.esffmea.ISFMEA <em>SFMEA</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SFMEA</em>'.
	 * @see org.polarsys.esf.esffmea.ISFMEA
	 * @generated
	 */
	EClass getSFMEA();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esffmea.ISFMEA#getBase_Package <em>Base Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Package</em>'.
	 * @see org.polarsys.esf.esffmea.ISFMEA#getBase_Package()
	 * @see #getSFMEA()
	 * @generated
	 */
	EReference getSFMEA_Base_Package();

	/**
	 * Returns the meta object for the reference list '{@link org.polarsys.esf.esffmea.ISFMEA#getSBlocksFMEAList <em>SBlocks FMEA List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>SBlocks FMEA List</em>'.
	 * @see org.polarsys.esf.esffmea.ISFMEA#getSBlocksFMEAList()
	 * @see #getSFMEA()
	 * @generated
	 */
	EReference getSFMEA_SBlocksFMEAList();

	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.esffmea.ISBlockFMEA <em>SBlock FMEA</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SBlock FMEA</em>'.
	 * @see org.polarsys.esf.esffmea.ISBlockFMEA
	 * @generated
	 */
	EClass getSBlockFMEA();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esffmea.ISBlockFMEA#getSBlock <em>SBlock</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>SBlock</em>'.
	 * @see org.polarsys.esf.esffmea.ISBlockFMEA#getSBlock()
	 * @see #getSBlockFMEA()
	 * @generated
	 */
	EReference getSBlockFMEA_SBlock();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.esffmea.ISBlockFMEA#getCriticality <em>Criticality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Criticality</em>'.
	 * @see org.polarsys.esf.esffmea.ISBlockFMEA#getCriticality()
	 * @see #getSBlockFMEA()
	 * @generated
	 */
	EAttribute getSBlockFMEA_Criticality();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esffmea.ISBlockFMEA#getBase_Class <em>Base Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Class</em>'.
	 * @see org.polarsys.esf.esffmea.ISBlockFMEA#getBase_Class()
	 * @see #getSBlockFMEA()
	 * @generated
	 */
	EReference getSBlockFMEA_Base_Class();

	/**
	 * Returns the meta object for the reference list '{@link org.polarsys.esf.esffmea.ISBlockFMEA#getSFailureModesFMEAList <em>SFailure Modes FMEA List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>SFailure Modes FMEA List</em>'.
	 * @see org.polarsys.esf.esffmea.ISBlockFMEA#getSFailureModesFMEAList()
	 * @see #getSBlockFMEA()
	 * @generated
	 */
	EReference getSBlockFMEA_SFailureModesFMEAList();

	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.esffmea.IAbstractSFMEAElement <em>Abstract SFMEA Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract SFMEA Element</em>'.
	 * @see org.polarsys.esf.esffmea.IAbstractSFMEAElement
	 * @generated
	 */
	EClass getAbstractSFMEAElement();

	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA <em>SFailure Mode FMEA</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SFailure Mode FMEA</em>'.
	 * @see org.polarsys.esf.esffmea.ISFailureModeFMEA
	 * @generated
	 */
	EClass getSFailureModeFMEA();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getRemarks <em>Remarks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Remarks</em>'.
	 * @see org.polarsys.esf.esffmea.ISFailureModeFMEA#getRemarks()
	 * @see #getSFailureModeFMEA()
	 * @generated
	 */
	EAttribute getSFailureModeFMEA_Remarks();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getInitialCriticality <em>Initial Criticality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Initial Criticality</em>'.
	 * @see org.polarsys.esf.esffmea.ISFailureModeFMEA#getInitialCriticality()
	 * @see #getSFailureModeFMEA()
	 * @generated
	 */
	EAttribute getSFailureModeFMEA_InitialCriticality();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getFinalCriticality <em>Final Criticality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Final Criticality</em>'.
	 * @see org.polarsys.esf.esffmea.ISFailureModeFMEA#getFinalCriticality()
	 * @see #getSFailureModeFMEA()
	 * @generated
	 */
	EAttribute getSFailureModeFMEA_FinalCriticality();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getInitialOccurence <em>Initial Occurence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Initial Occurence</em>'.
	 * @see org.polarsys.esf.esffmea.ISFailureModeFMEA#getInitialOccurence()
	 * @see #getSFailureModeFMEA()
	 * @generated
	 */
	EAttribute getSFailureModeFMEA_InitialOccurence();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getFinalOccurence <em>Final Occurence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Final Occurence</em>'.
	 * @see org.polarsys.esf.esffmea.ISFailureModeFMEA#getFinalOccurence()
	 * @see #getSFailureModeFMEA()
	 * @generated
	 */
	EAttribute getSFailureModeFMEA_FinalOccurence();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getInitialSeverity <em>Initial Severity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Initial Severity</em>'.
	 * @see org.polarsys.esf.esffmea.ISFailureModeFMEA#getInitialSeverity()
	 * @see #getSFailureModeFMEA()
	 * @generated
	 */
	EAttribute getSFailureModeFMEA_InitialSeverity();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getFinalSeverity <em>Final Severity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Final Severity</em>'.
	 * @see org.polarsys.esf.esffmea.ISFailureModeFMEA#getFinalSeverity()
	 * @see #getSFailureModeFMEA()
	 * @generated
	 */
	EAttribute getSFailureModeFMEA_FinalSeverity();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#isDetectable <em>Is Detectable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Detectable</em>'.
	 * @see org.polarsys.esf.esffmea.ISFailureModeFMEA#isDetectable()
	 * @see #getSFailureModeFMEA()
	 * @generated
	 */
	EAttribute getSFailureModeFMEA_IsDetectable();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getInitialDetectability <em>Initial Detectability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Initial Detectability</em>'.
	 * @see org.polarsys.esf.esffmea.ISFailureModeFMEA#getInitialDetectability()
	 * @see #getSFailureModeFMEA()
	 * @generated
	 */
	EAttribute getSFailureModeFMEA_InitialDetectability();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getBase_Property <em>Base Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Property</em>'.
	 * @see org.polarsys.esf.esffmea.ISFailureModeFMEA#getBase_Property()
	 * @see #getSFailureModeFMEA()
	 * @generated
	 */
	EReference getSFailureModeFMEA_Base_Property();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getSFailureMode <em>SFailure Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>SFailure Mode</em>'.
	 * @see org.polarsys.esf.esffmea.ISFailureModeFMEA#getSFailureMode()
	 * @see #getSFailureModeFMEA()
	 * @generated
	 */
	EReference getSFailureModeFMEA_SFailureMode();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getInitialPreventiveActions <em>Initial Preventive Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Initial Preventive Actions</em>'.
	 * @see org.polarsys.esf.esffmea.ISFailureModeFMEA#getInitialPreventiveActions()
	 * @see #getSFailureModeFMEA()
	 * @generated
	 */
	EAttribute getSFailureModeFMEA_InitialPreventiveActions();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getFinalPreventiveActions <em>Final Preventive Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Final Preventive Actions</em>'.
	 * @see org.polarsys.esf.esffmea.ISFailureModeFMEA#getFinalPreventiveActions()
	 * @see #getSFailureModeFMEA()
	 * @generated
	 */
	EAttribute getSFailureModeFMEA_FinalPreventiveActions();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getLocalEffects <em>Local Effects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Local Effects</em>'.
	 * @see org.polarsys.esf.esffmea.ISFailureModeFMEA#getLocalEffects()
	 * @see #getSFailureModeFMEA()
	 * @generated
	 */
	EAttribute getSFailureModeFMEA_LocalEffects();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getCauses <em>Causes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Causes</em>'.
	 * @see org.polarsys.esf.esffmea.ISFailureModeFMEA#getCauses()
	 * @see #getSFailureModeFMEA()
	 * @generated
	 */
	EAttribute getSFailureModeFMEA_Causes();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getFinalDetectability <em>Final Detectability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Final Detectability</em>'.
	 * @see org.polarsys.esf.esffmea.ISFailureModeFMEA#getFinalDetectability()
	 * @see #getSFailureModeFMEA()
	 * @generated
	 */
	EAttribute getSFailureModeFMEA_FinalDetectability();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getSystemEffects <em>System Effects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>System Effects</em>'.
	 * @see org.polarsys.esf.esffmea.ISFailureModeFMEA#getSystemEffects()
	 * @see #getSFailureModeFMEA()
	 * @generated
	 */
	EAttribute getSFailureModeFMEA_SystemEffects();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getCustomerEffects <em>Customer Effects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Customer Effects</em>'.
	 * @see org.polarsys.esf.esffmea.ISFailureModeFMEA#getCustomerEffects()
	 * @see #getSFailureModeFMEA()
	 * @generated
	 */
	EAttribute getSFailureModeFMEA_CustomerEffects();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getSBlockFMEA <em>SBlock FMEA</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>SBlock FMEA</em>'.
	 * @see org.polarsys.esf.esffmea.ISFailureModeFMEA#getSBlockFMEA()
	 * @see #getSFailureModeFMEA()
	 * @generated
	 */
	EReference getSFailureModeFMEA_SBlockFMEA();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	IESFFMEAFactory getESFFMEAFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esffmea.impl.SFMEA <em>SFMEA</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polarsys.esf.esffmea.impl.SFMEA
		 * @see org.polarsys.esf.esffmea.impl.ESFFMEAPackage#getSFMEA()
		 * @generated
		 */
		EClass SFMEA = eINSTANCE.getSFMEA();

		/**
		 * The meta object literal for the '<em><b>Base Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SFMEA__BASE_PACKAGE = eINSTANCE.getSFMEA_Base_Package();

		/**
		 * The meta object literal for the '<em><b>SBlocks FMEA List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SFMEA__SBLOCKS_FMEA_LIST = eINSTANCE.getSFMEA_SBlocksFMEAList();

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esffmea.impl.SBlockFMEA <em>SBlock FMEA</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polarsys.esf.esffmea.impl.SBlockFMEA
		 * @see org.polarsys.esf.esffmea.impl.ESFFMEAPackage#getSBlockFMEA()
		 * @generated
		 */
		EClass SBLOCK_FMEA = eINSTANCE.getSBlockFMEA();

		/**
		 * The meta object literal for the '<em><b>SBlock</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SBLOCK_FMEA__SBLOCK = eINSTANCE.getSBlockFMEA_SBlock();

		/**
		 * The meta object literal for the '<em><b>Criticality</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SBLOCK_FMEA__CRITICALITY = eINSTANCE.getSBlockFMEA_Criticality();

		/**
		 * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SBLOCK_FMEA__BASE_CLASS = eINSTANCE.getSBlockFMEA_Base_Class();

		/**
		 * The meta object literal for the '<em><b>SFailure Modes FMEA List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SBLOCK_FMEA__SFAILURE_MODES_FMEA_LIST = eINSTANCE.getSBlockFMEA_SFailureModesFMEAList();

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esffmea.impl.AbstractSFMEAElement <em>Abstract SFMEA Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polarsys.esf.esffmea.impl.AbstractSFMEAElement
		 * @see org.polarsys.esf.esffmea.impl.ESFFMEAPackage#getAbstractSFMEAElement()
		 * @generated
		 */
		EClass ABSTRACT_SFMEA_ELEMENT = eINSTANCE.getAbstractSFMEAElement();

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esffmea.impl.SFailureModeFMEA <em>SFailure Mode FMEA</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polarsys.esf.esffmea.impl.SFailureModeFMEA
		 * @see org.polarsys.esf.esffmea.impl.ESFFMEAPackage#getSFailureModeFMEA()
		 * @generated
		 */
		EClass SFAILURE_MODE_FMEA = eINSTANCE.getSFailureModeFMEA();

		/**
		 * The meta object literal for the '<em><b>Remarks</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SFAILURE_MODE_FMEA__REMARKS = eINSTANCE.getSFailureModeFMEA_Remarks();

		/**
		 * The meta object literal for the '<em><b>Initial Criticality</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SFAILURE_MODE_FMEA__INITIAL_CRITICALITY = eINSTANCE.getSFailureModeFMEA_InitialCriticality();

		/**
		 * The meta object literal for the '<em><b>Final Criticality</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SFAILURE_MODE_FMEA__FINAL_CRITICALITY = eINSTANCE.getSFailureModeFMEA_FinalCriticality();

		/**
		 * The meta object literal for the '<em><b>Initial Occurence</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SFAILURE_MODE_FMEA__INITIAL_OCCURENCE = eINSTANCE.getSFailureModeFMEA_InitialOccurence();

		/**
		 * The meta object literal for the '<em><b>Final Occurence</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SFAILURE_MODE_FMEA__FINAL_OCCURENCE = eINSTANCE.getSFailureModeFMEA_FinalOccurence();

		/**
		 * The meta object literal for the '<em><b>Initial Severity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SFAILURE_MODE_FMEA__INITIAL_SEVERITY = eINSTANCE.getSFailureModeFMEA_InitialSeverity();

		/**
		 * The meta object literal for the '<em><b>Final Severity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SFAILURE_MODE_FMEA__FINAL_SEVERITY = eINSTANCE.getSFailureModeFMEA_FinalSeverity();

		/**
		 * The meta object literal for the '<em><b>Is Detectable</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SFAILURE_MODE_FMEA__IS_DETECTABLE = eINSTANCE.getSFailureModeFMEA_IsDetectable();

		/**
		 * The meta object literal for the '<em><b>Initial Detectability</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SFAILURE_MODE_FMEA__INITIAL_DETECTABILITY = eINSTANCE.getSFailureModeFMEA_InitialDetectability();

		/**
		 * The meta object literal for the '<em><b>Base Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SFAILURE_MODE_FMEA__BASE_PROPERTY = eINSTANCE.getSFailureModeFMEA_Base_Property();

		/**
		 * The meta object literal for the '<em><b>SFailure Mode</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SFAILURE_MODE_FMEA__SFAILURE_MODE = eINSTANCE.getSFailureModeFMEA_SFailureMode();

		/**
		 * The meta object literal for the '<em><b>Initial Preventive Actions</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SFAILURE_MODE_FMEA__INITIAL_PREVENTIVE_ACTIONS = eINSTANCE.getSFailureModeFMEA_InitialPreventiveActions();

		/**
		 * The meta object literal for the '<em><b>Final Preventive Actions</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SFAILURE_MODE_FMEA__FINAL_PREVENTIVE_ACTIONS = eINSTANCE.getSFailureModeFMEA_FinalPreventiveActions();

		/**
		 * The meta object literal for the '<em><b>Local Effects</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SFAILURE_MODE_FMEA__LOCAL_EFFECTS = eINSTANCE.getSFailureModeFMEA_LocalEffects();

		/**
		 * The meta object literal for the '<em><b>Causes</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SFAILURE_MODE_FMEA__CAUSES = eINSTANCE.getSFailureModeFMEA_Causes();

		/**
		 * The meta object literal for the '<em><b>Final Detectability</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SFAILURE_MODE_FMEA__FINAL_DETECTABILITY = eINSTANCE.getSFailureModeFMEA_FinalDetectability();

		/**
		 * The meta object literal for the '<em><b>System Effects</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SFAILURE_MODE_FMEA__SYSTEM_EFFECTS = eINSTANCE.getSFailureModeFMEA_SystemEffects();

		/**
		 * The meta object literal for the '<em><b>Customer Effects</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SFAILURE_MODE_FMEA__CUSTOMER_EFFECTS = eINSTANCE.getSFailureModeFMEA_CustomerEffects();

		/**
		 * The meta object literal for the '<em><b>SBlock FMEA</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SFAILURE_MODE_FMEA__SBLOCK_FMEA = eINSTANCE.getSFailureModeFMEA_SBlockFMEA();

	}

} //IESFFMEAPackage
