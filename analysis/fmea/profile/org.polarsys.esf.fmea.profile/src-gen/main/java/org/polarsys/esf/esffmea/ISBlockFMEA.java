/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esffmea;

import org.eclipse.emf.common.util.EList;

import org.polarsys.esf.esfarchitectureconcepts.ISBlock;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SBlock FMEA</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.polarsys.esf.esffmea.ISBlockFMEA#getSBlock <em>SBlock</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.ISBlockFMEA#getCriticality <em>Criticality</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.ISBlockFMEA#getBase_Class <em>Base Class</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.ISBlockFMEA#getSFailureModesFMEAList <em>SFailure Modes FMEA List</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esffmea.IESFFMEAPackage#getSBlockFMEA()
 * @model
 * @generated
 */
public interface ISBlockFMEA extends IAbstractSFMEAElement {
	/**
	 * Returns the value of the '<em><b>SBlock</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SBlock</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SBlock</em>' reference.
	 * @see #setSBlock(ISBlock)
	 * @see org.polarsys.esf.esffmea.IESFFMEAPackage#getSBlockFMEA_SBlock()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	ISBlock getSBlock();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esffmea.ISBlockFMEA#getSBlock <em>SBlock</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SBlock</em>' reference.
	 * @see #getSBlock()
	 * @generated
	 */
	void setSBlock(ISBlock value);

	/**
	 * Returns the value of the '<em><b>Criticality</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Criticality</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Criticality</em>' attribute.
	 * @see #setCriticality(int)
	 * @see org.polarsys.esf.esffmea.IESFFMEAPackage#getSBlockFMEA_Criticality()
	 * @model default="0" dataType="org.eclipse.uml2.types.Integer" required="true" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	int getCriticality();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esffmea.ISBlockFMEA#getCriticality <em>Criticality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Criticality</em>' attribute.
	 * @see #getCriticality()
	 * @generated
	 */
	void setCriticality(int value);

	/**
	 * Returns the value of the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Class</em>' reference.
	 * @see #setBase_Class(org.eclipse.uml2.uml.Class)
	 * @see org.polarsys.esf.esffmea.IESFFMEAPackage#getSBlockFMEA_Base_Class()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	org.eclipse.uml2.uml.Class getBase_Class();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esffmea.ISBlockFMEA#getBase_Class <em>Base Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Class</em>' reference.
	 * @see #getBase_Class()
	 * @generated
	 */
	void setBase_Class(org.eclipse.uml2.uml.Class value);

	/**
	 * Returns the value of the '<em><b>SFailure Modes FMEA List</b></em>' reference list.
	 * The list contents are of type {@link org.polarsys.esf.esffmea.ISFailureModeFMEA}.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esffmea.ISFailureModeFMEA#getSBlockFMEA <em>SBlock FMEA</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SFailure Modes FMEA List</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SFailure Modes FMEA List</em>' reference list.
	 * @see org.polarsys.esf.esffmea.IESFFMEAPackage#getSBlockFMEA_SFailureModesFMEAList()
	 * @see org.polarsys.esf.esffmea.ISFailureModeFMEA#getSBlockFMEA
	 * @model opposite="sBlockFMEA" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<ISFailureModeFMEA> getSFailureModesFMEAList();

} // ISBlockFMEA
