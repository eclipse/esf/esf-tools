/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esffmea.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.polarsys.esf.esfarchitectureconcepts.ISBlock;

import org.polarsys.esf.esffmea.IESFFMEAPackage;
import org.polarsys.esf.esffmea.ISBlockFMEA;
import org.polarsys.esf.esffmea.ISFailureModeFMEA;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SBlock FMEA</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.polarsys.esf.esffmea.impl.SBlockFMEA#getSBlock <em>SBlock</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.impl.SBlockFMEA#getCriticality <em>Criticality</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.impl.SBlockFMEA#getBase_Class <em>Base Class</em>}</li>
 *   <li>{@link org.polarsys.esf.esffmea.impl.SBlockFMEA#getSFailureModesFMEAList <em>SFailure Modes FMEA List</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SBlockFMEA extends AbstractSFMEAElement implements ISBlockFMEA {
	/**
	 * The cached value of the '{@link #getSBlock() <em>SBlock</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSBlock()
	 * @generated
	 * @ordered
	 */
	protected ISBlock sBlock;

	/**
	 * The default value of the '{@link #getCriticality() <em>Criticality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCriticality()
	 * @generated
	 * @ordered
	 */
	protected static final int CRITICALITY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getBase_Class() <em>Base Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_Class()
	 * @generated
	 * @ordered
	 */
	protected org.eclipse.uml2.uml.Class base_Class;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SBlockFMEA() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IESFFMEAPackage.Literals.SBLOCK_FMEA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISBlock getSBlock() {
		if (sBlock != null && sBlock.eIsProxy()) {
			InternalEObject oldSBlock = (InternalEObject)sBlock;
			sBlock = (ISBlock)eResolveProxy(oldSBlock);
			if (sBlock != oldSBlock) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IESFFMEAPackage.SBLOCK_FMEA__SBLOCK, oldSBlock, sBlock));
			}
		}
		return sBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISBlock basicGetSBlock() {
		return sBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSBlock(ISBlock newSBlock) {
		ISBlock oldSBlock = sBlock;
		sBlock = newSBlock;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFFMEAPackage.SBLOCK_FMEA__SBLOCK, oldSBlock, sBlock));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getCriticality() {
		// TODO: implement this method to return the 'Criticality' attribute
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCriticality(int newCriticality) {
		// TODO: implement this method to set the 'Criticality' attribute
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Class getBase_Class() {
		if (base_Class != null && base_Class.eIsProxy()) {
			InternalEObject oldBase_Class = (InternalEObject)base_Class;
			base_Class = (org.eclipse.uml2.uml.Class)eResolveProxy(oldBase_Class);
			if (base_Class != oldBase_Class) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IESFFMEAPackage.SBLOCK_FMEA__BASE_CLASS, oldBase_Class, base_Class));
			}
		}
		return base_Class;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Class basicGetBase_Class() {
		return base_Class;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBase_Class(org.eclipse.uml2.uml.Class newBase_Class) {
		org.eclipse.uml2.uml.Class oldBase_Class = base_Class;
		base_Class = newBase_Class;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFFMEAPackage.SBLOCK_FMEA__BASE_CLASS, oldBase_Class, base_Class));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ISFailureModeFMEA> getSFailureModesFMEAList() {
		// TODO: implement this method to return the 'SFailure Modes FMEA List' reference list
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case IESFFMEAPackage.SBLOCK_FMEA__SBLOCK:
				if (resolve) return getSBlock();
				return basicGetSBlock();
			case IESFFMEAPackage.SBLOCK_FMEA__CRITICALITY:
				return getCriticality();
			case IESFFMEAPackage.SBLOCK_FMEA__BASE_CLASS:
				if (resolve) return getBase_Class();
				return basicGetBase_Class();
			case IESFFMEAPackage.SBLOCK_FMEA__SFAILURE_MODES_FMEA_LIST:
				return getSFailureModesFMEAList();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case IESFFMEAPackage.SBLOCK_FMEA__SBLOCK:
				setSBlock((ISBlock)newValue);
				return;
			case IESFFMEAPackage.SBLOCK_FMEA__CRITICALITY:
				setCriticality((Integer)newValue);
				return;
			case IESFFMEAPackage.SBLOCK_FMEA__BASE_CLASS:
				setBase_Class((org.eclipse.uml2.uml.Class)newValue);
				return;
			case IESFFMEAPackage.SBLOCK_FMEA__SFAILURE_MODES_FMEA_LIST:
				getSFailureModesFMEAList().clear();
				getSFailureModesFMEAList().addAll((Collection<? extends ISFailureModeFMEA>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case IESFFMEAPackage.SBLOCK_FMEA__SBLOCK:
				setSBlock((ISBlock)null);
				return;
			case IESFFMEAPackage.SBLOCK_FMEA__CRITICALITY:
				setCriticality(CRITICALITY_EDEFAULT);
				return;
			case IESFFMEAPackage.SBLOCK_FMEA__BASE_CLASS:
				setBase_Class((org.eclipse.uml2.uml.Class)null);
				return;
			case IESFFMEAPackage.SBLOCK_FMEA__SFAILURE_MODES_FMEA_LIST:
				getSFailureModesFMEAList().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case IESFFMEAPackage.SBLOCK_FMEA__SBLOCK:
				return sBlock != null;
			case IESFFMEAPackage.SBLOCK_FMEA__CRITICALITY:
				return getCriticality() != CRITICALITY_EDEFAULT;
			case IESFFMEAPackage.SBLOCK_FMEA__BASE_CLASS:
				return base_Class != null;
			case IESFFMEAPackage.SBLOCK_FMEA__SFAILURE_MODES_FMEA_LIST:
				return !getSFailureModesFMEAList().isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //SBlockFMEA
