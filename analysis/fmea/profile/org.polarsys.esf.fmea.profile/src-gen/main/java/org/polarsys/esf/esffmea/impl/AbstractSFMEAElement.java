/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esffmea.impl;

import org.eclipse.emf.ecore.EClass;

import org.polarsys.esf.esfcore.impl.AbstractSSafetyConcept;

import org.polarsys.esf.esffmea.IAbstractSFMEAElement;
import org.polarsys.esf.esffmea.IESFFMEAPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract SFMEA Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AbstractSFMEAElement extends AbstractSSafetyConcept implements IAbstractSFMEAElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractSFMEAElement() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IESFFMEAPackage.Literals.ABSTRACT_SFMEA_ELEMENT;
	}

} //AbstractSFMEAElement
