/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.fmea.table.tester;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementMatcher;
import org.eclipse.gmf.runtime.emf.type.core.ISpecializationType;
import org.eclipse.papyrus.infra.nattable.tester.ITableTester;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.esffmea.IESFFMEAPackage;
import org.polarsys.esf.fmea.profile.service.types.set.ESFFMEATypesSet;
import org.polarsys.esf.fmea.table.ESFFMEATableActivator;

/**
 *
 * Class responsible to test if the FMEA Table can be created.
 *
 * @author $Author: ogurcan $
 * @version $Revision: 168 $
 */
public final class FMEATableTester
	implements ITableTester {

	/**
	 *
	 * Default constructor.
	 */
	public FMEATableTester() {
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	public IStatus isAllowed(final Object pContext) {
		IStatus vStatus = null;
		if (pContext instanceof Element) {
			Element vEl = (Element) pContext;
			ISpecializationType vType = (ISpecializationType) ElementTypeRegistry.getInstance()
				.getType(ESFFMEATypesSet.SFAILUREMODEFMEA_TYPE_ID);
			if (vType != null) {
				IElementMatcher vMatcher = vType.getMatcher();
				Profile vProfile = null;
				if (pContext instanceof Package || vMatcher.matches(vEl)) {
					vProfile = UMLUtil.getProfile(IESFFMEAPackage.eINSTANCE, vEl);
				}

				if (vProfile != null) {
					final String vPackageQN = vProfile.getQualifiedName();
					if (vEl.getNearestPackage().getAppliedProfile(vPackageQN, true) != null) {
						vStatus = new Status(
							IStatus.OK,
							ESFFMEATableActivator.PLUGIN_ID,
							"The context allowed to create a FMEA Table"); //$NON-NLS-1$
					} else {
						vStatus = new Status(
							IStatus.ERROR,
							ESFFMEATableActivator.PLUGIN_ID,
							"The profile " + vPackageQN //$NON-NLS-1$
								+ " is not applied on the model"); //$NON-NLS-1$
					}
				}
			}
		}

		if (vStatus == null) {
			vStatus = new Status(
				IStatus.ERROR,
				ESFFMEATableActivator.PLUGIN_ID,
				"The context is not an UML Element"); //$NON-NLS-1$
		}
		return vStatus;
	}

}
