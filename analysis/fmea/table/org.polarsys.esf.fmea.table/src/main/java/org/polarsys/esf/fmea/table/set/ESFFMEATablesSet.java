/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.fmea.table.set;

/**
 * ESFFMEA tables set.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public final class ESFFMEATablesSet {

	/**
	 * FMEA Table ID.
	 * NOTE: The ID must be the same in:
	 *  - FMEATable.configuration
	 *  - FMEATable.nattableconfiguration
	 */
	public static final String FMEA_TABLE_ID =
		"FMEATable"; //$NON-NLS-1$

	/**
	 * FMEA Table name.
	 * NOTE: The name must be the same in:
	 *  - FMEATable.nattableconfiguration
	 */
	public static final String FMEA_TABLE_NAME =
		"FMEATable"; //$NON-NLS-1$


	/**
	 * Default constructor, private as it's a utility class.
	 */
	private ESFFMEATablesSet() {
		// Nothing to do
	}
}
