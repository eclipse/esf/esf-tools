/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.fmea.table.manager.axis;

import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.ISpecializationType;
import org.eclipse.papyrus.uml.nattable.manager.axis.AbstractStereotypedElementUMLSynchronizedOnFeatureAxisManager;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.esffmea.ISFailureModeFMEA;
import org.polarsys.esf.esffmea.impl.ESFFMEAPackage;
import org.polarsys.esf.fmea.profile.service.types.set.ESFFMEATypesSet;

/**
 *
 * Axis Manager for FMEATable.
 *
 * @author $Author: ogurcan $
 * @version $Revision: 168 $
 */
public class FMEAAxisManager
	extends AbstractStereotypedElementUMLSynchronizedOnFeatureAxisManager<ISFailureModeFMEA> {

	/**
	 *
	 * Default constructor.
	 */
	public FMEAAxisManager() {
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	public boolean canCreateAxisElement(final String pElementId) {
		ISpecializationType vType = (ISpecializationType) ElementTypeRegistry.getInstance()
			.getType(ESFFMEATypesSet.SFAILUREMODEFMEA_TYPE_ID);
		return vType.getId().equals(pElementId);
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	protected Element getStereotypeBaseElement(final ISFailureModeFMEA pStereotypeApplication) {
		return pStereotypeApplication.getBase_Property();
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	protected boolean isInstanceOfRequiredStereotypeApplication(final Object pObject) {
		return pObject instanceof ISFailureModeFMEA;
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	protected boolean isAllowedAsBaseElement(final Element pElement) {
		return pElement instanceof Property;
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	protected ISFailureModeFMEA getStereotypeApplication(final Element pElement) {
		return UMLUtil.getStereotypeApplication(pElement, ISFailureModeFMEA.class);
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	protected String getStereotypeApplicationBasePropertyName() {
		return ESFFMEAPackage.eINSTANCE.getSFailureModeFMEA_Base_Property().getName();
	}

}

