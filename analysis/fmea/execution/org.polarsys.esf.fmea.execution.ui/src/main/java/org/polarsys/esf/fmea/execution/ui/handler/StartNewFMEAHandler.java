/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.fmea.execution.ui.handler;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.papyrus.infra.widgets.editors.TreeSelectorDialog;
import org.eclipse.papyrus.infra.widgets.providers.EncapsulatedContentProvider;
import org.eclipse.papyrus.uml.tools.providers.UMLLabelProvider;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.UMLPackage;
import org.polarsys.esf.core.common.ui.provider.ESFContentProvider;
import org.polarsys.esf.core.utils.ModelUtil;
import org.polarsys.esf.fmea.execution.setup.NewFMEASetup;

/**
 * Handler class for starting a new local analysis.
 *
 * @author $Author: ogurcan $
 * @version $Revision: 168 $
 */
public final class StartNewFMEAHandler
	extends AbstractHandler {

	/**
	 * Default constructor.
	 */
	public StartNewFMEAHandler() { }

	/**
	 * Get the selected element and setup a new Local Analysis.
	 *
	 * {@inheritDoc}
	 */
	@Override
	public Object execute(final ExecutionEvent pEvent) throws ExecutionException {
		ISelection vSelection = HandlerUtil.getCurrentSelection(pEvent);

		Class vSelectedClass =
			(Class) ModelUtil.getSelectedEObjectOfType(vSelection, UMLPackage.eINSTANCE.getClass_());

		final Package vSelectedPkg =
				(Package) ModelUtil.getSelectedEObjectOfType(vSelection, UMLPackage.eINSTANCE.getPackage());
		
		if (vSelectedPkg != null) {
			TreeSelectorDialog dialog = new TreeSelectorDialog(Display.getCurrent().getActiveShell());
			dialog.setLabelProvider((new UMLLabelProvider()));
			ESFContentProvider cp = new ESFContentProvider(vSelectedPkg, UMLPackage.eINSTANCE.getClass_());
			dialog.setContentProvider(new EncapsulatedContentProvider(cp));
			dialog.setTitle("Select class");
			dialog.setDescription("FMEA analysis can be executed on classes or components. Select below");
			int code = dialog.open();
			
			if (code == Dialog.OK) {
				Object result = dialog.getResult()[0];
				if (result instanceof Class) {
					vSelectedClass = (Class) result;
				}
			}
		}
		if (vSelectedClass != null) {
			NewFMEASetup.setup(vSelectedClass);
		}
		return null;
	}

}
