/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.fmea.execution.ui.handler;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.core.utils.ESFTablesUtil;
import org.polarsys.esf.core.utils.ModelUtil;
import org.polarsys.esf.esffmea.ISBlockFMEA;
import org.polarsys.esf.fmea.table.set.ESFFMEATablesSet;

/**
 * Handler class for creating a FMEA Table.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class CreateFMEATableHandler
	extends AbstractHandler {

	/**
	 * Default constructor.
	 */
	public CreateFMEATableHandler() {
	}

	/**
	 * Get the selected element 'BlockFMEA' and call the action for creating a FMEA Table.
	 *
	 * {@inheritDoc}
	 */
	@Override
	public Object execute(final ExecutionEvent pEvent) throws ExecutionException {

		ISelection vSelection = HandlerUtil.getCurrentSelection(pEvent);

		final Class vSelectedClass =
			(Class) ModelUtil.getSelectedEObjectOfType(vSelection, UMLPackage.eINSTANCE.getClass_());

		if ((vSelectedClass != null)
			&& (UMLUtil.getStereotypeApplication(vSelectedClass, ISBlockFMEA.class) != null)) {
			ESFTablesUtil.createTable(
				ESFFMEATablesSet.FMEA_TABLE_ID,
				ESFFMEATablesSet.FMEA_TABLE_NAME,
				vSelectedClass);
		}
		return null;
	}

}
