/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.fmea.execution.setup;

import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.core.utils.ESFTablesUtil;
import org.polarsys.esf.core.utils.ModelUtil;
import org.polarsys.esf.esffmea.ISBlockFMEA;
import org.polarsys.esf.esffmea.impl.SBlockFMEA;
import org.polarsys.esf.fmea.profile.tools.util.ESFFMEAUtil;
import org.polarsys.esf.fmea.table.set.ESFFMEATablesSet;

/**
 * Setup class for starting a new FMEA.
 *
 * @author $Author: ogurcan $
 * @version $Revision: 168 $
 */
public final class NewFMEASetup {

	/**
	 * Default constructor, private as it's a utility class.
	 */
	private NewFMEASetup() {
	}

	/**
	 * Setup of a new FMEA.
	 *
	 * @param block
	 *            The block to be analysed
	 */
	public static void setup(final Class block) {

		Model vESFModel = ModelUtil.getWorkingModel();
		if (vESFModel != null) {

			ESFFMEAUtil.applyESFFMEAProfile(block.getNearestPackage());
			
			ESFFMEAUtil.createSBlockFMEA(block);
			
			ISBlockFMEA sblockFMEA = UMLUtil.getStereotypeApplication(block, SBlockFMEA.class);
			
			if (sblockFMEA != null) {
				ESFTablesUtil.createTable(
						ESFFMEATablesSet.FMEA_TABLE_ID,
						ESFFMEATablesSet.FMEA_TABLE_NAME,
						block);
			}
		}
	}
}
