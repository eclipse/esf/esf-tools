/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.safetyreq.profile.tools.util;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.papyrus.sysml14.requirements.internal.impl.RequirementsPackageImpl;
import org.eclipse.papyrus.sysml14.util.SysMLResource;
import org.eclipse.papyrus.uml.tools.utils.PackageUtil;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.core.utils.ModelUtil;
import org.polarsys.esf.esfsafetyrequirements.impl.ESFSafetyRequirementsPackage;
import org.polarsys.esf.esfsafetyrequirements.impl.SSafetyRequirement;
import org.polarsys.esf.safetyreq.profile.set.ESFSafetyRequirementsSet;

/**
 * Utilities for working with ESFSafetyRequirements profile.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public final class ESFSafetyRequirementsUtil {

	/** Apply ESFSSafetyReq profile label. */
	private static final String APPLY_ESFSAFETYREQUIREMENTS_PROFILE_LABEL = "Apply ESFSSafetyRequirments profile"; //$NON-NLS-1$

	/** Apply SysML profile label. */
	private static final String APPLY_SYSML_PROFILE_LABEL = "Apply SysML profile"; //$NON-NLS-1$

	/** Create SSafetyRequirment label. */
	private static final String CREATE_SSAFETYREQUIREMENT_LABEL = "Create SSafetyRequirment"; //$NON-NLS-1$

	/** Apply SSafetyRequirment stereotype label. */
	private static final String APPLY_SSAFETYREQUIREMENT_STEREOTYPE_LABEL = "Apply SSafetyRequirment stereotype"; //$NON-NLS-1$

	/**
	 * Default constructor.
	 */
	private ESFSafetyRequirementsUtil() {
	}

	/**
	 * Apply ESFSafetyRequirements profile to the given model element.
	 *
	 * @param pModel The model target
	 */
	public static void applyESFSafetyRequirementsProfile(final Model pModel) {
		TransactionalEditingDomain vDomain = ModelUtil.getTransactionalEditingDomain(pModel);

		RecordingCommand vApplyESFSafetyReqProfileCmd =
			new RecordingCommand(vDomain, APPLY_ESFSAFETYREQUIREMENTS_PROFILE_LABEL) {

				@Override
				protected void doExecute() {
					// Retrieve ESFSSafetyRequirements profile and apply it
					Profile vESFSSafetyRequirementsProfile = (Profile) PackageUtil.loadPackage(
						URI.createURI(ESFSafetyRequirementsSet.PROFILE_PATH),
						pModel.eResource().getResourceSet());

					if (vESFSSafetyRequirementsProfile != null) {
						PackageUtil.applyProfile(pModel, vESFSSafetyRequirementsProfile, true);
					}
				}
			};

		// Verify if command can be executed
		if (vApplyESFSafetyReqProfileCmd.canExecute()) {
			// Execute command
			vDomain.getCommandStack().execute(vApplyESFSafetyReqProfileCmd);
		}
	}

	/**
	 * Apply 'SSafetyRequirement' Stereotype.
	 *
	 * @param pRequirement The requirement
	 */
	public static void applySSafetyRequirementStereotype(final Class pRequirement) {
		TransactionalEditingDomain vDomain = ModelUtil.getTransactionalEditingDomain(pRequirement);

		RecordingCommand vApplySSafetyRequirementCmd =
			new RecordingCommand(vDomain, APPLY_SSAFETYREQUIREMENT_STEREOTYPE_LABEL) {

				@Override
				protected void doExecute() {
					UMLUtil.StereotypeApplicationHelper.getInstance(pRequirement)
						.applyStereotype(pRequirement, ESFSafetyRequirementsPackage.eINSTANCE.getSSafetyRequirement());
				}
			};

		// Verify if command can be executed
		if (vApplySSafetyRequirementCmd.canExecute()) {
			// Execute command
			vDomain.getCommandStack().execute(vApplySSafetyRequirementCmd);
		}
	}

	/**
	 * Apply SysML profile to the given model element.
	 *
	 * @param pModel The model target
	 */
	public static void applySysMLProfile(final Model pModel) {
		TransactionalEditingDomain vDomain = ModelUtil.getTransactionalEditingDomain(pModel);

		RecordingCommand vApplySysMLProfileCmd =
			new RecordingCommand(vDomain, APPLY_SYSML_PROFILE_LABEL) {

				@Override
				protected void doExecute() {
					// Retrieve SysML profile and apply it
					Profile vSysMLProfile = (Profile) PackageUtil.loadPackage(
						URI.createURI(SysMLResource.PROFILE_PATH),
						pModel.eResource().getResourceSet());

					if (vSysMLProfile != null) {
						PackageUtil.applyProfile(pModel, vSysMLProfile, true);
					}
				}
			};

		// Verify if command can be executed
		if (vApplySysMLProfileCmd.canExecute()) {
			// Execute command
			vDomain.getCommandStack().execute(vApplySysMLProfileCmd);
		}

	}



	/**
	 * Create a new SSafetyRequirement element.
	 *
	 * @param pPackage The package where the SSafetyRequirement must be created
	 */
	public static void createSSafetyRequirement(final Package pPackage) {
		TransactionalEditingDomain vDomain = ModelUtil.getTransactionalEditingDomain(pPackage);

		RecordingCommand vApplySSafetyRequirementCmd =
			new RecordingCommand(vDomain, CREATE_SSAFETYREQUIREMENT_LABEL) {

				@Override
				protected void doExecute() {
					String vName = SSafetyRequirement.class.getSimpleName() + pPackage.getOwnedElements().size();
					// Create a new SSafetyRequirement element
					Class vSSafetyRequirement = pPackage.createOwnedClass(vName, false);
					// Apply 'SSafetyRequirement' stereotype on the created class
					UMLUtil.StereotypeApplicationHelper.getInstance(vSSafetyRequirement)
						.applyStereotype(vSSafetyRequirement,
							ESFSafetyRequirementsPackage.eINSTANCE.getSSafetyRequirement());
					// Apply 'Requirement' stereotype on the created class
					UMLUtil.StereotypeApplicationHelper.getInstance(vSSafetyRequirement)
						.applyStereotype(vSSafetyRequirement, RequirementsPackageImpl.eINSTANCE.getRequirement());
				}
			};

		// Verify if command can be executed
		if (vApplySSafetyRequirementCmd.canExecute()) {
			// Execute command
			vDomain.getCommandStack().execute(vApplySSafetyRequirementCmd);
		}
	}
}
