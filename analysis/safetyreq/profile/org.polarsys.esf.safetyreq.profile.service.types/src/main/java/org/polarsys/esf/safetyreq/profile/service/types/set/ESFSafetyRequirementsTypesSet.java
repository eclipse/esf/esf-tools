/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.safetyreq.profile.service.types.set;

/**
 * ESFSafetyRequirements types set.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public final class ESFSafetyRequirementsTypesSet {

	/** Specialized Types ID of SSafetyRequirement. */
	public static final String SSAFETYREQUIREMENT_TYPE_ID =
		"org.polarsys.esf.ESFSafetyRequirements.SSafetyRequirement"; //$NON-NLS-1$

	/**
	 * Default constructor, private as it's a utility class.
	 */
	private ESFSafetyRequirementsTypesSet() {
		// Nothing to do
	}
}
