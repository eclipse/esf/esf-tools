/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.esfsafetyrequirements.impl;

import org.eclipse.emf.common.util.EList;
import org.polarsys.esf.esfcore.impl.GenericAbstractSElement;
import org.polarsys.esf.esffmea.ISFailureModeFMEA;
import org.polarsys.esf.esfsafetyrequirements.IMSSafetyRequirement;

/**
 * This class can override the generated class {@link SSafetyRequirement} and will be
 * used by the custom factory.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class MSSafetyRequirement
	extends SSafetyRequirement
	implements IMSSafetyRequirement {

	/**
	 * Default constructor.
	 */
	public MSSafetyRequirement() {
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return GenericAbstractSElement.getName(getBase_Class());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getUUID() {
		return GenericAbstractSElement.getUUID(getBase_Class());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getCriticality() {
		int vCriticality = 0;
		EList<ISFailureModeFMEA> vSFailureModesList = getSFailureModesFMEAList();

		if (!vSFailureModesList.isEmpty()) {
			for (ISFailureModeFMEA vSFailureMode : vSFailureModesList) {
				if (vCriticality < vSFailureMode.getFinalCriticality()) {
					vCriticality = vSFailureMode.getFinalCriticality();
				}
			}
		}

		return vCriticality;
	}

}
