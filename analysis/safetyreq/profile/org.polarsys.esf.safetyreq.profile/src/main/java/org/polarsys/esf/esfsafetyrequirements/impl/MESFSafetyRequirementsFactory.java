/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.esfsafetyrequirements.impl;

import org.polarsys.esf.esfsafetyrequirements.IESFSafetyRequirementsFactory;
import org.polarsys.esf.esfsafetyrequirements.IMESFSafetyRequirementsFactory;
import org.polarsys.esf.esfsafetyrequirements.IMSSafetyRequirement;

/**
 * This factory overrides the generated factory {@link ESFSafetyRequirementsFactory}
 * and returns the new generated interfaces.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class MESFSafetyRequirementsFactory
	extends ESFSafetyRequirementsFactory
	implements IMESFSafetyRequirementsFactory {

	/**
	 * Default constructor.
	 */
	public MESFSafetyRequirementsFactory() {
		super();
	}

	/**
	 * @return The initialised factory
	 */
	public static IMESFSafetyRequirementsFactory init() {
		IESFSafetyRequirementsFactory vFactory = ESFSafetyRequirementsFactory.init();

		// Check if the factory returned by the standard implementation can suit,
		// otherwise create a new instance
		if (!(vFactory instanceof IMESFSafetyRequirementsFactory)) {
			vFactory = new MESFSafetyRequirementsFactory();
		}

		return (IMESFSafetyRequirementsFactory) vFactory;
	}

	@Override
	public IMSSafetyRequirement createSSafetyRequirement() {
		IMSSafetyRequirement vSSafetyRequirement = new MSSafetyRequirement();
		return vSSafetyRequirement;
	}
}
