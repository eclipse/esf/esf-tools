/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.safetyreq.profile.set;


/**
 * ESFSafetyReq profile set.
 *
 * @author  $Author: ymunoz $
 * @version $Revision: 169 $
 */
public final class ESFSafetyRequirementsSet {
	/** ESFSafetyRequirements profile path. */
	public static final String PROFILE_PATH =
		"pathmap://ESFSAFETYREQUIREMENTS_PROFILE/esfsafetyrequirements.profile.uml"; //$NON-NLS-1$

	/** ESFSafetyRequirements profile URI. */
	public static final String PROFILE_URI =
		"http://www.polarsys.org/esf/0.7.0/ESFSafetyRequirements"; //$NON-NLS-1$

	/**
	 * Default constructor, private as it's a utility class.
	 */
	private ESFSafetyRequirementsSet() {
	 // Nothing to do
	}
}
