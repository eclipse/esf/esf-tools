/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esfsafetyrequirements.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.uml2.types.TypesPackage;

import org.eclipse.uml2.uml.UMLPackage;

import org.polarsys.esf.esfcore.IESFCorePackage;

import org.polarsys.esf.esffmea.IESFFMEAPackage;

import org.polarsys.esf.esfsafetyrequirements.IESFSafetyRequirementsFactory;
import org.polarsys.esf.esfsafetyrequirements.IESFSafetyRequirementsPackage;
import org.polarsys.esf.esfsafetyrequirements.ISSafetyRequirement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class ESFSafetyRequirementsPackage
	extends EPackageImpl
	implements IESFSafetyRequirementsPackage {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass sSafetyRequirementEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>
	 * Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.polarsys.esf.esfsafetyrequirements.IESFSafetyRequirementsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ESFSafetyRequirementsPackage() {
		super(eNS_URI, IESFSafetyRequirementsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>
	 * This method is used to initialize {@link IESFSafetyRequirementsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static IESFSafetyRequirementsPackage init() {
		if (isInited)
			return (IESFSafetyRequirementsPackage) EPackage.Registry.INSTANCE
				.getEPackage(IESFSafetyRequirementsPackage.eNS_URI);

		// Obtain or create and register package
		ESFSafetyRequirementsPackage theESFSafetyRequirementsPackage =
			(ESFSafetyRequirementsPackage) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof ESFSafetyRequirementsPackage
					? EPackage.Registry.INSTANCE.get(eNS_URI)
					: new ESFSafetyRequirementsPackage());

		isInited = true;

		// Initialize simple dependencies
		IESFFMEAPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theESFSafetyRequirementsPackage.createPackageContents();

		// Initialize created meta-data
		theESFSafetyRequirementsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theESFSafetyRequirementsPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(IESFSafetyRequirementsPackage.eNS_URI, theESFSafetyRequirementsPackage);
		return theESFSafetyRequirementsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getSSafetyRequirement() {
		return sSafetyRequirementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getSSafetyRequirement_Criticality() {
		return (EAttribute) sSafetyRequirementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getSSafetyRequirement_SFailureModesFMEAList() {
		return (EReference) sSafetyRequirementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getSSafetyRequirement_Base_Class() {
		return (EReference) sSafetyRequirementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public IESFSafetyRequirementsFactory getESFSafetyRequirementsFactory() {
		return (IESFSafetyRequirementsFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package. This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		sSafetyRequirementEClass = createEClass(SSAFETY_REQUIREMENT);
		createEAttribute(sSafetyRequirementEClass, SSAFETY_REQUIREMENT__CRITICALITY);
		createEReference(sSafetyRequirementEClass, SSAFETY_REQUIREMENT__SFAILURE_MODES_FMEA_LIST);
		createEReference(sSafetyRequirementEClass, SSAFETY_REQUIREMENT__BASE_CLASS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model. This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		IESFCorePackage theESFCorePackage =
			(IESFCorePackage) EPackage.Registry.INSTANCE.getEPackage(IESFCorePackage.eNS_URI);
		TypesPackage theTypesPackage = (TypesPackage) EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);
		IESFFMEAPackage theESFFMEAPackage =
			(IESFFMEAPackage) EPackage.Registry.INSTANCE.getEPackage(IESFFMEAPackage.eNS_URI);
		UMLPackage theUMLPackage = (UMLPackage) EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		sSafetyRequirementEClass.getESuperTypes().add(theESFCorePackage.getAbstractSRequirement());

		// Initialize classes, features, and operations; add parameters
		initEClass(
			sSafetyRequirementEClass,
			ISSafetyRequirement.class,
			"SSafetyRequirement", //$NON-NLS-1$
			!IS_ABSTRACT,
			!IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(
			getSSafetyRequirement_Criticality(),
			theTypesPackage.getInteger(),
			"criticality", //$NON-NLS-1$
			null,
			1,
			1,
			ISSafetyRequirement.class,
			IS_TRANSIENT,
			IS_VOLATILE,
			IS_CHANGEABLE,
			!IS_UNSETTABLE,
			!IS_ID,
			IS_UNIQUE,
			IS_DERIVED,
			!IS_ORDERED);
		initEReference(
			getSSafetyRequirement_SFailureModesFMEAList(),
			theESFFMEAPackage.getSFailureModeFMEA(),
			null,
			"sFailureModesFMEAList", //$NON-NLS-1$
			null,
			0,
			-1,
			ISSafetyRequirement.class,
			!IS_TRANSIENT,
			!IS_VOLATILE,
			IS_CHANGEABLE,
			!IS_COMPOSITE,
			IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE,
			IS_UNIQUE,
			!IS_DERIVED,
			!IS_ORDERED);
		initEReference(
			getSSafetyRequirement_Base_Class(),
			theUMLPackage.getClass_(),
			null,
			"base_Class", //$NON-NLS-1$
			null,
			1,
			1,
			ISSafetyRequirement.class,
			!IS_TRANSIENT,
			!IS_VOLATILE,
			IS_CHANGEABLE,
			!IS_COMPOSITE,
			IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE,
			IS_UNIQUE,
			!IS_DERIVED,
			!IS_ORDERED);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/uml2/2.0.0/UML
		createUMLAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/uml2/2.0.0/UML</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void createUMLAnnotations() {
		String source = "http://www.eclipse.org/uml2/2.0.0/UML"; //$NON-NLS-1$
		addAnnotation(this, source, new String[] {"originalName", "ESFSafetyRequirements" //$NON-NLS-1$ //$NON-NLS-2$
		});
	}

} // ESFSafetyRequirementsPackage
