/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esfsafetyrequirements;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.polarsys.esf.esfcore.IESFCorePackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each operation of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * 
 * @see org.polarsys.esf.esfsafetyrequirements.IESFSafetyRequirementsFactory
 * @model kind="package"
 * annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='ESFSafetyRequirements'"
 * @generated
 */
public interface IESFSafetyRequirementsPackage
	extends EPackage {

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	String eNAME = "esfsafetyrequirements"; //$NON-NLS-1$

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	String eNS_URI = "http://www.polarsys.org/esf/0.7.0/ESFSafetyRequirements"; //$NON-NLS-1$

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	String eNS_PREFIX = "ESFSafetyRequirements"; //$NON-NLS-1$

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	IESFSafetyRequirementsPackage eINSTANCE =
		org.polarsys.esf.esfsafetyrequirements.impl.ESFSafetyRequirementsPackage.init();

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esfsafetyrequirements.impl.SSafetyRequirement <em>SSafety
	 * Requirement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.polarsys.esf.esfsafetyrequirements.impl.SSafetyRequirement
	 * @see org.polarsys.esf.esfsafetyrequirements.impl.ESFSafetyRequirementsPackage#getSSafetyRequirement()
	 * @generated
	 */
	int SSAFETY_REQUIREMENT = 0;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSAFETY_REQUIREMENT__UUID = IESFCorePackage.ABSTRACT_SREQUIREMENT__UUID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSAFETY_REQUIREMENT__NAME = IESFCorePackage.ABSTRACT_SREQUIREMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSAFETY_REQUIREMENT__DESCRIPTION = IESFCorePackage.ABSTRACT_SREQUIREMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>SElements List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSAFETY_REQUIREMENT__SELEMENTS_LIST = IESFCorePackage.ABSTRACT_SREQUIREMENT__SELEMENTS_LIST;

	/**
	 * The feature id for the '<em><b>Criticality</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSAFETY_REQUIREMENT__CRITICALITY = IESFCorePackage.ABSTRACT_SREQUIREMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>SFailure Modes FMEA List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSAFETY_REQUIREMENT__SFAILURE_MODES_FMEA_LIST = IESFCorePackage.ABSTRACT_SREQUIREMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSAFETY_REQUIREMENT__BASE_CLASS = IESFCorePackage.ABSTRACT_SREQUIREMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>SSafety Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSAFETY_REQUIREMENT_FEATURE_COUNT = IESFCorePackage.ABSTRACT_SREQUIREMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>SSafety Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SSAFETY_REQUIREMENT_OPERATION_COUNT = IESFCorePackage.ABSTRACT_SREQUIREMENT_OPERATION_COUNT + 0;

	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.esfsafetyrequirements.ISSafetyRequirement <em>SSafety
	 * Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>SSafety Requirement</em>'.
	 * @see org.polarsys.esf.esfsafetyrequirements.ISSafetyRequirement
	 * @generated
	 */
	EClass getSSafetyRequirement();

	/**
	 * Returns the meta object for the attribute
	 * '{@link org.polarsys.esf.esfsafetyrequirements.ISSafetyRequirement#getCriticality <em>Criticality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Criticality</em>'.
	 * @see org.polarsys.esf.esfsafetyrequirements.ISSafetyRequirement#getCriticality()
	 * @see #getSSafetyRequirement()
	 * @generated
	 */
	EAttribute getSSafetyRequirement_Criticality();

	/**
	 * Returns the meta object for the reference list
	 * '{@link org.polarsys.esf.esfsafetyrequirements.ISSafetyRequirement#getSFailureModesFMEAList <em>SFailure Modes
	 * FMEA List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>SFailure Modes FMEA List</em>'.
	 * @see org.polarsys.esf.esfsafetyrequirements.ISSafetyRequirement#getSFailureModesFMEAList()
	 * @see #getSSafetyRequirement()
	 * @generated
	 */
	EReference getSSafetyRequirement_SFailureModesFMEAList();

	/**
	 * Returns the meta object for the reference
	 * '{@link org.polarsys.esf.esfsafetyrequirements.ISSafetyRequirement#getBase_Class <em>Base Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Base Class</em>'.
	 * @see org.polarsys.esf.esfsafetyrequirements.ISSafetyRequirement#getBase_Class()
	 * @see #getSSafetyRequirement()
	 * @generated
	 */
	EReference getSSafetyRequirement_Base_Class();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	IESFSafetyRequirementsFactory getESFSafetyRequirementsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each operation of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	interface Literals {

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esfsafetyrequirements.impl.SSafetyRequirement
		 * <em>SSafety Requirement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see org.polarsys.esf.esfsafetyrequirements.impl.SSafetyRequirement
		 * @see org.polarsys.esf.esfsafetyrequirements.impl.ESFSafetyRequirementsPackage#getSSafetyRequirement()
		 * @generated
		 */
		EClass SSAFETY_REQUIREMENT = eINSTANCE.getSSafetyRequirement();

		/**
		 * The meta object literal for the '<em><b>Criticality</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute SSAFETY_REQUIREMENT__CRITICALITY = eINSTANCE.getSSafetyRequirement_Criticality();

		/**
		 * The meta object literal for the '<em><b>SFailure Modes FMEA List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SSAFETY_REQUIREMENT__SFAILURE_MODES_FMEA_LIST =
			eINSTANCE.getSSafetyRequirement_SFailureModesFMEAList();

		/**
		 * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SSAFETY_REQUIREMENT__BASE_CLASS = eINSTANCE.getSSafetyRequirement_Base_Class();

	}

} // IESFSafetyRequirementsPackage
