/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *   
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 */
package org.polarsys.esf.esfsafetyrequirements.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.polarsys.esf.esfsafetyrequirements.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class ESFSafetyRequirementsFactory
	extends EFactoryImpl
	implements IESFSafetyRequirementsFactory {

	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static IESFSafetyRequirementsFactory init() {
		try {
			IESFSafetyRequirementsFactory theESFSafetyRequirementsFactory =
				(IESFSafetyRequirementsFactory) EPackage.Registry.INSTANCE
					.getEFactory(IESFSafetyRequirementsPackage.eNS_URI);
			if (theESFSafetyRequirementsFactory != null) {
				return theESFSafetyRequirementsFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ESFSafetyRequirementsFactory();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ESFSafetyRequirementsFactory() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case IESFSafetyRequirementsPackage.SSAFETY_REQUIREMENT:
				return createSSafetyRequirement();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier"); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ISSafetyRequirement createSSafetyRequirement() {
		SSafetyRequirement sSafetyRequirement = new SSafetyRequirement();
		return sSafetyRequirement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public IESFSafetyRequirementsPackage getESFSafetyRequirementsPackage() {
		return (IESFSafetyRequirementsPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static IESFSafetyRequirementsPackage getPackage() {
		return IESFSafetyRequirementsPackage.eINSTANCE;
	}

} // ESFSafetyRequirementsFactory
