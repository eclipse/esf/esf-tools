/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.safetyreq.table.tester;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementMatcher;
import org.eclipse.gmf.runtime.emf.type.core.ISpecializationType;
import org.eclipse.papyrus.infra.nattable.tester.ITableTester;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.esfsafetyrequirements.IESFSafetyRequirementsPackage;
import org.polarsys.esf.safetyreq.profile.service.types.set.ESFSafetyRequirementsTypesSet;
import org.polarsys.esf.safetyreq.table.ESFSafetyReqTableActivator;

/**
 *
 * Class responsible to test if the ESFSafetyRequirements Table can be created.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public final class ESFSafetyRequirementsTableTester
	implements ITableTester {

	/**
	 *
	 * Default constructor.
	 */
	public ESFSafetyRequirementsTableTester() {
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	public IStatus isAllowed(final Object pContext) {
		IStatus vStatus = null;
		if (pContext instanceof Element) {
			Element vEl = (Element) pContext;
			ISpecializationType vType = (ISpecializationType) ElementTypeRegistry.getInstance()
				.getType(ESFSafetyRequirementsTypesSet.SSAFETYREQUIREMENT_TYPE_ID);
			if (vType != null) {
				IElementMatcher vMatcher = vType.getMatcher();
				Profile vProfile = null;
				if (pContext instanceof Package || vMatcher.matches(vEl)) {
					vProfile = UMLUtil.getProfile(IESFSafetyRequirementsPackage.eINSTANCE, vEl);
				}

				if (vProfile != null) {
					final String vPackageQN = vProfile.getQualifiedName();
					if (vEl.getNearestPackage().getAppliedProfile(vPackageQN, true) != null) {
						vStatus = new Status(
							IStatus.OK,
							ESFSafetyReqTableActivator.PLUGIN_ID,
							"The context allowed to create a ESFSafetyRequirements Table"); //$NON-NLS-1$
					} else {
						vStatus = new Status(
							IStatus.ERROR,
							ESFSafetyReqTableActivator.PLUGIN_ID,
							"The profile " + vPackageQN //$NON-NLS-1$
								+ " is not applied on the model"); //$NON-NLS-1$
					}
				}
			}
		}

		if (vStatus == null) {
			vStatus = new Status(
				IStatus.ERROR,
				ESFSafetyReqTableActivator.PLUGIN_ID,
				"The context is not an UML Element"); //$NON-NLS-1$
		}
		return vStatus;
	}

}
