/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.safetyreq.execution.ui.tester;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.papyrus.emf.facet.custom.metamodel.v0_2_0.internal.treeproxy.EObjectTreeElement;
import org.eclipse.papyrus.sysml14.requirements.Requirement;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Class dedicated to verify if the selected element is a SysML Requirement.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class SysMLRequirementTester
	extends PropertyTester {

	/** Property "isSysMLRequirement". */
	private static final String IS_SYSML_REQUIREMENT = "isSysMLRequirement"; //$NON-NLS-1$

	/**
	 * Default constructor.
	 */
	public SysMLRequirementTester() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean test(
		final Object pReceiver,
		final String pProperty,
		final Object[] pArgs,
		final Object pExpectedValue) {
		boolean vAnswer = false;

		if (IS_SYSML_REQUIREMENT.equals(pProperty) && pReceiver instanceof IStructuredSelection) {
			IStructuredSelection vSelection = (IStructuredSelection) pReceiver;
			final Object vFirstElement = vSelection.getFirstElement();

			// If selected element in a EditPart
			if (vFirstElement instanceof EditPart) {
				EditPart vEditPart = (EditPart) vFirstElement;
				if (vEditPart.getModel() instanceof View) {
					View vView = (View) vEditPart.getModel();
					Class vClass = null;
					if (vView.getElement() instanceof Class) {
						vClass = (Class) vView.getElement();
					}
					if ((vClass != null) && (UMLUtil.getStereotypeApplication(vClass, Requirement.class) != null)) {
						vAnswer = true;
					}
				}
			} else if (vFirstElement instanceof EObjectTreeElement) {
				// If selected element in the Model Explorer
				EObjectTreeElement vTreeEl = (EObjectTreeElement) vFirstElement;
				if (vTreeEl.getEObject() instanceof Class) {
					Class vClass = (Class) vTreeEl.getEObject();
					if (UMLUtil.getStereotypeApplication(vClass, Requirement.class) != null) {
						vAnswer = true;
					}
				}
			}
		}
		return new Boolean(vAnswer).equals(pExpectedValue);
	}

}
