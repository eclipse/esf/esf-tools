/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.safetyreq.execution.ui.handler;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.papyrus.sysml14.requirements.Requirement;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.core.utils.ModelUtil;
import org.polarsys.esf.esfsafetyrequirements.ISSafetyRequirement;
import org.polarsys.esf.esfsafetyrequirements.impl.ESFSafetyRequirementsPackage;
import org.polarsys.esf.safetyreq.profile.tools.util.ESFSafetyRequirementsUtil;

/**
 * Handler class for setting an element as a SSafetyRequirement.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class SetSSafetyRequirementHandler
	extends AbstractHandler {

	/**
	 * Default constructor.
	 */
	public SetSSafetyRequirementHandler() {
	}

	/**
	 * Get the selected element (SysML Requirement) and set as a SSafetyRequirement.
	 *
	 * {@inheritDoc}
	 */
	@Override
	public Object execute(final ExecutionEvent pEvent) throws ExecutionException {
		ISelection vSelection = HandlerUtil.getCurrentSelection(pEvent);

		final Class vSelectedClass =
			(Class) ModelUtil.getSelectedEObjectOfType(vSelection, UMLPackage.eINSTANCE.getClass_());

		if ((vSelectedClass != null) && (UMLUtil.getStereotypeApplication(vSelectedClass, Requirement.class) != null)) {
			Model vESFModel = ModelUtil.getWorkingModel();
			if (vESFModel != null) {

				// Verify and apply (if necessary) ESFSafetyRequirements profile
				if (vESFModel.getAppliedProfile(ESFSafetyRequirementsPackage.eNS_PREFIX) == null) {
					ESFSafetyRequirementsUtil.applyESFSafetyRequirementsProfile(vESFModel);
				}

				if (UMLUtil.getStereotypeApplication(vSelectedClass, ISSafetyRequirement.class) == null) {
					// Apply 'SSafetyRequirement' Stereotype
					ESFSafetyRequirementsUtil.applySSafetyRequirementStereotype(vSelectedClass);
				}
			}
		}

		return null;
	}
}
