/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.safetyreq.execution.ui.handler;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.papyrus.sysml14.sysmlPackage;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.UMLPackage;
import org.polarsys.esf.core.utils.ESFTablesUtil;
import org.polarsys.esf.core.utils.ModelUtil;
import org.polarsys.esf.esfsafetyrequirements.impl.ESFSafetyRequirementsPackage;
import org.polarsys.esf.safetyreq.profile.tools.util.ESFSafetyRequirementsUtil;
import org.polarsys.esf.safetyreq.table.set.ESFSafetyRequirementsTablesSet;

/**
 * Handler class for creating an ESFSafetyRequirements Table.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class CreateESFSafetyRequirementsTableHandler
	extends AbstractHandler {

	/**
	 * Default constructor.
	 */
	public CreateESFSafetyRequirementsTableHandler() {
	}

	/**
	 * Get the selected element (Package/Model) and call the action for creating an ESFSafetyRequirements Table.
	 *
	 * Table.
	 *
	 * {@inheritDoc}
	 */
	@Override
	public Object execute(final ExecutionEvent pEvent) throws ExecutionException {

		ISelection vSelection = HandlerUtil.getCurrentSelection(pEvent);

		final Package vSelectedPackage =
			(Package) ModelUtil.getSelectedEObjectOfType(vSelection, UMLPackage.eINSTANCE.getPackage());

		if (vSelectedPackage != null) {
			Model vESFModel = ModelUtil.getWorkingModel();
			if (vESFModel != null) {

				// Verify and apply (if necessary) ESFSafetyRequirements profile
				if (vESFModel.getAppliedProfile(ESFSafetyRequirementsPackage.eNS_PREFIX) == null) {
					ESFSafetyRequirementsUtil.applyESFSafetyRequirementsProfile(vESFModel);
				}

				// Verify and apply (if necessary) SysML profile
				if (vESFModel.getAppliedProfile(sysmlPackage.eNS_PREFIX) == null) {
					ESFSafetyRequirementsUtil.applySysMLProfile(vESFModel);
				}

				if ((vESFModel.getAppliedProfile(ESFSafetyRequirementsPackage.eNS_PREFIX) != null)
					&& (vESFModel.getAppliedProfile(sysmlPackage.eNS_PREFIX) != null)) {
					// Create an ESFSafetyRequirements Table
					ESFTablesUtil.createTable(
						ESFSafetyRequirementsTablesSet.SAFETYREQ_TABLE_ID,
						ESFSafetyRequirementsTablesSet.SAFETYREQ_TABLE_NAME,
						vSelectedPackage);
				}
			}
		}

		return null;
	}

}
